// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Bristol_428.cs
//
// Author: icy.liu 2011
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_Bristol_428 : InstType_Wavemeter
    {
        
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Bristol_428(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            //InstrumentDataRecord Bristol_228data = new InstrumentDataRecord(
            //    "Bristol Wavemeter 228A",				// hardware name 
            //    "0",  			// minimum valid firmware version 
            //    "xxxxxxxxxx");			// maximum valid firmware version 
            //Bristol_228data.Add("MinWavelength_nm", "1250");// minimum wavelength
            //Bristol_228data.Add("MaxWavelength_nm", "1650");
            //ValidHardwareData.Add("InstrVariant1", Bristol_228data);

            InstrumentDataRecord Bristol_428data = new InstrumentDataRecord(
                 "Bristol Wavemeter 428A",				// hardware name 
                  "0",  			// minimum valid firmware version 
                 "xxxxxxxxxx");			// maximum valid firmware version 
            Bristol_428data.Add("MinWavelength_nm", "1250");// minimum wavelength
            Bristol_428data.Add("MaxWavelength_nm", "1650");
            ValidHardwareData.Add("InstrVariant2", Bristol_428data);
            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord Bristol_428ChassisData = new InstrumentDataRecord(
                "Chassis_Bristol_428",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "xxxxxxxxxxxxxx");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Bristol_428", Bristol_428ChassisData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            //instrumentChassis = (ChassisType_Visa)base.InstrumentChassis;
            this.instrumentChassis = (Chassis_Bristol_428)base.InstrumentChassis;
        }
        #endregion

        #region Public Instrument Methods and Properties
        /// <summary>
        /// Unique hardware identification string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the full IDN? string which has 3 comma seperated substrings
                //string[] idn = instrumentChassis.Query_Unchecked("*IDN?\r\n", this).Split(',');

                //// Build the string from 2 substrings, the manufacturer name & the model number
                //string hid = idn[0] + idn[1];

                //// Log event 
                //LogEvent("'HardwareIdentity' returned : " + hid);

                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Hardware firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the full IDN? string which has 3 comma seperated substrings
                //string[] idn = instrumentChassis.Query_Unchecked("*IDN?\r\n", this).Split(',');

                //// The 3rd substring containins the firmware version.
                //string fv = idn[3].Trim();

                //// Log event 
                //LogEvent("'FirmwareVersion' returned : " + fv);

                // Return 
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return true;
                //return base.IsOnline;   //raul changed
            }
            set
            {
                instrumentChassis.IsOnline = value;
            }
        }


        public bool ContinueMeasMode
        {
            get
            {
                string cmd = ":init:cont?";
                //string rtn = this.Query(cmd);
                //int status = int.Parse(rtn);
                return true;
            }
            set
            {
                //string cmd = ":init:cont " + (value ? "on" : "off");
                //this.Write(cmd);
            }
        }
        /// <summary>
        /// Configures the instrument into a default state.
        /// </summary>
        public override void SetDefaultState()
        {
            // Send a reset, *RST
            //instrumentChassis.Query_Unchecked("*RST\r\n", this);
          instrumentChassis.Write("*RST\r\n", this);
          instrumentChassis.Write("*CLS\r\n", this);
        }

        #endregion

        #region Public Wavemeter InstrumentType Methods and Properties

        /// <summary>
        /// Return frequency in GHz
        /// </summary>
        public override double Frequency_GHz
        {
            get
            {
                
                // Set the display to frequency
                // System.Threading.Thread.Sleep(20);
                //instrumentChassis.Write_Unchecked(":DISP:UNIT:WAV THZ\r\n", this);
                //instrumentChassis.Read_Unchecked(this);
                // Read frequency in GHz
                //System.Threading.Thread.Sleep(300);

                //instrumentChassis.Write(":MEAS:SCAL:FREQ?\r\n", this);
                
                //string rtn = instrumentChassis.Read_Unchecked(this);
                //instrumentChassis.Write_Unchecked(":MEAS:SCAL:FREQ?\r\n", this);
                //System.Threading.Thread.Sleep(700);
                string rtn1 = instrumentChassis.Query(":MEAS:SCAL:FREQ?\r\n", this);
                string rtn = instrumentChassis.Query(":MEAS:SCAL:FREQ?\r\n", this);
                
                // Convert and return result
                const Double HzToGigaHzScaleFactor = 1e3;
             
                return Convert.ToDouble(rtn)*HzToGigaHzScaleFactor;
            }
        }


        public  string  Average( )
        {

            // Query medium
          string   rtn = instrumentChassis.Query(":SENS:AVER?\r\n", this);

            // Convert to enum 
            string times;
            switch (rtn)
            {
                case "OFF\r\n": times = "OFF"; break;
                case "2\r\n": times = "2"; break;
                case "4\r\n": times = "4"; break;
                case "8\r\n": times = "8"; break;
                case "16\r\n": times = "16"; break;
                case "32\r\n": times = "32"; break;
                case "64\r\n": times = "64"; break;
                case "128\r\n": times = "128"; break;
                default: throw new InstrumentException("Unrecognised measurement medium '" + rtn + "'");
            }

            // Return result
            return times;

        }


        public void Set_Average(string Average_time)
        {
            // Build string
            string temp;
            //Average_time += "\r\n";
            if (Average_time.ToUpper() == "OFF")
            {
                temp ="OFF\r\n";
            }
            else
            { 
            
            switch (Average_time)
            {

                case "0": temp = "0\r\n"; break;
                case "2": temp = "2\r\n"; break;
                case "4": temp = "4\r\n"; break;
                case "8": temp = "8\r\n"; break;
                case "16": temp = "16\r\n"; break;
                case "32": temp = "32\r\n"; break;
                case "64": temp = "64\r\n"; break;
                case "128": temp = "128\r\n"; break;
                default: throw new InstrumentException("Unrecognised measurement medium '" + Average_time + "'");
            }

        }
            // Write to instrument
            instrumentChassis.Write(":SENS:AVER " + temp, this);

        }
   

        /// <summary>
        /// Return wavelength in nm
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                //System.Threading.Thread.Sleep(20);
                // Set the display to wavelength
                //instrumentChassis.Write_Unchecked(":DISP:UNIT:WAV NM\r\n", this);
                // instrumentChassis.Write_Unchecked(":FETC:SCAL:WAV?\r\n", this);
                //System.Threading.Thread.Sleep(20);
                // string rtn = instrumentChassis.Read_Unchecked(this);
                // Read wavelength in nm
                //System.Threading.Thread.Sleep(300);
                string rtn1 = instrumentChassis.Query(":MEAS:SCAL:WAV?\r\n", this);
                string rtn = instrumentChassis.Query(":MEAS:SCAL:WAV?\r\n", this);
                //instrumentChassis.Write_Unchecked(":MEAS:SCAL:WAV?\r\n", this);
                //System.Threading.Thread.Sleep(700);
                //string rtn = instrumentChassis.Read(this);
                // Cast and return result
                return Convert.ToDouble(rtn);
            }
        }
        public bool SafeModeOperation
        {
            get
            {
                return (safeModeOperation);
            }
            set
            {
                safeModeOperation = value;
            }
        }
        
        /// <summary>
        /// Returns the signal input power level in dBm
        /// </summary>
        public override double Power_dBm
        {
            get
            {
                // Set power to dbm
                instrumentChassis.Write(":UNIT:POW DBM\r\n", this);
                //instrumentChassis.Read_Unchecked(this);
                // Read power in dBm
                instrumentChassis.Write(":READ:SCAL:POW?\r\n", this);
                System.Threading.Thread.Sleep(100);
                string rtn = instrumentChassis.Read(this);

                // Cast and return result
                return Convert.ToDouble(rtn.Replace("\r","").Replace("\n",""));
            }
        }

        /// <summary>
        /// Returns the signal input power level in mW
        /// </summary>
        public override double Power_mW
        {
            get
            {
                // Set power to mW
                instrumentChassis.Write(":UNIT:POW MW\r\n", this);
                //instrumentChassis.Read_Unchecked(this);

                // Read power in mW
                instrumentChassis.Write(":READ:SCAL:POW?\r\n", this);
                string rtn = instrumentChassis.Read(this); ;

                // Cast and return result
                return Convert.ToDouble(rtn);
            }
        }

        /// <summary>
        /// Reads/sets the measurement medium
        /// </summary>
        public override Medium MeasurementMedium
        {
            get
            {
                // Query medium
                string rtn = instrumentChassis.Query(":SENS:MED?\r\n", this);

                // Convert to enum 
                Medium medm;
                switch (rtn)
                {
                    case "VACUUM\r\n": medm = Medium.Vacuum; break;
                    case "AIR\r\n": medm = Medium.Air; break;
                    default: throw new InstrumentException("Unrecognised measurement medium '" + rtn + "'");
                }

                // Return result
                return medm;
            }

            set
            {
                // Build string
                string medm;
                switch (value)
                {
                    case Medium.Vacuum: medm = "VACUUM\r\n"; break;
                    case Medium.Air: medm = "AIR\r\n"; break;
                    default: throw new InstrumentException("Unrecognised measurement medium '" + value.ToString() + "'");
                }

                // Write to instrument
                instrumentChassis.Write(":SENS:MED " + medm, this);
            }
        }

        #endregion


        #region Private Data

        // Chassis reference
        private Chassis_Bristol_428 instrumentChassis;
        private bool safeModeOperation;





        //public enum Average_Times
        //{

        //    time0 = 0,
        //    time1 = 2,
        //    time2 = 4,
        //    time3 = 8,
        //    time4 = 16,
        //    time5 = 32,
        //    time6 = 64,
        //    time7 = 128
        //}
        #endregion
    }
}

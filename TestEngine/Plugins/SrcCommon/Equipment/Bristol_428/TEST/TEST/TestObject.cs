using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;

namespace TEST
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        #region References to the objects to test - Chassis and Instruments
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_Bristol_428 testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_Bristol_428 testInst;            
        #endregion
        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "TCPIP::192.168.1.9::23::SOCKET";
        //const string testChassis = "Chas_CVF_200L";
        //const string instr1Name = "Inst_CVF_200";
          #endregion
        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_Bristol_428("testChassis", "Chassis_Bristol_428", visaResource);
            TestOutput("testChassis", "Created OK");
           
            //testChassis = new Chassis_Ag34970A("MyChassis", "Chassis_Ag34970A", "GPIB0::7::INSTR");
            //TestOutput(testChassis, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            testInst = new Inst_Bristol_428("testInst", "Inst_Bristol_428", "", "", testChassis);
      
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            TestOutput("testChassis", "IsOnline set true OK");
            testInst.IsOnline = true;
       TestOutput("testInst", "IsOnline set true OK");
                   
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            string hid=testInst.HardwareIdentity;
            string fvr = testInst.FirmwareVersion;
            testInst.SetDefaultState();
           double a= testInst.Wavelength_nm ;
           double f = testInst.Frequency_GHz;
           double g = testInst.Power_dBm;
           double e = testInst.Power_mW;
         
            
            //testInst.SetDefaultState(
            // You can use Assert to check return values from tests
            Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            Assert.IsNaN(Double.NaN);
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }
        #endregion


}

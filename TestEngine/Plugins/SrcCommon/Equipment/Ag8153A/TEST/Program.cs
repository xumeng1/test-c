using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Equip_TestAg8153
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create the test object
            Test_Object test = new Test_Object();
            // Call the setup function (includes putting equipment online)
            test.Setup();
            // Remember the time
            DateTime start = DateTime.Now;
            try
            {
                int counter = 0;
                while (true)
                {
                    counter++;
                    Console.WriteLine("******************************"); 
                    Console.WriteLine("!!!*** ROUND " + counter + " ***!!!");
                    Console.WriteLine("******************************");
                    test.T07_DefaultState();
                    test.T01_Versions();
                    test.T02_MeterMode();
                    test.T03_MeterSetup();
                    test.T04_MeterRange();
                    test.T05_TakeReadings();
                    //test.T06_Zeroing();
                    test.T07_DefaultState();
                    test.T08_TimingTest();

                    test.T09_LotsOfReads();
                }
            }
            finally
            {
                // Shutdown, whether this is due to an exception (Assert failure, or otherwise).
                test.ShutDown();
            }
            TimeSpan t = DateTime.Now - start;
            Console.WriteLine("Took {0}", t);
            Console.WriteLine("Press RETURN to exit");
            Console.Read();
            // Need this line due to the Logging Thread hanging around
            Environment.Exit(0);
        }
    }
}

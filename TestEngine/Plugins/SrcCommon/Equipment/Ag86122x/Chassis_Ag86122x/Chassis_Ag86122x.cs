// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.ChassisNS
//
// Chassis_Ag86122x.cs
//
// Author: K Pillar
// Design: As specified in Ag86122x DD 

using System;
using System.Windows.Forms;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
	
	
	/// <summary>
	/// Chassis driver for Ag86122x wavemeter
	/// </summary>
    public class Chassis_Ag86122x : ChassisType_TryComm
    {

        #region Public Functions
        
        
        /// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
        public Chassis_Ag86122x(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information

            // Add details of Ag86122A chassis
			ChassisDataRecord Ag86122AData = new ChassisDataRecord(
				"Agilent Technologies 86122A",		                    // hardware name 
				"0",								// minimum valid firmware version 
				"3.0");						// maximum valid firmware version 
            ValidHardwareData.Add("Ag86122A", Ag86122AData);

            // Add details of Ag86122B chassis
            ChassisDataRecord Ag86122BData = new ChassisDataRecord(
                "Agilent Technologies 86122B",		                    // hardware name 
                "0",								// minimum valid firmware version 
                "3.0");
            ValidHardwareData.Add("Ag86122B", Ag86122BData);

            // Add details of Ag86122C chassis
            ChassisDataRecord Ag86122CData = new ChassisDataRecord(
                "Agilent Technologies 86122C",		                    // hardware name 
                "0",								// minimum valid firmware version 
                "3.0");						// maximum valid firmware version 
            ValidHardwareData.Add("Ag86122C", Ag86122CData);

            // Add details of Ks86122C chassis
            ChassisDataRecord Ks86122CData = new ChassisDataRecord(
                "Keysight Technologies 86122C",		                    // hardware name 
                "0",								// minimum valid firmware version 
                "3.0");						// maximum valid firmware version 
            ValidHardwareData.Add("Ks86122C", Ks86122CData);

            ChassisDataRecord Ag86120BData = new ChassisDataRecord(
                "Agilent Technologies 86120B",		                    // hardware name 
                "0",								// minimum valid firmware version 
                "3.0");						// maximum valid firmware version 
            ValidHardwareData.Add("Ag86120B", Ag86120BData);

            ChassisDataRecord Ag86120CData = new ChassisDataRecord(
                "Agilent Technologies 86120C",		                    // hardware name 
                "0",								// minimum valid firmware version 
                 "3.0");						// maximum valid firmware version 
            ValidHardwareData.Add("Ag86120C", Ag86120CData);

            ChassisDataRecord hp86120CData = new ChassisDataRecord(
                "HEWLETT-PACKARD  86120C",		                    // hardware name 
                "0",								// minimum valid firmware version 
                 "3.0");						// maximum valid firmware version 
            ValidHardwareData.Add("HP86120C", hp86120CData);

            ChassisDataRecord hp86120BData = new ChassisDataRecord(
                "HEWLETT-PACKARD  86120B",		                    // hardware name 
                "0",								// minimum valid firmware version 
                 "3.0");						// maximum valid firmware version 
            ValidHardwareData.Add("HP86120B", hp86120BData);
            
		}

        /// <summary>
        /// Override of basic string to return the status of the questionable Data register
        /// </summary>
        /// <returns>Error string</returns>
        public override string GetErrorString()
        {
            string resp = Query_Unchecked("STAT:QUES:EVEN?", null);
            string baseErrStr = base.GetErrorString();

            return String.Format("Questionable Data Register? {0}\n{1}", resp, baseErrStr);
        }


		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query("*IDN?", null).Split(',');		
				
				// Return the firmware version in the 4th comma seperated field
				return idn[3].Trim();
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query("*IDN?", null).Split(',');		
		
				// Return field 1, the manufacturer name and field 2, the model number
				return idn[0] +" " + idn[1];
			}
		}

        /// <summary>
        /// Setup the chassis as soon as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                // if setting online
                if (value)
                {
                    //List of status bits
                    const byte commandError = 32;
                    const byte executionError = 16;
                    const byte deviceDependentError = 8;
                    const byte queryError = 4;

                    byte seFlags;
                    seFlags = (commandError | executionError | deviceDependentError | queryError);

                    // clear the status registers
                    this.Write("*CLS", null);

                    this.StandardEventRegisterMask = seFlags;
                }
            }
        }

        #endregion
    }
}

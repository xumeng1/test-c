// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag86122x.cs
//
// Author: K Pillar
// Design: As specified in Ag86122x DD 

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Agilent 86122x wavemeter driver
    /// </summary>
    public class Inst_Ag86122x : InstType_SharedWaveMeter
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Slot ID for the instrument</param>
        /// <param name="subSlotId">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ag86122x(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            // Configure valid hardware information

            // Add all Ag86122x details
            InstrumentDataRecord Ag86122AData = new InstrumentDataRecord(
                "Agilent Technologies 86122A",    				// hardware name 
                "0",											// minimum valid firmware version 
                "3.0");									// maximum valid firmware version 
            Ag86122AData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86122AData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag86122A", Ag86122AData);

            InstrumentDataRecord Ag86122BData = new InstrumentDataRecord(
                "Agilent Technologies 86122B",    				// hardware name 
                "0",											// minimum valid firmware version 
                "3.0");									// maximum valid firmware version 
            Ag86122BData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86122BData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag86122B", Ag86122BData);

            InstrumentDataRecord Ag86122CData = new InstrumentDataRecord(
                "Agilent Technologies 86122C",    				// hardware name 
                "0",											// minimum valid firmware version 
                "3.0");									// maximum valid firmware version 
            Ag86122CData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86122CData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag86122C", Ag86122CData);

            InstrumentDataRecord Ks86122CData = new InstrumentDataRecord(
                "Keysight Technologies 86122C",    				// hardware name 
                "0",											// minimum valid firmware version 
                "3.0");									// maximum valid firmware version 
            Ks86122CData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ks86122CData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ks86122C", Ks86122CData);
           
            InstrumentDataRecord Ag86120BData = new InstrumentDataRecord(
                "Agilent Technologies 86120B",    				// hardware name 
                "0",											// minimum valid firmware version 
                "3.0");									// maximum valid firmware version 
            Ag86120BData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86120BData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag861220B", Ag86120BData);


            InstrumentDataRecord Ag86120CData = new InstrumentDataRecord(
                "Agilent Technologies 86120C",    				// hardware name 
                "0",											// minimum valid firmware version 
                 "3.0");									  // maximum valid firmware version 
            Ag86120CData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86120CData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag861220C", Ag86120CData);

            InstrumentDataRecord hp86120CData = new InstrumentDataRecord(
                "HEWLETT-PACKARD  86120C",    				// hardware name 
                "0",											// minimum valid firmware version 
                 "3.0");									  // maximum valid firmware version 
            hp86120CData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            hp86120CData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("HP861220C", hp86120CData);

            InstrumentDataRecord hp86120BData = new InstrumentDataRecord(
                "HEWLETT-PACKARD  86120B",    				// hardware name 
                "0",											// minimum valid firmware version 
                 "3.0");									  // maximum valid firmware version 
            hp86120BData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            hp86120BData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("HP861220B", hp86120BData);

			// Configure valid chassis information
			// Add Chassis_Ag86122x chassis details
            InstrumentDataRecord Ag86122xChassisData = new InstrumentDataRecord(	
				"Chassis_Ag86122x",								// chassis driver name  
				"0",											// minimum valid chassis driver version  
				"01.00.00");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag86122x", Ag86122xChassisData);

         


			// Initialise the local chassis reference cast to the actual chassis type
			instrumentChassis = (Chassis_Ag86122x) base.InstrumentChassis;

            safeModeOperation =true;
		}


        #region Public Instrument Methods and Properties

        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the Chassis 
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware, the plugin hardware and the channel number
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }


        /// <summary>
        /// Configures the instrument into a default state.
        /// </summary>
        public override void SetDefaultState()
        {
            this.SwitchOpticalLineOn();

            // Send a reset, *RST, and a cls
            this.Write("*RST");
            this.Write("*CLS");

            this.ContinueMeasMode = false;

            this.SwitchOpticalLineOff();
        }

        /// <summary>
        /// Set/Get WaveMeter on/off line - chongjian.liang 2016.04.28
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                this.SwitchOpticalLineOn();

                this.instrumentChassis.IsOnline = value;

                base.IsOnline = value;

                this.SwitchOpticalLineOff();
            }
        }

        #endregion

        #region Public Wavemeter InstrumentType Methods and Properties

        /// <summary>
        /// Return frequency in GHz
        /// </summary>
        public override double Frequency_GHz
        {
            get
            {
                this.SwitchOpticalLineOn();

                //Putting the marker onto the max peak wavelength
                instrumentChassis.Write("DISP:MARK:MAX", this);

                // Read frequency in GHz
                //string cmd = ":MEAS:SCAL:POW:FREQ? DEF";
                string cmd = ":MEAS:SCAL:POW:FREQ? MAX";//Read max freq directly. Modified by Andy Li.
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                //Hp returns in Hz
                const Double HzToGigaHzScaleFactor = 1e-9;

                double result = getDblFromString(cmd, rtn);

                // convert to GHz
                return (result * HzToGigaHzScaleFactor);
			}
		}

        public override bool ContinueMeasMode
        {
            get
            {
                this.SwitchOpticalLineOn();
                
                string cmd = ":init:cont?";
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                int status = int.Parse(rtn);
                return status == 1;
            }
            set
            {
                this.SwitchOpticalLineOn();
                
                string cmd = ":init:cont " + (value ? "on" : "off");
                this.Write(cmd);

                this.SwitchOpticalLineOff();
            }
        }

		/// <summary>
		/// Return wavelength in nm
		/// </summary>
		public override double Wavelength_nm 
		{
			get 
			{
                this.SwitchOpticalLineOn();
                
                ////Putting the marker onto the max peak wavelength
                this.Write("DISP:MARK:MAX");
                
                //// Read wavelength in nm
                //string cmd = ":MEAS:SCAL:POW:WAV? DEF";
                string cmd = ":MEAS:SCAL:POW:WAV? MAX";//Read max freq directly. Modified by Andy Li.
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                double result = getDblFromString(cmd, rtn);

                //Hp returns in Meters
                const Double MetersToNanometers = 1e9;

                return (result * MetersToNanometers);
            }
        }

        /// <summary>
        /// Returns the signal input power level in dBm
        /// </summary>
        public override double Power_dBm
        {
            get
            {
                this.SwitchOpticalLineOn();

                // Set power to dbm
                this.Write(":UNIT:POW DBM");

                string cmd = ":MEAS:POW?";
                // Read power in dBm
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                double result = getDblFromString(cmd, rtn);

                return result;
            }
        }

        /// <summary>
        /// Returns the signal input power level in mW
        /// </summary>
        public override double Power_mW
        {
            get
            {
                this.SwitchOpticalLineOn();

                // Set power to W
                this.Write(":UNIT:POW W");

                string cmd = ":MEAS:POW?";

                // Read power in W
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                double result = getDblFromString(cmd, rtn);

                const double WattsToMilliWattsScaleFactor = 1000;

                // return result converted to mW
                return (result * WattsToMilliWattsScaleFactor);
            }
        }

        /// <summary>
        /// Reads/sets the measurement medium
        /// </summary>
        public override Medium MeasurementMedium
        {
            get
            {
                this.SwitchOpticalLineOn();

                // Query medium
                string rtn = this.Query(":SENS:CORR:MED?");

                this.SwitchOpticalLineOff();
                
				// Convert to enum 
				Medium medm;
				switch (rtn)
				{
					case "VAC": medm = Medium.Vacuum; break;
					case "AIR": medm = Medium.Air; break;
					default: throw new InstrumentException("Unrecognised measurement medium '" + rtn + "'");
				}

				// Return result
				return medm;
			}

			set 
			{
				// Build string
				string medm;
				switch (value)
				{
					case Medium.Vacuum: medm = "VAC"; break;
					case Medium.Air: medm = "AIR"; break;
					default: throw new InstrumentException("Unrecognised measurement medium '" + value.ToString() + "'");
				}

                this.SwitchOpticalLineOn();

				// Write to instrument
				this.Write(":SENS:CORR:MED " + medm);

                this.SwitchOpticalLineOff();
			}
        }

        /// <summary>
        /// Gets / sets whether we are in safe mode of operation, 
        /// we are in this mode by default
        /// </summary>
        public bool SafeModeOperation
        {
            get
            {
                return safeModeOperation;
            }
            set
            {
                safeModeOperation = value;
            }
        }

        public void EnableDisplay(bool enable)
        {
            this.SwitchOpticalLineOn();

            if (enable)
            {
                this.Write(":DISP:WIND:GRAP:STAT ON");
                this.Write(":DISP:WIND:STAT ON");
                this.Write("DISP:WIND2:STAT ON");
            }
            else
            {
                this.Write(":DISP:WIND:GRAP:STAT OFF");
                this.Write(":DISP:WIND:STAT OFF");
                this.Write("DISP:WIND2:STAT OFF");
            }

            this.SwitchOpticalLineOff();
        }
        #endregion

        #region private functions

        /// <summary>
        /// Write, depending on safemodeoperation will call correct thing in chassis
        /// </summary>
        /// <param name="cmd">the command we wish to write to chassis</param>
        private void Write(string cmd)
        {
            if (this.safeModeOperation)
            {
                instrumentChassis.Write(cmd, this);
            }
            else
            {
                //async = true, errorcheck = false
                instrumentChassis.Write(cmd, this, true, false);
            }
        }

        /// <summary>
        /// Query, depending on safemodeoperation will call correct thing in chassis
        /// </summary>
        /// <param name="cmd">the query string</param>
        /// <returns></returns>
        private string Query(string cmd)
        {
            if (this.safeModeOperation)
            {
                return (instrumentChassis.Query(cmd, this));
            }
            else
            {
                // errorcheck = false
                return (instrumentChassis.Query(cmd, this, false));
            }
        }

        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }


        #endregion

        #region Private Data

        private Chassis_Ag86122x instrumentChassis;
        private bool safeModeOperation;
        #endregion
    }
}

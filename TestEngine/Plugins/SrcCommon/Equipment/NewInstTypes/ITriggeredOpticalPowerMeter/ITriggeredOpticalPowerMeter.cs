using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.InstrTypes
{
    /// <summary>
    /// Interface definition for the functions provided by a triggered optical power meter
    /// </summary>
    public interface IInstType_TriggeredOpticalPowerMeter : IInstType_OpticalPowerMeter
    {
        /// <summary>
        /// Configures the instrument to wait for a trigger before measuring.
        /// </summary>
        /// <param name="enabled">TRUE to wait for a trigger before measuring, FALSE for continuous mode</param>
        void EnableInputTrigger(bool enabled);

        /// <summary>
        /// Configures the instrument to send a trigger pulse on completion of measurement.
        /// </summary>
        /// <param name="enabled">Whether to send an output trigger pulse.</param>
        void EnableOutputTrigger(bool enabled);

        /// <summary>
        /// Starts logging.
        /// </summary>
        void StartDataLogging();

        /// <summary>
        /// Exit logging mode.
        /// </summary>
        void StopDataLogging();

        /// <summary>
        /// Set up data logging
        /// </summary>
        /// <param name="numberOfPoints">Number of data points to collect</param>
        /// <param name="averagingTime_s">Measurement averaging time</param>
        void ConfigureDataLogging(int numberOfPoints, double averagingTime_s);

        /// <summary>
        /// Is data logging in progress ?
        /// </summary>
        /// <returns>TRUE if the process is still in progress.</returns>
        bool DataLoggingInProgress();

        /// <summary>
        /// Retrieve sweep data
        /// </summary>
        /// <returns>An array of power data.</returns>
        double[] GetSweepData();
      }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using System.Collections;

namespace Bookham.TestLibrary.InstrTypes
{
    /// <summary>
    /// Interface for a collection of Digital IO lines
    /// </summary>
    public interface IInstType_DigiIOCollection : IInstrument
    {
        /// <summary>
        /// Get the Digital IO line that is represented by a given Digital IO line number in this collection
        /// </summary>
        /// <param name="lineNumber">Digital IO line number</param>
        /// <returns>Digital IO line (as interface)</returns>
        IInstType_DigitalIO GetDigiIoLine(int lineNumber);

        /// <summary>
        /// Get the underlying collection to enable foreach over all the lines in the collection.
        /// No assumption made about whether this is a list or dictionary, just must be a generic
        /// collection of type IInstType_DigitalIO
        /// </summary>
        /// <returns>Collection as IEnumerable of IInstType_DigitalIO</returns>
        IEnumerable<IInstType_DigitalIO> DigiIoLines { get;}
    }

}

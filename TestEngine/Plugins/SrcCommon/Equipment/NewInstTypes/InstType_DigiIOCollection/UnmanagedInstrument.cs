using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestEngine.PluginInterfaces.Instrument
{    
    /// <summary>
    /// Unmanaged Instrument - i.e. not one that is a separate plugin component that the Test Engine
    /// core can independently instantiate. Use this base class for instruments that are generated 
    /// by containers to avoid having to override the Test Engine base functionality with ChassisDrivers, 
    /// Firmware Versions etc...
    /// </summary>
    public abstract class UnmanagedInstrument : Instrument
    {
        /// <summary>
        /// Constructor, taking a chassis
        /// </summary>
        /// <param name="chassis">Chassis for our unmanaged instrument</param>
        /// <param name="name">Instrument name</param>
        public UnmanagedInstrument(string name, Bookham.TestEngine.PluginInterfaces.Chassis.Chassis chassis)
            : base(name, "UnmanagedInstrument", "", "", chassis)
        {
        }

        /// <summary>
        /// Constructor - use singleton Virtual Chassis object
        /// </summary>
        /// <param name="name">Instrument name</param>
        public UnmanagedInstrument(string name)
            : base(name, "UnmanagedInstrument", "", "", VirtualChassis.Singleton)
        {
        }

        #region Private data
        private bool isOnline;        
        #endregion
        
        /// <summary>
        /// Don't do any chassis or hardware checking. Use a local cache for IsOnline
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return isOnline;
            }
            set
            {
                isOnline = value;
            }
        }        

        /// <summary>
        /// Firmware Version - just give back what my chassis says
        /// </summary>
        public override string FirmwareVersion
        {
            get { return this.InstrumentChassis.FirmwareVersion; }
        }

        /// <summary>
        /// Hardware Identity - just give back what my chassis says
        /// </summary>
        public override string HardwareIdentity
        {
            get { return this.InstrumentChassis.HardwareIdentity; }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestEngine.PluginInterfaces.Chassis
{
    /// <summary>
    /// Virtual Chassis
    /// </summary>
    public class VirtualChassis : Chassis
    {
        /// <summary>
        /// Only one chassis available
        /// </summary>
        public static VirtualChassis Singleton
        {
            get
            {
                return virtualChassisInstance;
            }
        }

        private static VirtualChassis virtualChassisInstance = new VirtualChassis();

        private VirtualChassis()
            : base("VirtualChassis", "VirtualChassis", "")
        {
        }

        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get { return "N/A"; }
        }

        /// <summary>
        /// Hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            get { return "N/A"; }
        }
    }
}

using Bookham.TestEngine.PluginInterfaces.Instrument;
using System;

namespace Bookham.TestLibrary.InstrTypes
{
    public abstract class InstType_TriggeredElectricalSource : InstType_ElectricalSource
    {
        public InstType_TriggeredElectricalSource(string instrumentName, string driverName, string slotId, string subSlotId, Bookham.TestEngine.PluginInterfaces.Chassis.Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
        }

        public abstract void InitSourceIMeasureV_MeasurementAccuracy(double voltageCompliance_V, double voltageRange_V, bool sourceMeasAutoDelay, double sourceMeasureDelay_s, int numberOfAverages, double integrationRate, bool AutoZeroState);
        public abstract void InitSourceIMeasureV_CurrentSweep(double Imin_A, double Imax_A, int numPts, int inputTriggerLine, int outputTriggerLine);
        public abstract void InitSourceIMeasureV_TriggeredFixedCurrent(double I, int numPts, int inputTriggerLine, int outputTriggerLine);
        public abstract void InitSourceIMeasureV_UntriggeredSpotMeas();

        public abstract void InitSourceVMeasureI_MeasurementAccuracy(double currentCompliance_A, double currentRange_A, bool sourceMeasAutoDelay, double sourceMeasureDelay_s, int numberOfAverages, double integrationRate, bool AutoZeroState);
        public abstract void InitSourceVMeasureI_VoltageSweep(double Vmin_V, double Vmax_V, int numPts, int inputTriggerLine, int outputTriggerLine);
        public abstract void InitSourceVMeasureI_TriggeredFixedVoltage(double V, int numPts, int inputTriggerLine, int outputTriggerLine);
        public abstract void InitSourceVMeasureI_UntriggeredSpotMeas();

        public abstract void ConfigureTriggering(int numPts, int inputTriggerLine, int outputTriggerLine, bool expectInputTrigger);
        public abstract void ConfigureTriggerlines(int inputTriggerLine, int outputTriggerLine);
        public abstract void DisableTriggering();

        public abstract void Trigger();
        public abstract bool WaitForSweepToFinish();
        public abstract void AbortSweep();

        // TODO - make this generic
        public abstract void GetSweepDataSet(out Double[] SourceData, out Double[] SenseData);
        public abstract Single[] ParseBinaryData(byte[] buffer);
        //public abstract double IEEE754_As_Double(char byte4, char byte3, char byte2, char byte1);

        //public abstract void SelectFrontTerminals();
        //public abstract void SelectRearTerminals();
        //public abstract void EnableInterlock();
        //public abstract void DisableInterlock();

        //public abstract bool InterlockIsEnabled();
        //public abstract void ClearErrorQueue();
        public abstract void CleanUpSweep();
        //public abstract void SetASCII();

        public abstract void ClearSweepTriggering();
        public abstract void ClearSweepTraceData();
        public abstract void StartSweep();


        //
        // Source voltage, measure current
        //
        public abstract void SourceFixedVoltage();
        public abstract void SourceSweptVoltage(double Vmin_V, double Vmax_V, int numPts);
        public abstract bool SenseCurrent(double IsenseCompliance, double IsenseRange);
        //public abstract void SenseCurrentRange(double IsenseRange);
        //public abstract void SetVoltage(double value_volts);
        //public abstract bool SetIsenseCompliance(double compliance_reqd);
        //public abstract double GetIsenseRange();
        //public abstract double GetIsenseCompliance();
        //public abstract double MeasureCurrent();
        //public abstract double SpotMeasureCurrent();
        //public abstract void SetCurrentReadingFormat();

        //
        // Source current, measure voltage
        //
        public abstract void SourceFixedCurrent();
        public abstract void SourceSweptCurrent(double Imin_A, double Imax_A, int numPts);
        public abstract void SenseVoltage(double VsenseComplicance, double VsenseRange);
        //public abstract void SenseVoltageRange(double VsenseRange);
        public abstract void SetCurrent(double value_amps);
        //public abstract void SetVsenseCompliance(double compliance_reqd);
        //public abstract double MeasureVoltage();
        //public abstract void SetIsourceRange(double Range_Reqd);

        //
        // Measurement accuracy
        //
        public abstract void SetMeasurementAccuracy(bool SourceMeasAutoDelay, double SourceMeasSpecificDelay_s, int numAverages, double integrationRate, bool AutoZeroState);
        public abstract void SetMeasurementAccuracy(bool SourceMeasAutoDelay, double SourceMeasSpecificDelay_s, int numAverages, double integrationRate);

        public abstract void SetSourceDelayMeasure(bool autoDelay, double specificDelay_s);
        //public abstract void SetAveraging(int counts);
        //public abstract void SetIntegrationRate(double rate);
        //public abstract void SetAutoZero(bool AutoZeroState);
        public abstract void AutoZeroRefresh();
        //public abstract void SetFourWireSense(bool State);

        //
        // Triggering
        //
        //public abstract void TriggerAmplitudeAmps(double Value);
        //public abstract void TriggerAmplitudeVolts(double Value);


        //
        // ON/OFF
        //
        //public abstract void outputON();
        //public abstract void outputOFF();
        
        //
        // Digital I/O
        //
        //public abstract void setDigitalOutput(int value);


        //
        // Status
        //
        //public abstract void getMeasurementStatus(bool interlockDefeated, bool complianceLimitReached);
        //public abstract bool errorHasOccurred(string errMesg);
        //public abstract void SetMAV_StatusByteMask(bool State);
        //public abstract void SetMSB_StatusByteMask(bool State);
        public abstract void ConfigureCurrentSweepReadings();
        public abstract void ConfigureVoltageSweepReadings();

        //
        // Low level bus commands - should be in GPIBio
        //
        //public abstract void WaitForOPC();
        //public abstract void WaitForSRQ();

    }
}

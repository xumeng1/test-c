// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_AQ6370.cs
//
// Author: echoxl.wang, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    public class Chassis_YKAQ6370 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_YKAQ6370(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord aq6370Data = new ChassisDataRecord(
                "YOKOGAWA AQ6370B",			// hardware name 
                "0.0",			// minimum valid firmware version 
                "99.99");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", aq6370Data);
            
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // idn[0]: manufacture name: YOKOGAWA
                // idn[1]: instrument name
                // idn[2]: serial number (9 digit string)
                // idn[3]: firmware version
                return idn[3];//
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];//
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
               
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                   // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                   this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                   this.Write("*CLS", null);                    
                }
            }
        }
        

        #endregion
    }
}

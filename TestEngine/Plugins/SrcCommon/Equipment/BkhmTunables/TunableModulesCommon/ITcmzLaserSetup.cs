using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Wrapper functions for TCMZ Goldbox Registers not covered by IDsdbrLaserSetup
    /// </summary>
    public interface ITcmzLaserSetup
    {
        /// <summary>
        /// Enter Protected Command Mode
        /// </summary>
        void EnterProtectMode();

        /// <summary>
        /// Exit protected Command mode
        /// </summary>
        void ExitProtectMode();

        /// <summary>
        /// Enable Calibration Mode
        /// </summary>
        void EnableCalibrationMode();

        /// <summary>
        /// Disable Calibration mode
        /// </summary>
        void DisableCalibrationMode();

        /// <summary>
        /// Get/set Settling time for settings
        /// </summary>
        int SettlingTime_ms
        {
            get;
            set;
        }

        /// <summary>
        /// Get DAC_PWC value
        /// </summary>
        /// <returns>DAC_PWC value</returns>
        int GetPWCDac();

        
        /// <summary>
        /// Set DAC_PWC
        /// </summary>
        /// <param name="dac">DAC value</param>
        void SetPWCDac(int dac);

        /// <summary>
        /// Get Mz Bias Left DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzLeftBiasDac();

        
        /// <summary>
        /// Set MZ Left Bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        void SetMzLeftBiasDac(int dac);

        
        /// <summary>
        /// Get Mz Bias Left DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzLeftBiasDacNominal();

        
        /// <summary>
        /// Set MZ Left Bias DAC Nominal Value 
        /// </summary>
        /// <param name="dac">DAC value</param>
        void SetMzLeftBiasDacNominal(int dac);

        
        /// <summary>
        /// Get Mz Bias Right DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzRightBiasDac();
        
        /// <summary>
        /// Set MZ Right Bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        void SetMzRightBiasDac(int dac);
        
        /// <summary>
        /// Get Mz Mod DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzModSetDac();
        
        /// <summary>
        /// Set MZ Mod Set DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        void SetMzModSetDac(int dac);

        /// <summary>
        /// Get Mz Mod DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzModSetDacNominal();

        /// <summary>
        /// Set Mz Mod DAC Nominal value 
        /// </summary>
        /// <param name="dac">Dac Value</param>
        void SetMzModSetDacNominal(int dac);
            
        /// <summary>
        /// Get Mz Left Imbalance DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzLeftImbalDac();

        /// <summary>
        /// Set Mz Left Imbalance DAC value
        /// </summary>
        /// <param name="dac">Dac Value</param>
        void SetMzLeftImbalDac(int dac);
        
        /// <summary>
        /// Get Mz Left Imbalance DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzLeftImbalDacNominal();

        /// <summary>
        /// Set Mz Left Imbalance DAC Nominal value 
        /// </summary>
        /// <param name="dac">Dac Value</param>
        void SetMzLeftImbalDacNominal(int dac);
        
        /// <summary>
        /// Get Mz Right Imbalance DAC value
        /// </summary>
        /// <returns>Dac Value</returns>       
        int GetMzRightImbalDac();
       
        /// <summary>
        /// Set Mz Right Imbalance DAC value
        /// </summary>
        /// <param name="dac">Dac Value</param>
        void SetMzRightImbalDac(int dac);
      
        /// <summary>
        /// Get Mz Right Imbalance DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        int GetMzRightImbalDacNominal();


        /// <summary>
        /// Set Mz Right Imbalance DAC Nominal value 
        /// </summary>
        /// <param name="dac">Dac Value</param>
        void SetMzRightImbalDacNominal(int dac);

        /// <summary>
        /// Get MMI DC ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        int GetMmiDc_ADC();

        /// <summary>
        /// Get MMI AC ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        int GetMmiAc_ADC();
    
        /// <summary>
        /// Get Offset for Mz Crossing  control loop 
        /// </summary>
        /// <returns>The offset</returns>
        int GetMmiDiffOffset();

        /// <summary>
        /// Set Offset for Mz Crossing  control loop. 
        /// </summary>
        /// <param name="offset">offset</param>
        void SetMmiDiffOffset(int offset);

        /// <summary>
        /// Set Position in n point look up table to store Mmitap derived power reading used for interpolating Tx power
        /// </summary>
        /// <param name="calPoint">The index</param>
        void SetTxPowMonCalibrationPoint(byte calPoint);

        /// <summary>
        /// Get SOA Slope in nanoWatts/dacCount
        /// </summary>
        /// <returns>The SOA Slope</returns>
        int GetSoaSlope();
    
        /// <summary>
        /// Store SOA Slope in nanoWatts/dacCount
        /// </summary>
        /// <param name="slope">The slope</param>
        void SetSoaSlope(int slope);

        
        /// <summary>
        /// Get the Calibrated SOA set Register value.
        /// This is the initial SOA value 
        /// used by the closed loop power control.
        /// </summary>
        /// <returns>value</returns>
        int GetCalSoaRegister();

        /// <summary>
        /// Set the CAL_SET_SOA Register. This is the initial SOA value 
        /// used by the closed loop power control.
        /// </summary>
        /// <param name="value"></param>
        void SetCalSoaRegister(int value);

        /// <summary>
        /// Enable/Disable Crossing Point control loop
        /// </summary>
        /// <param name="enabled"></param>
        void SetCrossingControlLoopOn(bool enabled);
        
        /// <summary>
        /// Get MZ Submount Temperature (MzTEMPMON) 
        /// </summary>
        /// <returns>The temperature in Deg C</returns>
        double GetMzSubmountTemperature_DegC();
    
        /// <summary>
        /// Disable Modulation from Laser Driver
        /// </summary>
        void DisableMzLaserModulation();

        /// <summary>
        /// Enable Modulation from Laser Driver 
        /// </summary>
        void EnableMzLaserModulation();

        /// <summary>
        ///  Disable Laser Output
        ///  Use with caution - may not work properly at Tx Module Setup Stage.
        /// </summary>
        void DisableLaserOutput();

        /// <summary>
        /// Enable Laser Output 
        /// Use with caution - may not work properly at Tx Module Setup Stage.
        /// </summary>
        void EnableLaserOutput();


        /// <summary>
        /// Disable Internal PRBS generator
        /// </summary>
        void DisableInternalPrbs();


        /// <summary>
        /// Enable Internal PRBS generator
        /// </summary>
        void EnableInternalPrbs();
      
    }
}

// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// IMSA300pin.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Wrapper functions for the MSA Standard commands.
    /// See "300 pin Multi Source Agreement for 10 and 40 Gigabit
    /// Transponders (SERDES Transceivers)" by the 300 pin MSA group.
    /// </summary>
    public interface IMSA300pin
    {
        /// <summary>
        /// Set the Tx Command register.
        /// </summary>
        /// <param name="txCmd">New register value.</param>
        void SetTxCommand(TxCommand txCmd);

        /// <summary>
        /// Read the current Tx Command register.
        /// </summary>
        /// <returns>Register value.</returns>
        TxCommand ReadTxCommand();

        /// <summary>
        /// Instruct the instrument to make an internal copy of the TxCommand register value.
        /// </summary>
        void SaveTxRegister();

        /// <summary>
        /// Instruct the instrument to restore the internal copy of the TxCommand register.
        /// </summary>
        void RestoreTxRegister();

        /// <summary>
        /// Get the value of LsEnable bit of the TxCommand register.
        /// </summary>
        /// <returns>True for one, false for zero.</returns>
        bool GetLsEnable();

        /// <summary>
        /// Set only the LsEnable bit of the TxCommand register, leaving the other bits unmodified.
        /// </summary>
        /// <param name="lsEnable">True for one, false for zero.</param>
        void SetLsEnable(bool lsEnable);

        /// <summary>
        /// Set the Rx Command register.
        /// </summary>
        /// <param name="rxCmd">New register value.</param>
        void SetRxCommand(RxCommand rxCmd);

        /// <summary>
        /// Read the current Tx Command register.
        /// </summary>
        /// <returns>Register value.</returns>
        RxCommand ReadRxCommand();

        /// <summary>
        /// Instruct the instrument to make an internal copy of the TxCommand register value.
        /// </summary>
        void SaveRxRegister();

        /// <summary>
        /// Instruct the instrument to restore the internal copy of the TxCommand register.
        /// </summary>
        void RestoreRxRegister();

        /// <summary>
        /// Select a new ITU channel.
        /// </summary>
        /// <param name="bandChannel">Band and channel.</param>
        void SelectITUChannel(BandChannel bandChannel);

        /// <summary>
        /// Select a new ITU channel.
        /// This overload will enter MSA Protect Mode, change the Channel, and then
        /// exit MSA Protect Mode if the protectModeChange parameter is TRUE.
        /// </summary>
        /// <param name="bandChannel">Band and channel.</param>
        /// <param name="protectModeChange">True if it is necessary to enter MSA Protected Mode first</param>
        void SelectITUChannel(BandChannel bandChannel, bool protectModeChange);


        /// <summary>
        /// Get the current ITU channel setting.
        /// </summary>
        /// <returns>Band and channel.</returns>
        BandChannel GetITUChannel();

        /// <summary>
        /// Get the current Rx Threshold.
        /// </summary>
        /// <returns>register value.</returns>
        double ReadRxThreshold();

        /// <summary>
        /// Set the Rx Threshold.
        /// </summary>
        /// <param name="thresh_pc">New register value.</param>
        void SetRxThreshold(double thresh_pc);

        /// <summary>
        /// Get the laser bias current in mA.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserBiasCurrent_mA();

        /// <summary>
        /// Get the actual laser output power in microWatts.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserOutputPower_uW();

        /// <summary>
        /// Gets the current sub-mount temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetSubMountTemp_degC();

        /// <summary>
        /// Get Rx AC power.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetRxACPower();

        /// <summary>
        /// Get average Rx power in nanoWatts.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetRxAvgPower_nW();

        /// <summary>
        /// Get the laser frequency offset value.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserFrequencyOffset_MHz();

        /// <summary>
        /// Get module temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetModuleTemp_degC();

        /// <summary>
        /// Get APD temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetAPDTemp_degC();

        /// <summary>
        /// Get module bias.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetModBias();

        /// <summary>
        /// Get the laser's absolute temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserAbsoluteTemperature_degC();

        /// <summary>
        /// Get the last channel of this instrument's capabilities.
        /// </summary>
        /// <returns>Register value.</returns>
        BandChannel GetLastChannel();

        /// <summary>
        /// Gets the instrument's supplier code.
        /// </summary>
        /// <returns>Register value. "B" = Bookham.</returns>
        string GetSupplier();

        /// <summary>
        /// Gets the module type string.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetModuleType();

        /// <summary>
        /// Get the first channel of this instrument's capabilities.
        /// </summary>
        /// <returns>Register value.</returns>
        BandChannel GetFirstChannel();

        /// <summary>
        /// Get this instrumnet's channel spacing.
        /// </summary>
        /// <returns>Register value.</returns>
        double ChannelSpacing_GHz();

        /// <summary>
        /// Get the unit's serial number.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetUnitSerialNumber();

        /// <summary>
        /// Get the date of manufacture.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetManufactureDate();

        /// <summary>
        /// Get the unit's part number.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetUnitPartNumber();

        /// <summary>
        /// Query the link status.
        /// </summary>
        /// <returns>True for working. False for failed.</returns>
        bool LinkStatus();

        /// <summary>
        /// Eneter instrument "Pin mode".
        /// </summary>
        void EnterPinMode();

        /// <summary>
        /// Query the maximum I2C rate supported.
        /// </summary>
        /// <returns>Register value.</returns>
        int ReadMaxI2CRate_kbps();

        /// <summary>
        /// Enter protect mode.
        /// </summary>
        void EnterProtectMode();

        /// <summary>
        /// Enter vendor protection mode.
        /// </summary>
        /// <param name="password">Vendor password.</param>
        void EnterVendorProtectMode(string password);

        /// <summary>
        /// Leave protect mode.
        /// </summary>
        void ExitProtectMode();

        /// <summary>
        /// Reset CPN.
        /// </summary>
        void ResetCPN();

        /// <summary>
        /// Enter soft mode.
        /// </summary>
        void EnterSoftMode();

        /// <summary>
        /// Perform a loopback over the I2C link.
        /// </summary>
        void Loopback();

        /// <summary>
        /// Get the hardware revision code
        /// </summary>
        /// <returns>The Hardware Revision Code</returns>
        string GetHardwareRevisionCode();

        /// <summary>
        /// Get the Firmware Revision Code 
        /// </summary>
        /// <returns>The Firmware Revision Code</returns>
        string GetFirmwareRevisionCode();

        /// <summary>
        /// Read 300Pin MSA Capability Byte.
        /// </summary>
        /// <returns>The Capability Byte</returns>
        byte ReadMSACapability();

       /// <summary>
        /// Read 300Pin MSA Operating Mode as a Byte
        /// </summary>
        /// <returns>The Operating Mode Byte</returns>
        byte ReadOperatingModeByte();

        /// <summary>
        /// Read 300Pin MSA Edition Byte.
        /// </summary>
        /// <returns>The MSA Edition Byte</returns>
        byte ReadMSAEdition();

        /// <summary>
        /// Get LsWAVEMON Value 
        /// </summary>
        /// <returns>Rx Wavelength in GHz</returns>
        double GetLsWAVEMON_GHz();

        /// <summary>
        /// Get RxPOWMON Value 
        /// </summary>
        /// <returns>Rx Power in mW</returns>
        double GetRxPOWMON_mW();

        /// <summary>
        /// Get LsTEMPMON Value 
        /// </summary>
        /// <returns>Laser Temperature in Degrees C</returns>
        double GetLsTEMPMON_DegC();

        /// <summary>
        /// Get LsPOWMON  Value 
        /// </summary>
        /// <returns>Laser output power in mW</returns>
        double GetLsPOWMON_mW();


        /// <summary>
        /// Get LsBIASMON Value 
        /// </summary>
        /// <returns>Laser Bias Monitor Currrent in mA</returns>
        double GetLsBIASMON_mA();

        /// <summary>
        /// Get the state of the PRBSERRDET (an error was detected by the PRBS error checker) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetPRBSERRDETState();

        /// <summary>
        /// Get the state of the RXS(SFI-5 DEMUX status) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetRXSState();

        /// <summary>
        /// Get the state of the RxLOCKERR (Loss of lock of RxPOCLK) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetRxLOCKERRState();

        /// <summary>
        /// Get the state of the RxSIGALM (Loss AC power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetRxSIGALMState();

        /// <summary>
        /// Get the state of the RxPOWALM (Loss DC power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetRxPOWALMState();

        
        /// <summary>
        /// Get the state of the RxALM INT alarm (all RX alarms ORed)
        /// </summary>
        /// <returns>True if active</returns>
        bool GetRxALMINTState();

        /// <summary>
        /// Read the Rx Alarm Status Register 
        /// </summary>
        /// <returns>The Alarm status register, split into bytes</returns>
        RxAlarmStatusRegister ReadRxAlarmStatusRegister();

        /// <summary>
        /// Get the state of the TxFIFO ERR (Mux FIFO error indicator) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetTxFIFOERRState();

        
        /// <summary>
        /// Get the state of the ModBIASALM (Modulator bias) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetModBIASALMState();


        /// <summary>
        /// Get the state of the LsPOWALM (Laser power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetLsPOWALMState();
        
        /// <summary>
        /// Get the state of the TxLOCKERR (Loss of TxPLL lock indicator) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetTxLOCKERRState();

        
        /// <summary>
        /// Get the state of the LsTEMPALM (Laser temperature) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetLsTEMPALMState();

        
        /// <summary>
        /// Get the state of the LsBIASALM (Laser bias current) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetLsBIASALMState();

        /// <summary>
        /// Get the state of the TxALM INT alarm (all TX alarms ORed)
        /// </summary>
        /// <returns>True if active</returns>
        bool GetTxALMINTState();

        
        /// <summary>
        /// Get the state of the LsWAVALM (Laser Wavelength) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetLsWAVALMState();

        
        /// <summary>
        /// Get the state of the TxDSCERR  (Latching SFI-5 DESKEW Channel error, cleared on read) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetTxDSCERRState();

        /// <summary>
        /// Get the state of the TxLOFALM (Loss of Frame) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetTxLOFALMState();

        /// <summary>
        /// Get the state of the TxOOA  (SFI-5 DESKEW) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetTxOOAState();

        
        /// <summary>
        /// Get the state of the ModTEMPALM (Modulator Temperature) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetModTEMPALMState();

        
        /// <summary>
        /// Get the state of the EOLALM (Laser end of life) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetEOLALMState();

        
        /// <summary>
        /// Read the Tx Alarm Status Register 
        /// </summary>
        /// <returns>The Alarm status register, split into bytes</returns>
        TxAlarmStatusRegister ReadTxAlarmStatusRegister();
    }

    /// <summary>
    /// Data class for the Tx Alarm Status register
    /// </summary>
    public class TxAlarmStatusRegister
    {
        /// <summary> Data Byate 1 </summary>
        public readonly byte Data1;
        /// <summary> Data Byate 2 </summary>
        public readonly byte Data2;
        /// <summary> Data Byate 3 </summary>
        public readonly byte Data3;

        /// <summary>
        /// Construct the status register content.
        /// </summary>
        /// <param name="data1">Tx Alarm Status Register data 1.</param>
        /// <param name="data2">Tx Alarm Status Register data 2.</param>
        /// <param name="data3">Tx Alarm Status Register data 3.</param>
        public TxAlarmStatusRegister(byte data1, byte data2, byte data3)
        {
            this.Data1 = data1;
            this.Data2 = data2;
            this.Data3 = data3;
        }
    }


    /// <summary>
    /// Data class for the Rx Alarm Status register
    /// </summary>
    public class RxAlarmStatusRegister
    {
        /// <summary>
        /// Data Byte 1
        /// </summary>
        public readonly byte Data1;

        /// <summary>
        /// Data Byte 2
        /// </summary>
        public readonly byte Data2;

        /// <summary>
        /// Construct the status register content.
        /// </summary>
        /// <param name="data1">Rx Alarm Status Register data 1.</param>
        /// <param name="data2">Rx Alarm Status Register data 2.</param>
        public RxAlarmStatusRegister(byte data1, byte data2)
        {
            this.Data1 = data1;
            this.Data2 = data2;
        }
    }
}

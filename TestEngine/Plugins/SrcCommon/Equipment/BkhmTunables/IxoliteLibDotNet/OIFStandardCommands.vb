Option Strict Off
Option Explicit On
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("OIFStandardCommands_NET.OIFStandardCommands")> Public Class OIFStandardCommands
	
	
	
	Public Function ReadNOP() As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ReadNOP = CShort(Comms.ReadOIF(NOPREG, 0, 1))
	End Function
	
	
	Public Function GetNOP() As Integer
		' 0
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetNOP = CInt(Comms.ReadOIF(NOPREG, 0, 1))
		If GetNOP = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF NOP Register.")
		End If
	End Function
	
	
	
	
	Public Function GetDeviceType() As String
		' 1
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetDeviceType = Comms.ReadOIFString(DEVICETYPEREG)
		If GetDeviceType = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Device Type.")
		End If
	End Function
	
	
	
	Public Function GetManufacturer() As String
		' 2
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetManufacturer = Comms.ReadOIFString(MANUFACTURERREG)
		If GetManufacturer = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Manufacturer.")
		End If
	End Function
	
	
	
	Public Function GetModelNumber() As String
		' 3
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetModelNumber = Comms.ReadOIFString(MODELNUMBERREG)
		If GetModelNumber = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Model Number.")
		End If
	End Function
	
	
	
	Public Function GetSerialNumber() As String
		' 4
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetSerialNumber = Comms.ReadOIFString(SERIALNUMBERREG)
		If GetSerialNumber = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Serial Number.")
		End If
	End Function
	
	
	
	Public Function GetManufacturingDate() As String
		' 5
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetManufacturingDate = Comms.ReadOIFString(MANUFACTURINGDATEREG)
		If GetManufacturingDate = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Manufacturing Date.")
		End If
	End Function
	
	
	
	Public Function GetReleaseCode() As String
		' 6
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetReleaseCode = Comms.ReadOIFString(RELEASECODEREG)
		If GetReleaseCode = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Release Code.")
		End If
	End Function
	
	
	
	
	Public Sub GetModuleRevisionCodes(ByRef hwrev As String, ByRef fwrev As String)
		' 6
		' See the OIF manual for a description of the Release Code format.
		' The string must contain at least the PV code and one firmware/hardware code.
		Dim strData As String
		Dim fwpos As Short
		Dim hwpos As Short
		Dim aspos As Short
		Dim pvpos As Short
		Dim nextcolon As Short
		Dim lenf As Short
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		strData = Comms.ReadOIFString(RELEASECODEREG)
		lenf = Len(strData)
		
		If GetReleaseCode = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Release Code.")
			GoTo Aborted
		Else
			pvpos = InStr(1, strData, "PV ", CompareMethod.Text)
			fwpos = InStr(1, strData, "FW ", CompareMethod.Text)
			hwpos = InStr(1, strData, "HW ", CompareMethod.Text)
			If (pvpos <= 0) Or ((fwpos <= 0) And (hwpos <= 0)) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Invalid OIF Release Code format.")
				GoTo Aborted
			Else
				If fwpos <= 0 Then
					fwrev = ""
				Else
					nextcolon = InStr(fwpos, strData, ":", CompareMethod.Text)
					If nextcolon <= 0 Then
						fwrev = Mid(strData, fwpos + 3)
					Else
						fwrev = Mid(strData, fwpos + 3, nextcolon - fwpos - 3)
					End If
				End If
				If hwpos <= 0 Then
					hwrev = ""
				Else
					nextcolon = InStr(hwpos, strData, ":", CompareMethod.Text)
					If nextcolon <= 0 Then
						hwrev = Mid(strData, hwpos + 3)
					Else
						hwrev = Mid(strData, hwpos + 3, nextcolon - hwpos - 3)
					End If
				End If
			End If
		End If
		Exit Sub
		
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetModuleRevisionCodes")
Aborted: 
		hwrev = "Error"
		fwrev = "Error"
	End Sub
	
	
	Public Function GetReleaseBackwards() As String
		' 7
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetReleaseBackwards = Comms.ReadOIFString(RELEASEBACKWARDS)
		If GetReleaseBackwards = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Release Backwards.")
		End If
	End Function
	
	
	
	Public Function SaveConfig() As Boolean
		' 8
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SaveConfig = Comms.WriteOIF(GENERALCONFIG, STOREDEFAULTCONFIG, 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SaveConfig Then Logger.logFATAL("Error: Failed to set OIF SDC bit.")
	End Function
	
	
	
	
	Public Function SetGeneralConfigRCS(ByRef what As String) As Boolean
		' 8
		If what = "on" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetGeneralConfigRCS = Comms.WriteOIF(GENERALCONFIG, 1, 1)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetGeneralConfigRCS = Comms.WriteOIF(GENERALCONFIG, 0, 1)
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetGeneralConfigRCS Then Logger.logFATAL("Error: Failed to set OIF RCS bit.")
	End Function
	
	
	
	
	Public Function GetGeneralConfiguration(ByRef gcf As Integer) As Boolean
		' 8
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		gcf = CInt(Comms.ReadOIF(GENERALCONFIG, 0, 1))
		If gcf = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF GenCfg.")
			GetGeneralConfiguration = False
		Else
			GetGeneralConfiguration = True
		End If
	End Function
	
	
	
	
	Public Function GetIOCap(ByRef iocap As Integer) As Boolean
		' 0D
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		iocap = CInt(Comms.ReadOIF(IOCAPREG, 0, 1))
		If iocap = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF IOCap Register.")
			GetIOCap = False
		Else
			GetIOCap = True
		End If
	End Function
	
	
	
	Public Function SetIOCap(ByRef iocap As Integer) As Boolean
		' 0D
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetIOCap = Comms.WriteOIF(IOCAPREG, CDbl(iocap), 1)
		If Not SetIOCap Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF IOCap.")
		End If
	End Function
	
	
	Public Function GetStatusF() As Integer
		' 20
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetStatusF = Comms.ReadOIF(STATUSF, 0, 1)
		If GetStatusF = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF StatusF.")
		End If
	End Function
	
	
	
	Public Function SetStatusF(ByVal stf As Integer) As Boolean
		' 20
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetStatusF = Comms.WriteOIF(STATUSF, CDbl(stf), 1)
		If Not SetStatusF Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF StatusF.")
		End If
	End Function
	
	
	
	Public Function GetStatusW() As Integer
		' 21
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetStatusW = Comms.ReadOIF(STATUSW, 0, 1)
		If GetStatusW = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF StatusW.")
		End If
	End Function
	
	
	
	Public Function SetStatusW(ByVal stw As Integer) As Boolean
		' 21
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetStatusW = Comms.WriteOIF(STATUSW, CDbl(stw), 1)
		If Not SetStatusW Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF StatusW.")
		End If
	End Function
	
	
	
	Public Function GetFPowTh(ByRef thresh As Double) As Boolean
		' 22
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thresh = Comms.ReadOIF(FPOWTH, 0, 100)
		If thresh = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF FPowTh.")
			GetFPowTh = False
		Else
			GetFPowTh = True
		End If
	End Function
	
	
	
	Public Function SetFPowTh(ByRef thresh As Double) As Boolean
		' 22
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetFPowTh = Comms.WriteOIF(FPOWTH, thresh, 100)
		If Not SetFPowTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF FPowTh.")
		End If
	End Function
	
	
	
	
	Public Function GetWPowTh(ByRef thresh As Double) As Boolean
		' 23
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thresh = Comms.ReadOIF(WPOWTH, 0, 100)
		If thresh = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF WPowTh.")
			GetWPowTh = False
		Else
			GetWPowTh = True
		End If
	End Function
	
	
	
	Public Function SetWPowTh(ByRef thresh As Double) As Boolean
		' 23
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetWPowTh = Comms.WriteOIF(WPOWTH, thresh, 100)
		If Not SetWPowTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF WPowTh.")
		End If
	End Function
	
	
	
	
	Public Function GetFFreqTh(ByRef thresh As Double) As Boolean
		' 24
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thresh = Comms.ReadOIF(FFREQTH, 0, 10)
		If thresh = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF FFreqTh.")
			GetFFreqTh = False
		Else
			GetFFreqTh = True
		End If
	End Function
	
	
	
	Public Function SetFFreqTh(ByRef thresh As Double) As Boolean
		' 24
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetFFreqTh = Comms.WriteOIF(FFREQTH, thresh, 10)
		If Not SetFFreqTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF FFreqTh.")
		End If
	End Function
	
	
	
	
	Public Function GetWFreqTh(ByRef thresh As Double) As Boolean
		' 25
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thresh = Comms.ReadOIF(WFREQTH, 0, 10)
		If thresh = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF WFreqTh.")
			GetWFreqTh = False
		Else
			GetWFreqTh = True
		End If
	End Function
	
	
	
	Public Function SetWFreqTh(ByRef thresh As Double) As Boolean
		' 25
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetWFreqTh = Comms.WriteOIF(WFREQTH, thresh, 10)
		If Not SetWFreqTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF WFreqTh.")
		End If
	End Function
	
	
	
	
	Public Function GetFThermTh(ByRef thresh As Double) As Boolean
		' 26
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thresh = Comms.ReadOIF(FTHERMTH, 0, 100)
		If thresh = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF FThermTh.")
			GetFThermTh = False
		Else
			GetFThermTh = True
		End If
	End Function
	
	
	
	Public Function SetFThermTh(ByRef thresh As Double) As Boolean
		' 26
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetFThermTh = Comms.WriteOIF(FTHERMTH, thresh, 100)
		If Not SetFThermTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF FThermTh.")
		End If
	End Function
	
	
	
	
	Public Function GetWThermTh(ByRef thresh As Double) As Boolean
		' 27
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thresh = Comms.ReadOIF(WTHERMTH, 0, 100)
		If thresh = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF WThermTh.")
			GetWThermTh = False
		Else
			GetWThermTh = True
		End If
	End Function
	
	
	
	Public Function SetWThermTh(ByRef thresh As Double) As Boolean
		' 27
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetWThermTh = Comms.WriteOIF(WTHERMTH, thresh, 100)
		If Not SetWThermTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF WThermTh.")
		End If
	End Function
	
	
	
	Public Function GetSRQT(ByRef trig As Integer) As Boolean
		' 28
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		trig = CInt(Comms.ReadOIF(SRQT, 0, 1))
		If trig = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF SRQT.")
			GetSRQT = False
		Else
			GetSRQT = True
		End If
	End Function
	
	
	
	Public Function SetSRQT(ByRef trig As Integer) As Boolean
		' 28
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetSRQT = Comms.WriteOIF(SRQT, CDbl(trig), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetSRQT Then Logger.logFATAL("Error: Failed to set OIF SRQT.")
	End Function
	
	
	
	Public Function GetFATALT(ByRef trig As Integer) As Boolean
		' 29
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		trig = CInt(Comms.ReadOIF(FATALT, 0, 1))
		If trig = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF FATALT.")
			GetFATALT = False
		Else
			GetFATALT = True
		End If
	End Function
	
	
	
	Public Function SetFATALT(ByRef trig As Integer) As Boolean
		' 29
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetFATALT = Comms.WriteOIF(FATALT, CDbl(trig), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetFATALT Then Logger.logFATAL("Error: Failed to set OIF FATALT.")
	End Function
	
	
	
	Public Function GetALMT(ByRef trig As Integer) As Boolean
		' 2A
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		trig = CInt(Comms.ReadOIF(ALMT, 0, 1))
		If trig = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF ALMT.")
			GetALMT = False
		Else
			GetALMT = True
		End If
	End Function
	
	
	
	Public Function SetALMT(ByRef trig As Integer) As Boolean
		' 2A
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetALMT = Comms.WriteOIF(ALMT, CDbl(trig), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetALMT Then Logger.logFATAL("Error: Failed to set OIF ALMT.")
	End Function
	
	
	
	
	Public Function SelectChannel(ByRef chan As Short) As Boolean
		' 30
		' This procedure expects an OIF channel number. If a match with MSA numbers is required
		' then the first channel frequency and spacing must have been set appropriately, i.e. grid
		' spacing is -50GHz and the first channel frequency is 196100GHz for "C" band or 190900GHz
		' for "L" band.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SelectChannel = Comms.WriteOIF(CHANNELSELECT, CDbl(chan), 1)
		If Not SelectChannel Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF channel.")
		End If
	End Function
	
	
	
	
	Public Function GetChannel(ByRef chan As Short) As Boolean
		' 30
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		chan = CShort(Comms.ReadOIF(CHANNELSELECT, 0, 1))
		If chan = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF channel.")
			GetChannel = False
		Else
			GetChannel = True
		End If
	End Function
	
	
	
	Public Function SetPowerSetpoint(ByRef pwr As Double) As Boolean
		' 31
		' Power is in dBm.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetPowerSetpoint = Comms.WriteOIF(POWERSETPOINT, pwr, 100)
		If Not SetPowerSetpoint Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF power.")
		End If
	End Function
	
	
	
	Public Function GetPowerSetpoint(ByRef pwr As Double) As Boolean
		' 31
		' Power is in dBm.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pwr = Comms.ReadOIF(POWERSETPOINT, 0, 100)
		If pwr = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF power.")
			GetPowerSetpoint = False
		Else
			GetPowerSetpoint = True
		End If
	End Function
	
	
	
	Public Function SoftwareEnableOutput(ByRef what As String) As Boolean
		' 32
		If what = "enable" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SoftwareEnableOutput = Comms.WriteOIF(MODULERESETENABLE, 8, 1)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SoftwareEnableOutput = Comms.WriteOIF(MODULERESETENABLE, 0, 1)
		End If
	End Function
	
	
	
	Public Function ModuleReset() As Boolean
		' 32
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ModuleReset = Comms.WriteOIF(MODULERESETENABLE, 1, 1)
		If Not ModuleReset Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF Module Reset bit.")
		End If
	End Function
	
	
	
	Public Function SoftReset() As Boolean
		' 32
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SoftReset = Comms.WriteOIF(MODULERESETENABLE, 2, 1)
		If Not SoftReset Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF Soft Reset bit.")
		End If
	End Function
	
	
	
	Public Function GetSoftwareEnableOutput(ByRef sena As Short) As Boolean
		' 32
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		sena = CShort(Comms.ReadOIF(MODULERESETENABLE, 0, 1))
		If sena = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF ResEna.")
			GetSoftwareEnableOutput = False
		Else
			sena = (sena \ 8) And 1 ' Only want to send back 1 or 0.
			GetSoftwareEnableOutput = True
		End If
	End Function
	
	
	
	Public Function GetModuleConfigBehaviour(ByRef mcb As Integer) As Boolean
		' 33
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		mcb = CInt(Comms.ReadOIF(MODCONFIGBEHAVIOUR, 0, 1))
		If mcb = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF MCB.")
			GetModuleConfigBehaviour = False
		Else
			GetModuleConfigBehaviour = True
		End If
	End Function
	
	
	
	Public Function SetModuleConfigBehaviour(ByRef mcb As Byte) As Boolean
		' 33
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetModuleConfigBehaviour = Comms.WriteByteOIF(MODCONFIGBEHAVIOUR, 0, mcb)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetModuleConfigBehaviour Then Logger.logFATAL("Error: Failed to set OIF MCB.")
	End Function
	
	
	
	
	Public Function SetFrequencyGrid(ByRef ghzgrid As Short) As Boolean
		' 34
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetFrequencyGrid = Comms.WriteOIF(FREQUENCYGRID, CDbl(ghzgrid), 10)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetFrequencyGrid Then Logger.logFATAL("Error: Failed to set OIF Frequency Grid.")
	End Function
	
	
	
	Public Function GetFrequencyGrid(ByRef ghzgrid As Short) As Boolean
		' 34
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ghzgrid = CShort(Comms.ReadOIF(FREQUENCYGRID, 0, 10))
		If ghzgrid = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Frequency Grid.")
			GetFrequencyGrid = False
		Else
			GetFrequencyGrid = True
		End If
	End Function
	
	
	
	
	' NOTE: It should be possible to write the high and low values of the following
	' frequencies in any order, but at this time (v2.2.2) the Santur code requires
	' the GHz part first.
	
	Public Function SetFirstChanFreq(ByRef freq As Double) As Boolean
		' 35/36
		' Frequency must be in GHz.
		Dim thz As Short
		
		thz = FMod(freq, 1000)
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(FIRSTCHANFREQ2, CDbl(thz), 10) Then GoTo Aborted
		thz = Int(freq / 1000)
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(FIRSTCHANFREQ1, CDbl(thz), 1) Then GoTo Aborted
		SetFirstChanFreq = True
		Exit Function
		
Aborted: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("Error: Failed to set OIF First Channel Frequency.")
		SetFirstChanFreq = False
	End Function
	
	
	
	Public Function GetFirstChanFreq(ByRef fcf As Double) As Boolean
		' 35/36
		' Frequency must be in GHz.
		Dim thz As Short
		Dim ghz As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thz = Comms.ReadOIF(FIRSTCHANFREQ1, 0, 1)
		If thz = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF FCF1 (THz).")
			GetFirstChanFreq = False
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ghz = Comms.ReadOIF(FIRSTCHANFREQ2, 0, 10)
			If ghz = GENERAL_ERROR_CODE Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed to get OIF FCF2 (GHz).")
				GetFirstChanFreq = False
			Else
				fcf = thz * 1000# + ghz
				GetFirstChanFreq = True
			End If
		End If
	End Function
	
	
	
	Public Function GetLaserFrequency(ByRef lf As Double) As Boolean
		' 40/41
		' Frequency must be in GHz.
		Dim thz As Short
		Dim ghz As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thz = Comms.ReadOIF(LASERFREQUENCY1, 0, 1)
		If thz = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF LF1 (THz).")
			GetLaserFrequency = False
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ghz = Comms.ReadOIF(LASERFREQUENCY2, 0, 10)
			If ghz = GENERAL_ERROR_CODE Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed to get OIF LF2 (GHz).")
				GetLaserFrequency = False
			Else
				lf = thz * 1000# + ghz
				GetLaserFrequency = True
			End If
		End If
	End Function
	
	
	
	
	Public Function GetLaserOutputPower(ByRef oop As Double) As Boolean
		' 42
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		oop = Comms.ReadOIF(LASEROUTPUTPWR, 0, 100) ' OIF returns dBm.
		If oop = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF OOP.")
			GetLaserOutputPower = False
		Else
			GetLaserOutputPower = True
		End If
	End Function
	
	
	
	
	Public Function GetCurrentTemperature(ByRef ctemp As Double) As Boolean
		' 43
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ctemp = Comms.ReadOIF(CURRENTOSATEMP, 0, 100)
		If ctemp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF CTemp.")
			GetCurrentTemperature = False
		Else
			GetCurrentTemperature = True
		End If
	End Function
	
	
	
	
	Public Function GetOpticalPowerMin(ByRef oop As Double) As Boolean
		' 50
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		oop = Comms.ReadOIF(OPTICALPOWERMIN, 0, 100) ' OIF returns dBm.
		If oop = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF OPSL.")
			GetOpticalPowerMin = False
		Else
			GetOpticalPowerMin = True
		End If
	End Function
	
	
	
	
	Public Function GetOpticalPowerMax(ByRef oop As Double) As Boolean
		' 51
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		oop = Comms.ReadOIF(OPTICALPOWERMAX, 0, 100) ' OIF returns dBm.
		If oop = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF OPSH.")
			GetOpticalPowerMax = False
		Else
			GetOpticalPowerMax = True
		End If
	End Function
	
	
	
	
	Public Function GetLaserFirstFreq(ByRef lfl As Double) As Boolean
		' 52/53
		' Frequency must be in GHz.
		Dim thz As Short
		Dim ghz As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thz = Comms.ReadOIF(LASERFIRSTFREQ1, 0, 1)
		If thz = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF LFL1 (THz).")
			GetLaserFirstFreq = False
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ghz = Comms.ReadOIF(LASERFIRSTFREQ2, 0, 10)
			If ghz = GENERAL_ERROR_CODE Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed to get OIF LFL2 (GHz).")
				GetLaserFirstFreq = False
			Else
				lfl = thz * 1000# + ghz
				GetLaserFirstFreq = True
			End If
		End If
	End Function
	
	
	
	Public Function SetLaserFirstFreq(ByRef freq As Double) As Boolean
		' 52/53
		' Frequency must be in GHz.
		Dim thz As Short
		
		thz = FMod(freq, 1000)
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(LASERFIRSTFREQ2, CDbl(thz), 10) Then GoTo Aborted
		thz = Int(freq / 1000)
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(LASERFIRSTFREQ1, CDbl(thz), 1) Then GoTo Aborted
		SetLaserFirstFreq = True
		Exit Function
		
Aborted: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("Error: Failed to set OIF Laser First Frequency.")
		SetLaserFirstFreq = False
	End Function
	
	
	
	Public Function GetLaserLastFreq(ByRef lfh As Double) As Boolean
		' 54/55
		' Frequency must be in GHz.
		Dim thz As Short
		Dim ghz As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		thz = Comms.ReadOIF(LASERLASTFREQ1, 0, 1)
		If thz = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF LFH1 (THz).")
			GetLaserLastFreq = False
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ghz = Comms.ReadOIF(LASERLASTFREQ2, 0, 10)
			If ghz = GENERAL_ERROR_CODE Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed to get OIF LFH2 (GHz).")
				GetLaserLastFreq = False
			Else
				lfh = thz * 1000# + ghz
				GetLaserLastFreq = True
			End If
		End If
	End Function
	
	
	
	Public Function SetLaserLastFreq(ByRef freq As Double) As Boolean
		' 54/55
		' Frequency must be in GHz.
		Dim thz As Short
		
		thz = FMod(freq, 1000)
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(LASERLASTFREQ2, CDbl(thz), 10) Then GoTo Aborted
		thz = Int(freq / 1000)
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(LASERLASTFREQ1, CDbl(thz), 1) Then GoTo Aborted
		SetLaserLastFreq = True
		Exit Function
		
Aborted: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("Error: Failed to set OIF Laser Last Frequency.")
		SetLaserLastFreq = False
	End Function
	
	
	
	
	Public Function SetMinFrequencyGrid(ByRef ghzgrid As Double) As Boolean
		' 56
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetMinFrequencyGrid = Comms.WriteOIF(LASERMINGRID, ghzgrid, 10)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetMinFrequencyGrid Then Logger.logFATAL("Error: Failed to set OIF Min Frequency Grid.")
	End Function
	
	
	
	
	Public Function GetMinFrequencyGrid(ByRef ghzgrid As Short) As Boolean
		' 56
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ghzgrid = Comms.ReadOIF(LASERMINGRID, ghzgrid, 10)
		If ghzgrid = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF Min Frequency Grid.")
			GetMinFrequencyGrid = False
		Else
			GetMinFrequencyGrid = True
		End If
	End Function
	
	
	
	
	Public Function GetDitherEnable(ByRef de As Integer) As Boolean
		' 59
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		de = CInt(Comms.ReadOIF(DITHERE, 0, 1))
		If de = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF DitherE.")
			GetDitherEnable = False
		Else
			GetDitherEnable = True
		End If
	End Function
	
	
	
	Public Function SetDitherEnable(ByRef de As Byte) As Boolean
		' 59
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetDitherEnable = Comms.WriteByteOIF(DITHERE, 0, de)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetDitherEnable Then Logger.logFATAL("Error: Failed to set OIF DitherE.")
	End Function
	
	
	
	
	Public Function GetDitherR(ByRef dith As Short) As Boolean
		' 5A
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dith = CShort(Comms.ReadOIF(DITHERR, 0, 1))
		If dith = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF DitherR.")
			GetDitherR = False
		Else
			GetDitherR = True
		End If
	End Function
	
	
	
	Public Function SetDitherR(ByRef dith As Short) As Boolean
		' 5A
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetDitherR = Comms.WriteOIF(DITHERR, CDbl(dith), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetDitherR Then Logger.logFATAL("Error: Failed to set OIF DitherR.")
	End Function
	
	
	
	
	Public Function GetDitherF(ByRef dith As Double) As Boolean
		' 5B
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dith = Comms.ReadOIF(DITHERF, 0, 10)
		If dith = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF DitherF.")
			GetDitherF = False
		Else
			GetDitherF = True
		End If
	End Function
	
	
	
	Public Function SetDitherF(ByRef dith As Double) As Boolean
		' 5B
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetDitherF = Comms.WriteOIF(DITHERF, dith, 10)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetDitherF Then Logger.logFATAL("Error: Failed to set OIF DitherF.")
	End Function
	
	
	
	
	Public Function GetDitherA(ByRef dith As Double) As Boolean
		' 5C
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dith = Comms.ReadOIF(DITHERA, 0, 10)
		If dith = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF DitherA.")
			GetDitherA = False
		Else
			GetDitherA = True
		End If
	End Function
	
	
	
	Public Function SetDitherA(ByRef dith As Double) As Boolean
		' 5C
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetDitherA = Comms.WriteOIF(DITHERA, dith, 10)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetDitherA Then Logger.logFATAL("Error: Failed to set OIF DitherA.")
	End Function
	
	


    Public Function GetTCaseL(ByRef tcl As Double) As Boolean
        ' 5D
        tcl = Comms.ReadOIF(TCASEL, 0, 1)
        If tcl = GENERAL_ERROR_CODE Then
            Logger.logFATAL("Error: Failed to get OIF TCaseL.")
            GetTCaseL = False
        Else
            ' two's complement fix
            If tcl > 32768 Then
                tcl = tcl - 65536
            End If
            ' convert into degrees
            tcl = tcl / 100
            GetTCaseL = True
        End If
    End Function



    Public Function SetTCaseL(ByVal tcl As Double) As Boolean
        ' 5D
        SetTCaseL = Comms.WriteOIF(TCASEL, tcl, 100)
        If Not SetTCaseL Then Logger.logFATAL("Error: Failed to set OIF TCaseL.")
    End Function


    Public Function GetTCaseH(ByRef tch As Double) As Boolean
        ' 5E
        tch = Comms.ReadOIF(TCASEH, 0, 1)
        If tch = GENERAL_ERROR_CODE Then
            Logger.logFATAL("Error: Failed to get OIF TCaseH.")
            GetTCaseH = False
        Else
            ' two's complement fix
            If tch > 32768 Then
                tch = tch - 65536
            End If
            ' convert into degrees
            tch = tch / 100
            GetTCaseH = True
        End If
    End Function



    Public Function SetTCaseH(ByVal tch As Double) As Boolean
        ' 5E
        SetTCaseH = Comms.WriteOIF(TCASEH, tch, 100)
        If Not SetTCaseH Then Logger.logFATAL("Error: Failed to set OIF TCaseH.")
    End Function
	
    Public Function xGetTCaseL(ByRef tcl As Short) As Boolean
        ' 5D
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        tcl = CShort(Comms.ReadOIF(TCASEL, 0, 1))
        If tcl = GENERAL_ERROR_CODE Then
            'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Logger.logFATAL("Error: Failed to get OIF TCaseL.")
            xGetTCaseL = False
        Else
            xGetTCaseL = True
        End If
    End Function
	
	
	
    Public Function xSetTCaseL(ByRef tcl As Short) As Boolean
        ' 5D
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        xSetTCaseL = Comms.WriteOIF(TCASEL, CDbl(tcl), 1)
        'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If Not xSetTCaseL Then Logger.logFATAL("Error: Failed to set OIF TCaseL.")
    End Function
	
	
	
	
    Public Function xGetTCaseH(ByRef tch As Short) As Boolean
        ' 5E
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        tch = CShort(Comms.ReadOIF(TCASEH, 0, 1))
        If tch = GENERAL_ERROR_CODE Then
            'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Logger.logFATAL("Error: Failed to get OIF TCaseH.")
            xGetTCaseH = False
        Else
            xGetTCaseH = True
        End If
    End Function
	
	
	
    Public Function xSetTCaseH(ByRef tch As Short) As Boolean
        ' 5E
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        xSetTCaseH = Comms.WriteOIF(TCASEH, CDbl(tch), 1)
        'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If Not xSetTCaseH Then Logger.logFATAL("Error: Failed to set OIF TCaseH.")
    End Function
	
	
	
	
	Public Function GetFAgeTh(ByRef age As Short) As Boolean
		' 5F
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		age = CShort(Comms.ReadOIF(FAGETH, 0, 1))
		If age = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF FAgeTh.")
			GetFAgeTh = False
		Else
			GetFAgeTh = True
		End If
	End Function
	
	
	
	Public Function SetFAgeTh(ByRef age As Short) As Boolean
		' 5F
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetFAgeTh = Comms.WriteOIF(FAGETH, CDbl(age), 1)
		If Not SetFAgeTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF FAgeTh.")
		End If
	End Function
	
	
	
	
	Public Function GetWAgeTh(ByRef age As Short) As Boolean
		' 60
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		age = CShort(Comms.ReadOIF(WAGETH, 0, 1))
		If age = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get OIF WAgeTh.")
			GetWAgeTh = False
		Else
			GetWAgeTh = True
		End If
	End Function
	
	
	
	Public Function SetWAgeTh(ByRef age As Short) As Boolean
		' 60
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetWAgeTh = Comms.WriteOIF(WAGETH, CDbl(age), 1)
		If Not SetWAgeTh Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set OIF WAgeTh.")
		End If
	End Function
End Class
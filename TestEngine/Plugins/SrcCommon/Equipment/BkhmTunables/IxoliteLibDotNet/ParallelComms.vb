Option Strict Off
Option Explicit On
Friend Class ParallelComms
	
	
	' This is a low level driver for controlling an I2C slave via a simple interface
	' on a PC parallel port.  Error checking is performed on all operations
	' M.Rigby-Jones 8th November 2000
	' Updated by Joe Collins 2004.
	
	' This class module should contain all the interface specific commands required
	' to access the module via the MSA. The references to a "text interface" relate
	' to the Santur laser module which is no longer in use (Dec '04). The parallel
	' interface on the PC is connected to an I2C interface on the module, and all
	' the hardware control functions in the PC are performed by software. Two pins
	' on the parallel port are used to simulate SCL and SDA on the I2C interface,
	' and by setting or clearing these pins individual bits are clocked in and out
	' of the PC.
	
	' Declare API imports
	'Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
	Private Declare Function DlPortReadPortUchar Lib "dlportio.dll" (ByVal port As Integer) As Byte
	Private Declare Sub DlPortWritePortUchar Lib "dlportio.dll" (ByVal port As Integer, ByVal value As Byte)
	
	
	Public TimeVal As Integer
	
	Private Enum ErrCodes
		busOK = 0
		busSDAStuckLow = 1
		busSDAStuckHigh = 2
		busSCLStuckLow = 4
		busSCLStuckHigh = 8
		busAckTimeout = 16 ' Swopped with busPortDrvFail in v3.0.9
		busPortDrvFail = 32
		busBusy = 64
	End Enum
	
	' The PositionCodes enum is used to track which level in the comms process we are at.
	' A bit is set in "CurrentPosition" whenever we enter an appropriate procedure and is only
	' cleared if we exit error-free. In this way the bits should remain set until we hit the
	' top and exit back to the caller.
	
	Private Enum PositionCodes
		posLOST = 0
		posWaitAck = &H20s ' LS 5 bits reserved for ErrCodes 1 - 16.
		posNAck = &H40s
		posAck = &H80s
		posStop = &H100s
		posStart = &H200s
		posAddrRead = &H400s
		posAddrWrite = &H800s
		posReceive = &H1000s
		posTransmit = &H2000s
		posRcvReply = &H4000s
		posSendCommand = &H8000s
		posInit = &HF000s
	End Enum
	
	' Standard MSA error codes
	Private Const MSA_OK As Short = &H0s ' command executed
	Private Const MSA_UNKNOWN_COMMAND As Short = &H1s ' unknown command
	Private Const MSA_FRAME_ERROR As Short = &H2s ' invalid length
	Private Const MSA_OUT_OF_RANGE As Short = &H3s ' parameter out of range
	Private Const MSA_TIME_OUT As Short = &H4s ' command timing out of range?
	Private Const MSA_CHECK_ERROR As Short = &H5s ' checksum error
	Private Const MSA_MODULE_BUSY As Short = &H7s ' module is processing critical code
	Private Const MSA_STILL_PROCESSING As Short = &H8s ' module is processing previous command
	Private Const MSA_COMMAND_NOT_EXECUTED As Short = &H9s ' unable to execute command
	Private Const MSA_COMMAND_FAILED As Short = &HAs ' failed to execute command
	Private Const MSA_INVALID As Short = &H7Fs ' Invalid response
	
	
	Private Const SLAVE_BASE_ADDRESS As Byte = &H40s
	Private Const SLAVE_ADDRESS_RANGE As Byte = 7
	Private Const CPN_MASK As Byte = &H80s
	Private Const STATUS_MASK As Byte = &H7Fs
	Private Const SDARead As Short = &H80s ' status port bit mask for SDA readback ()
	Private Const SCLRead As Short = &H20s ' status port bit mask for SCL readback ()
	Private Const MAXINT As Short = 32767
    Private Const BIT_DELAY As Integer = 2 ' 2 microseconds delay for bit period
	Private Const MIN_ERR As Short = 0
	Private Const MAX_ERR As Short = 64 ' Changed from 32 (JC 28-04-04)
	Private Const MAX_POS As Short = 16 ' Number of bits used in "CurrentPosition"
	Private Const DATA_OUT As Byte = &HF0s
	Private Const DATA_IN As Byte = &HF1s
	Private Const LO As Boolean = False ' just to make things more readable
	Private Const HI As Boolean = True
	Private Const READ_BIT As Byte = 1
	Private Const WRITE_BIT As Byte = 0
	
	Private Const INI_INTERFACE As String = "i2c_interface"
	
	Private lngDataPort As Integer ' Physical address of LPT data port.
	Private lngStatusPort As Integer ' Physical address of LPT status port. (Data + 1)
	Private bytSavedStatus As Byte ' Saves data on LPT port before I2C session starts.
	Private bytPortData As Byte
	Private intAckTimeout As Short ' Max time (ms) to wait for an ack.
	Private intClkTimeout As Short ' Max time (ms) to wait on a clock stretch.
	Private intError As Short ' Variable to hold error codes.
	Private bytBitVal(3) As Byte ' To store bit values when not on-line.
	Private blnBusy As Boolean
	Private ClockPeriod As Integer
	Private DeviceAddr As Byte ' Slave address of current I2C device (shifted left).
	Private DeviceIndex As Byte ' Slave address as a value 0 to 7 (0x40 to 0x47).
	Private ExpectedCPN(7) As Byte ' Indexed by DeviceIndex.
	Private PreviousComms(7) As Byte ' Indexed by DeviceIndex.
	Private IgnoreCPNErrors(7) As Boolean ' Indexed by DeviceIndex.
	Private MultipleWrites(7) As Boolean ' Indexed by DeviceIndex.
	
	Private bytSclHiMask As Byte
	Private bytSclLoMask As Byte
	Private bytSdaHiMask As Byte
	Private bytSdaLoMask As Byte
	Private bytSclInMask As Byte
	Private bytSdaInMask As Byte
	
	Private SCLOutbit As Byte
	Private SDAOutBit As Byte
	Private SCLOutInvert As Boolean
	Private SDAOutInvert As Boolean
	Private SCLInBit As Byte
	Private SDAInBit As Byte
	Private SCLInInvert As Boolean
	Private SDAInInvert As Boolean
	
	Private blnGenCmdErrors As Boolean
	Private blnGenLLenErrors As Boolean ' Long length
	Private blnGenSLenErrors As Boolean ' Short length
	Private blnGenChkErrors As Boolean
	
	Private CurrentPosition As PositionCodes
	
	
	
	
	
	
	
	
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		Dim indx As Short
		CurrentPosition = 0
		lngDataPort = 0
		lngStatusPort = 0
		DeviceAddr = 0
		DeviceIndex = 0
		For indx = 0 To 7
			IgnoreCPNErrors(indx) = False
			MultipleWrites(indx) = False
			ExpectedCPN(indx) = 255 ' This initial value is checked later.
		Next indx
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	
	
	
	Public Function ApplyInterfaceSettings() As Boolean
		' Initialize data then read specific values from the INI file.
		' Note that the "+3" on the array sizing allows for the command/status, the length
		' and the checksum as well as the parameter bytes. The index (count) is at 0.
		' The address is shifted up one place because the R/W bit is bit 1.
		Dim sIniData As String
		Dim fromdev As Byte
		Dim todev As Byte
		
		On Error GoTo ErrorHandler
		
		' Setup the defaults.
		intAckTimeout = 200 ' 200 ms timeout
		intClkTimeout = 5000
		SCLOutbit = 1
		SDAOutBit = 0
		SDAOutInvert = False
		SCLOutInvert = False
		SDAInBit = 7
		SCLInBit = 5
		SDAInInvert = False
		SCLInInvert = True
		ClockPeriod = 50
		intError = 0 ' Clear the error status word.
		InitTimer()
		
		fromdev = SLAVE_BASE_ADDRESS * 2
		todev = (SLAVE_BASE_ADDRESS + SLAVE_ADDRESS_RANGE) * 2
		
		sIniData = GetIniData(INI_INTERFACE, "ack_timeout")
		If Len(sIniData) Then intAckTimeout = Val(sIniData)
		
		sIniData = GetIniData(INI_INTERFACE, "clock_timeout")
		If Len(sIniData) Then intClkTimeout = Val(sIniData)
		
		' DataOut bit.
		sIniData = GetIniData(INI_INTERFACE, "data_out_bit")
		If Len(sIniData) Then
			SDAOutBit = Val(sIniData)
			If (SDAOutBit < 0) Or (SDAOutBit > 7) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("SDAOutputBit invalid (must be 0 to 7)")
				GoTo Aborted
			End If
		End If
		
		sIniData = GetIniData(INI_INTERFACE, "data_out_invert")
		If Len(sIniData) Then SDAOutInvert = CBool(sIniData)
		
		' ClockOut bit.
		sIniData = GetIniData(INI_INTERFACE, "clock_out_bit")
		If Len(sIniData) Then
			SCLOutbit = Val(sIniData)
			If (SCLOutbit < 0) Or (SCLOutbit > 7) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("SCLOutputBit invalid (must be 0 to 7)")
				GoTo Aborted
			End If
		End If
		
		sIniData = GetIniData(INI_INTERFACE, "clock_out_invert")
		If Len(sIniData) Then SCLOutInvert = CBool(sIniData)
		
		' DataIn bit.
		sIniData = GetIniData(INI_INTERFACE, "data_in_bit")
		If Len(sIniData) Then
			SDAInBit = Val(sIniData)
			If (SDAInBit < 0) Or (SDAInBit > 7) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("SDAInputBit invalid (must be 0 to 7)")
				GoTo Aborted
			End If
		End If
		
		sIniData = GetIniData(INI_INTERFACE, "data_in_invert")
		If Len(sIniData) Then SDAInInvert = CBool(sIniData)
		
		' ClockIn bit.
		sIniData = GetIniData(INI_INTERFACE, "clock_in_bit")
		If Len(sIniData) Then
			SCLInBit = Val(sIniData)
			If (SCLInBit < 0) Or (SCLInBit > 7) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("SCLInputBit invalid (must be 0 to 7)")
				GoTo Aborted
			End If
		End If
		
		sIniData = GetIniData(INI_INTERFACE, "clock_in_invert")
		If Len(sIniData) Then SCLInInvert = CBool(sIniData)
		
		sIniData = GetIniData(INI_INTERFACE, "clock_pulse_period")
		If Len(sIniData) Then
			ClockPeriod = CInt(sIniData)
		Else
			ClockPeriod = BIT_DELAY
		End If
		
		Call CalcSDAMasks()
		Call CalcSCLMasks()
		Call CalcSCLInMasks()
		Call CalcSDAInMasks()
		ApplyInterfaceSettings = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ApplyInterfaceSettings")
Aborted: 
		ApplyInterfaceSettings = False
	End Function
	
	
	
	
	
	
	Public Property DeviceAddress() As Byte
		Get
			DeviceAddress = DeviceAddr
		End Get
		Set(ByVal Value As Byte)
			' The address must be given as an 8bit value with the LSB clear (for the R/W bit).
			' For example, an address of 0x41 will come in as 0x82. This is the address of the currently
			' accessed module.
			' The DeviceIndex allows us to hold data such as the address and CPN value for each of the
			' possible 8 modules on the bus by using arrays.
			DeviceAddr = Value
			DeviceIndex = DeviceAddr / 2
			DeviceIndex = DeviceIndex And &H7s
		End Set
    End Property

    Public Property MaxI2CRate_kHz() As Integer
        Get
            Dim speed_kHz As Integer
            speed_kHz = 1000 / ClockPeriod
            MaxI2CRate_kHz = speed_kHz
        End Get
        Set(ByVal Value As Integer)
            Dim clock_us As Integer
            clock_us = 1000 / Value
            ClockPeriod = clock_us
        End Set
    End Property
	
	
	Public ReadOnly Property DeviceAddressTx() As Byte
		Get
			DeviceAddressTx = DeviceAddr + WRITE_BIT
		End Get
	End Property
	
	
	Public ReadOnly Property DeviceAddressRx() As Byte
		Get
			DeviceAddressRx = DeviceAddr + READ_BIT
		End Get
	End Property
	
	
	
	Public ReadOnly Property mode() As GlobalData.CommsModeType
		Get
			mode = GlobalData.CommsModeType.PARALLELMODE
		End Get
	End Property
	
	
	
	
	
	
    Public Property PortAddress() As Long
        Get
            PortAddress = lngDataPort ' Read printer port base address
        End Get
        Set(ByVal Value As Long)
            ' Set the base address of the printer port to which the I2C interface is connected.
            ' Status port = data port + 1
            lngDataPort = Value
            lngStatusPort = lngDataPort + 1
        End Set
    End Property
	
	
	
	Public ReadOnly Property Busy() As Boolean
		Get
			Busy = blnBusy
		End Get
	End Property
	
	
	
	Public ReadOnly Property ErrorCode() As Byte
		Get
			ErrorCode = intError
		End Get
	End Property
	
	
	
	Public WriteOnly Property GenerateCommandErrors() As Boolean
		Set(ByVal Value As Boolean)
			blnGenCmdErrors = Value
		End Set
	End Property
	
	
	
	
	Public WriteOnly Property GenerateShortLengthErrors() As Boolean
		Set(ByVal Value As Boolean)
			blnGenSLenErrors = Value
		End Set
	End Property
	
	
	
	
	Public WriteOnly Property GenerateLongLengthErrors() As Boolean
		Set(ByVal Value As Boolean)
			blnGenLLenErrors = Value
		End Set
	End Property
	
	
	
	
	Public WriteOnly Property GenerateChecksumErrors() As Boolean
		Set(ByVal Value As Boolean)
			blnGenChkErrors = Value
		End Set
	End Property
	
	
	
	
	
	Public Function StatusOnly(ByRef fullstatusbyte As Byte) As Byte
		StatusOnly = fullstatusbyte And STATUS_MASK
	End Function
	
	
	
	Private Sub CalcSDAMasks()
		' Calculate the bit masks for setting/clearing the SDA pin.
		If SDAOutInvert Then
			bytSdaLoMask = (2 ^ SDAOutBit)
			bytSdaHiMask = &HFFs - (2 ^ SDAOutBit)
		Else
			bytSdaLoMask = &HFFs - (2 ^ SDAOutBit)
			bytSdaHiMask = (2 ^ SDAOutBit)
		End If
	End Sub
	
	
	
	
	Private Sub CalcSCLMasks()
		' Calculate the bit masks for setting/clearing the SCL pin.
		If SCLOutInvert Then
			bytSclLoMask = (2 ^ SCLOutbit)
			bytSclHiMask = &HFFs - (2 ^ SCLOutbit)
		Else
			bytSclLoMask = &HFFs - (2 ^ SCLOutbit)
			bytSclHiMask = (2 ^ SCLOutbit)
		End If
	End Sub
	
	
	
	
	Private Sub CalcSDAInMasks()
		' Calculate bit mask to read the SDA pin.
		bytSdaInMask = (2 ^ SDAInBit)
	End Sub
	
	
	
	
	Private Sub CalcSCLInMasks()
		' Calculate bit mask to read the SCL pin.
		bytSclInMask = (2 ^ SCLInBit)
	End Sub
	
	
	
	
	Private Function GetSDA() As Boolean
		Dim blnState As Boolean
		blnState = CBool(DlPortReadPortUchar(lngStatusPort) And bytSdaInMask)
		GetSDA = IIf(SDAInInvert, Not blnState, blnState)
	End Function
	
	
	
	
	Private Function GetSCL() As Boolean
		Dim blnState As Boolean
		blnState = CBool(DlPortReadPortUchar(lngStatusPort) And bytSclInMask)
		GetSCL = IIf(SCLInInvert, Not blnState, blnState)
	End Function
	
	Public Function GetParPortStatus() As Byte
		' Gets the raw parallel port status register.
		GetParPortStatus = DlPortReadPortUchar(lngStatusPort)
	End Function
	
	
	
	Private Sub ReleaseSDA()
		' Release the SDA line.  No read back or timeout. Note: released is HI.
		' MRJ 22/05/2000
		If SDAOutInvert Then
			bytPortData = (bytPortData And bytSdaHiMask) ' Mask SDA bit low
		Else
			bytPortData = (bytPortData Or bytSdaHiMask) ' Mask SDA bit high
		End If
		DlPortWritePortUchar(lngDataPort, bytPortData) ' Set SCL
	End Sub
	
	
	
	
	
	Private Function SetSDA(ByRef state As Boolean) As Boolean
		' Set the state of the SDA line.  The line will be polled until either it reaches
		' it's destination state, or a timeout condition occurs.  Returns true if state set
		' before timeout, otherwise returns false.
		' MRJ 22/05/2000
		Dim intCounter As Short
		
		ClearErrorBit((ErrCodes.busSDAStuckHigh + ErrCodes.busSDAStuckLow))
		If SDAOutInvert Then
			If state = LO Then
				bytPortData = (bytPortData Or bytSdaLoMask) ' Mask SDA bit high
			Else
				bytPortData = (bytPortData And bytSdaHiMask) ' Mask SDA bit low
			End If
		Else
			If state = LO Then
				bytPortData = (bytPortData And bytSdaLoMask) ' Mask SDA bit low
			Else
				bytPortData = (bytPortData Or bytSdaHiMask) ' Mask SDA bit high
			End If
		End If
		
		DlPortWritePortUchar(lngDataPort, bytPortData) ' Set SDA
		intCounter = intClkTimeout
		SetSDA = False
		Do 
			If GetSDA() = state Then
				SetSDA = True
				Exit Do
			End If
			intCounter = intCounter - 1
		Loop While intCounter
		
		If SetSDA = False Then
			If state = HI Then
				SetErrorBit((ErrCodes.busSDAStuckLow))
			Else
				SetErrorBit((ErrCodes.busSDAStuckHigh))
			End If
		End If
	End Function
	
	
	
	
	
	Private Function SetSCL(ByRef state As Boolean) As Boolean
		' Set the state of the SCL line.  The line will be polled until either it reaches
		' its destination state, or a timeout condition occurs.  Returns true if state set
		' before timeout, otherwise returns false.
		' MRJ 22/05/2000
		Dim intCounter As Short
		
		ClearErrorBit((ErrCodes.busSCLStuckHigh + ErrCodes.busSCLStuckLow))
		If SCLOutInvert Then
			If state = LO Then
				bytPortData = (bytPortData Or bytSclLoMask) ' Mask SDA bit high
			Else
				bytPortData = (bytPortData And bytSclHiMask) ' Mask SDA bit low
			End If
		Else
			If state = LO Then
				bytPortData = (bytPortData And bytSclLoMask) ' Mask SDA bit low
			Else
				bytPortData = (bytPortData Or bytSclHiMask) ' Mask SDA bit high
			End If
		End If
		DlPortWritePortUchar(lngDataPort, bytPortData) ' Set SCL
		
		intCounter = intClkTimeout ' Read back the state of the line
		SetSCL = False ' until it has reached the set state
		Do  ' or a timeout has occurred.
			If GetSCL() = state Then
				SetSCL = True
				Exit Do
			End If
			intCounter = intCounter - 1
		Loop While intCounter
		
		If SetSCL = False Then
			If state = HI Then
				SetErrorBit((ErrCodes.busSCLStuckLow))
			Else
				SetErrorBit((ErrCodes.busSCLStuckHigh))
			End If
		End If
	End Function
	
	
	
	
	
	Private Sub SetSCL_NoErr(ByRef state As Boolean)
		' Set the state of the SCL line without setting or clearing any error bits.
		If SCLOutInvert Then
			If state = LO Then
				bytPortData = (bytPortData Or bytSclLoMask) ' Mask SDA bit high
			Else
				bytPortData = (bytPortData And bytSclHiMask) ' Mask SDA bit low
			End If
		Else
			If state = LO Then
				bytPortData = (bytPortData And bytSclLoMask) ' Mask SDA bit low
			Else
				bytPortData = (bytPortData Or bytSclHiMask) ' Mask SDA bit high
			End If
		End If
		DlPortWritePortUchar(lngDataPort, bytPortData) ' Set SCL
	End Sub
	
	
	
	
	
	Private Sub SetSDA_NoErr(ByRef state As Boolean)
		' Set the state of the SDA line without setting or clearing any error bits.
		If SDAOutInvert Then
			If state = LO Then
				bytPortData = (bytPortData Or bytSdaLoMask) ' Mask SDA bit high
			Else
				bytPortData = (bytPortData And bytSdaHiMask) ' Mask SDA bit low
			End If
		Else
			If state = LO Then
				bytPortData = (bytPortData And bytSdaLoMask) ' Mask SDA bit low
			Else
				bytPortData = (bytPortData Or bytSdaHiMask) ' Mask SDA bit high
			End If
		End If
		DlPortWritePortUchar(lngDataPort, bytPortData) ' Set SDA
	End Sub
	
	
	Public Sub SetParPortData(ByVal dataRegMask As Byte, ByVal dataRegBits As Byte)
		' Set the masked bits of the parallel port data register to equivalent bits of dataRegBits.
		bytPortData = (bytPortData And (Not dataRegMask))
		bytPortData = (bytPortData Or (dataRegBits And dataRegMask))
		DlPortWritePortUchar(lngDataPort, bytPortData)
	End Sub
	
	
	Private Function I2Cgenstart() As Boolean
		' Transmits start condition - high to low transition on SDA while SCL is high.
		' Reset SCL low and release SDA afterwards. Ensure that SDA goes low midway
		' through the clock period.
		I2Cgenstart = False
		SetPosition((PositionCodes.posStart))
		If (GetSDA = LO) Then
			If (Not SetSDA(HI)) Then Exit Function ' Error - SDA stuck low.
			' usDelay ClockPeriod / 2
		End If
		If (GetSCL = LO) Then
			If (Not SetSCL(HI)) Then Exit Function ' Error - SCL stuck low.
		End If
		usDelay(ClockPeriod / 4)
		If (Not SetSDA(LO)) Then Exit Function ' Error - SDA stuck high.
		usDelay(ClockPeriod / 4)
		If (Not SetSCL(LO)) Then Exit Function ' Error - SCL stuck high.
		ReleaseSDA()
		ClearPosition((PositionCodes.posStart))
		I2Cgenstart = True
	End Function
	
	
	
	
	
	Private Function I2Cgenstop() As Boolean
		' Transmits stop condition - low to high transition on SDA while SCL is high.
		' Set SCL low first so that SDA can be changed without affecting anything.
		' Release both SCL and SDA afterwards. Ensure that SDA goes high midway
		' through the clock period.
		I2Cgenstop = False
		SetPosition((PositionCodes.posStop))
		If (GetSDA = HI) Then
			If (Not SetSDA(LO)) Then Exit Function ' Error - SDA stuck high.
			' usDelay ClockPeriod / 2
		End If
		If (Not SetSCL(HI)) Then Exit Function ' Error - SCL stuck low.
		usDelay(ClockPeriod / 4)
		If (Not SetSDA(HI)) Then Exit Function ' Error - SDA stuck low.
		usDelay(ClockPeriod / 4)
		' If (Not SetSCL(LO)) Then Exit Function  ' Error - SCL stuck high.
		ReleaseSDA()
		ClearPosition((PositionCodes.posStop))
		I2Cgenstop = True
	End Function
	
	
	
	
	Private Function I2CStop_NoErr() As Boolean
		' Transmits a stop on the bus with no error checking.
		' Lines could be in an undefined state so have to set both lines high
		' without performing a start.
		If GetSCL() = HI Then SetSCL_NoErr(LO)
		If GetSDA() = HI Then SetSDA_NoErr(LO)
		SetSCL_NoErr(HI)
		usDelay(ClockPeriod / 2)
		SetSDA_NoErr(HI)
		usDelay(ClockPeriod / 2)
		SetSCL_NoErr(LO)
		ReleaseSDA()
	End Function
	
	
	
	
	
	Private Function I2Cgiveack() As Boolean
		' Gives an ACK to a slave. Pull SDA line low and give a clock pulse.
		' Note assumption that clock starts low (normal).
		I2Cgiveack = False
		SetPosition((PositionCodes.posAck))
		If (GetSDA = HI) Then
			If (Not SetSDA(LO)) Then Exit Function
			' usDelay ClockPeriod / 2
		End If
		If (Not SetSCL(HI)) Then Exit Function
		usDelay(ClockPeriod / 2)
		If (Not SetSCL(LO)) Then Exit Function ' Error - SCL stuck high.
		ReleaseSDA()
		ClearPosition((PositionCodes.posAck))
		I2Cgiveack = True
	End Function
	
	
	
	
	
	Private Function I2Cgivenack() As Boolean
		' Gives a NACK to a slave. Set SDA line high and give a clock pulse.
		' Note assumption that clock starts low (normal).
		I2Cgivenack = False
		SetPosition((PositionCodes.posNAck))
		If (GetSDA = LO) Then
			If (Not SetSDA(HI)) Then Exit Function
			' usDelay ClockPeriod / 2
		End If
		If (Not SetSCL(HI)) Then Exit Function
		usDelay(ClockPeriod / 2)
		If (Not SetSCL(LO)) Then Exit Function ' Error - SCL stuck high.
		ReleaseSDA()
		ClearPosition((PositionCodes.posNAck))
		I2Cgivenack = True
	End Function
	
	
	
	
	
	Public Function Init() As Boolean
		' This procedure only checks that the bits can be twiddled on a parallel port.
		' This does not mean that there is anything at the other end.
		' Sends a stop sequence (acts as an abort). If this fails then we assume that
		' a slave is holding the SDA low and we send up to 16 clock pulses while checking
		' for SDA to go high again, i.e. be released by the slave. SCL is checked for a
		' stuck line and will cause a failure.
		Dim intCount As Short
		Const MAX_TRIES As Short = 16
		
		Init = False
		ClearAllErrors()
		CurrentPosition = 0
		SetPosition((PositionCodes.posInit))
		If (I2Cgenstop() = True) Then
			Init = True
		Else
			ReleaseSDA()
			intCount = 0
			Do 
				If Not SetSCL(HI) Then Exit Function
				usDelay(ClockPeriod)
				If Not SetSCL(LO) Then Exit Function
				usDelay(ClockPeriod / 2)
				intCount = intCount + 1
			Loop While ((GetSDA() = LO) And intCount < MAX_TRIES)
			
			If intCount < MAX_TRIES Then Init = True Else Exit Function
		End If
		ClearPosition((PositionCodes.posInit))
	End Function
	
	
	
	
	Public Function I2Copen() As Boolean
		' Save current port setting before opening.
        'Dim intIndex As Short
		ClearAllErrors()
		bytSavedStatus = DlPortReadPortUchar(lngDataPort)
		bytPortData = bytSavedStatus
		I2Copen = Init()
	End Function
	
	
	
	
	Private Sub CheckCPN(ByRef RxData As Byte)
		' The CPN bit is used as a simple message sequencing check, and is specific to the
		' comms address of the device. At the first communication with a device we don't
		' know what the CPN bit will be set to so we just take whatever comes.
		Dim CurrCPN As Byte
		Dim CurrStatus As Byte
        'Dim msg As String
        'Dim reply As Byte
		
		If IgnoreCPNErrors(DeviceIndex) Then
			' Ignore CPN bit!
		Else
			CurrCPN = RxData And CPN_MASK
			CurrStatus = RxData And STATUS_MASK
			
			If ExpectedCPN(DeviceIndex) = 255 Then
				' First attempt to communicate with this device - just set the expected CPN
				' for the next command to the current value. The expected CPN will be toggled
				' before being checked at the next command.
				ExpectedCPN(DeviceIndex) = CurrCPN
			Else
				' The CPN should only toggle if the returned status is OK. However, if
				' there have been multiple reads then we have no idea what the CPN should
				' be so we take the current returned value.
				If CurrStatus = MSA_OK Then
					If Not MultipleWrites(DeviceIndex) Then
						' Toggle the ExpectedCPN.
						ExpectedCPN(DeviceIndex) = ExpectedCPN(DeviceIndex) Xor CPN_MASK
					Else
						ExpectedCPN(DeviceIndex) = CurrCPN
					End If
				Else
					' Leave it.
				End If
				
				If CurrCPN <> ExpectedCPN(DeviceIndex) Then
					' Generate error and take appropriate action based on the response.
					' Either ignore the error, toggle the CPN, or turn off checking.
					' ####### v3.2.0 #### Removed the CPN reset options.
					'msg = "CPN Failure. Reset expected CPN (yes/no) or ignore CPN errors (cancel)?"
					'reply = MsgBox(msg, vbYesNoCancel)
					'Select Case reply
					'    Case vbYes
					' Toggle it.
					'        ExpectedCPN(DeviceIndex) = ExpectedCPN(DeviceIndex) Xor CPN_MASK
					'    Case vbNo
					' Don't toggle now then it will be OK next time.
					'    Case vbCancel
					' Turn off error reporting.
					'        IgnoreCPNErrors(DeviceIndex) = True
					'    Case Else
					' ????????????
					'End Select
					'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					Logger.logFATAL("Error: CPN Failure. Resetting.")
					ExpectedCPN(DeviceIndex) = ExpectedCPN(DeviceIndex) Xor CPN_MASK
				End If
			End If
		End If
	End Sub
	
	
	
	
	Public Sub ResetCPN()
		' When a ResetCPN command is sent the module returns status with CPN=0.
		' The ExpectedCPN will be toggled before being checked against the received CPN
		' in Function SendCommand, so we need to set the ExpectedCPN to 1 here.
		ExpectedCPN(DeviceIndex) = CPN_MASK
	End Sub
	
	
	
	Private Function I2Creceive(ByRef bytResult As Byte) As Boolean
		' Receives a byte from a slave module. The bits are clocked in on an SCL high.
		' Changed in v2.0.6 to incorporate an error return as per the transmit function.
		Dim bytMask As Object
		I2Creceive = False
		SetPosition((PositionCodes.posReceive))
		bytResult = 0
		SetErrorBit((ErrCodes.busOK))
		ReleaseSDA()
		'UPGRADE_WARNING: Couldn't resolve default property of object bytMask. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		bytMask = 128 ' start with MSB
		Do 
			If (Not SetSCL(HI)) Then Exit Function ' Error - clock stuck low.
			usDelay(ClockPeriod / 4)
			'UPGRADE_WARNING: Couldn't resolve default property of object bytMask. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If GetSDA() Then bytResult = bytResult + bytMask
			usDelay(ClockPeriod / 4)
			If (Not SetSCL(LO)) Then Exit Function ' Error - clock stuck high.
			usDelay(ClockPeriod / 2) ' DO WE NEED THIS OR ARE WE SLOW ENOUGH?
			'UPGRADE_WARNING: Couldn't resolve default property of object bytMask. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If bytMask = 1 Then ' Just clocked in the LSB so finish and quit.
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logLastMsgRcvd. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logLastMsgRcvd(bytResult)
				I2Creceive = True
				Exit Do
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object bytMask. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				bytMask = bytMask / 2 ' Move to next least significant bit.
			End If
		Loop 
		ClearPosition((PositionCodes.posReceive))
	End Function
	
	
	
	
	Private Function I2CaddressWrite() As Boolean
		' This function sends the device address with the Write Bit (bit1=0).
		' The device address is saved in I2Ctransmit but we want the array to restart with
		' the command byte after every address write. If the address write fails then the
		' last message is not cleared.
		I2CaddressWrite = False
		SetPosition((PositionCodes.posAddrWrite))
		If Not I2Cgenstart() Then Exit Function
		If Not I2Ctransmit(DeviceAddressTx) Then Exit Function
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.clearLastMsgSent. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.clearLastMsgSent()
		ClearPosition((PositionCodes.posAddrWrite))
		I2CaddressWrite = True
	End Function
	
	
	
	
	Private Function I2CaddressRead() As Boolean
		' This function sends the device address with the Read Bit (bit1=1).
		' The saved message index is then backed off because we don't want to save the
		' device address but it is saved in I2Ctransmit. The received message index is
		' set to zero, clearing out any previous message. If the address write fails then
		' the last message is not cleared.
		I2CaddressRead = False
		SetPosition((PositionCodes.posAddrRead))
		If Not I2Cgenstart() Then Exit Function
		If Not I2Ctransmit(DeviceAddressRx) Then Exit Function
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.clearLastMsgRcvd. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.clearLastMsgRcvd()
        'Not required.
        'Logger.chompLastMsgSent() ' Back off the last sent message.
		ClearPosition((PositionCodes.posAddrRead))
		I2CaddressRead = True
	End Function
	
	
	
	
	Private Function I2Ctransmit(ByRef bytData As Byte) As Boolean
		' Transmits a byte on the bus, starting with the MSB. The SCL is set high
		' for a bit period after the SDA is set for each bit in the byte.
		' Any failure to set the SDA or SCL appropriately will cause a failure.
		' I2CWaitForAck moved into this function in v2.0.6 because it is ALWAYS called
		' after a transmit.
		Dim bytMask As Byte
		
		On Error GoTo errhandler
		I2Ctransmit = False
		SetPosition((PositionCodes.posTransmit))
		If (GetSCL = HI) Then ' Should never happen anyway.
			If (Not SetSCL(LO)) Then Exit Function ' Error - clock stuck high.
		End If
		bytMask = 128
		
		Do While bytMask > 0
			' Set SDA to next bit value and generate a clock pulse.
			If Not (SetSDA(CBool(bytData And bytMask))) Then GoTo errhandler
			' usDelay ClockPeriod / 2
			If (Not SetSCL(HI)) Then GoTo errhandler ' Error - clock stuck low.
			usDelay(ClockPeriod / 2)
			If (Not SetSCL(LO)) Then GoTo errhandler ' Error - clock stuck high.
			usDelay(ClockPeriod / 2)
			bytMask = bytMask / 2 ' Move down to next bit.
		Loop 
		
		If (Not I2CWaitForAck) Then GoTo errhandler
		
		' Update the sent string and quit.
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logLastMsgSent. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Logger.logLastMsgSent(bytData)
		ClearPosition((PositionCodes.posTransmit))
		I2Ctransmit = True
		Exit Function
		
errhandler: 
		ReleaseSDA()
	End Function
	
	
	
	
	
	Private Function I2CWaitForAck() As Boolean
		' The master (host) releases SDA then generates a clock pulse during which the
		' slave should set SDA low.
        'Dim sglAckTimer As Single
		Dim blnAckFlag As Boolean
		Dim intAckTimer As Short
		
		I2CWaitForAck = False
		SetPosition((PositionCodes.posWaitAck))
		ClearErrorBit((ErrCodes.busAckTimeout))
		ReleaseSDA()
		usDelay(ClockPeriod / 2)
		If (Not SetSCL(HI)) Then Exit Function ' Error - SCL stuck low.
		usDelay(ClockPeriod / 4)
		
		blnAckFlag = False
		intAckTimer = 0
		Do While (intAckTimer < intAckTimeout)
			If (GetSDA() = LO) Then ' Ack received.
				blnAckFlag = True
				Exit Do
			Else
				usDelay((1000)) ' No ack, wait for 1ms before rechecking.
				System.Windows.Forms.Application.DoEvents() ' Process queued events.
				intAckTimer = intAckTimer + 1
			End If
		Loop 
		
		' Finish clock cycle.
		usDelay(ClockPeriod / 4)
		If (Not SetSCL(LO)) Then Exit Function ' Error - SCL stuck high.
		If Not blnAckFlag Then SetErrorBit((ErrCodes.busAckTimeout)) Else ClearPosition((PositionCodes.posWaitAck))
		I2CWaitForAck = blnAckFlag ' Return success or failure.
	End Function
	
	
	
	Private Sub SetPosition(ByRef where As PositionCodes)
		CurrentPosition = CurrentPosition Or where
	End Sub
	
	
	
	Private Sub ClearPosition(ByRef where As PositionCodes)
		CurrentPosition = CurrentPosition And (Not where)
	End Sub
	
	
	
	
	
	Public Function PositionDescription() As String
		' Return a string containing a description of every error bit set in the code.
		' This function should be called to determine the reason for an error return.
		Dim iBitMask As PositionCodes
		Dim bitcnt As Short
        Dim sPositionDesc As String = ""
		Dim posn As PositionCodes
		
		If CurrentPosition = PositionCodes.posLOST Then
			PositionDescription = "Nowhere!"
		Else
			iBitMask = 1
			bitcnt = 1
			Do 
				posn = (CurrentPosition And iBitMask)
				Select Case posn
					Case ErrCodes.busSDAStuckLow
						sPositionDesc = sPositionDesc & "SDA stuck low, "
					Case ErrCodes.busSDAStuckHigh
						sPositionDesc = sPositionDesc & "SDA stuck high, "
					Case ErrCodes.busSCLStuckLow
						sPositionDesc = sPositionDesc & "SCL stuck low, "
					Case ErrCodes.busSCLStuckHigh
						sPositionDesc = sPositionDesc & "SCL stuck high, "
					Case ErrCodes.busAckTimeout
						sPositionDesc = sPositionDesc & "Ack timeout, "
					Case PositionCodes.posWaitAck
						sPositionDesc = sPositionDesc & "Wait for Ack, "
					Case PositionCodes.posNAck
						sPositionDesc = sPositionDesc & "Send NAck, "
					Case PositionCodes.posAck
						sPositionDesc = sPositionDesc & "Send Ack, "
					Case PositionCodes.posStop
						sPositionDesc = sPositionDesc & "Send Stop, "
					Case PositionCodes.posStart
						sPositionDesc = sPositionDesc & "Send Start, "
					Case PositionCodes.posAddrRead
						sPositionDesc = sPositionDesc & "Address Read, "
					Case PositionCodes.posAddrWrite
						sPositionDesc = sPositionDesc & "Address Write, "
					Case PositionCodes.posReceive
						sPositionDesc = sPositionDesc & "Receive, "
					Case PositionCodes.posTransmit
						sPositionDesc = sPositionDesc & "Transmit, "
					Case PositionCodes.posRcvReply
						sPositionDesc = sPositionDesc & "Receive Reply, "
					Case PositionCodes.posSendCommand
						sPositionDesc = sPositionDesc & "Send Command, "
					Case PositionCodes.posInit
						sPositionDesc = sPositionDesc & "Init (Comms Open), "
				End Select
				iBitMask = iBitMask * 2
				bitcnt = bitcnt + 1
			Loop While bitcnt <= MAX_POS
			
			' Need to remove the last 2 characters (", ") from the string.
			If sPositionDesc <> "" Then
                PositionDescription = Left(sPositionDesc, Len(sPositionDesc) - 2)
            Else
                PositionDescription = ""
            End If
		End If
	End Function
	
	
	
	Private Sub SetErrorBit(ByRef intBitNo As ErrCodes)
		' v3.0.9 - 2 wire error codes added to position indicator.
		If (intBitNo >= MIN_ERR) And (intBitNo <= MAX_ERR) Then
			intError = intError Or intBitNo
		End If
		If intBitNo <= ErrCodes.busAckTimeout Then CurrentPosition = CurrentPosition Or intBitNo
	End Sub
	
	
	
	
	Private Sub ClearErrorBit(ByRef intBitNo As ErrCodes)
		' Clear a bit within the error word.
		intError = intError And (MAXINT - intBitNo)
	End Sub
	
	
	
	
	Private Sub ClearAllErrors()
		intError = ErrCodes.busOK
	End Sub
	
	
	
	
	
	Public Function ErrorDescription(ByRef code As Byte) As String
		' Return a string containing a description of every error bit set in the code.
		' This function should be called to determine the reason for an error return.
		Dim iBitMask As Short
        Dim sErrorDesc As String = ""
		
		If code = ErrCodes.busOK Then
			ErrorDescription = "No Error"
		Else
			iBitMask = 1
			Do 
				Select Case (code And iBitMask)
					Case ErrCodes.busSDAStuckLow
						sErrorDesc = sErrorDesc & "SDA stuck low, "
					Case ErrCodes.busSDAStuckHigh
						sErrorDesc = sErrorDesc & "SDA stuck high, "
					Case ErrCodes.busSCLStuckLow
						sErrorDesc = sErrorDesc & "SCL stuck low, "
					Case ErrCodes.busSCLStuckHigh
						sErrorDesc = sErrorDesc & "SCL stuck high, "
					Case ErrCodes.busAckTimeout
						sErrorDesc = sErrorDesc & "Timeout waiting for acknowledge, "
					Case ErrCodes.busPortDrvFail
						sErrorDesc = sErrorDesc & "Port driver failed to initialise, "
					Case ErrCodes.busBusy
						sErrorDesc = sErrorDesc & "Bus busy, "
				End Select
				iBitMask = iBitMask * 2
			Loop While iBitMask <= MAX_ERR
			
			' Need to remove the last 2 characters (", ") from the string.
			ErrorDescription = Left(sErrorDesc, Len(sErrorDesc) - 2)
		End If
	End Function
	
	
	
	
	Public Function SendCommand(ByRef TxData() As Byte, ByRef TxDatalen As Byte) As Boolean
		' The send/receive parts have just been split out for flexibility and use in the
		' Comms Test form. TxDatalen is just the length of the parameters, not including
		' the command, length and checksum bytes.
		' The array myTxData is used so as not to corrupt the input array when generating
		' errors. Note that command errors have to be generated externally because they
		' affect the checksum. Also the array length is 1 larger than necessary to allow
		' for sending 1 extra byte when generating length errors.
		Dim bytCount As Byte
		Dim mintx As Byte
		Dim maxtx As Byte
		Dim myTxData(MAX_MSA_PARAM_LENGTH + 3) As Byte
		
		On Error GoTo errhandler
		SendCommand = False
		If blnBusy Then
			SetErrorBit(ErrCodes.busBusy)
			Exit Function
		End If
		
		CurrentPosition = 0
		SetPosition((PositionCodes.posSendCommand))
		ClearAllErrors()
		
		If TxDatalen > MAX_MSA_PARAM_LENGTH Then GoTo errhandler
		
		blnBusy = True
		' Need to track repeated transmits for CPN checking.
		If PreviousComms(DeviceIndex) = DATA_OUT Then
			MultipleWrites(DeviceIndex) = True
		Else
			MultipleWrites(DeviceIndex) = False
		End If
		PreviousComms(DeviceIndex) = DATA_OUT
		
		' Copy tx data to local buffer.
		mintx = LBound(TxData)
		maxtx = mintx + TxDatalen + 2
		For bytCount = 0 To maxtx - mintx
			myTxData(bytCount) = TxData(mintx + bytCount)
		Next bytCount
		
		If blnGenCmdErrors = True Then
			' Need to corrupt the command byte and regenerate the checksum.
			myTxData(0) = &HFFs
			myTxData(TxDatalen + 2) = Checksum(DeviceAddressTx, myTxData, TxDatalen + 2)
		End If
		
		If blnGenChkErrors = True Then
			' Corrupt the checksum byte.
			myTxData(TxDatalen + 2) = myTxData(TxDatalen + 2) + 1
		End If
		
		mintx = 0
		If blnGenSLenErrors = True Then
			maxtx = TxDatalen + 1 ' This will truncate the checksum.
		ElseIf blnGenLLenErrors = True Then 
			maxtx = TxDatalen + 3 ' Send 1 extra byte.
			myTxData(maxtx) = DeviceAddr ' Just to confuse it a bit!
		Else
			maxtx = TxDatalen + 2 ' Normal.
		End If
		
		If Not I2CaddressWrite Then GoTo errhandler
		
		For bytCount = 0 To maxtx
			If Not I2Ctransmit(myTxData(bytCount)) Then GoTo errhandler
			usDelay(BIT_DELAY)
		Next bytCount
		
		If Not I2Cgenstop() Then GoTo errhandler
		usDelay(BIT_DELAY * 2)
		blnBusy = False
		ClearPosition((PositionCodes.posSendCommand))
		SendCommand = True
		Exit Function
		
errhandler: 
		I2CStop_NoErr()
		blnBusy = False
	End Function
	
	
	
	
	
	Public Function ReceiveReply(ByRef RxData() As Byte, ByRef RxDatalen As Byte) As Boolean
		' Changed in v2.0.6 so as to read the number of bytes the slave returns in the
		' length byte. This is then checked with the length expected. The checksum is also
		' based on this length, not the expected length.
		' RxDatalen is unused in this procedure but is required for compatibility with the
		' serial comms procedure.
		Dim bytCount As Byte
		Dim bytLen As Byte
		
		On Error GoTo errhandler
		ReceiveReply = False
		If blnBusy Then
			SetErrorBit(ErrCodes.busBusy)
			Exit Function
		End If
		
		ClearAllErrors()
		blnBusy = True
		PreviousComms(DeviceIndex) = DATA_IN
		CurrentPosition = 0
		SetPosition((PositionCodes.posRcvReply))
		
		If Not I2CaddressRead Then GoTo errhandler
		
		' Read the status byte then the message length.
		If Not I2Creceive(RxData(0)) Then GoTo errhandler
		I2Cgiveack()
		If Not I2Creceive(RxData(1)) Then GoTo errhandler
		I2Cgiveack()
		If RxData(1) <= MAX_MSA_PARAM_LENGTH Then
			bytLen = RxData(1)
		Else
			bytLen = MAX_MSA_PARAM_LENGTH
		End If
		
		' Read the rest of the message (may be only the checksum if length=0).
		For bytCount = 0 To bytLen
			If (Not I2Creceive(RxData(bytCount + 2))) Then GoTo errhandler
			If (bytCount < bytLen) Then
				I2Cgiveack() ' Give ack if more data to be received.
			Else
				I2Cgivenack() ' Give nack if last byte.
			End If
			usDelay(BIT_DELAY)
		Next bytCount
		
		If Not I2Cgenstop() Then GoTo errhandler
		usDelay(BIT_DELAY * 2)
		blnBusy = False
		ClearPosition((PositionCodes.posRcvReply))
		ReceiveReply = True
		Exit Function
		
errhandler: 
		I2CStop_NoErr() ' Generate a Stop signal (=abort).
		blnBusy = False
	End Function
	
	
	
	
	
	Public Sub SendText(ByRef outp As String)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("WARNING! You are using text output on the parallel interface." & vbCrLf & "This is not currently supported.")
	End Sub
	
	
	
	
	Public Function ReadText() As String
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("WARNING! You are using text input on the parallel interface." & vbCrLf & "This is not currently supported.")
		ReadText = ""
	End Function
	
	
	
	
	
	Public Function WriteOIF(ByRef reg As Byte, ByRef value As Double, ByRef factor As Double) As Boolean
		' This procedure, and the equivalent read function, are generic routines which can
		' write or read many data values depending on the multiplication factor. Allowing
		' factor to be a double takes account of values which are divided, e.g. the loop
		' PID coefficients, by entering for example 0.1 as the factor.
		Dim intval As Short
		Dim lngval As Integer
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		'UPGRADE_WARNING: Lower bound of array lngtemp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim lngtemp(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFDEVICEREG
		Send(2) = reg
		
		If factor = 0 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logMESSAGE("SOFTWARE ERROR: factor=0 in WriteOIF: resetting to 1.")
			factor = 1
		End If
		
		lngval = CInt(value * factor)
		Call ConvLongToBytes(lngtemp, lngval)
		Send(3) = lngtemp(3)
		Send(4) = lngtemp(4)
		
		WriteOIF = SendMSA(SETINDEXEDCAL, Send, 4, Receive, 0)
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WriteOIF &H" & Hex(reg) & " " & CStr(value))
Aborted: 
		WriteOIF = False
	End Function
	
	
	
	
	
	Public Function ReadOIF(ByRef reg As Byte, ByRef sdata As Short, ByRef factor As Double) As Double
		' Note that "sdata" is normally zero except for commands such as "Channel Power"
		' which require the channel number. The data must be converted to an integer
		' value by the caller. If factor=0 then the data is returned straight (same as factor=1).
		Dim intval As Short
		Dim dblval As Double
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		Dim ReturnCode As Byte
		Dim hexstr As String
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFDEVICEREG
		Send(2) = reg
		Call ConvIntToBytes(temp, sdata)
		Send(3) = temp(1)
		Send(4) = temp(2)
		
		If Not SendMSA(GETINDEXEDCAL, Send, 4, Receive, 2) Then
			ReadOIF = GENERAL_ERROR_CODE
		Else
			If factor = 0 Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logMESSAGE("SOFTWARE ERROR: factor=0 in ReadOIF: resetting to 1.")
				factor = 1
			End If
			dblval = CDbl(Receive(1)) * 256# + CDbl(Receive(2))
			intval = ConvBytesToInt(Receive)
			ReadOIF = dblval / factor
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadOIF &H" & Hex(reg))
Aborted: 
		ReadOIF = GENERAL_ERROR_CODE
	End Function
	
	
	
	
	
	Public Function WriteByteOIF(ByRef reg As Byte, ByRef upper As Byte, ByRef lower As Byte) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(5) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(10) As Byte
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFDEVICEREG
		Send(2) = reg
		Send(3) = upper
		Send(4) = lower
		
		WriteByteOIF = SendMSA(SETINDEXEDCAL, Send, 4, Receive, 0)
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WriteByteOIF &H" & Hex(reg))
Aborted: 
		WriteByteOIF = False
	End Function
	
	
	
	
	Public Function ReadByteOIF(ByRef reg As Byte, ByRef upper As Byte, ByRef lower As Byte) As Boolean
		' Just reads and returns the byte values.
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(5) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(10) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFDEVICEREG
		Send(2) = reg
		
		If Not SendMSA(GETINDEXEDCAL, Send, 2, Receive, 2) Then
			ReadByteOIF = False
		Else
			upper = Receive(1)
			lower = Receive(2)
			ReadByteOIF = True
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadByteOIF &H" & Hex(reg))
Aborted: 
		ReadByteOIF = False
	End Function
	
	
	
	
	Public Function WriteOIFString(ByVal reg As Byte, ByVal oifstr As String, ByVal cnt As Short) As Boolean
		' Strings usually have to be terminated with nulls, but since only the caller knows how long
		' their string is we have to take a length parameter.
		Dim lenf As Short
		Dim indx As Short
		Dim junk As String
		Dim d1 As Byte
		Dim d2 As Byte
		'UPGRADE_WARNING: Lower bound of array d was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim d(260) As Byte
		
		On Error GoTo ErrorHandler
		
		lenf = Len(oifstr)
		If lenf > cnt Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Warning: '" & oifstr & "' is longer than the specified count in WriteOIFString.")
		End If
		If cnt > 256 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Warning: Count exceeds 256 in WriteOIFString.")
			cnt = 256
		End If
		
		' Copy from the string to an array and pad out.
		For indx = 1 To cnt + 3
			If indx > lenf Then
				d(indx) = 0
			Else
				d(indx) = Asc(Mid(oifstr, indx, 1))
			End If
		Next indx
		
		' Send the count to the module.
		If Not WriteOIF(reg, CDbl(cnt), 1) Then GoTo Aborted
		
		' Now send the remaining data in byte pairs.
		For indx = 1 To (cnt + 1) \ 2
			If Not WriteByteOIF(OIFAEAREG, d(2 * indx - 1), d(2 * indx)) Then GoTo Aborted
		Next indx
		WriteOIFString = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WriteOIFString &H" & Hex(reg))
Aborted: 
		WriteOIFString = False
	End Function
	
	
	
	
	
	Public Function ReadOIFString(ByRef reg As Byte) As String
		Dim cnt As Short
		Dim indx As Short
		Dim junk As String
		Dim d1 As Byte
		Dim d2 As Byte
		
		On Error GoTo ErrorHandler
		
		cnt = ReadOIF(reg, 0, 1)
		If cnt = GENERAL_ERROR_CODE Then
			GoTo Aborted
		Else
			If cnt <= 0 Then
				' Don't know why (or if) this would ever happen. What do you do with
				' a zero length string?
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logMESSAGE("WARNING: Zero length AEA returned.")
				GoTo Aborted
			Else
				junk = ""
				For indx = 1 To (cnt + 1) \ 2
					If Not ReadByteOIF(OIFAEAREG, d1, d2) Then
						GoTo Aborted
					Else
						junk = junk & Chr(d1) & Chr(d2)
					End If
				Next indx
				ReadOIFString = junk
			End If
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadOIFString &H" & Hex(reg))
Aborted: 
		ReadOIFString = strGENERAL_ERROR_CODE
	End Function
	
	
	
	
	Public Function WriteCal(ByRef reg As Byte, ByRef value As Double, ByRef factor As Double, Optional ByRef indx As Object = Nothing) As Boolean
		' This procedure, and the equivalent read function, are generic routines which can
		' write or read many data values depending on the multiplication factor. Allowing
		' factor to be a double takes account of values which are divided, e.g. the loop
		' PID coefficients, by entering for example 0.1 as the factor.
		' The WriteCal and ReadCal routines talk to the PIC.
		Dim intval As Short
		Dim lngval As Integer
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		'UPGRADE_WARNING: Lower bound of array lngtemp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim lngtemp(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		Dim offset As Short
		
		On Error GoTo ErrorHandler
		
		Send(1) = reg
		'UPGRADE_NOTE: IsMissing() was changed to IsNothing(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="8AE1CB93-37AB-439A-A4FF-BE3B6760BB23"'
		If IsNothing(indx) Then
			offset = 0
		Else
			offset = 1
			'UPGRADE_WARNING: Couldn't resolve default property of object indx. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Send(2) = CByte(indx)
		End If
		
		If factor = 0 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logMESSAGE("SOFTWARE ERROR: factor=0 in WriteCal: resetting to 1.")
			factor = 1
		End If
		
		lngval = CInt(value * factor)
		Call ConvLongToBytes(lngtemp, lngval)
		Send(offset + 2) = lngtemp(3)
		Send(offset + 3) = lngtemp(4)
		
		'UPGRADE_NOTE: IsMissing() was changed to IsNothing(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="8AE1CB93-37AB-439A-A4FF-BE3B6760BB23"'
		If IsNothing(indx) Then
			WriteCal = SendMSA(SETCAL, Send, 3, Receive, 0)
		Else
			WriteCal = SendMSA(SETINDEXEDCAL, Send, 4, Receive, 0)
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WriteCal &H" & Hex(reg) & " " & Val(CStr(value)))
Aborted: 
		WriteCal = False
	End Function
	
	
	
	
	Public Function ReadCal(ByRef reg As Byte, ByRef factor As Double, Optional ByRef indx As Object = Nothing) As Double
		' If factor=0 then the data is returned straight.
		' If factor=-1 then the data is considered to be 2 bytes, otherwise the data is
		' divided by the factor and returned as a double.
		Dim intval As Short
		Dim dblval As Double
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		Dim ReturnCode As Boolean
		Dim hexstr As String
		
		On Error GoTo ErrorHandler
		
		Send(1) = reg
		'UPGRADE_NOTE: IsMissing() was changed to IsNothing(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="8AE1CB93-37AB-439A-A4FF-BE3B6760BB23"'
		If IsNothing(indx) Then
			ReturnCode = SendMSA(GETCAL, Send, 1, Receive, 2)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object indx. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Send(2) = indx
			ReturnCode = SendMSA(GETINDEXEDCAL, Send, 2, Receive, 2)
		End If
		
		If ReturnCode = False Then
			ReadCal = GENERAL_ERROR_CODE ' Fail code.
		Else
			If factor = 0 Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logMESSAGE("SOFTWARE ERROR: factor=0 in ReadCal: resetting to 1.")
				factor = 1
			End If
			dblval = CDbl(Receive(1)) * 256# + CDbl(Receive(2))
			ReadCal = dblval / factor
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadCal &H" & Hex(reg))
Aborted: 
		ReadCal = GENERAL_ERROR_CODE ' Fail code.
	End Function
	
	
	
	
	Private Sub KillProgressBar()
		If ProgBar Is Nothing Then
			' Progress bar cancelled by user. Just exit.
		Else
			' Either a comms error or a good status return - either way we exit.
			ProgBar.Hide()
			ProgBar.Close()
			'UPGRADE_NOTE: Object ProgBar may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			ProgBar = Nothing
		End If
	End Sub
	
	
	
	
	Private Function MSAErrorDescription(ByRef code As Byte) As String
		' Return a string containing a description of every error bit set in the code.
		' This function should be called to determine the reason for an error.
		' These codes are defined in the MSA v4.
		Select Case code
			Case MSA_OK
				MSAErrorDescription = "No error - command executed"
			Case MSA_UNKNOWN_COMMAND
				MSAErrorDescription = "Command not supported or wrong protection mode"
			Case MSA_FRAME_ERROR
				MSAErrorDescription = "Frame length doesn't match the length byte sent"
			Case MSA_OUT_OF_RANGE
				MSAErrorDescription = "At least one parameter out of range"
			Case MSA_TIME_OUT
				MSAErrorDescription = "Command timing out of range"
			Case MSA_CHECK_ERROR
				MSAErrorDescription = "Calculated check byte doesn't match byte sent"
			Case MSA_MODULE_BUSY
				MSAErrorDescription = "Module has started executing long command"
			Case MSA_STILL_PROCESSING
				MSAErrorDescription = "Module still processing previous command"
			Case MSA_COMMAND_NOT_EXECUTED
				MSAErrorDescription = "Module not able to execute command"
			Case MSA_COMMAND_FAILED
				MSAErrorDescription = "Module failed to complete a legal command"
			Case MSA_INVALID
				MSAErrorDescription = "Invalid response from module"
			Case Else
				MSAErrorDescription = "UNKNOWN MSA ERROR CODE"
		End Select
	End Function
	
	
	
	
	Private Function Checksum(ByRef bytaddr As Byte, ByRef bytData() As Byte, ByRef bytLen As Byte) As Byte
		' The address is expected in the correct format, i.e. with the R/W bit set or cleared
		' as required for receive or transmit. The length is just that of the data bytes,
		' including the command and length bytes.
		' There is no assumption about array start points - they can be anything.
		Dim bytChecksum As Byte
		Dim intIndex As Byte
		
		bytChecksum = (bytaddr)
		For intIndex = LBound(bytData) To (LBound(bytData) + bytLen - 1)
			bytChecksum = bytChecksum Xor bytData(intIndex)
		Next intIndex
		
		If bytChecksum > 0 Then
			Checksum = bytChecksum - 1
		Else
			Checksum = 255
		End If
	End Function
	
	
	
	
	Private Function FormattedMessage(ByRef cmd As Byte, ByRef params() As Byte, ByRef ParamLen As Byte) As Byte()
		' Reformat the Params, putting the command byte at the start and a checksum
		' at the end. Note that the checksum calculation is done from 0 to N of TxData.
		' NOTE: Params is assumed to start at 1.
		Dim intIndex As Byte
		Dim bytTotalTxLen As Byte
		Dim TxData(MAX_MSA_PARAM_LENGTH + 2) As Byte
		
		bytTotalTxLen = ParamLen + 2 ' 2 byte overhead for length and command.
		TxData(0) = cmd
		TxData(1) = ParamLen
		
		For intIndex = 1 To ParamLen
			TxData(intIndex + 1) = params(intIndex)
		Next intIndex
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.DeviceAddressTx. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		TxData(bytTotalTxLen) = Checksum(Comms.DeviceAddressTx, TxData, bytTotalTxLen)
		FormattedMessage = VB6.CopyArray(TxData)
	End Function
	
	
	
	
	Private Function WaitForLongReply(ByRef RxData() As Byte, ByRef RxDatalen As Byte) As Boolean
		' This routine waits for 100ms before attempting to receive a response again.
		' If the module continues to be busy then a longer wait is started with a
		' progress bar and cancel button. This automatically cancels when the command
		' returns with other than busy. A receive failure just aborts the wait and
		' returns with whatever the original message was.
		Dim bytStatus As Byte
		Dim cnt As Short
		
		On Error GoTo ErrorHandler
		
		cnt = 0
		bytStatus = StatusOnly(RxData(0))
		Do While (bytStatus = MSA_MODULE_BUSY) And (cnt < 3)
			msDelay((100))
			If ReceiveReply(RxData, RxDatalen) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LastMsgRcvdStr. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logMESSAGE("    Rpt-Rcvd " & Logger.LastMsgRcvdStr)
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.StatusOnly. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				bytStatus = Comms.StatusOnly(RxData(0))
			Else
				' Low level I2C error.
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ErrorCode. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ErrorDescription. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logFATAL("RECEIVE FAILURE: " & Comms.ErrorDescription(Comms.ErrorCode))
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.PositionDescription. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logFATAL("COMMS DEBUG: " & Comms.PositionDescription)
				GoTo Aborted
			End If
			cnt = cnt + 1
		Loop 
		
		If cnt >= 3 Then
			bytStatus = StatusOnly(RxData(0))
			Do While (bytStatus = MSA_MODULE_BUSY)
				sDelay((1))
				If ReceiveReply(RxData, RxDatalen) Then
					'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LastMsgRcvdStr. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					Call Logger.logMESSAGE("    Rpt-Rcvd " & Logger.LastMsgRcvdStr)
					bytStatus = StatusOnly(RxData(0))
				Else
					' Low level I2C error.
					'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ErrorCode. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ErrorDescription. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					Call Logger.logFATAL("RECEIVE FAILURE: " & Comms.ErrorDescription(Comms.ErrorCode))
					'UPGRADE_WARNING: Couldn't resolve default property of object Comms.PositionDescription. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					Call Logger.logFATAL("COMMS DEBUG: " & Comms.PositionDescription)
					GoTo Aborted
				End If
				System.Windows.Forms.Application.DoEvents()
			Loop 
			KillProgressBar()
		End If
		
		WaitForLongReply = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WaitForLongReply")
Aborted: 
		WaitForLongReply = False
	End Function
	
	
	
	
	Public Function SendMSA(ByRef bytCommand As Byte, ByRef params() As Byte, ByRef ParamLen As Byte, ByRef RxData() As Byte, ByRef RxLen As Byte, Optional ByRef ActualLen As Byte = 0) As Boolean
		' The data length arguments to this procedure are the lengths of the
		' "command parameters" and "reply parameters" as defined in the MSA spec. The lower
		' level routines add the appropriate numbers for length and checksum bytes etc.
		' Both the Tx and Rx data arrays should have been dimensioned by the calling routine,
		' so we need a larger Rx array here to hold the status, length and checksum bytes.
		' NOTE: Params is assumed to start at 1 when received in FormattedMessage.
		Dim bytChecksum As Byte
		Dim bytStatus As Byte
		Dim ErrLogString As String
		Dim intIndex As Byte
		Dim Received(MAX_MSA_PARAM_LENGTH + 2) As Byte
		Dim RxMax As Byte
		Dim RxMin As Byte
		Dim RcvdLen As Byte
		Dim oifsend(3) As Byte
		
		On Error GoTo SendCommandErr
		KillProgressBar()
		
		If SendCommand(FormattedMessage(bytCommand, params, ParamLen), ParamLen) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LastMsgSentStr. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logMESSAGE("Sent " & Logger.LastMsgSentStr)
			msDelay((20)) ' Need at least 5mS for the command to be actioned.
			
			If ReceiveReply(Received, RxLen) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LastMsgRcvdStr. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logMESSAGE("    Rcvd " & Logger.LastMsgRcvdStr)
				bytStatus = StatusOnly(Received(0))
				
				If bytStatus = MSA_MODULE_BUSY Then
					If Not WaitForLongReply(Received, RxLen) Then GoTo Aborted
					bytStatus = StatusOnly(Received(0))
				End If
				
				RcvdLen = Received(1)
				'UPGRADE_NOTE: IsMissing() was changed to IsNothing(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="8AE1CB93-37AB-439A-A4FF-BE3B6760BB23"'
				If Not IsNothing(ActualLen) Then ActualLen = RcvdLen ' Return the received length.
				If (RcvdLen > MAX_MSA_PARAM_LENGTH) Then
					bytChecksum = &HEEs
				Else
					'UPGRADE_WARNING: Couldn't resolve default property of object Comms.DeviceAddressRx. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					bytChecksum = Checksum(Comms.DeviceAddressRx, Received, RcvdLen + 2)
				End If
				
				' Check status byte and CPN bit. If the CPN is incorrect it will generate
				' an error message but continue to treat the rest of the message as normal.
				CheckCPN((Received(0)))
				ErrLogString = ""
				
				If (bytStatus <> MSA_OK) Then
					ErrLogString = "Msg Error: Status=" & bytStatus & ": " & MSAErrorDescription(bytStatus)
					'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					Call Logger.logFATAL(ErrLogString)
					SendMSA = False
				Else
					If (RcvdLen > MAX_MSA_PARAM_LENGTH) Then
						ErrLogString = "Msg Error (Length): " & "Exp=" & RxLen & "  Rcvd=" & RcvdLen
						'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						Call Logger.logFATAL(ErrLogString)
						SendMSA = False
					Else
						If bytChecksum <> Received(RcvdLen + 2) Then
							ErrLogString = "Msg Error (Checksum): " & "Exp=" & Hex(bytChecksum) & "  Rcvd=" & Hex(Received(RxLen + 2))
							'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							Call Logger.logFATAL(ErrLogString)
							SendMSA = False
						Else
							SendMSA = True
						End If
					End If
				End If
				
				If ErrLogString = "" Then
					' Everything is ok. Move the Rx data to the output array (the higher
					' levels don't want the status and length bytes), and copy the message
					' to the last successful array (done in the Logger module).
					RxMin = 1 'Replacing LBound(RxData) due to VB6/.NET convertion.
					RxMax = UBound(RxData)
					If (RxMax - RxMin + 1) < RxLen Then
						' SOFTWARE ERROR: the receiving array isn't big enough!!!
						'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						Call Logger.logMESSAGE("SOFTWARE ERROR: Receive array too small - command " & CStr(bytCommand))
						For intIndex = 1 To (RxMax - RxMin + 1)
							RxData(RxMin + intIndex - 1) = Received(intIndex + 1)
						Next intIndex
						SendMSA = False
					Else
						For intIndex = 1 To RxLen
							RxData(RxMin + intIndex - 1) = Received(intIndex + 1)
						Next intIndex
					End If
                    ' Does nothing.
                    'Logger.LastMsgOK() 
				Else
					' Nothing to do here - error messages already sent.
				End If
				' SendMSA = bytStatus   ######### Changed to boolean in v1.0.1
			Else
				' Low level I2C error, e.g. message length doesn't match length byte.
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logFATAL("RECEIVE FAILURE: " & ErrorDescription(bytStatus))
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logFATAL("COMMS DEBUG: " & PositionDescription)
				SendMSA = False ' ERR_I2C
			End If
		Else
			' Low level I2C error
			bytStatus = intError
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logFATAL("SEND FAILURE: " & ErrorDescription(bytStatus))
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logFATAL("COMMS DEBUG: " & PositionDescription)
			SendMSA = False ' ERR_I2C
		End If
		Exit Function
		
SendCommandErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SendMSA")
Aborted: 
		SendMSA = False ' ERR_UNKNOWN
	End Function
End Class
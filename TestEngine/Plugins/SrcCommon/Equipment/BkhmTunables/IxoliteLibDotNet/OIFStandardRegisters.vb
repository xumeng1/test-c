Option Strict Off
Option Explicit On
Module OIFStandardRegisters
	
	
	' This class module holds a set of OIF commands as defined in documents -
	' "The OIF Compliant Tunable Laser MSA, PV:1.2.3" and
	' "Integrable Tunable Laser Assembly MSA (ITLA-MSA), OIF2005.128.02"
	
	
	' General
	Public Const NOPREG As Byte = &H0s
	Public Const DEVICETYPEREG As Byte = &H1s ' r  9 byte string.
	Public Const MANUFACTURERREG As Byte = &H2s
	Public Const MODELNUMBERREG As Byte = &H3s
	Public Const SERIALNUMBERREG As Byte = &H4s
	Public Const MANUFACTURINGDATEREG As Byte = &H5s
	Public Const RELEASECODEREG As Byte = &H6s
	Public Const RELEASEBACKWARDS As Byte = &H7s
	Public Const GENERALCONFIG As Byte = &H8s ' rw
	Public Const IOCAPREG As Byte = &HDs
	' Status
	Public Const STATUSF As Byte = &H20s ' rw
	Public Const STATUSW As Byte = &H21s ' rw
	Public Const FPOWTH As Byte = &H22s ' rw +/-dB*100
	Public Const WPOWTH As Byte = &H23s ' rw +/-dB*100
	Public Const FFREQTH As Byte = &H24s ' rw +/-GHz*10
	Public Const WFREQTH As Byte = &H25s ' rw +/-GHz*10
	Public Const FTHERMTH As Byte = &H26s ' rw +/-degC*100
	Public Const WTHERMTH As Byte = &H27s ' rw +/-degC*100
	Public Const SRQT As Byte = &H28s ' rw
	Public Const FATALT As Byte = &H29s ' rw
	Public Const ALMT As Byte = &H2As ' rw
	' Optical
	Public Const CHANNELSELECT As Byte = &H30s ' rw
	Public Const POWERSETPOINT As Byte = &H31s ' rw dBm * 100
	Public Const MODULERESETENABLE As Byte = &H32s ' Set to 8 to enable output.
	Public Const MODCONFIGBEHAVIOUR As Byte = &H33s
	Public Const FREQUENCYGRID As Byte = &H34s
	Public Const FIRSTCHANFREQ1 As Byte = &H35s ' rw THz part of frequency.
	Public Const FIRSTCHANFREQ2 As Byte = &H36s ' rw (THz Mod 1000) * 10
	Public Const LASERFREQUENCY1 As Byte = &H40s ' r  THz part of frequency.
	Public Const LASERFREQUENCY2 As Byte = &H41s ' r  (THz Mod 1000) * 10
	Public Const LASEROUTPUTPWR As Byte = &H42s ' r  dBm * 100
	Public Const CURRENTOSATEMP As Byte = &H43s ' r  deg * 100
	' Capabilities
	Public Const OPTICALPOWERMIN As Byte = &H50s ' r  dBm * 100
	Public Const OPTICALPOWERMAX As Byte = &H51s ' r  dBm * 100
	Public Const LASERFIRSTFREQ1 As Byte = &H52s ' rw 52-55 as per 35-36.
	Public Const LASERFIRSTFREQ2 As Byte = &H53s
	Public Const LASERLASTFREQ1 As Byte = &H54s
	Public Const LASERLASTFREQ2 As Byte = &H55s
	Public Const LASERMINGRID As Byte = &H56s
	' MSA
	Public Const DITHERE As Byte = &H59s
	Public Const DITHERR As Byte = &H5As ' rw kHz (10-200)
	Public Const DITHERF As Byte = &H5Bs ' rw GHz * 10
	Public Const DITHERA As Byte = &H5Cs ' rw % * 10
	Public Const TCASEL As Byte = &H5Ds
	Public Const TCASEH As Byte = &H5Es
	Public Const FAGETH As Byte = &H5Fs ' rw %
	Public Const WAGETH As Byte = &H60s ' rw %
	
	Public Const STOREDEFAULTCONFIG As Double = &H8000s
	
	' Public Const OIFAEAREG As Byte = &HB          ' Moved to global constants in v1.0.1
	Public Const OIFREADCOMMAND As Byte = 0
	Public Const OIFWRITECOMMAND As Byte = 1
	Public Const OIFERRORFIELD_MASK As Byte = &HFs
	Public Const NOPTIMEOUT As Short = 20 ' secs
End Module
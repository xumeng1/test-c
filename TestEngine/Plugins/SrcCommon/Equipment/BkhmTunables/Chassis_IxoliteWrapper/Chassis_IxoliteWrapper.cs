// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// Chassis_IxoliteWrapper.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.BlackBoxes;
using System.IO.Ports;
using IxoliteLibDotNet;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis class wrapping the Ixolite Library.
    /// </summary>
    public class Chassis_IxoliteWrapper: Bookham.TestEngine.PluginInterfaces.Chassis.Chassis
    {
        #region Private data
        /// <summary>
        /// Reference to Ixolite setup class.
        /// </summary>
        private readonly IxoliteLibDotNet.LibSetup ixoSetup;

        // References to command Izolite classes.
        private readonly IxoliteLibDotNet.MSAStandardCommands ixoMsaStd;
        private readonly IxoliteLibDotNet.MSAVendorCommands ixoMsaVen;
        private readonly IxoliteLibDotNet.OIFStandardCommands ixoOifStd;
        private readonly IxoliteLibDotNet.OIFVendorCommands ixoOifVen;

        /// <summary>
        /// Flag indicating if the D0, D1, S5 and S7 parallel port lines may be used.
        /// </summary>
        private bool i2cInUse;

        /// <summary>
        /// Initialised Parallel Port Address
        /// </summary>
        private long parPortAddr;

        /// <summary>
        /// Device I2C address
        /// </summary>
        private byte i2cAddr;

        /// <summary>
        /// I2C Maximum rate
        /// </summary>
        private int maxI2CRate_kHz = 0;

        /// <summary>
        /// Which Instance of Chassis Ixolite is this?
        /// </summary>
        private int thisInstance;

        /// <summary>
        /// How many Instances of Chassis Ixolite are there?
        /// </summary>
        private static int nbrInstances = 0;

        /// <summary>
        /// Which instance of Chassis Ixolite last accessed the hardware?
        /// </summary>
        private static int currentInstance = 0;

        #endregion

        /// <summary>
        /// Set up chassis.
        /// </summary>
        /// <param name="chassisName">Name of chassis.</param>
        /// <param name="driverName">Name of driver.</param>
        /// <param name="resourceStringId">"LPT,[port addr HEX],[I2C addr HEX],[maxI2CRate_kHz]" or "COM,[com port],[rate]".</param>
        public Chassis_IxoliteWrapper(string chassisName, string driverName, string resourceStringId)
            : base(chassisName, driverName, resourceStringId)
        {
            //store Chassis ixolite Instance information
            thisInstance = ++nbrInstances;
            currentInstance = thisInstance;
            
            this.ValidHardwareData.Add("Ixolite",  new ChassisDataRecord("Unknown",  "Unknown", "Unknown"));

            this.ixoSetup = new IxoliteLibDotNet.LibSetup();
            this.ixoSetup.IxoInitialise(new IxoliteMsgLogger());
            this.ixoMsaStd = new IxoliteLibDotNet.MSAStandardCommands();
            this.ixoMsaVen = new IxoliteLibDotNet.MSAVendorCommands();
            this.ixoOifStd = new IxoliteLibDotNet.OIFStandardCommands();
            this.ixoOifVen = new IxoliteLibDotNet.OIFVendorCommands();

            /* Set up Comms */

            string[] resByComma = resourceStringId.Split(',');
            if (resByComma[0] == "LPT")
            {
                if ((resByComma.Length < 3) || (resByComma.Length > 4))
                {
                    throw new ChassisException("Bad resource string. Use \"LPT,nnn,nn,nnn\". eg \"LPT,378,41\"");
                }

                /* Extract parallel port addr */
                this.parPortAddr = long.Parse(resByComma[1], System.Globalization.NumberStyles.HexNumber);
                
                /* Extract I2C module address. */
                this.i2cAddr = byte.Parse(resByComma[2], System.Globalization.NumberStyles.HexNumber);

                // Extract max I2C rate
                
                if (resByComma.Length > 3)
                {
                    this.maxI2CRate_kHz = int.Parse(resByComma[3], System.Globalization.CultureInfo.InvariantCulture);
                }

                /* Set up Parallel Comms */
                this.ixoSetup.SetParallelComms(ref this.parPortAddr, ref this.i2cAddr, ref this.maxI2CRate_kHz);
                this.i2cInUse = true;
            }
            else if (resByComma[0] == "COM")
            {
                if ((resByComma.Length != 2) && (resByComma.Length != 3))
                {
                    throw new ChassisException(
                        "Bad resource string. Use \"COM,n\" or \"COM,n,rate\". eg \"COM,1,9600\"");
                }

                /* Load com port number and rate from resource id. (Default 9600). */
                string comPort = resByComma[1];
                string rate = (resByComma.Length == 3) ? resByComma[2] : "9600";

                /* Set up Serial Binary Comms */
                this.ixoSetup.SetSerialComms(buildSerPort(comPort, rate));
                this.i2cInUse = false;
            }
            else if (resByComma[0] == "COMPAR")
            {
                if ((resByComma.Length != 3) && (resByComma.Length != 4))
                {
                    throw new ChassisException(
                        "Bad resource string. Use \"COMPAR,n,parport\" or \"COMPAR,n,rate,parport\". eg \"COMPAR,1,9600,378\"");
                }

                /* Load com port number and rate from resource id. (Default 9600). */
                string comPort = resByComma[1];
                string rate = (resByComma.Length == 4) ? resByComma[2] : "9600";

                /* Last item is parport address. */
                short parPort = short.Parse(resByComma[resByComma.Length - 1], System.Globalization.NumberStyles.HexNumber);

                /* Set up Serial Binary Comms with parallel port support. */
                this.ixoSetup.SetSerialCommsWithParallelPinControl(buildSerPort(comPort, rate), parPort);
                this.i2cInUse = false;
            }
            else
            {
                throw new ChassisException("Bad resource string. Use \"LPT,nnn,nn\" or \"COM,n\".");
            }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }


        /// <summary>
        /// For systems with multiple 300 Pin MSA Compliant  devices to drive - force the corrrect
        /// Paralel Port Address, Device Address and Max I2C rate. 
        /// </summary>
        private void restoreParallelCommsContext()
        {
            //No need to restore context if there is only 1 instance of Chassis Ixolite
            if (nbrInstances > 1)
            {
                //Only restore context if a different instance of ixolite last Accessed the hardware.
                if (currentInstance != thisInstance)
                {
                    //Call new IxoliteLibDotNet call to restore context for Parallel Port Based I2C Comms.
                    this.ixoSetup.RestoreParallelCommsContext(ref this.parPortAddr, ref this.i2cAddr, ref this.maxI2CRate_kHz);
                    currentInstance = thisInstance;
                }
            }
        }

        #region Chassis abstract function implementations
        public override string FirmwareVersion
        {
            get { return "Unknown"; }
        }

        public override string HardwareIdentity
        {
            get { return "Unknown"; }
        }
        #endregion

        #region Error handling
        /// <summary>
        /// Update the logs after a call into Ixolite.
        /// </summary>
        private string updateLogger(string errorText, TunableProtocol errorProtocol)
        {
            System.Text.StringBuilder retStr = new StringBuilder("");
            bool errorflag = false;

            while (this.ixoSetup.IsLogEmpty() == false)
            {
                string taggedMessage = this.ixoSetup.LogRead();
                string message = taggedMessage.Substring(1);

                if (taggedMessage[0] == 'E')
                {
                    errorflag = true;
                }

                this.LogEvent( message);

                retStr.Append(message);
                retStr.Append("\r\n");
            }

            if (errorflag)
            {
                throw new TunableModuleException(errorText + "\r\n" + retStr.ToString(), errorProtocol);
            }

            return retStr.ToString();
        }

        /// <summary>
        /// General string returned by VB indicating error.
        /// </summary>
        private const string errorStringNeg9999 = "-9999";

        /// <summary>
        /// General double value returned by VB indicating error.
        /// </summary>
        private const double errorDoubleNeg9999 = -9999.0;

        /// <summary>
        /// General short value returned by VB indicating error.
        /// </summary>
        private const short errorShortNeg9999 = -9999;

        /// <summary>
        /// General int value returned by VB indicating error.
        /// </summary>
        private const int errorIntNeg9999 = -9999;

        /// <summary>
        /// Self contained exception thrower.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="protocol">Protocol id.</param>
        private static void throwError(string message, TunableProtocol protocol)
        {
            throw new TunableModuleException(message, protocol);
        }
        #endregion

        #region Ixolite return value checking functions
        /// <summary>
        /// Check result of an Ixolite function returning bool. false represents error.
        /// </summary>
        /// <param name="result">Result of Ixolite function call.</param>
        /// <param name="error">Exception message.</param>
        /// <param name="protocol">Protocol causing error.</param>
        private void checkIxoliteReturnBool(bool result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == false) throwError(error + "\r\n" + logText, protocol);
        }

        /// <summary>
        /// Checks if a result value is near the -9999 error code.
        /// </summary>
        /// <param name="result">Result of Ixolite function returning double or float.</param>
        /// <param name="maxDev">Maximum devation from -9999 allowable.</param>
        /// <returns>True if result is within -9999 plus-or-minus maxDev. False otherwise.</returns>
        private bool isNearNeg9999(double result, double maxDev)
        {
            return (result > errorDoubleNeg9999-maxDev) && (result < errorDoubleNeg9999+maxDev); 
        }

        /// <summary>
        /// Check result of an Ixolite function returning double. -9999.0 (within 0.01%) represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private double checkIxoliteReturnDouble(double result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (isNearNeg9999(result, 1)) throwError(error + "\r\n" + logText, protocol);
            return result;
        }

        /// <summary>
        /// Check result of an Ixolite function returning float. -9999.0 (within 0.1%) represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private float checkIxoliteReturnFloat(float result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (isNearNeg9999(result, 10)) throwError(error + "\r\n" + logText, protocol);
            return result;
        }


        /// <summary>
        /// Check result of an Ixolite function returning string. "-9999" represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non "-9999" return value.</returns>
        private string checkIxoliteReturnString(string result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == errorStringNeg9999) throwError(error + "\r\n" + logText, protocol);
            return result.TrimEnd('\0');
        }

        /// <summary>
        /// Check result of an Ixolite function returning short. -9999 represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private short checkIxoliteReturnShort(short result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == errorShortNeg9999) throwError(error + "\r\n" + logText, protocol);
            return result;
        }

        /// <summary>
        /// Check result of an Ixolite function returning int. -9999 represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private int checkIxoliteReturnInt(int result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == errorIntNeg9999) throwError(error + "\r\n" + logText, protocol);
            return result;
        }
        #endregion

        #region Helper functions
        private ITUBand bandStringToBand(string bandStr, string functionName)
        {
            switch (bandStr)
            {
                case "C":
                    return ITUBand.CBand;
                case "L":
                    return ITUBand.LBand;
                case "S":
                    return ITUBand.SBand;
                default:
                    throw new TunableModuleException(functionName + " returned \"" + bandStr + "\", instead of C, L or S.", TunableProtocol.MSA);
            }
        }

        private string bandToLetter(ITUBand band)
        {
            switch (band)
            {
                case ITUBand.CBand: return "C";
                case ITUBand.LBand: return "L";
                case ITUBand.SBand: return "S";
                default:
                    {
                        throw new ChassisException("Called bandToLetter with bad value - " + band.ToString());
                    }
            }
        }

        /// <summary>
        /// Calls (Get/Set)ControlRegister with mask and bits.
        /// </summary>
        /// <param name="mask">Bit mask. Bits with a value of 1 in mask are modified in register only.</param>
        /// <param name="newBits">New bit values for masked bits.</param>
        public void ModifyControlRegister(int mask, int newBits)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Fetch old register value. */
            int oldReg = this.GetControlRegister();

            /* Modify by mask and new value. Zeroify the masked bits then OR in new value. */
            int newReg = (oldReg & ~mask) | (newBits & mask);

            /* Send new values back to device. */
            this.SetControlRegister(newReg);
        }

        /// <summary>
        /// Calls ModifyControlRegsister for single bit sets.
        /// </summary>
        /// <param name="bitNum">Bit number to modify 0..15.</param>
        /// <param name="bitVal">New bit value.</param>
        public void ModifyControlRegisterSingleBit(int bitNum, bool bitVal)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            if ((bitNum < 0) || (bitNum > 15))
            {
                throw new ChassisException("ModifyControlRegisterSingleBit called with bad bitNum (" + bitNum + ")");
            }

            int mask = 1 << bitNum;
            this.ModifyControlRegister(mask, bitVal ? mask : 0);
        }

        /// <summary>
        /// Extracts a single bit (as a bool) from the control register.
        /// </summary>
        /// <param name="bitNum">Bit number 0..15.</param>
        /// <returns>Bit value as a boolean.</returns>
        public bool GetControlRegisterSingleBit(int bitNum)
        {

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            if ((bitNum < 0) || (bitNum > 15))
            {
                throw new ChassisException("GetControlRegisterSingleBit called with bad bitNum (" + bitNum + ")");
            }

            return (this.GetControlRegister() & (1 << bitNum)) != 0;
        }

        /// <summary>
        /// Converts a bool parameter value to a string, where true translates to an enum-esque string value.
        /// False values convert to an empty string, which qualify as "any other value".
        /// </summary>
        /// <param name="nv">Bool value.</param>
        /// <param name="ifTrue">String to return if nv is true.</param>
        /// <returns>Value of ifTrue, or "".</returns>
        private string boolToString(bool nv, string ifTrue)
        {
            return nv ? ifTrue : "";
        }
        #endregion

        #region Joe Collins legacy functions

        #region Wrap MSAStandardCommands.cls (Legacy)
        public void SetTxCommand(TxCommand txCmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte d1 = txCmd.Data1;
            byte d2 = txCmd.Data2;
            byte d3 = txCmd.Data3;

            this.checkIxoliteReturnBool(this.ixoMsaStd.SetTxCommand(ref d1, ref d2, ref d3), "SetTxCommand failed.", TunableProtocol.MSA);
        }

        public TxCommand ReadTxCommand()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte d1 = 0;
            byte d2 = 0;
            byte d3 = 0;
            checkIxoliteReturnBool(this.ixoMsaStd.ReadTxCommand(ref d1, ref d2, ref d3), "ReadTxCommand failed.", TunableProtocol.MSA);
            return new TxCommand(d1, d2, d3);
        }

        public void SetRxThreshold(double thresh_pc)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            this.checkIxoliteReturnBool(this.ixoMsaStd.SetRxThreshold(ref thresh_pc), "SetRxThreshold failed.", TunableProtocol.MSA);
        }


        public double ReadRxThreshold()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            try
            {
                return this.ixoMsaStd.ReadRxThreshold();
            }
            catch
            {
                throw new ChassisException("GetRxThreshold failed.");
            }
        }
        public void SaveTxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            this.checkIxoliteReturnBool(this.ixoMsaStd.SaveTxRegister(), "SaveTxRegister failed.", TunableProtocol.MSA);
        }

        public void SaveRxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            this.checkIxoliteReturnBool(this.ixoMsaStd.SaveRxRegister(), "SaveRxRegister failed.", TunableProtocol.MSA);
        }

        public void RestoreTxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.RestoreTxRegister(), "RestoreTxRegister failed.", TunableProtocol.MSA);
        }

        public void RestoreRxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.RestoreRxRegister(), "RestoreRxRegister failed.", TunableProtocol.MSA);
        }

        public void EnterProtectMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.EnterProtectMode(), "EnterProtectMode failed.", TunableProtocol.MSA);
        }

        public void EnterVendorProtectMode(string password)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.EnterVendorProtectMode(ref password), "EnterVendorProtectMode failed.", TunableProtocol.MSA);
        }

        public void EnterSoftMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.EnterSoftMode(), "EnterSoftMode failed.", TunableProtocol.MSA);
        }

        public void EnterPinMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.EnterPinMode(), "EnterPinMode failed.", TunableProtocol.MSA);
        }

        public void ExitProtectMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.ExitProtectMode(), "ExitProtectMode failed.", TunableProtocol.MSA);
        }

        public void ResetCPN()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.ResetCPN(), "ResetCPN failed.", TunableProtocol.MSA);
        }

        public void SelectITUChannel(ITUBand band, double chan)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandStr = bandToLetter(band);
            this.checkIxoliteReturnBool(this.ixoMsaStd.SelectITUChannel(ref bandStr, ref chan), "SelectITUChannel failed.", TunableProtocol.MSA);
        }

        public BandChannel GetFirstChannel()
        {
            string bandStr="";
            double chan=0.0;

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.GetFirstChannel(ref bandStr, ref chan), "GetFirstChannel failed.", TunableProtocol.MSA);
            return new BandChannel(bandStringToBand(bandStr, "GetFirstChannel"), chan);
        }

        public BandChannel GetLastChannel()
        {
            string bandStr = "";
            double chan = 0.0;
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.GetLastChannel(ref bandStr, ref chan), "GetLastChannel failed.", TunableProtocol.MSA);
            return new BandChannel(bandStringToBand(bandStr, "GetLastChannel"), chan);
        }

        public BandChannel GetITUChannel()
        {
            string bandStr = "";
            double chan = 0.0;
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.GetITUChannel(ref bandStr, ref chan), "GetITUChannel failed.", TunableProtocol.MSA);
            return new BandChannel(bandStringToBand(bandStr, "GetITUChannel"), chan);
        }

        public double ChannelSpacing()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.ChannelSpacing(), "ChannelSpacing failed.", TunableProtocol.MSA);
        }

        public string GetUnitSerialNumber_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnString(this.ixoMsaStd.GetUnitSerialNumber(), "GetUnitSerialNumber failed.", TunableProtocol.MSA);
        }

        public string GetUnitPartNumber_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnString(this.ixoMsaStd.GetUnitPartNumber(), "GetUnitPartNumber failed.", TunableProtocol.MSA);
        }

        public string GetManufactureDate_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnString(this.ixoMsaStd.GetManufactureDate(), "GetManufactureDate failed.", TunableProtocol.MSA);
        }

        public string GetSupplier_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnString(this.ixoMsaStd.GetSupplier(), "GetSupplier failed.", TunableProtocol.MSA);
        }

        public string GetModuleType()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnString(this.ixoMsaStd.GetModuleType(), "GetModuleType failed.", TunableProtocol.MSA);
        }

        public double GetLaserBiasCurrent()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.LaserBiasCurrent(), "LaserBiasCurrent failed.", TunableProtocol.MSA);
        }

        public double LaserOutputPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.LaserOutputPower(), "LaserOutputPower failed.", TunableProtocol.MSA);
        }

        public double SubMountTemp()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.SubMountTemp(), "SubMountTemp failed.", TunableProtocol.MSA);
        }

        public double RxACPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.RxACPower(), "RxACPower failed.", TunableProtocol.MSA);
        }

        public double RxAvgPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.RxAvgPower(), "RxAvgPower failed.", TunableProtocol.MSA);
        }

        public double LaserWavelength()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.LaserWavelength(), "LaserWavelength failed.", TunableProtocol.MSA);
        }

        public double ModuleTemp()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.ModuleTemp(), "ModuleTemp failed.", TunableProtocol.MSA);
        }

        public double APDTemp()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.APDTemp(), "APDTemp failed.", TunableProtocol.MSA);
        }

        public double LaserAbsoluteTemperature()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.LaserAbsoluteTemperature(), "LaserAbsoluteTemperature failed.", TunableProtocol.MSA);
        }

        public double ModBias()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return this.checkIxoliteReturnDouble(this.ixoMsaStd.ModBias(), "ModBias failed.", TunableProtocol.MSA);
        }

        public void LinkStatus()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.LinkStatus(), "LinkStatus failed.", TunableProtocol.MSA);
        }

        public int ReadMaxI2CRate()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string kbps = this.checkIxoliteReturnString(this.ixoMsaStd.ReadMaxI2CRate(), "ReadMaxI2CRate failed.", TunableProtocol.MSA);

            /* Remove "kpbs" suffix if it exists. */
            int lengthSubtractFour = kbps.Length - 4;
            if (kbps.Substring(lengthSubtractFour) == "kbps")
            {
                kbps = kbps.Substring(0, lengthSubtractFour);
            }

            return int.Parse(kbps);
        }

        public void Loopback()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            this.checkIxoliteReturnBool(this.ixoMsaStd.Loopback(), "Loopback failed.", TunableProtocol.MSA);
        }

        public bool WaitForTempToSettle(int count, double wait_s, double target_degC, double devi_degC)
        {
            int n;

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Calculate bounds */
            double lowTemp = target_degC - devi_degC;
            double highTemp = target_degC + devi_degC;

            for (n = 0; n <= count; ++n)
            {
                /* Read laser temparature. */
                double now_degC = LaserAbsoluteTemperature();

                /* Check against bounds. */
                if ((now_degC >= lowTemp) && (now_degC <= highTemp))
                {
                    /* In range, return true for success. */
                    return true;
                }

                /* else, wait. unless this is the final loop. */
                if (n != count)
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(wait_s));
                }
            }

            /* If we get here, the temperature did not settle quickly enough. */
            return false;
        }

        #endregion

        #region Wrap MSAVendorCommands.cls (Legacy)

        public void SetCalMzMod(short digipot, bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");
            checkIxoliteReturnBool(this.ixoMsaVen.SetCalMzMod(ref digipot, ref eeprom), "SetCalMzMod failed.", TunableProtocol.MSA);
        }

        public short GetCalMzMod(bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");
            return checkIxoliteReturnShort(this.ixoMsaVen.GetCalMzMod(ref eeprom), "GetCalMzMod failed.", TunableProtocol.MSA);
        }

        public void SetCalMzBiasN(short modBias, bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");
            checkIxoliteReturnBool(this.ixoMsaVen.SetCalMzBiasN(ref modBias, ref eeprom), "ZZZ failed.", TunableProtocol.MSA);
        }

        public short GetCalMzBiasN(bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");
            return checkIxoliteReturnShort(this.ixoMsaVen.GetCalMzBiasN(ref eeprom), "GetCalMzBiasN failed.", TunableProtocol.MSA);
        }

        public short GetADCReading(AdcId adc)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            byte adcAsByte = (byte)adc;
            return checkIxoliteReturnShort(this.ixoMsaVen.GetADCReading(ref adcAsByte), "GetADCReading failed.", TunableProtocol.MSA);
        }

        public short GetSerdesRegister(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnShort(this.ixoMsaVen.GetSerdesRegister(ref index), "GetSerdesRegister failed.", TunableProtocol.MSA);
        }

        public void SetSerdesRam(byte index, double value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetSerdesRam(ref index, ref value), "SetSerdesRam failed.", TunableProtocol.MSA);
        }

        public void SetSerdesRegister(byte index, double value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetSerdesRegister(ref index, ref value), "SetSerdesRegister failed.", TunableProtocol.MSA);
        }

        public void SetSerdesEEPROM(byte index, double value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetSerdesEEPROM(ref index, ref value), "SetSerdesEEPROM failed.", TunableProtocol.MSA);
        }

        public void SetRxPwrMonCalTable(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetRxPwrMonCalTable(ref index), "SetRxPwrMonCalTable failed.", TunableProtocol.MSA);
        }

        public short GetRxPwrMonCalTable(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnShort(this.ixoMsaVen.GetRxPwrMonCalTable(ref index), "GetRxPwrMonCalTable failed.", TunableProtocol.MSA);
        }

        public short GetRxPwrMonCalTableSize()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnShort(this.ixoMsaVen.GetRxPwrMonCalTableSize(), "GetRxPwrMonCalTableSize failed.", TunableProtocol.MSA);
        }

        public short GetRxPwrPoint(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnShort(this.ixoMsaVen.GetRxPwrPoint(ref index), "GetRxPwrPoint failed.", TunableProtocol.MSA);
        }

        public float GetRawOpticalPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnFloat(this.ixoMsaVen.GetRawOpticalPower(), "GetRawOpticalPower failed.", TunableProtocol.MSA);
        }

        public int GetModMonPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnInt(this.ixoMsaVen.GetModMonPower(), "GetModMonPower failed.", TunableProtocol.MSA);
        }

        public int GetMZModAmplitudeMon()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnInt(this.ixoMsaVen.GetMZModAmplitudeMon(), "GetMZModAmplitudeMon failed.", TunableProtocol.MSA);
        }

        public void SaveRxCal()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SaveRxCal(), "SaveRxCal failed.", TunableProtocol.MSA);
        }

        public void RecallRxCal()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.RecallRxCal(), "RecallRxCal failed.", TunableProtocol.MSA);
        }

        public void SetCalTxPowerMon(short uW)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetCalTxPowerMon(ref uW), "SetCalTxPowerMon failed.", TunableProtocol.MSA);
        }

        public void SetBOLLaserPower(short uW)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetBOLLaserPower(ref uW), "SetBOLLaserPower failed.", TunableProtocol.MSA);
        }

        public void SetModulator(short dac)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetModulator(ref dac), "SetModulator failed.", TunableProtocol.MSA);
        }

        public short GetModulatorDac()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return checkIxoliteReturnShort(this.ixoMsaVen.GetModulatorDac(), "GetModulatorDac failed.", TunableProtocol.MSA);
        }

        public void SaveModulator(short dac)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SaveModulator(ref dac), "SaveModulator failed.", TunableProtocol.MSA);
        }

        public void SetModMonGain(bool high)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string highStr = boolToString(high, "high");
            checkIxoliteReturnBool(this.ixoMsaVen.SetModMonGain(ref highStr), "SetModMonGain failed.", TunableProtocol.MSA);
        }

        public void SetMZLoop(bool open)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string openStr = boolToString(open, "open");
            checkIxoliteReturnBool(this.ixoMsaVen.SetMZLoop(ref openStr), "SetMZLoop failed.", TunableProtocol.MSA);
        }

        public void SetChirp(bool positive)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string positiveStr = boolToString(positive, "positive");
            checkIxoliteReturnBool(this.ixoMsaVen.SetChirp(ref positiveStr), "SetChirp failed.", TunableProtocol.MSA);
        }

        public void ModulatorDriver(bool enable)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string enableStr = boolToString(enable, "enable");
            checkIxoliteReturnBool(this.ixoMsaVen.ModulatorDriver(ref enableStr), "ModulatorDriver failed.", TunableProtocol.MSA);
        }

        public void SaveModulatorSettings()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SaveModulatorSettings(), "SaveModulatorSettings failed.", TunableProtocol.MSA);
        }

        public void RecallModulatorSettings()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.RecallModulatorSettings(), "RecallModulatorSettings failed.", TunableProtocol.MSA);
        }

        public void SetFirstChanBand(ITUBand band)
        {            
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandLetter = bandToLetter(band);
            checkIxoliteReturnBool(this.ixoMsaVen.SetFirstChanBand(ref bandLetter), "SetFirstChanBand failed.", TunableProtocol.MSA);
        }

        public void SetLastChanBand(ITUBand band)
        {            
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandLetter = bandToLetter(band);
            checkIxoliteReturnBool(this.ixoMsaVen.SetLastChanBand(ref bandLetter), "SetLastChanBand failed.", TunableProtocol.MSA);
        }

        public void SetPinModeBand(ITUBand band)
        {            
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandLetter = bandToLetter(band);
            checkIxoliteReturnBool(this.ixoMsaVen.SetPinModeBand(ref bandLetter), "SetPinModeBand failed.", TunableProtocol.MSA);
        }

        public void SetFirstChanFreq_MSA(double GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetFirstChanFreq(ref GHz), "SetFirstChanFreq failed.", TunableProtocol.MSA);
        }

        public void SetLastChanFreq(double GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetLastChanFreq(ref GHz), "SetLastChanFreq failed.", TunableProtocol.MSA);
        }

        public void SetPinModeFreq(double GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetPinModeFreq(ref GHz), "SetPinModeFreq failed.", TunableProtocol.MSA);
        }

        public void SetChanSpacing(short GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetChanSpacing(ref GHz), "SetChanSpacing failed.", TunableProtocol.MSA);
        }

        public void SaveBootData()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SaveBootData(), "SaveBootData failed.", TunableProtocol.MSA);
        }

        public void OIFLaser(bool enable)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string enableStr = boolToString(enable, "enable");
            checkIxoliteReturnBool(this.ixoMsaVen.OIFLaser(ref enableStr), "OIFLaser failed.", TunableProtocol.MSA);
        }

        public void SetSerialNumber_MSA(string value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetSerialNumber(ref value), "SetSerialNumber failed.", TunableProtocol.MSA);
        }

        public void SetManufactureDate_MSA(string date)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetManufactureDate(ref date), "ZZZ failed.", TunableProtocol.MSA);
        }

        public void SetHardwareRevision(string rev)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetHardwareRevision(ref rev), "SetHardwareRevision failed.", TunableProtocol.MSA);
        }

        public void SetPartNumber_MSA(string partno)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            checkIxoliteReturnBool(this.ixoMsaVen.SetPartNumber(ref partno), "SetPartNumber failed.", TunableProtocol.MSA);
        }

        public void SetLaserTemperature_ohms(double rTarget)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            /* Calculate ADC from rTarget. */
            double adc = 65535.0 * rTarget / (rTarget + 10000);

            checkIxoliteReturnBool(this.ixoMsaVen.SetLaserTempByAdc(ref adc), "SetLaserTempByAdc failed.", TunableProtocol.MSA);
        }

        public void SetMzTemperature_ohms(double rTarget)
        {
#warning TBD
        }

        public double GetLaserTemperature_ohms()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            double adc = checkIxoliteReturnDouble(this.ixoMsaVen.GetLaserTempByAdc(), "GetLaserTempByAdc failed.", TunableProtocol.MSA);

            /* Calculate the target back from the ADC. */
            return (10000.0 * adc) / (65535.0 - adc);
        }

        #endregion

        #region Wrap OIFStandardCommands.cls (Legacy)

        public int GetNOP()
        {
            return checkIxoliteReturnInt(this.ixoOifStd.GetNOP(), "GetNOP failed.", TunableProtocol.OIF);
            
        }

        public string GetDeviceType()
        {
            return checkIxoliteReturnString(this.ixoOifStd.GetDeviceType(), "GetDeviceType failed.", TunableProtocol.OIF);
        }

        public string GetManufacturer_OIF()
        {
            return checkIxoliteReturnString(this.ixoOifStd.GetManufacturer(), "GetManufacturer failed.", TunableProtocol.OIF);
        }

        public string GetModelNumber_OIF()
        {
            return checkIxoliteReturnString(this.ixoOifStd.GetModelNumber(), "GetModelNumber failed.", TunableProtocol.OIF);
        }

        public string GetSerialNumber_OIF()
        {
            return checkIxoliteReturnString(this.ixoOifStd.GetSerialNumber(), "GetSerialNumber failed.", TunableProtocol.OIF);
        }

        public string GetManufacturingDate_OIF()
        {
            return checkIxoliteReturnString(this.ixoOifStd.GetManufacturingDate(), "GetManufacturingDate failed.", TunableProtocol.OIF);
        }

        public string GetReleaseCode()
        {
            return checkIxoliteReturnString(this.ixoOifStd.GetReleaseCode(), "GetReleaseCode failed.", TunableProtocol.OIF);
        }

        public string GetReleaseBackwards()
        {
            return checkIxoliteReturnString(this.ixoOifStd.GetReleaseBackwards(), "GetReleaseBackwards failed.", TunableProtocol.OIF);
        }

        public void SaveConfig()
        {
            checkIxoliteReturnBool(this.ixoOifStd.SaveConfig(), "SaveConfig failed.", TunableProtocol.OIF);
        }

        public int GetIOCap()
        {
            int iocapValue=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetIOCap(ref iocapValue), "GetIOCap failed.", TunableProtocol.OIF);
            return iocapValue;
        }

        public void SetIOCap(int value)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetIOCap(ref value), "SetIOCap failed.", TunableProtocol.OIF);
        }

        public OifStatus GetStatusF()
        {
            return new OifStatus(checkIxoliteReturnInt(this.ixoOifStd.GetStatusF(), "GetStatusF failed.", TunableProtocol.OIF));
        }

        public void ClearStatusF()
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetStatusF(0x00FF), "SetStatusF failed.", TunableProtocol.OIF);
        }

        public OifStatus GetStatusW()
        {
            return new OifStatus(checkIxoliteReturnInt(this.ixoOifStd.GetStatusW(), "GetStatusW failed.", TunableProtocol.OIF));
        }

        public void ClearStatusW()
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetStatusW(0x00FF), "SetStatusW failed.", TunableProtocol.OIF);
        }

        public double GetFPowTh()
        {
            double returnValue=0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetFPowTh(ref returnValue), "GetFPowTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFPowTh(double thresh)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetFPowTh(ref thresh), "SetFPowTh failed.", TunableProtocol.OIF);
        }

        public double GetWPowTh()
        {
            double returnValue=0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetWPowTh(ref returnValue), "GetWPowTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetWPowTh(double th)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetWPowTh(ref th), "SetWPowTh failed.", TunableProtocol.OIF);
        }

        public double GetFFreqTh()
        {
            double returnValue = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetFFreqTh(ref returnValue), "GetFFreqTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFFreqTh(double th)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetFFreqTh(ref th), "SetFFreqTh failed.", TunableProtocol.OIF);
        }

        public double GetWFreqTh()
        {
            double returnValue = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetWFreqTh(ref returnValue), "GetWFreqTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetWFreqTh(double th)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetWFreqTh(ref th), "SetWFreqTh failed.", TunableProtocol.OIF);
        }

        public double GetFThermTh()
        {
            double returnValue = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetFThermTh(ref returnValue), "GetFThermTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFThermTh(double th)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetFThermTh(ref th), "SetFThermTh failed.", TunableProtocol.OIF);
        }

        public double GetWThermTh()
        {
            double returnValue = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetWThermTh(ref returnValue), "GetWThermTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetWThermTh(double th)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetWThermTh(ref th), "SetWThermTh failed.", TunableProtocol.OIF);
        }

        public OifStatus GetSRQT()
        {
            int returnValue = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetSRQT(ref returnValue), "GetSRQT failed.", TunableProtocol.OIF);
            return new OifStatus(returnValue);
        }

        public void SetSRQT(OifStatus mask)
        {
            int maskAsInt = mask.GetAsInt();
            checkIxoliteReturnBool(this.ixoOifStd.SetSRQT(ref maskAsInt), "SetSRQT failed.", TunableProtocol.OIF);
        }

        public OifStatus GetFATALT()
        {
            int returnValue = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetFATALT(ref returnValue), "GetFATALT failed.", TunableProtocol.OIF);
            return new OifStatus(returnValue);
        }

        public void SetFATALT(OifStatus mask)
        {
            int maskAsInt = mask.GetAsInt();
            checkIxoliteReturnBool(this.ixoOifStd.SetFATALT(ref maskAsInt), "SetFATALT failed.", TunableProtocol.OIF);
        }

        public OifStatus GetALMT()
        {
            int returnValue = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetALMT(ref returnValue), "GetALMT failed.", TunableProtocol.OIF);
            return new OifStatus(returnValue);
        }

        public void SetALMT(OifStatus mask)
        {
            int maskAsInt = mask.GetAsInt();
            checkIxoliteReturnBool(this.ixoOifStd.SetALMT(ref maskAsInt), "SetALMT failed.", TunableProtocol.OIF);
        }

        public void SelectChannel(short chan)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SelectChannel(ref chan), "SelectChannel failed.", TunableProtocol.OIF);
        }

        public short GetChannel()
        {
            short chan=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetChannel(ref chan), "GetChannel failed.", TunableProtocol.OIF);
            return chan;
        }

        public void SetPowerSetpoint(double pwr)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetPowerSetpoint(ref pwr), "SetPowerSetpoint failed.", TunableProtocol.OIF);
        }

        public double GetPowerSetpoint()
        {
            double pwr=0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetPowerSetpoint(ref pwr), "GetPowerSetpoint failed.", TunableProtocol.OIF);
            return pwr;
        }

        public void SoftwareEnableOutput(bool Sena)
        {
            string enableStr = boolToString(Sena, "enable");
            checkIxoliteReturnBool(this.ixoOifStd.SoftwareEnableOutput(ref enableStr), "SoftwareEnableOutput failed.", TunableProtocol.OIF);
        }

        public void ModuleReset()
        {
            checkIxoliteReturnBool(this.ixoOifStd.ModuleReset(), "ModuleReset failed.", TunableProtocol.OIF);
        }

        public void SoftReset()
        {
            checkIxoliteReturnBool(this.ixoOifStd.SoftReset(), "SoftReset failed.", TunableProtocol.OIF);
        }

        public bool GetSoftwareEnableOutput()
        {
            short sena = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetSoftwareEnableOutput(ref sena), "GetSoftwareEnableOutput failed.", TunableProtocol.OIF);
            return (sena != 0);
        }

        public bool GetADT()
        {
            int mcb=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetModuleConfigBehaviour(ref mcb), "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);
            return (mcb & 0x0002) != 0;   /* bit 1 */
        }

        public void SetADT(bool adt)
        {
            int mcb = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetModuleConfigBehaviour(ref mcb), "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);

            int newMcb = (mcb & ~0x0002) | (adt ? 0x0002 : 0x0000);  /* Write into bit 1 */
            byte newMcbAsByte = checked((byte)newMcb);
            checkIxoliteReturnBool(this.ixoOifStd.SetModuleConfigBehaviour(ref newMcbAsByte), "SetModuleConfigBehaviour failed.", TunableProtocol.OIF);
        }

        public bool GetSDF()
        {
            int mcb = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetModuleConfigBehaviour(ref mcb), "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);
            return (mcb & 0x0004) != 0;   /* bit 2 */
        }

        public void SetSDF(bool sdf)
        {
            int mcb = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetModuleConfigBehaviour(ref mcb), "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);

            int newMcb = (mcb & ~0x0004) | (sdf ? 0x0004 : 0x0000);  /* Write into bit 2 */
            byte newMcbAsByte = checked((byte)newMcb);
            checkIxoliteReturnBool(this.ixoOifStd.SetModuleConfigBehaviour(ref newMcbAsByte), "SetModuleConfigBehaviour failed.", TunableProtocol.OIF);
        }

        public void SetFrequencyGrid(short GHz)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetFrequencyGrid(ref GHz), "SetFrequencyGrid failed.", TunableProtocol.OIF);
        }

        public short GetFrequencyGrid()
        {
            short returnValue=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetFrequencyGrid(ref returnValue), "GetFrequencyGrid failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public double GetFirstChanFreq_OIF()
        {
            double returnValue = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetFirstChanFreq(ref returnValue), "GetFirstChanFreq failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFirstChanFreq_OIF(double freq)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetFirstChanFreq(ref freq), "SetFirstChanFreq failed.", TunableProtocol.OIF);
        }

        public double GetLaserFrequency()
        {
            double lf = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetLaserFrequency(ref lf), "GetLaserFrequency failed.", TunableProtocol.OIF);
            return lf;
           
        }

        public double GetLaserOutputPower()
        {
            double oop = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetLaserOutputPower(ref oop), "GetLaserOutputPower failed.", TunableProtocol.OIF);
            return oop;
        }

        public double GetCurrentTemperature()
        {
            double ctemp = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetCurrentTemperature(ref ctemp), "GetCurrentTemperature failed.", TunableProtocol.OIF);
            return ctemp;
        }

        public double GetOpticalPowerMin()
        {
            double oop = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetOpticalPowerMin(ref oop), "GetOpticalPowerMin failed.", TunableProtocol.OIF);
            return oop;
        }

        public double GetOpticalPowerMax()
        {
            double oop = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetOpticalPowerMax(ref oop), "GetOpticalPowerMax failed.", TunableProtocol.OIF);
            return oop;
        }

        public double GetLaserFirstFreq()
        {
            double rval = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetLaserFirstFreq(ref rval), "GetLaserFirstFreq failed.", TunableProtocol.OIF);
            return rval;
        }

        public void SetLaserFirstFreq(double freq)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetLaserFirstFreq(ref freq), "SetLaserFirstFreq failed.", TunableProtocol.OIF);
        }

        public double GetLaserLastFreq()
        {
            double rval = 0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetLaserLastFreq(ref rval), "GetLaserLastFreq failed.", TunableProtocol.OIF);
            return rval;
        }

        public void SetLaserLastFreq(double freq)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetLaserLastFreq(ref freq), "SetLaserLastFreq failed.", TunableProtocol.OIF);
        }

        public void SetMinFrequencyGrid(double GHz)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetMinFrequencyGrid(ref GHz), "SetMinFrequencyGrid failed.", TunableProtocol.OIF);
        }

        public short GetMinFrequencyGrid()
        {
            short rval = 0;
            checkIxoliteReturnBool(this.ixoOifStd.GetMinFrequencyGrid(ref rval), "GetMinFrequencyGrid failed.", TunableProtocol.OIF);
            return rval;
        }

        public DitherState GetDitherEnable()
        {
            int retVal=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetDitherEnable(ref retVal), "GetDitherEnable failed.", TunableProtocol.OIF);
            return new DitherState(retVal);
        }

        public void SetDitherEnable(DitherState flags)
        {
            byte de = checked((byte)(flags.GetAsInt()));
            checkIxoliteReturnBool(this.ixoOifStd.SetDitherEnable(ref de), "SetDitherEnable failed.", TunableProtocol.OIF);
        }

        public short GetDitherR()
        {
            short retVal=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetDitherR(ref retVal), "GetDitherR failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetDitherR(short dith)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetDitherR(ref dith), "SetDitherR failed.", TunableProtocol.OIF);
        }

        public double GetDitherF()
        {
            double retVal=0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetDitherF(ref retVal), "GetDitherF failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetDitherF(double dith)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetDitherF(ref dith), "SetDitherF failed.", TunableProtocol.OIF);
        }

        public double GetDitherA()
        {
            double retVal=0.0;
            checkIxoliteReturnBool(this.ixoOifStd.GetDitherA(ref retVal), "GetDitherA failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetDitherA(double dith)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetDitherA(ref dith), "SetDitherA failed.", TunableProtocol.OIF);
        }

        public double GetTCaseL()
        {
            double retVal=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetTCaseL(ref retVal), "GetTCaseL failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetTCaseL(double tcl)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetTCaseL(tcl), "SetTCaseL failed.", TunableProtocol.OIF);
        }

        public double GetTCaseH()
        {
            double retVal=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetTCaseH(ref retVal), "GetTCaseH failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetTCaseH(double tch)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetTCaseH(tch), "SetTCaseH failed.", TunableProtocol.OIF);
        }

        public short GetFAgeTh()
        {
            short retVal=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetFAgeTh(ref retVal), "GetFAgeTh failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetFAgeTh(short age)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetFAgeTh(ref age), "SetFAgeTh failed.", TunableProtocol.OIF);
        }

        public short GetWAgeTh()
        {
            short retVal=0;
            checkIxoliteReturnBool(this.ixoOifStd.GetWAgeTh(ref retVal), "GetWAgeTh failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetWAgeTh(short age)
        {
            checkIxoliteReturnBool(this.ixoOifStd.SetWAgeTh(ref age), "SetWAgeTh failed.", TunableProtocol.OIF);
        }

        #endregion

        #region Wrap OIFVendorCommand.cls (IOifBkhm) (Legacy)

        public double GetPCBTemp()
        {
            double monTemp = checkIxoliteReturnDouble(this.ixoOifVen.GetPCBTemp(), "GetPCBTemp failed.", TunableProtocol.OIF);
            if (monTemp > 327.67)
            {
                return monTemp - 655.36;
            }
            else
            {
                return monTemp;
            }
        }

        public double GetWaveCode()
        {
            return checkIxoliteReturnDouble(this.ixoOifVen.GetWaveCode(), "GetWaveCode failed.", TunableProtocol.OIF);            
        }

        public void SetWaveCode(short wc)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetWaveCode(ref wc), "SetWaveCode failed.", TunableProtocol.OIF);
        }

        public void SaveChannel(short chan)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SaveChannel(ref chan), "SaveChannel failed.", TunableProtocol.OIF);
        }

        public int GetSOADAC()
        {
            int dac = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetSOADAC(ref dac), "GetSOADAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetSOADAC(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetSOADAC(ref dac), "SetSOADAC failed.", TunableProtocol.OIF);
        }

        public int GetGainDAC()
        {
            int dac = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetGainDAC(ref dac), "GetGainDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetGainDAC(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetGainDAC(ref dac), "SetGainDAC failed.", TunableProtocol.OIF);
        }

        public int GetFrontEvenDAC()
        {
            int dac = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetFrontEvenDAC(ref dac), "GetFrontEvenDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetFrontEvenDAC(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetFrontEvenDAC(ref dac), "SetFrontEvenDAC failed.", TunableProtocol.OIF);
        }

        public int GetFrontOddDAC()
        {
            int dac = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetFrontOddDAC(ref dac), "GetFrontOddDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetFrontOddDAC(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetFrontOddDAC(ref dac), "SetFrontOddDAC failed.", TunableProtocol.OIF);
        }

        public int GetRearDAC()
        {
            int dac = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetRearDAC(ref dac), "GetRearDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetRearDAC(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetRearDAC(ref dac), "SetRearDAC failed.", TunableProtocol.OIF);
        }

        public int GetPhaseDAC()
        {
            int dac=0;
            checkIxoliteReturnBool(this.ixoOifVen.GetPhaseDAC(ref dac), "GetPhaseDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetPhaseDAC(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetPhaseDAC(ref dac), "SetPhaseDAC failed.", TunableProtocol.OIF);
        }

        public short GetDitherDAC()
        {
            short dac=0;
            checkIxoliteReturnBool(this.ixoOifVen.GetDitherDAC(ref dac), "GetDitherDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetDitherDAC(short dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetDitherDAC(ref dac), "SetDitherDAC failed.", TunableProtocol.OIF);
        }

        public int GetLockerTxMon()
        {
            int ltxmon = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLockerTxMon(ref ltxmon), "GetLockerTxMon failed.", TunableProtocol.OIF);
            return ltxmon;
        }

        public int GetLockerRefMon()
        {
            int lrefmon = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLockerRefMon(ref lrefmon), "GetLockerRefMon failed.", TunableProtocol.OIF);
            return lrefmon;
        }

        public int GetLaserPowerMon(short n)
        {
            int lpm = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLaserPowerMon(ref lpm, ref n), "GetLaserPowerMon failed.", TunableProtocol.OIF);
            return lpm;
        }

        public int GetLsPhaseSense()
        {
            int lsps = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLsPhaseSense(ref lsps), "GetLsPhaseSense failed.", TunableProtocol.OIF);
            return lsps;
        }

        public short GetTxCoarsePot()
        {
            short tcp = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetTxCoarsePot(ref tcp), "GetTxCoarsePot failed.", TunableProtocol.OIF);
            return tcp;
        }

        public void SetTxCoarsePot(short pot)
        {
            /* Ixolite function takes two references, returning back the set value. */
            short tcp=pot;
            checkIxoliteReturnBool(this.ixoOifVen.SetTxCoarsePot(ref tcp, ref pot), "SetTxCoarsePot failed.", TunableProtocol.OIF);
        }

        public short GetRefCoarsePot()
        {
            short rcp = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetRefCoarsePot(ref rcp), "GetRefCoarsePot failed.", TunableProtocol.OIF);
            return rcp;
        }

        public void SetRefCoarsePot(short rcp)
        {
            short pot = rcp;
            checkIxoliteReturnBool(this.ixoOifVen.SetRefCoarsePot(ref rcp, ref pot), "SetRefCoarsePot failed.", TunableProtocol.OIF);
        }

        public int GetPhaseMin()
        {
            int pmin=0;
            checkIxoliteReturnBool(this.ixoOifVen.GetPhaseMin(ref pmin), "GetPhaseMin failed.", TunableProtocol.OIF);
            return pmin;
        }

        public void SetPhaseMin(int pmin)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetPhaseMin(ref pmin), "SetPhaseMin failed.", TunableProtocol.OIF);
        }

        public int GetPhaseMax()
        {
            int pmax=0;
            checkIxoliteReturnBool(this.ixoOifVen.GetPhaseMax(ref pmax), "GetPhaseMax failed.", TunableProtocol.OIF);
            return pmax;
        }

        public void SetPhaseMax(int pmax)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetPhaseMax(ref pmax), "SetPhaseMax failed.", TunableProtocol.OIF);
        }

        public int GetPowerMonFactor()
        {
            double retVal = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetPowerMonFactor(ref retVal), "GetPowerMonFactor failed.", TunableProtocol.OIF);
            return (int)retVal;
        }

        public void SetPowerMonFactor(int pmf)
        {
            double pmfAsDouble = pmf;
            checkIxoliteReturnBool(this.ixoOifVen.SetPowerMonFactor(ref pmfAsDouble), "SetPowerMonFactor failed.", TunableProtocol.OIF);
        }

        public int GetLockerErrorFactor()
        {
            double retVal = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLockerErrorFactor(ref retVal), "GetLockerErrorFactor failed.", TunableProtocol.OIF);
            return (int)retVal;
        }

        public void SetLockerErrorFactor(int lef)
        {
            double lefAsDouble = lef;
            checkIxoliteReturnBool(this.ixoOifVen.SetLockerErrorFactor(ref lefAsDouble), "SetLockerErrorFactor failed.", TunableProtocol.OIF);
        }

        public void SelectChannel_OIFVen(short channel)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SelectChannel(ref channel), "SelectChannel failed.", TunableProtocol.OIF);
        }

        public int GetControlRegister()
        {
            byte upper = 0;
            byte lower = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetRawControlRegister(ref upper, ref lower), "GetRawControlRegister failed.", TunableProtocol.OIF);
            return (upper << 8) | lower;
        }

        public void SetControlRegister(int newVal)
        {
            byte upper = checked((byte)((newVal >> 8) & 0x00ff));
            byte lower = checked((byte)(newVal & 0x00ff));

            checkIxoliteReturnBool(this.ixoOifVen.SetRawControlRegister(upper, lower), "SetRawControlRegister failed.", TunableProtocol.OIF);
        }

        public double GetLaserPowerEstimate()
        {
            return checkIxoliteReturnDouble(this.ixoOifVen.GetLaserPowerEstimate(), "GetLaserPowerEstimate failed.", TunableProtocol.OIF);
        }

        public int GetPowerMonOffset()
        {
            double retVal = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetPowerMonOffset(ref retVal), "GetPowerMonOffset failed.", TunableProtocol.OIF);
            return (int)retVal;
        }

        public void SetPowerMonOffset(int pmo)
        {
            double pmoAsDouble = pmo;
            checkIxoliteReturnBool(this.ixoOifVen.SetPowerMonOffset(ref pmoAsDouble), "SetPowerMonOffset failed.", TunableProtocol.OIF);
        }

        /* The following functions call Ixolite with an "I" parameter value, meaning current, or any other value
         * for voltage. */
        public short GetSrcNumber(SrcMode srcMode)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            short retVal = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetSrcNumber(ref istr, ref retVal), "GetSrcNumber failed.", TunableProtocol.OIF);
            return retVal;
        }


        public short GetSrcIndex(SrcMode srcMode)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            short retVal = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetSrcIndex(ref istr, ref retVal), "GetSrcIndex failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSrcIndex(SrcMode srcMode, short pot)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            checkIxoliteReturnBool(this.ixoOifVen.SetSrcIndex(ref istr, ref pot), "SetSrcIndex failed.", TunableProtocol.OIF);
        }

        public void SetSrcNumber(SrcMode srcMode, short pot)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            checkIxoliteReturnBool(this.ixoOifVen.SetSrcNumber(ref istr, ref pot), "SetSrcNumber failed.", TunableProtocol.OIF);
        }

        /* Set up index and number before calling G/SetSrcValue */
        public float GetSrcValue(SrcMode srcMode)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");

            byte[] floatAsBytes = new byte[4];
            checkIxoliteReturnBool(this.ixoOifVen.GetSrcLSW(ref istr, ref floatAsBytes[1], ref floatAsBytes[0]), "GetSrcLSW failed.", TunableProtocol.OIF);
            checkIxoliteReturnBool(this.ixoOifVen.GetSrcMSW(ref istr, ref floatAsBytes[3], ref floatAsBytes[2]), "GetSrcMSW failed.", TunableProtocol.OIF);

            return System.BitConverter.ToSingle(floatAsBytes, 0);
        }

        public void SetSrcValue(SrcMode srcMode, float value)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            byte[] floatAsBytes = System.BitConverter.GetBytes(value);

            checkIxoliteReturnBool(this.ixoOifVen.SetSrcLSW(ref istr, ref floatAsBytes[1], ref floatAsBytes[0]), "SetSrcLSW failed.", TunableProtocol.OIF);
            checkIxoliteReturnBool(this.ixoOifVen.SetSrcMSW(ref istr, ref floatAsBytes[3], ref floatAsBytes[2]), "SetSrcMSW failed.", TunableProtocol.OIF);
        }

        public short GetSBSDitherMultiplier()
        {
            /* Note that the value is only 8 bits, but carried as short, duer a quirk in JC's code. */
            short retVal = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetSBSDitherMultiplier(ref retVal), "GetSBSDitherMultiplier failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSBSDitherMultiplier(byte mult)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetSBSDitherMultiplier(ref mult), "SetSBSDitherMultiplier failed.", TunableProtocol.OIF);
        }

        public double GetWavelengthError()
        {
            return checkIxoliteReturnDouble(this.ixoOifVen.GetWavelengthError(), "GetWavelengthError failed.", TunableProtocol.OIF);
        }

        public int GetBaseDac()
        {
            int retVal = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetBaseCamp(ref retVal), "GetBaseCamp failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetBaseDac(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetBaseCamp(ref dac), "SetBaseCamp failed.", TunableProtocol.OIF);
        }

        public int GetBaseDac2()
        {
            int retVal = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetBaseCamp2(ref retVal), "GetBaseCamp2 failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetBaseDac2(int dac)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetBaseCamp2(ref dac), "SetBaseCamp2 failed.", TunableProtocol.OIF);
        }

        public double GetPlateauPower()
        {
            double retVal = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetPlateauPower(ref retVal), "GetPlateauPower failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetPlateauPower(double pwr)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetPlateauPower(ref pwr), "SetPlateauPower failed.", TunableProtocol.OIF);
        }

        public double GetSinkSlope()
        {
            double retVal = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetSinkSlope(ref retVal), "GetSinkSlope failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSinkSlope(double slp)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetSinkSlope(ref slp), "SetSinkSlope failed.", TunableProtocol.OIF);
        }

        public double GetSourceSlope()
        {
            double retVal = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetSourceSlope(ref retVal), "GetSourceSlope failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSourceSlope(double slp)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetSourceSlope(ref slp), "SetSourceSlope failed.", TunableProtocol.OIF);
        }


        /* Constants for use by Control Reg Front Section code. */
        private const int oddBitLocation = 6;
        private const int evenBitLocation = 8;

        public void SetControlRegFrontSection(int i)
        {
            /* Check valid range. */
            if ((i < 1) || (i > 7))
            {
                throw new ChassisException("SetControlRegFrontSection called with " + i + ". (Must be 0..7)");
            }

            /* Calculate new value, moving ODD (i/2) and EVEN ((i-1)/2) into its slot.
             *  i   ODD  EVEN       Control register format
             *  1    00   00        .... ..ee oo.. ....
             *  2    01   00
             *  3    01   01
             *  4    10   01
             *  5    10   10
             *  6    11   10
             *  7    11   11
             */
            int newBits = ((i / 2) << oddBitLocation) | (((i - 1) / 2) << evenBitLocation);

            /* Modify control register, masking bits 6..9. */
            this.ModifyControlRegister(0x03C0, newBits);
        }

        public int GetControlRegFrontSection()
        {
            /* Get control register values */
            int regValue = this.GetControlRegister();

            /* Extract odd and even parts. */
            int odd  = (regValue >> oddBitLocation ) & 0x0003;
            int even = (regValue >> evenBitLocation) & 0x0003;

            /* Find bit 0 of i by looking at the differnce between odd and even. */
            int bit0;
            if (odd == even)
            {
                bit0 = 1;   /* If ODD == EVEN, i is ODD. */
            }
            else if (odd == (even+1))
            {
                bit0 = 0;   /* If ODD != EVEN, i is EVEN. */
            }
            else
            {
                throw new TunableModuleException("Control register has bad value. ODD=" + odd + " EVEN=" + even, TunableProtocol.OIF);
            }

            /* Bits 1 and 2 are odd value. Bit 0 is bit 0. */
            return (odd * 2) + bit0;
        }

        public void SetControlRegLockerOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(15, on);
        }

        public bool GetControlRegLockerOn()
        {
            return this.GetControlRegisterSingleBit(15);
        }

        public void SetControlRegLockerSlopeOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(14, on);
        }

        public bool GetControlRegLockerSlopeOn()
        {
            return this.GetControlRegisterSingleBit(14);
        }

        public void SetControlRegLaserTecOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(13, on);
        }

        public bool GetControlRegLaserTecOn()
        {
            return this.GetControlRegisterSingleBit(13);
        }

        public void SetControlRegLaserPSUOn(bool on)
        {
            /* Bit 10 is the Laser PSU bit. */
            this.ModifyControlRegisterSingleBit(10, on);
        }

        public bool GetControlRegLaserPSUOn()
        {
            return this.GetControlRegisterSingleBit(10);
        }

        public void SetControlRegAutoLockStart()
        {
            this.ModifyControlRegisterSingleBit(4, true);
        }

        public void SetControlRegDitherOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(3, on);
        }

        public bool GetControlRegDitherOn()
        {
            return this.GetControlRegisterSingleBit(3);
        }

        public void SetControlRegProfilingOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(1, on);
        }

        public bool GetControlRegProfilingOn()
        {
            return this.GetControlRegisterSingleBit(1);
        }
        #endregion

        #region Wrap OIFVendorCommands.cls (IWbttItlaCommon) (Legacy)

        public void SetUnitSerialNumber_OIF(string serno)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetUnitSerialNumber(ref serno), "SetUnitSerialNumber failed.", TunableProtocol.OIF);
        }

        public void SetUnitBuildDate_OIF(string bdate)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetUnitBuildDate(ref bdate), "SetUnitBuildDate failed.", TunableProtocol.OIF);
        }

        public void SetModuleNumber_OIF(string moduleNumber)
        {
            /* Ixolite VB uses this register's old name. */
            checkIxoliteReturnBool(this.ixoOifVen.SetUnitManufacturer(ref moduleNumber), "SetModuleNumber failed.", TunableProtocol.OIF);
        }

        public bool GetPowerControlStateOn()
        {
            string state = "";
            checkIxoliteReturnBool(this.ixoOifVen.GetPowerControlState(ref state), "GetPowerControlState failed.", TunableProtocol.OIF);
            return (state == "on");
        }

        public void SetPowerControlStateOn(bool enabled)
        {
            string stateStr = boolToString(enabled, "on");
            checkIxoliteReturnBool(this.ixoOifVen.SetPowerControlState(ref stateStr), "SetPowerControlState failed.", TunableProtocol.OIF);
        }

        public string GetLaserSerialNumber_OIF()
        {
            return checkIxoliteReturnString(this.ixoOifVen.GetLaserSerialNumber(), "GetLaserSerialNumber failed.", TunableProtocol.OIF);
        }

        public void SetLaserSerialNumber_OIF(string serno)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetLaserSerialNumber(ref serno), "SetLaserSerialNumber failed.", TunableProtocol.OIF);
        }

        public string GetBoardSerialNumber_OIF()
        {
            return checkIxoliteReturnString(this.ixoOifVen.GetBoardSerialNumber(), "GetBoardSerialNumber failed.", TunableProtocol.OIF);
        }

        public void SetBoardSerialNumber_OIF(string serno)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetBoardSerialNumber(ref serno), "SetBoardSerialNumber failed.", TunableProtocol.OIF);
        }

        #endregion

        #region Wrap OIFVendorCommands.cls (IWbttFcuCommon) (Legacy)

        public short GetVTECLevel()
        {
            return checkIxoliteReturnShort(this.ixoOifVen.GetVTECLevel(), "GetVTECLevel failed.", TunableProtocol.OIF);
        }

        public short GetLsBiasSense()
        {
            short lsbs = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLsBiasSense(ref lsbs), "GetLsBiasSense failed.", TunableProtocol.OIF);
            return lsbs;
        }

        public int GetLockerErrorMon()
        {
            int errmon = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLockerErrorMon(ref errmon), "GetLockerErrorMon failed.", TunableProtocol.OIF);
            return errmon;
        }

        public int GetLockerReference()
        {
            int lckref = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLockerReference(ref lckref), "GetLockerReference failed.", TunableProtocol.OIF);
            return lckref;
        }

        public short GetTxFinePot()
        {
            short tfp = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetTxFinePot(ref tfp), "GetTxFinePot failed.", TunableProtocol.OIF);
            return tfp;
        }

        public void SetTxFinePot(short pot)
        {
            short tfp = pot;
            checkIxoliteReturnBool(this.ixoOifVen.SetTxFinePot(ref tfp, ref pot), "SetTxFinePot failed.", TunableProtocol.OIF);
        }

        public short GetLockerRangePot()
        {
            short pot = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLockerRangePot(ref pot), "GetLockerRangePot failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetLockerRangePot(short pot)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetLockerRangePot(ref pot), "SetLockerRangePot failed.", TunableProtocol.OIF);
        }

        public short GetPowerMonPot()
        {
            short pot = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetPowerMonPot(ref pot), "GetPowerMonPot failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetPowerMonPot(short pot)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetPowerMonPot(ref pot), "SetPowerMonPot failed.", TunableProtocol.OIF);
        }

        public short GetAtmelReference()
        {
            return checkIxoliteReturnShort(this.ixoOifVen.GetAtmelReference(), "GetAtmelReference failed.", TunableProtocol.OIF);
        }

        #endregion

        #region Wrap OIFVendorCommands.cls (WBTT only) (Legacy)
        public void SetMinPowerSetpoint(double dBm)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetMinPowerSetpoint(ref dBm), "SetMinPowerSetpoint failed.", TunableProtocol.OIF);
        }

        public void SetMaxPowerSetpoint(double dBm)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetMaxPowerSetpoint(ref dBm), "SetMaxPowerSetpoint failed.", TunableProtocol.OIF);
        }
        #endregion

        #region Wrap OIFVendorCommands.cls (FCU only) (Legacy)
        public void SetIVSource(float value, IVSourceIndexNumbers index)
        {
            /* Set index. */
            int indexAsInt = (int)index;
            checkIxoliteReturnBool(this.ixoOifVen.SetIVSourceIndex(ref indexAsInt), "SetIVSourceIndex failed.", TunableProtocol.OIF);

            /* Set value. */
            byte[] valueAsBytes = System.BitConverter.GetBytes(value);
            string lsw = "LSW";
            checkIxoliteReturnBool(this.ixoOifVen.SetIVSource(ref lsw, ref valueAsBytes[1], ref valueAsBytes[0]), "SetIVSource failed.", TunableProtocol.OIF);
            string msw = "MSW";
            checkIxoliteReturnBool(this.ixoOifVen.SetIVSource(ref msw, ref valueAsBytes[3], ref valueAsBytes[2]), "SetIVSource failed.", TunableProtocol.OIF);

            /* Apply new settings. */
            checkIxoliteReturnBool(this.ixoOifVen.SetIVSourceDAC(), "SetIVSourceDAC failed.", TunableProtocol.OIF);
       }

        public float GetIVSource(IVSourceIndexNumbers index)
        {
            int indexAsInt = (int)index;
            checkIxoliteReturnBool(this.ixoOifVen.SetIVSourceIndex(ref indexAsInt), "SetIVSourceIndex failed.", TunableProtocol.OIF);

            byte[] valueAsBytes = new byte[4];
            string lsw = "LSW";
            checkIxoliteReturnBool(this.ixoOifVen.GetIVSource(ref lsw, ref valueAsBytes[1], ref valueAsBytes[0]), "GetIVSource failed.", TunableProtocol.OIF);
            string msw = "MSW";
            checkIxoliteReturnBool(this.ixoOifVen.GetIVSource(ref msw, ref valueAsBytes[3], ref valueAsBytes[2]), "GetIVSource failed.", TunableProtocol.OIF);

            return System.BitConverter.ToSingle(valueAsBytes, 0);
        }

        public short GetTECADC()
        {
            short tadc = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetTECADC(ref tadc), "GetTECADC failed.", TunableProtocol.OIF);
            return tadc;
        }

        public short GetLeftMZBias()
        {
            short pot = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLeftMZBias(ref pot), "GetLeftMZBias failed.", TunableProtocol.OIF);
            return pot;
        }
 
        public void SetLeftMZBias(short pot)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetLeftMZBias(ref pot), "SetLeftMZBias failed.", TunableProtocol.OIF);
       }

        public short GetRightMZBias() 
        {
            short pot = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetRightMZBias(ref pot), "GetRightMZBias failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetRightMZBias(short pot) 
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetRightMZBias(ref pot), "SetRightMZBias failed.", TunableProtocol.OIF);
        }

        public short GetMZModulation() 
        {
            short pot = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetMZModulation(ref pot), "GetMZModulation failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetMZModulation(short pot) 
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetMZModulation(ref pot), "SetMZModulation failed.", TunableProtocol.OIF);
        }

        public short GetLeftImbalance() 
        {
            return ((short)GetOifRegisterAsUint(0xD7));
        }

        public void SetLeftImbalance(short dac) 
        {
            SetOifRegisterAsUint(0xD7, dac);
        }

        public short GetRightImbalance()
        {
            return  ((short)GetOifRegisterAsUint(0xD8));
        }

        public void SetRightImbalance(short dac)
        {
            SetOifRegisterAsUint(0xD8, dac);    
        }

        public int GetRawThermisterBridgeAlt_OifBkhm()
        {
            int therm = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetRawThermisterBridgeAlt(ref therm), "GetRawThermisterBridgeAlt failed.", TunableProtocol.OIF);
            return therm;
        }
        #endregion

        #region Wrap OIFVendorCommands.cls (ITLA only) (Legacy)
        public void SetOffGridTuning(double offGrid)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetOffGridTuning(ref offGrid), "SetOffGridTuning failed.", TunableProtocol.OIF);
        }

        public double GetOffGridTuning()
        {
            double offGrid = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetOffGridTuning(ref offGrid), "GetOffGridTuning failed.", TunableProtocol.OIF);
            return offGrid;
        }

        public void UnlockModule()
        {
            checkIxoliteReturnBool(this.ixoOifVen.UnlockModule(), "UnlockModule failed.", TunableProtocol.OIF);
        }

        public short GetOffGridPositive()
        {
            short pot = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetOffGridPositive(ref pot), "GetOffGridPositive failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetOffGridPositive(short pot)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetOffGridPositive(ref pot), "SetOffGridPositive failed.", TunableProtocol.OIF);
        }

        public short GetOffGridNegative()
        {
            short pot = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetOffGridNegative(ref pot), "GetOffGridNegative failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetOffGridNegative(short pot)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetOffGridNegative(ref pot), "SetOffGridNegative failed.", TunableProtocol.OIF);
        }

        public short GetChannelCentre()
        {
            short pot=0;
            checkIxoliteReturnBool(this.ixoOifVen.GetChannelCentre(ref pot), "GetChannelCentre failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetChannelCentre(short pot)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetChannelCentre(ref pot), "SetChannelCentre failed.", TunableProtocol.OIF);
        }

        
        public int GetModeCentrePhaseDAC_OifBkhm()
        {
            return GetOifRegisterAsInt(0xDC);
        }

        public void SetModeCentrePhaseDAC_OifBkhm(int dac)
        {
            SetOifRegisterAsUInt(0xDC, checked((UInt16)dac));
        }

        public int GetBOLPhaseDAC_OifBkhm()
        {
            return GetOifRegisterAsInt(0xDD);
        }

        public void SetBOLPhaseDAC_OifBkhm(int dac)
        {
            SetOifRegisterAsUInt(0xDD, checked((UInt16)dac));
        }

        public int GetSOACalibrationTableSize_OifBkhm()
        {
            return GetOifRegisterAsInt(0xDE);
        }

        public int GetSOACalibrationTableValue_uW_OifBkhm(int idx)
        {
            SetOifRegisterAsInt(0xDE, checked((short)idx));
            return GetOifRegisterAsInt(0xDF);
        }

        public void SetSOACalibrationTableValue_uW_OifBkhm(int idx, int value)
        {
            SetOifRegisterAsInt(0xDE, checked((short)idx));
            SetOifRegisterAsInt(0xDF, checked((short)value));
        }

        public double GetLowPowerEtalonSlope()
        {
            double retVal = 0.0;
            checkIxoliteReturnBool(this.ixoOifVen.GetLowPowerEtalonSlope(ref retVal), "GetLowPowerEtalonSlope failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetLowPowerEtalonSlope(double lef)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetLowPowerEtalonSlope(lef), "SetLowPowerEtalonSlope failed.", TunableProtocol.OIF);
        }



        #endregion

        #region Parallel port pin control
        /// <summary>
        /// Sets parallel port output data register.
        /// </summary>
        /// <param name="mask">Bits to modify.</param>
        /// <param name="bits">New bit values.</param>
        public void SetParPortData(byte mask, byte bits)
        {
            this.ixoSetup.SetParPortData(mask, bits);
        }

        /// <summary>
        /// Gets parallel port input status register.
        /// </summary>
        /// <returns>Current status register value.</returns>
        public byte GetParPortStatus()
        {
            return this.ixoSetup.GetParPortStatus();
        }

        /// <summary>
        /// Gets flag indicating if the I2C reserved pins are allowed to be used.
        /// </summary>
        public bool AllowParPortI2CReserved
        {
            get { return !this.i2cInUse; }
        }

        #endregion

        #region OIF register utility functions
        /// <summary>
        /// Gets stated OIF register as an unsigned integer.
        /// </summary>
        /// <param name="oifReg">Register number to get.</param>
        /// <returns>Uint16 value inside an int. 0..64k</returns>
        public int GetOifRegisterAsUint(byte oifReg)
        {
            byte upper = 0;
            byte lower = 0;

            bool success = this.ixoOifVen.GetOifRegisterAsBytes(oifReg, ref upper, ref lower);
            string errMsg = string.Format("Get OIF 0x{0:X02} failed.", oifReg);
            this.checkIxoliteReturnBool(success, errMsg, TunableProtocol.OIF);

            return (upper << 8) | lower;
        }

        /// <summary>
        /// Sets an OIF register as an unsigned integer.
        /// </summary>
        /// <param name="oifReg">OIF register number to set.</param>
        /// <param name="ui16">New value. 0..64k only.</param>
        public void SetOifRegisterAsUint(byte oifReg, int ui16)
        {
            byte upper = checked((byte)((ui16 >> 8) & 0xff));
            byte lower = checked((byte)(ui16 & 0xff));

            bool success = this.ixoOifVen.SetOifRegisterAsBytes(oifReg, upper, lower);
            string errMsg = string.Format("Set OIF 0x{0:X02} failed.", oifReg);
            this.checkIxoliteReturnBool(success, errMsg, TunableProtocol.OIF);
        }

        public short GetOifRegisterAsInt(short reg)
        {
            byte upper = 0;
            byte lower = 0;
            checkIxoliteReturnBool(this.ixoOifVen.GetOifRegisterAsBytes(checked((byte)reg), ref upper, ref lower),
                "GetOifRegisterAsBytes(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);

            short ret = (short)((upper << 8) | lower);
            return ret;
        }

        public void SetOifRegisterAsInt(short reg, short val)
        {
            byte upperRegByte = (byte)((val & 0xff00) >> 8);
            byte lowerRegByte = (byte)(val & 0x00ff);
            checkIxoliteReturnBool(this.ixoOifVen.SetOifRegisterAsBytes(checked((byte)reg), upperRegByte, lowerRegByte),
                "SetOifRegisterAsBytes(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);
        }

        /// <summary>
        /// All DAC transactions are using 2 8 bit registers
        /// We are expecting an unsigned 16 bit value, and break out the 
        /// upper and lower bytes from this data
        /// </summary>
        /// <param name="reg"></param>
        /// <param name="val"></param>
        public void SetOifRegisterAsUInt(short reg, UInt16 val)
        {
            byte upperRegByte = (byte)((val & 0xff00) >> 8);
            byte lowerRegByte = (byte)(val & 0x00ff);
            checkIxoliteReturnBool(this.ixoOifVen.SetOifRegisterAsBytes(checked((byte)reg), upperRegByte, lowerRegByte),
                "SetOifRegisterAsBytes(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);
        }

        /// <summary>
        /// Send a string to the device.
        /// </summary>
        /// <param name="reg">Register number</param>
        /// <param name="val">The string to set.</param>
        public void SetOifRegisterAsString(short reg, string val)
        {
            checkIxoliteReturnBool(this.ixoOifVen.SetOifRegisterAsString(checked((byte)reg), ref val),
                "SetOifRegisterAsString(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);
        }

        /// <summary>
        /// Get a string from the device.
        /// </summary>
        /// <param name="reg">Register Number.</param>
        /// <returns>The string from the device.</returns>
        public string GetOifRegisterAsString(short reg)
        {
            string response = "";
            bool success = this.ixoOifVen.GetOifRegisterAsString(checked((byte)reg), ref response);
            string errMsg = string.Format("Get OIF String 0x{0:X02} failed.", reg);
            this.checkIxoliteReturnBool(success, errMsg, TunableProtocol.OIF);

            return response;
        }
        #endregion

        #region I�C command utility functions

        /// <summary>
        /// Perform an I�C transaction by calling the VB code.
        /// </summary>
        /// <remarks>Arrays will be shifted to/from 1-base internally.</remarks>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="cmdPayload">Command payload, after length byte. (Zero-based)</param>
        /// <returns>Response payload, after length byte. (Zero-based)</returns>
        private byte[] performI2cTransaction(byte cmd, params byte[] cmdPayload)
        {
            /* Prepare command payload by moving it into a 1-based array. */
            byte[] shiftedCmdPayload = new byte[cmdPayload.Length + 1];
            Array.Copy(cmdPayload, 0, shiftedCmdPayload, 1, cmdPayload.Length);

            /* Prepare buffer for response. */
            byte[] respPayload = new byte[250];
            byte actualLen = 0;

            /* Call Ixolite. */
            bool success = ixoMsaVen.TransactionWithBytes(
                cmd, shiftedCmdPayload,
                ref respPayload, ref actualLen);
            string errMsg = String.Format("I2C command 0x{0:X02} failed.", cmd);
            checkIxoliteReturnBool(success, errMsg, TunableProtocol.MSA);

            /* Shift 1-based array back into a 0-based array. */
            byte[] trimResp = new byte[actualLen];
            Array.Copy(respPayload, 1, trimResp, 0, actualLen);
            return trimResp;
        }

        /// <summary>
        /// Send an MSA Set command with a sub command and an ascii string.
        /// </summary>
        /// <param name="cmd">Command byte.</param>
        /// <param name="subcmd">Sub command byte.</param>
        /// <param name="ascStr">String holding only ascii 7bit characters.</param>
        public void SetI2cWithSubcmdAndString(byte cmd, byte subcmd, string ascStr)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = new byte[ascStr.Length + 1];
            cmdPayload[0] = subcmd;
            Encoding.ASCII.GetBytes(ascStr, 0, ascStr.Length, cmdPayload, 1);

            /* Perform transaction, ignoring response payload. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Send an MSA command witha  sub command and an ascii string padded to a given length.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command byte.</param>
        /// <param name="ascStr">String payload holding only 7-bit characters.</param>
        /// <param name="pad">Byte value to pad string with.</param>
        /// <param name="strLen">Target length of string+padding. (Not including subcmd byte.)</param>
        public void SetI2cWithSubcmdAndPaddedString(byte cmd, byte subcmd, string ascStr, byte pad, int strLen)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = new byte[strLen + 1];
            cmdPayload[0] = subcmd;

            /* If pad byte is non-zero (filled with zeros on new) fill remainder, from [1] with
             * pad bytes. */
            if (pad != 0)
            {
                for (int i = 1; i < cmdPayload.Length; ++i)
                {
                    cmdPayload[i] = pad;
                }
            }
            
            /* Copy 7bit ASCII form of string into cmdPayload. */
            Encoding.ASCII.GetBytes(ascStr, 0, ascStr.Length, cmdPayload, 1);

            /* Perform transaction, ignoring response payload. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Send an MSA Set command with byte array payload.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="payload">Set payload.</param>
        public void SetI2cWithBytes(byte cmd, params byte[] payload)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, payload);
        }

        /// <summary>
        /// Send an MSA command with sub command and array of bytes.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command byte.</param>
        /// <param name="payload">Additional payload.</param>
        public void SetI2cWithSubcmdAndBytes(byte cmd, byte subcmd, params byte[] payload)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare complete payload. */
            byte[] fullcmd = new byte[payload.Length + 1];
            fullcmd[0] = subcmd;
            Array.Copy(payload, 0, fullcmd, 1, payload.Length);

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, fullcmd);
        }

        /// <summary>
        /// Copy an array, reversing the order.
        /// </summary>
        /// <param name="src">Source array.</param>
        /// <param name="srcIndex">Index of src to start copying from.</param>
        /// <param name="dst">Destination array.</param>
        /// <param name="dstIndex">Index of dst to copy into.</param>
        /// <param name="count">Number of bytes.</param>
        private void reverseArrayCopy(byte[] src, int srcIndex, byte[] dst, int dstIndex, int count)
        {
            for (int bytePos = 0; bytePos < count; ++bytePos)
            {
                dst[dstIndex + bytePos] = src[srcIndex + (count - 1 - bytePos)];
            }
        }

        /// <summary>
        /// Convert an array of bytes in big-endian order into a signed integer. Will handle signed
        /// or unsigned values.
        /// </summary>
        /// <param name="beArray">Array of byte.</param>
        /// <param name="signExtend">True if array holds a signed value.</param>
        /// <returns>Converted signed integer.</returns>
        private int bigEndianByteArrayToSint(byte[] beArray, bool signExtend)
        {
            if (BitConverter.IsLittleEndian)
            {
                /* Create an int-sized array of zero bytes. */
                byte[] leArray = BitConverter.GetBytes((int)0);

                /* If signExtend and value is -ve (bit7 of MSB is 1),
                 * fill the array with 0xff so the resulting value is
                 * negative. Two's complement requires the the unused MSBs
                 * by filled with zeros for +ve values and 0xff for
                 * -ve values.*/
                if (signExtend && (beArray[0] >= 0x80))
                {
                    for (int i = 0; i < leArray.Length; ++i)
                    {
                        leArray[i] = 0xff;
                    }
                }

                /* Collect array lengths. */
                int leLength = leArray.Length;
                int beLength = beArray.Length;

                /* Reverse array into left hand side of leArray. */
                reverseArrayCopy(beArray, 0, leArray, 0, beLength);

                /* Return LE array converted back to int. */
                return BitConverter.ToInt32(leArray, 0);
            }
            else
            {
                return BitConverter.ToInt32(beArray, 0);
            }
        }

        /// <summary>
        /// Convert a signed integer into a big endian array of bytes. Will handle
        /// signed or unsigned values.
        /// </summary>
        /// <param name="value">Signed integer to convert.</param>
        /// <param name="beLength">Target length of array.</param>
        /// <param name="expectNegValue">True if -ve values should be expected.</param>
        /// <returns>Converted array.</returns>
        private byte[] sintToBigEndianByteArray(int value, int beLength, bool expectNegValue)
        {
            /* Check if values are in range. */
            if (beLength > 3)
            {
                throw new TunableModuleException(
                    "sintToBigEndianByteArray called with beLength of " + beLength + ". (Max 3)",
                    TunableProtocol.MSA);
            }

            /* n bytes -> 2 ** (8*n) */
            int maxAllowed = (1 << (beLength * 8)) - 1;
            int minAllowed = -maxAllowed - 1;

            if ((value < minAllowed) || (value > maxAllowed))
            {
                throw new TunableModuleException(
                    "intToBigEndianByteArray - " + value + " cannot fit in " + beLength + " bytes.",
                    TunableProtocol.MSA);
            }

            if (BitConverter.IsLittleEndian)
            {
                /* Collect bytes. Useful bytes will be to the left, LSB first. */
                byte[] leArray = BitConverter.GetBytes(value);
                int leLength = leArray.Length;

                /* Construct zero filled array for response. */
                byte[] beArray = new byte[beLength];            

                /* Reverse left portion of leArray into beArray. */
                reverseArrayCopy(leArray, 0, beArray, 0, beLength);
                return beArray;
            }
            else
            {
                throw new TunableModuleException("Big Endian architures not supported.", TunableProtocol.MSA);
            }
        }

        /// <summary>
        /// Re-write a byte array with an additional byte at the beginning.
        /// </summary>
        /// <param name="b1">Byte to prefix.</param>
        /// <param name="arr2">Byte array to sufffix.</param>
        /// <returns>Combined array.</returns>
        private byte[] prefixToByteArray(byte b1, byte[] arr2)
        {
            /* Start a new array. */
            byte[] resp = new byte[arr2.Length + 1];

            /* Copy parts. */
            resp[0] = b1;
            arr2.CopyTo(resp, 1);

            /* Return. */
            return resp;
        }

        /// <summary>
        /// Re-write a byte array with two additional bytes at the beginning.
        /// </summary>
        /// <param name="b1">First byte to prefix.</param>
        /// <param name="b2">Second byte to prefix.</param>
        /// <param name="arr2">Byte array to sufffix.</param>
        /// <returns>Combined array.</returns>
        private byte[] prefixToByteArray(byte b1, byte b2, byte[] arr2)
        {
            /* Start a new array. */
            byte[] resp = new byte[arr2.Length + 2];

            /* Copy parts. */
            resp[0] = b1;
            resp[1] = b2;
            arr2.CopyTo(resp, 2);

            /* Return. */
            return resp;
        }

        /// <summary>
        /// Set an MSA value with a subcmd and an uint converted to BE byte array.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Subcmd byte.</param>
        /// <param name="value">Uint value to send.</param>
        /// <param name="nBytes">Number of bytes to send.</param>
        public void SetI2cWithSubcmdAndBigEndianUint(byte cmd, byte subcmd, int value, int bytesInValue)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = prefixToByteArray(subcmd, sintToBigEndianByteArray(value, bytesInValue, false));

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Perform an MSA Set command with a sub command, index byte and big endian
        /// unsigned integer payload.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="index">Index.</param>
        /// <param name="value">Value to set.</param>
        /// <param name="nBytes">Number of bytes for value.</param>
        public void SetI2cWithSubcmdIndexAndBigEndianUint(byte cmd, byte subcmd, byte index, int value, int bytesInValue)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = prefixToByteArray(subcmd, index, sintToBigEndianByteArray(value, bytesInValue, false));

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Perform an MSA Set command with a sub command and big endian
        /// signed integer payload.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="value">Value to set.</param>
        /// <param name="nBytes">Number of bytes for value.</param>
        public void SetI2cWithSubcmdAndBigEndianSint(byte cmd, byte subcmd, int value, int bytesInValue)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = prefixToByteArray(subcmd, sintToBigEndianByteArray(value, bytesInValue, true));

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, cmdPayload);            
        }

        /// <summary>
        /// Perform an MSA Get command with zero command payload, returning specified number of bytes.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="n">Number of bytes expected.</param>
        /// <returns>Returned bytes.</returns>
        public byte[] GetI2cReturnNBytes(byte cmd, int n)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, new byte[0]);
            if (resp.Length != n)
            {
                throw new TunableModuleException(
                    "Module returned " + resp.Length + " bytes. (Expected " + n + ").",
                    TunableProtocol.MSA);
            }

            return resp;
        }

        /// <summary>
        /// Perform an MSA Get command, with sub command byte, returning an array of
        /// bytes, length checked.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="n">Number of bytes expected in payload.</param>
        /// <returns>Length checked array of bytes.</returns>
        public byte[] GetI2cWithSubcmdReturnNBytes(byte cmd, byte subcmd, int n)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, subcmd);
            if (resp.Length != n)
            {
                throw new TunableModuleException(
                    "Module returned " + resp.Length + " bytes. (Expected " + n + ").",
                    TunableProtocol.MSA);
            }

            return resp;
        }

        /// <summary>
        /// Perform an I2C get command with no command payload, returning response byte array.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <returns>Response byte array.</returns>
        public byte[] GetI2cReturnBytes(byte cmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return performI2cTransaction(cmd, new byte[0]);
        }

        /// <summary>
        /// Perform an I2C get command with a subcommand, returning response string.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">MSA subcommand byte</param>
        /// <returns>Response byte array.</returns>
        public string GetI2cWithSubcmdReturnString(byte cmd, byte subcmd)
        {            
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            byte[] resp = performI2cTransaction(cmd, subcmd);
            return Encoding.ASCII.GetString(resp);
        }

        /// <summary>
        /// Get an MSA register and treat response as a big endian uint.
        /// </summary>
        /// <param name="cmd">MSA command</param>
        /// <returns></returns>
        public int GetI2cReturnBigEndianUint(byte cmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Get response, convert to uint and return. */
            return bigEndianByteArrayToSint(performI2cTransaction(cmd, new byte[0]), false);
        }

        /// <summary>
        /// Get an MSA register with a sub command and return big endian unsigned integer.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <returns>Register value.</returns>
        public int GetI2cWithSubcmdReturnBigEndianUint(byte cmd, byte subcmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, subcmd);
            return bigEndianByteArrayToSint(resp, false);
        }

        /// <summary>
        /// Get an MSA register with a sub command, index and return big endian unsigned integer.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="index">Index byte.</param>
        /// <returns>Register value.</returns>
        public int GetI2cWithSubcmdAndIndexReturnBigEndianUint(byte cmd, byte subcmd, byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, subcmd, index);
            return bigEndianByteArrayToSint(resp, false);
        }

        /// <summary>
        /// Get an MSA register with a sub command and return big endian signed integer.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="nBytes">Expected number of bytes.</param>
        /// <returns>Register value.</returns>
        public int GetI2cWithSubcmdReturnReturnBigEndianSint(byte cmd, byte subcmd, int nBytes)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, collect bytes. */
            byte[] resp = GetI2cWithSubcmdReturnNBytes(cmd, subcmd, nBytes);

            /* Transform into signed int. */
            return bigEndianByteArrayToSint(resp, true);
        }

        /// <summary>
        /// Perofrm an MSA command with no payload in either direction.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        public void SendI2cCommandNoPayload(byte cmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, new byte[0]);
        }

        /// <summary>
        /// Send an MSA command with a sub command, but otherwise no
        /// payload in either direction.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        public void SendI2cCommandWithSubcmd(byte cmd, byte subcmd)
        {

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, subcmd);
        }

        #endregion
        #endregion
    }
}

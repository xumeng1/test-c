using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.Instruments;




namespace YKAQ2200
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        //private Chassis_Ag34970A testChassis;
        private Chassis_YKAQ220x testChassis;
        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_YKAQ220x_PowerSupply testInstr;
        private Inst_YKAQ220x_PowerSupply testInstr1;
        private Inst_YKAQ220x_OpticalPowerMeter testPowewrMeter;
        private Inst_YKAQ220x_311_Attenuator testVoa311;
        private Inst_YKAQ220x_331_Attenuator testVoa331;
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_YKAQ220x("MyChassis", "Chassis_YKAQ220x", "GPIB0::20::INSTR");
            TestOutput(testChassis, "Created OK");


            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Inst_YKAQ220x_PowerSupply("DMM_Instr1", "Inst_YKAQ220x_PowerSupply", "2", "1", testChassis);
            testInstr1 = new Inst_YKAQ220x_PowerSupply("DMM_Instr1", "Inst_YKAQ220x_PowerSupply", "2", "5", testChassis);
            TestOutput(testInstr, "Created OK");
            testPowewrMeter = new Inst_YKAQ220x_OpticalPowerMeter( "DMM_Instr1", "Inst_YKAQ220x_OpticalPowerMeter", "1", "1", testChassis);
            // put them online
            testVoa311 = new Inst_YKAQ220x_311_Attenuator("DMM_Instr1", "Inst_YKAQ220x_311_Attenuator", "3", "", testChassis); ;
            testVoa331 = new Inst_YKAQ220x_331_Attenuator("DMM_Instr1", "Inst_YKAQ220x_331_Attenuator", "2", "", testChassis); ;
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK"); 
            testPowewrMeter.IsOnline=true;
            testVoa311.IsOnline=true;
            testVoa331.IsOnline = true;
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            testVoa311.SetDefaultState();
            testVoa311.OutputEnabled = true;
            testVoa311.Wavelength_nm = 1565.5;
            testVoa311.Attenuation_dB = 10;
            testVoa331.SetDefaultState();
            testVoa331.OutputEnabled = true;
          double poert= testVoa331.ReadPower();
            testVoa331.Wavelength_nm = 1565.5;
            testVoa331.ZeroDarkCurrent_Start();
            testVoa331.ZeroDarkCurrent_End();
            testVoa331.Attenuation_dB = 10;
            testPowewrMeter.ZeroDarkCurrent_Start();
            testPowewrMeter.ZeroDarkCurrent_End();
            testPowewrMeter.Wavelength_nm = 1550;
            testPowewrMeter.AveragingTime_s = 0.01;
            testPowewrMeter.Range = 0;
            //testPowewrMeter.ZeroDarkCurrent_Start();
          double pow=testPowewrMeter.ReadPower();
            
            TestOutput(testInstr, testInstr.FirmwareVersion);
            TestOutput(testInstr, testInstr.HardwareIdentity);
            Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            Assert.IsNaN(Double.NaN);
            testInstr.SetDefaultState();
            
            testInstr.VoltageSetPoint_Volt=5.0;
            testInstr.OutputEnabled = false;
            testInstr.OutputEnabled = true;
          double a= testInstr.VoltageActual_Volt;
          double f=testInstr.CurrentActual_amp;
          testInstr.CurrentComplianceSetPoint_Amp = 1.2;
          testInstr1.CurrentComplianceSetPoint_Amp = 4;//out of limits
          testInstr1.VoltageSetPoint_Volt = 5;
        double b= testInstr1.VoltageActual_Volt;
        }

        [Test]
        public void T02_SecondTest()
        {

            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

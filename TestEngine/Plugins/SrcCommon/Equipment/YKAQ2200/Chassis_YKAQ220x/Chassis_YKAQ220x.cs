// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_YKAQ220x.cs
//
// Author: cocop, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    public class Chassis_YKAQ220x : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_YKAQ220x(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord aq2211Data = new ChassisDataRecord(
                  "YOKOGAWA AQ2211",
                  "0",
                 "99.99");
            ValidHardwareData.Add("AQ2211", aq2211Data);

            ChassisDataRecord aq2212Data = new ChassisDataRecord(
                  "YOKOGAWA AQ2212",
                  "0",
                 "99.99");
            ValidHardwareData.Add("AQ2212", aq2212Data);
            ChassisDataRecord aq2200Data = new ChassisDataRecord(
                 "YOKOGAWA AQ2200",
                 "0",
                "99.99");
            ValidHardwareData.Add("AQ2200", aq2200Data);


            // This chassis doesn't support reading the Status Byte in low level GPIB.
            // Inform the base class so it can adapt its calls
            this.ErrorCheckUse_488_2_Command = true;
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                this.Clear();
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);
                //string a = idn.Split(',')[3].Trim();
                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                return idn[0] + " " + idn[1];
           
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);                    
                }
            }
        }
        /// <summary>
        /// Queries an IEEE standard binary block from an Agilent Chassis
        /// </summary>
        /// <param name="cmd">Command to send to equipment</param>
        /// <param name="i">Equipment reference</param>
        /// <returns>Reply byte array</returns>
        /// <remarks>Need new version to deal with Agilent extra termination character!</remarks>
        public new byte[] QueryIEEEBinary_Unchecked(string cmd, Instrument i)
        {
            // get the file data
            byte[] bytes = base.QueryIEEEBinary_Unchecked(cmd, i);
            // Agilent appends a linefeed character after this, so need to read this back too
            // so that it doesn't mess up any future comms. (ignore it)
            this.Read_Unchecked(null);
            return bytes;
        }
        /// <summary>
        /// Specific command to try and catch empty string responses from the 6626
        /// (put in as a result of integration test issue)
        /// </summary>
        /// <param name="cmd">Our string command</param>
        /// <param name="instr">the instrument</param>
        /// <returns>the string response</returns>
        public string EmptyString_QueryUnchecked(string cmd, Instrument instr)
        {
            string result;
            bool valid = false;
            int count = 0;
            do
            {
                result = this.Query_Unchecked(cmd, instr);

                if (result == "")
                {
                    // Sleep for 500ms waiting for the DUT to stabilise
                    System.Threading.Thread.Sleep(500);
                    count++;
                }
                else
                {
                    valid = true;
                }
            } while ((!valid) && (count < 3));

            return (result);
        }
        #endregion
    }
}

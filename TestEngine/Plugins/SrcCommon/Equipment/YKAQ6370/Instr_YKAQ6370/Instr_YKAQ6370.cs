// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_AQ6370.cs
//
// Author: echoxl.wang, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using System.IO;





namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.

    // YokoGaWa AQ6370 Optical Spectrum Analysis driver

    public class Instr_YKAQ6370 : InstType_OSA
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        
        public Instr_YKAQ6370(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_AQ6370 = new InstrumentDataRecord(
                "YOKOGAWA AQ6370B",				// hardware name 
                "0.0",  			// minimum valid firmware version 
                "99.99");			// maximum valid firmware version 
            ValidHardwareData.Add("AQ6370", instr_AQ6370);



            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassis_AQ6370 = new InstrumentDataRecord(
                "Chassis_YKAQ6370",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("AQ6370", chassis_AQ6370);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_YKAQ6370)chassisInit;

            string subFile = DateTime.Now.ToString().Replace('/', '_');
            subFile = subFile.Replace(':', '_');
            subFile = subFile.Replace(' ', '_');
            errorFile = this.errorLogFilePath + "_" + subFile + ".txt";
            
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string[] idn = this.instrumentChassis.Query_Unchecked("*IDN?", null).Split(',');

                // idn[0]: manufacture name: YOKOGAWA
                // idn[1]: instrument name
                // idn[2]: serial number (9 digit string)
                // idn[3]: firmware version
                return idn[3];//
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string[] idn = this.instrumentChassis.Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];//
            }
        }

        public override void SetDefaultState()
        {
            // Send reset command to instrument
            //this.Reset(); mqn taken out as it takes too long and it may reset auto align and mess with the spectral rbw
            // set scan area to C-Band
            this.WavelengthStart_nm = 1525;
            this.WavelengthStop_nm = 1565;
            this.Amplitude_dBperDiv = 10;
            this.TracePoints = 2001;
            // this.VideoAverage = 0;
            //this.VideoBandwidth = 100;
            this.ResolutionBandwidth = 1;
            this.SweepMode = SweepModes.Single;
            this.MarkerOn = true;
            // start single sweep
            this.SweepMode = SweepModes.Single;
            SetDataFormat_ASC();
            //this.Timeout_ms = 2000; // default time out is 300 ms
            
        }
        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    this.SetDefaultState();
                }
            }
        }
        #endregion

        #region OSA InstrumentType property overrides

        /// <summary>
        /// Reads/sets the display amplitude for the OSA channel in dB/Div. 
        /// </summary>
        /// <value>
        /// Set value has to be between 0.01 and 20 db/Div.
        /// </value>
        public override double Amplitude_dBperDiv
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":DISP:WIND:TRAC:Y1:SCAL:PDIV?", this);
                string response = this.TryWriteEquipCommand(":DISP:WIND:TRAC:Y1:SCAL:PDIV?", true);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                string command = ":DISP:WIND:TRAC:Y1:SCAL:PDIV " + value + " DB";
                this.TryWriteEquipCommand(command,false);
                
            }
        }

        /// <summary>
        /// This property is only partially available on this instrument (set only).
        /// </summary>
        public override bool AutoRange
        {
            get
            {
                throw new InstrumentException("AQ6370 have not this function!");

            }
            set
            {
                throw new InstrumentException("AQ6370 have not this function!");
            }
        }

        /// <summary>
        /// Returns the display trace, as an array of numeric values. 
        /// </summary>
        public override InstType_OSA.OptPowerPoint[] GetDisplayTrace
        {
            get
            {
                // read amplitude values from OSA 

                int numPoints = this.TracePoints;

                string returnStr = this.TryWriteEquipCommand(":TRAC:DATA:Y? TRA", true);

                //string[] response = instrumentChassis.Query_Unchecked(":TRAC:DATA:Y? TRA", this).Split(',');
                string[] response = returnStr.Split(',');

                InstType_OSA.OptPowerPoint[] trace = new OptPowerPoint[numPoints];
                //double waveLength = this.WavelengthStart_nm;
                //double waveLengthStep = this.WavelengthSpan_nm / (numPoints - 1);

                //for (int index = 0; index < numPoints; index++)
                //{
                //    trace[index].power_dB = response[index];
                //    trace[index].wavelength_nm = Math.Round(waveLength, 2);
                //    waveLength += waveLengthStep;
                //}
                returnStr = this.TryWriteEquipCommand(":TRAC:DATA:X? TRA", true);
                //string[] waveArray = instrumentChassis.Query_Unchecked(":TRAC:DATA:X? TRA", this).Split(',');
                string[] waveArray = returnStr.Split(',');
                for (int index = 0; index < numPoints; index++)
                {
                    trace[index].power_dB = Double.Parse(response[index]);
                    trace[index].wavelength_nm = Double.Parse(waveArray[index]);
                }

                return trace;

            }
        }

        /// <summary>
        /// Reads the present marker amplitude for the OSA channel.
        /// </summary>
        /// <remarks>
        /// Throws an exception if the instrument does not return the level in dBm.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerAmplitude_dBm
        {
            get
            {
                // Query the instrument

                //string response = instrumentChassis.Query_Unchecked(":CALC:MARK:Y? 0", this);
                string response = this.TryWriteEquipCommand(":CALC:MARK:Y? 0", true);

                markerOn = true;  // remember that marker is now on

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
        }

        /// <summary>
        /// Shows/hides the marker for the OSA channel. 
        /// </summary>
        /// <remarks>
        /// Since the instrument does not have way to read whether a marker is active or not,
        /// this is handled using a private variable that is set to true every time the marker
        /// activated and false when all markes are erased.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        public override bool MarkerOn
        {
            get
            {
                // return soft buffer
                return markerOn;
            }
            set
            {
                string command;
                if (value)
                {
                    // turns marker 1 (by default)ON
                    command = ":CALC:MARK ON";
                    markerOn = true;  // remember that marker is now on
                }
                else
                {
                    // turns all markers OFF
                    command = ":CALC:MARK:AOFF";
                    markerOn = false;  // remember that marker is now off
                }
                this.TryWriteEquipCommand(command,false);
            }
        }

        /// <summary>
        /// Reads/sets the marker wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between the current start and stop wavelengths.
        /// </value>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerWavelength_nm
        {
            get
            {
                // Query the instrument   

                //string response = instrumentChassis.Query_Unchecked(":CALC:MARK:X? 0", this);
                string response = this.TryWriteEquipCommand(":CALC:MARK:X? 0", true);

                markerOn = true;  // remember that marker is now on

                // Convert response to double and return
                return (Convert.ToDouble(response) * 1e9);
            }
            set
            {
                // Check whether value is in correct range
                if (value < this.WavelengthStart_nm || value > this.WavelengthStop_nm)
                {
                    throw new InstrumentException("Marker Wavelength must be between " + this.WavelengthStart_nm +
                                         " and " + this.WavelengthStop_nm + " nm. Set value = " + value);
                }
                else
                {
                    // Write the value to the instrument
                    string command = ":CALC:MARK:X " + (value / 1e9) + "NM";
                    this.TryWriteEquipCommand(command,false); 
                }
            }
        }

        /// <summary>
        /// Reads/sets the point averaging for the OSA channel. 
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int PointAverage
        {
            get
            {
                throw new InstrumentException("AQ6370 have not this function!");
            }
            set
            {
                throw new InstrumentException("AQ6370 have not this function!");

            }
        }

        /// <summary>
        /// Reads/sets the reference level for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -90 and +30 dBm.
        /// </value>
        public override double ReferenceLevel_dBm
        {
            get
            {
                // Query the instrument

                //string response = instrumentChassis.Query_Unchecked(":DISP:WIND:TRAC:Y1:SCAL:RLEV?", this);
                string response = this.TryWriteEquipCommand(":DISP:WIND:TRAC:Y1:SCAL:RLEV?", true);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < -90 || value > 30)
                {
                    throw new InstrumentException("Reference level must be between -90 and +30 dBm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    //instrumentChassis.Write_Unchecked(":DISP:WIND:TRAC:Y1:SCAL:RLEV " + value + "DBM", this);
                    this.TryWriteEquipCommand(":DISP:WIND:TRAC:Y1:SCAL:RLEV " + value + "DBM", false);
                }
            }
        }

        /// <summary>
        /// Reads/sets the resolution bandwidth for the OSA channel in NM.  
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 0.06, 0.07, 1, 2, and 5 nm.
        /// </value>
        public override double ResolutionBandwidth
        {
            get
            {

                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":SENS:BWID:RES?", this);
                string response = this.TryWriteEquipCommand(":SENS:BWID:RES?",true);
                // Convert response to double and return
                return (Convert.ToDouble(response) * 1e9);

            }
            set
            {
                // Write the value to the instrument
                string command = ":SENS:BWID:RES " + value + "NM";
                this.TryWriteEquipCommand(command,false);
            }
        }

        /// <summary>
        /// Returns the executional status for the OSA channel. 
        /// </summary>
        /// <value>
        /// Busy is returned when the OSA is in the process of measuring a spectrum.
        /// Otherwise Status, ie Dataready is returned.  If in continuous sweep mode we will 
        /// get busy status until the first sweep has completed, at which point Data is ready
        /// to be collected so status will change
        /// </value>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.ChannelState Status
        {
            get
            {
                if (mStatus != ChannelState.Busy)
                {
                    return (mStatus);
                }
                TimeSpan duration = DateTime.Now - mStatusStart;
                if (duration.TotalMilliseconds > mSatusPollDelayms) // impliments a none blocking delay
                {
                    try
                    {
                        // check if standard event register has bit set

                        byte esrReg = instrumentChassis.StandardEventRegister;
                        Int32 esrValue = Convert.ToInt32(esrReg);
                        if (esrValue == 0)
                        {
                            mStatus = ChannelState.DataReady;
                        }

                    }
                    catch (ChassisException ce)
                    {
                        // IGNORE 101 no peak found
                        if (!ce.Message.Contains("Failed VISA Query")) // ignore reset lock up
                        {
                            if (!ce.Message.Contains("101 :")) throw new ChassisException("Status generated unexpected OSA error", ce);
                        }
                    }

                    // prepare next poll pause
                    mStatusStart = DateTime.Now;
                }
                // check for OSA Lock up
                duration = DateTime.Now - mDTStartWaitTime;
                if (duration.TotalSeconds > mStatusTimeOutSeconds)
                {
                    throw new InstrumentException("The OSA appears to have locked up, suggest power cycle");
                }
                return mStatus;
            }

        }


        /// <summary>
        /// Reads/sets the sweep capture mode for the OSA channel.
        /// </summary>
        /// <value>
        /// Triggered mode is not available.
        /// Unspecified mode cannot be set.
        /// </value>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>

        public override InstType_OSA.SweepModes SweepMode
        {
            get
            {
                return mode;
            }
            set
            {
                // Select command
                string command = "";
                switch (value)
                {
                    case SweepModes.Single:
                        {
                            command = ":INIT:SMODE SINGLE";
                            //instrumentChassis.Write_Unchecked(command, this);
                            break;
                        }
                    case SweepModes.Continuous:
                        {
                            command = ":INIT:SMODE REPEAT";  // Start continuous sweep
                            // instrumentChassis.Write_Unchecked(command, this);
                            break;
                        }
                    case SweepModes.Unspecified:
                        {
                            throw new InstrumentException("Cannot set to 'Unspecified' mode.");

                        }
                    case SweepModes.Triggered:
                        {
                            throw new InstrumentException("'Triggered' mode not available.");
                        }
                }

                // Write the command to the instrument
               // instrumentChassis.Write_Unchecked(command, this);
                this.TryWriteEquipCommand(command, false);
                this.mode = value;
            }
        }

        /// <summary>
        /// Reads/sets the number of trace points for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 51, 101, 201, 501, 1001, 2001, and 5001.
        /// </value>
        public override int TracePoints
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":SENS:SWE:POIN?", this);
                string response = this.TryWriteEquipCommand(":SENS:SWE:POIN?", true);
                //string response = instrumentChassis.Query_Unchecked(":TRAC:SNUM?", this);
                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                // Write the value to the instrument
                //instrumentChassis.Write_Unchecked(":SENS:SWE:POIN " + value, this);
                this.TryWriteEquipCommand(":SENS:SWE:POIN " + value, false);
            }
        }

        /// <summary>
        /// Reads/sets the video averaging value for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int VideoAverage
        {
            get
            {
                throw new InstrumentException("AQ6370 have not this function!");
            }
            set
            {
                throw new InstrumentException("AQ6370 have not this function!");

            }
        }

        /// <summary>
        /// Reads/sets the video bandwidth for the OSA channel. (HZ) 
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 10, 100, 1k, 10k, 100k, and 1MHz.  Pass HZ value in
        /// </value>
        public override double VideoBandwidth
        {
            get
            {
                throw new InstrumentException("AQ6370 have not this function!");
            }
            set
            {
                throw new InstrumentException("AQ6370 have not this function!");
            }
        }

        /// <summary>
        /// Reads/sets the centre wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value in nm.
        /// </value>
        public override double WavelengthCentre_nm
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":SENS:WAV:CENT?", this);
                string response = this.TryWriteEquipCommand(":SENS:WAV:CENT?", true);
                // Convert response to double and return
                return (Convert.ToDouble(response) * 1e9);
            }
            set
            {
                // Write the value to the instrument
                string command = ":SENS:WAV:CENT " + value + "NM";
                this.TryWriteEquipCommand(command,false);
                
            }
        }

        /// <summary>
        /// Reads/sets the wavelength offset for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value in nm
        /// </value>
        public override double WavelengthOffset_nm
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked("SENS:CORR:WAV:SHIFT?", this);
                string response = this.TryWriteEquipCommand("SENS:CORR:WAV:SHIFT?", true);
                // Convert response to double and return,nm-->m
                return (Convert.ToDouble(response) * 1e9);

            }
            set
            {
                //Write the value to the instrument
                string command = "SENS:CORR:WAV:SHIFT " + value + "NM";
                this.TryWriteEquipCommand(command,false);
            }
        }





        /// <summary>
        /// Reads/sets the wavelength span for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value in nm
        /// </value>
        public override double WavelengthSpan_nm
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":SENS:WAV:SPAN?", this);
                string response = this.TryWriteEquipCommand(":SENS:WAV:SPAN?",true);
                // Convert response to double and return
                return (Convert.ToDouble(response) * 1e9);
            }

            set
            {
                // Write the value to the instrument
                string command = ":SENS:WAV:SPAN " + value + "NM";

                this.TryWriteEquipCommand(command,false);
            }

        }

        /// <summary>
        /// Reads/sets the start wavelength for the OSA channel (left-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1750nm.
        /// </value>
        public override double WavelengthStart_nm
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":SENS:WAV:STAR?", this);
                string response = this.TryWriteEquipCommand(":SENS:WAV:STAR?", true);
                // Convert response to double and return in NM (not Metres)
                return (Convert.ToDouble(response) * 1e9);
            }
            set
            {
                // Write the value to the instrument
                string command = ":SENS:WAV:STAR " + value + "NM";

                this.TryWriteEquipCommand(command,false);
                
            }
        }

        /// <summary>
        /// Reads/sets the stop wavelength for the OSA channel (right-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value in nm.
        /// </value>
        public override double WavelengthStop_nm
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":SENS:WAV:STOP?", this);
                string response = this.TryWriteEquipCommand(":SENS:WAV:STOP?", true);
                // Convert response to double and return in NM (not Metres)
                return (Convert.ToDouble(response) * 1e9);
            }
            set
            {
                // Write the value to the instrument
                string command=":SENS:WAV:STOP " + value + "NM";

                this.TryWriteEquipCommand(command,false);
            }
        }

        #endregion


        public void AutoMeasure()
        {
            
            //this.instrumentChassis.Write_Unchecked(":INIT:SMODE AUTO", this);
            this.TryWriteEquipCommand(":INIT:SMODE AUTO", false);
            //this.instrumentChassis.Write_Unchecked(":INIT", this);
            this.TryWriteEquipCommand(":INIT", false);

        }

        #region OSA InstrumentType function overrides

        /// <summary>
        /// Moves the marker to the minimum signal point on the display. 
        /// </summary>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToMinimum()
        {

               // Write the value to the instrument
                //instrumentChassis.Write_Unchecked(":CALC:MARK:MIN", this);
            this.TryWriteEquipCommand(":CALC:MARK:MIN", false);
            markerOn = true;
           
        }

        /// <summary>
        /// Moves the marker to the next peak to the left/right on the display.
        /// </summary>
        /// <param name="directionToMove">
        /// Enumeration representing the direction in which to search for the next peak.
        /// If unspecified is selected, the next peak is searched.
        /// </param>
        /// <remarks>
        /// Throws an exception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToNextPeak(InstType_OSA.Direction directionToMove)
        {
            string direction = "";

            switch (directionToMove)
            {
                case Direction.Left: direction = "LEFT";
                    break;
                case Direction.Right: direction = "RIGH";
                    break;
                case Direction.Unspecified: direction = "NEXT";
                    break;
            }
               //instrumentChassis.Write_Unchecked(":CALC:MARK:MAX:" + direction, this);
            this.TryWriteEquipCommand(":CALC:MARK:MAX:" + direction, false);
            markerOn = true;  // remember that marker is now on
            

        }

        /// <summary>
        /// Moves the marker to the maximum signal point on the display.
        /// </summary>
        /// <remarks>
        /// Throws an exception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToPeak()
        {
            this.TryWriteEquipCommand(":CALC:MARK:MAX", false);
            markerOn = true;  // remember that marker is now on
        }

        /// <summary>
        /// Starts a measurement sweep, and returns immediately.
        /// </summary>
        /// <remarks>
        /// Not using the *opc, as we would then need to set up timeouts
        /// based on num of points in sweeps etc, the onus here is on the user
        /// to start sweep and wait however long they need to before trying to read back results
        /// Status() property should be used to determine sweep progress/completion. 
        /// </remarks>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStop"/>
        public override void SweepStart()
        {
            //will initiate a new measurement sweep
            // prepare the test status
            //this.Timeout_ms = 300;
            this.PrepareForWait(2, 0x02, 200, 150);
           // instrumentChassis.Write_Unchecked(":INIT:IMM", this);
            this.TryWriteEquipCommand(":INIT:IMM", false);
        }

        /// <summary>
        /// Stops an active measurement sweep. 
        /// </summary>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        public override void SweepStop()
        {
           // instrumentChassis.Write_Unchecked(":ABOR", this);
            this.TryWriteEquipCommand(":ABOR", false);
        }


        /// <summary>
        /// Gets / sets the internal attenuator of the osa on or off
        /// in YkAq6370, this function is not available
        /// </summary>
        public override bool Attenuator
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
        }
        #endregion

        #region Instrument specific commands

        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        public void Reset()
        {
            // Start the reset
            this.PrepareForWait(2, 0x10, 1000, 100);
            //instrumentChassis.Write_Unchecked("*RST", this);
            this.TryWriteEquipCommand("*RST", false);
            //while (this.Status == ChannelState.Busy) ;

            markerOn = false; // remember that marker is now off
        }
        /// <summary>
        /// sets the level difference of the mode judgement criteria used for peak search or waveform analysis 
        /// set value from 0.01 to 50 db (0.01 step);
        /// </summary>
        public double PeakExcursion
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":CALC:PAR:WFB? NWAV,MDIF", this);
                string response = this.TryWriteEquipCommand(":CALC:PAR:WFB? NWAV,MDIF", true);
                // Convert response to double and return in NM (not Metres)
                return (Convert.ToDouble(response));
            }
            set
            {
               // instrumentChassis.Write_Unchecked(":CALC:PAR:WFB NWAV,MDIF," + value.ToString(), this);
                this.TryWriteEquipCommand(":CALC:PAR:WFB NWAV,MDIF," + value.ToString(), false);
            }
        }
        
        /// <summary>
        /// Clear the last state of the ESR register, to be done before any sweep or marker find
        /// </summary>
        /// <param name="esrRegister">Number of the ESR register. (1-3)</param>
        private void ClearESR()
        {

            //instrumentChassis.Query_Unchecked("*ESR?", this);
            this.TryWriteEquipCommand("*ESR?", true);

            mStatus = ChannelState.Unspecified; // do not know the state yet

        }

        /// <summary>
        /// clear any exisiting errors
        /// should be called before any command that has a CheckOSAErrors call after it, apart from Status
        /// </summary>
        private void ClearOSAErrors()
        {
            //string response = instrumentChassis.Query_Unchecked("*ESR?", this); // clear errors
            this.TryWriteEquipCommand("*ESR?", true);
        }
        /// <summary>
        /// Send a command and check that it has been set
        /// </summary>
        /// <param name="strCommand">The command header.</param>
        /// <param name="dblValue">Command value.</param>
        /// <param name="dblTimeOutSeconds">Time out for the retry loop.</param>
        /// <param name="intDecPlaces">TNumber of decimal points for the dblValue to return compare.</param>
        private void ChassisWriteWithCheck(string strCommand, double dblValue, double dblTimeOutSeconds, int intDecPlaces)
        {
            string response;
            DateTime StartTime;
            TimeSpan duration;
            StartTime = DateTime.Now;
            Int32 loops = 0; // used to introduce a pause on retry
            dblValue = Math.Round(dblValue, intDecPlaces);  // just make sure the caller does as expected
            do
            {
                //response = instrumentChassis.Query_Unchecked(strCommand + "?", this);  // what is there
                response = this.TryWriteEquipCommand(strCommand + "?", true);
                if (response.Contains(",") == true) response = response.Split(',')[0]; // peel off first element if marker
                if (Math.Round(Convert.ToDouble(response), intDecPlaces) == dblValue) break;          // does it compare

                instrumentChassis.Write_Unchecked(strCommand + " " + dblValue, this);     // no so send it
                duration = DateTime.Now - StartTime;

                if (loops++ > 0) System.Threading.Thread.Sleep(50);
            } while (duration.TotalSeconds < dblTimeOutSeconds);
        }

        private void PrepareForWait(byte bytReg, byte bytStatusBitMask, int PollIntervalms, int TimeOutSeconds)
        {
            // store the register and bit mask to be used in status
            mCurrentStatusRegister = bytReg;
            mCurrentStatusBitMask = bytStatusBitMask;
            mSatusPollDelayms = PollIntervalms;
            mStatusStart = DateTime.Now;

            // clear any existing status bits
            this.ClearESR();
            // clear general error register
            this.ClearOSAErrors();
            // mark current status as busy
            mStatus = ChannelState.Busy;
            // prepare start 
            mDTStartWaitTime = DateTime.Now;
            mStatusTimeOutSeconds = TimeOutSeconds;
        }


        /// <summary>
        /// Auto Align Calibration of the OSA, with the largest signal found in full span
        /// </summary>
        public override void AutoAlign() // page 100
        {
            // Start the reset
            //instrumentChassis.Write_Unchecked(":CAL:ALIG", this);
            this.TryWriteEquipCommand(":CAL:ALIG", false);
            //System.Threading.Thread.Sleep(500);

        }

        /// <summary>
        /// Returns the actual resulution bandwidth as displayed on the screen
        /// </summary>
        public double ResolutionBandwidth_Actual
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":SENS:BAND:BWID:RES?", this);
                string response = this.TryWriteEquipCommand(":SENS:BAND:BWID:RES?", true);
                // Convert response to double and return
                return Convert.ToDouble(response);

            }
        }



        /// <summary>
        /// Sets or returns the screen title text
        /// </summary>
        public string ScreenTitle
        {
            get
            {
                // Query the instrument   
                //string response = instrumentChassis.Query_Unchecked(":DISP:TEXT:DATA?", this);
                string response = this.TryWriteEquipCommand(":DISP:TEXT:DATA?", true);
                // Return
                return response;
            }

            set
            {
                string title = "'" + value.Trim() + "'";
                //instrumentChassis.Write_Unchecked(":DISP:TEXT:DATA " + title, this);
                this.TryWriteEquipCommand(":DISP:TEXT:DATA " + title, false);
            }
        }



        #endregion
        /// <summary>
        /// set/read noise band width
        /// </summary>
        public double NoiseBandWidth_nm
        {
            get
            {
                //string resp = instrumentChassis.Query_Unchecked(":CALC:PAR:WDM:NBW?", this);
                string resp = this.TryWriteEquipCommand(":CALC:PAR:WDM:NBW?", true);
                // convert bW unit from M to nm
                return (Convert.ToDouble(resp) * 1e9);
            }
            set
            {
                string command = ":CALC:PAR:WDM:NBW " + value + "nm";
                //instrumentChassis.Write_Unchecked(command, this);
                this.TryWriteEquipCommand(command, false);

            }
        }

        public double OSNR_dB
        {
            get
            {
                //instrumentChassis.Write_Unchecked(":CALC:CAT WDM", this);
                this.TryWriteEquipCommand(":CALC:CAT WDM", false);
                //set SNR calculation mode , in this mode, SNR uise trace A nor trace A and trace B
                //instrumentChassis.Write_Unchecked(":CALC:PAR:WDM:DUAL OFF", this);
                this.TryWriteEquipCommand(":CALC:PAR:WDM:DUAL OFF", false);
                //begin sweep
                //instrumentChassis.Write_Unchecked(":CALC", this);
                this.TryWriteEquipCommand(":CALC", false);
                //read result data
                //string response = instrumentChassis.Query_Unchecked(":CALC:DATA:CSNR?", this);
                string response = this.TryWriteEquipCommand(":CALC:DATA:CSNR?", true);
                return Convert.ToDouble(response);
            }
        }


        /// <summary>
        /// auto sweep point , on: auto point
        /// </summary>
        public Boolean AutoSweepPoint
        {
            get
            {
                string command = ":SENS:SWE:POINT:AUTO?";
                //string rep = this.instrumentChassis.Query_Unchecked(command, this);
                string rep = this.TryWriteEquipCommand(command, true);
                if (Convert.ToInt32(rep) == 0)
                    return false;
                if (Convert.ToInt32(rep) == 1)
                    return true;
                else
                    throw new Exception("Invalid auto sweep point!");

            }
            set
            {
                string command = ":SENS:SWE:POINT:AUTO ";
                string subCommd = "";
                if (value)
                    subCommd = "ON";
                else
                    subCommd = "OFF";
                command = command + subCommd;
                //this.instrumentChassis.Write_Unchecked(command, this);
                this.TryWriteEquipCommand(command, false);
            }
        }

        /// <summary>
        /// Set single sweep mode
        /// </summary>
        public void SetSingleSweepMode()
        {
            string command = ":INIT:SMODE 1";
            //this.instrumentChassis.Write_Unchecked(command, this);
            this.TryWriteEquipCommand(command, false);
        }

        /// <summary>
        /// read/set sensitivity
        /// </summary>
        public SensitivityEnum SensorSensitivity
        {
            get
            {
                string command = ":SENS:SENS?";
                //string rep = this.instrumentChassis.Query_Unchecked(command, this);
                string rep = this.TryWriteEquipCommand(command, true);
                switch (Int32.Parse(rep))
                {
                    case 0: return SensitivityEnum.NHLD;
                    case 1: return SensitivityEnum.NAUT;
                    case 2: return SensitivityEnum.MID;
                    case 3: return SensitivityEnum.HIGH1;
                    case 4: return SensitivityEnum.HIGH2;
                    case 5: return SensitivityEnum.HIGH3;
                    case 6: return SensitivityEnum.NORMAL;
                    default: throw new Exception("invalid sensor sensitivity number!");
                }
            }
            set
            {
                string command = ":SENS:SENS ";
                string subcommand = value.ToString();

                switch (value)
                {
                    case SensitivityEnum.NHLD: subcommand = "NHLD"; break;
                    case SensitivityEnum.NAUT: subcommand = "NAUT"; break;
                    case SensitivityEnum.NORMAL: subcommand = "NORMAL"; break;
                    case SensitivityEnum.MID: subcommand = "MID"; break;
                    case SensitivityEnum.HIGH1: subcommand = "HIGH1"; break;
                    case SensitivityEnum.HIGH2: subcommand = "HIGH2"; break;
                    case SensitivityEnum.HIGH3: subcommand = "HIGH3"; break;
                    default: throw new Exception("invalid sensitivity parameter!");
                }

                command = command + subcommand;
                //this.instrumentChassis.Write_Unchecked(command, this);
                this.TryWriteEquipCommand(command, false);
            }
        }



        /// <summary>
        /// Clear the instrument register
        /// </summary>
        public void Clear()
        {
            //this.instrumentChassis.Write_Unchecked("*CLS", this);
            string command = "*CLS";
            this.TryWriteEquipCommand(command,false);
            //this.instrumentChassis.Write_Unchecked("BLANK TRB", this);
        }

        /// <summary>
        /// the unit of display for the marker value
        /// </summary>
        public MarkerUnitEnum MarkerUnit
        {
            get
            {
                string command = ":CLC:MARK:UNIT?";
                //string resp = this.instrumentChassis.Query_Unchecked(command, this);
                string resp = this.TryWriteEquipCommand(command, true);
                if (Int32.Parse(resp) == 1)
                    return MarkerUnitEnum.FREQ; //resp=1
                else
                    return MarkerUnitEnum.WAV;  //resp=0
            }
            set
            {
                string command = ":CLC:MARK:UNIT ";
                string subcommand = value.ToString();
                command = command + subcommand;
                //this.instrumentChassis.Write_Unchecked(command, this);
                this.TryWriteEquipCommand(command, false);
            }
        }
        /// <summary>
        /// set data format as ASCii code
        /// </summary>
        public void SetDataFormat_ASC()
        {
            //this.instrumentChassis.Write_Unchecked(":FORM:DATA ASC", this);
            this.TryWriteEquipCommand(":FORM:DATA ASC", false);

        }

        #region private function
        /// <summary>
        /// try to write or query equipment many times
        /// </summary>
        /// <param name="commandStr"></param>
        /// <param name="isQueryEquip"></param>
        private string TryWriteEquipCommand(string commandStr,bool isQueryEquip)
        {
            //this.Timeout_ms = 1000;
            int Total_tryTimes = 10;
            int tryTimes ;
            tryTimes = Total_tryTimes;
            //System.Threading.Thread.Sleep(500);

            //string subFile = DateTime.Now.ToString().Replace('/', '_');
            //subFile = subFile.Replace(':', '_');
            //subFile = subFile.Replace(' ', '_');
            //string errorFile = this.errorLogFilePath + "_" + subFile + ".txt";

            string returnValue="";

            while (true)
            {
                try
                {
                    if (isQueryEquip) //query equip
                    {
                        returnValue = instrumentChassis.Query_Unchecked(commandStr, this);
                    }
                    else //write equip
                    {
                        instrumentChassis.Write_Unchecked(commandStr, this);
                    }
                }
                catch (Exception e)
                {
                    tryTimes--;

                    // create error log file - chongjian.liang 2013.4.19
                    if (!File.Exists(errorFile))
                    {
                        string directoryName = Path.GetDirectoryName(errorFile);

                        if (!Directory.Exists(directoryName))
                        {
                            Directory.CreateDirectory(directoryName);
                        }

                        File.Create(errorFile).Close();
                    }

                    if (sw == null)
                    {
                        sw = new StreamWriter(errorFile);
                    }

                    if (tryTimes < 0)
                    {
                        throw new InstrumentException("Instrument Error:" + e.Message);
                    }
                    int temp = Total_tryTimes - tryTimes;
                    string fileString ="Error Command is :" +commandStr+ ", try " + temp.ToString() + " times at:  " + DateTime.Now.ToString();
                    sw.WriteLine(fileString);
                    sw.Flush();
                    
                    continue;
                }
                break;

            }//end of while
            return returnValue;
        }

       
        #endregion

        #region Private data

        /// <summary>
        /// Soft marker on/off buffer
        /// </summary>
        private bool markerOn;
        private InstType_OSA.SweepModes mode;

        /// <summary>
        /// The status of sweeps or marker sets
        /// </summary>
        private ChannelState mStatus;
        /// <summary>
        /// The current working register for testing status
        /// </summary>
        private byte mCurrentStatusRegister;
        /// <summary>
        /// The current working register test bit mask for testing status, normally specifys only one bit
        /// </summary>
        private byte mCurrentStatusBitMask;
        /// <summary>
        ///Current ESR register value
        /// </summary>
        private byte mCurrentESR;
        /// <summary>
        ///Time out for any status check after a wcommand which involves waiting such as sweep
        /// </summary>
        private Int32 mSatusPollDelayms;
        /// <summary>
        ///Start Time marker for sweep or marker search used for OSA lock up detection
        /// </summary>
        private DateTime mDTStartWaitTime;
        /// <summary>
        ///This is used to delay the next status query period, need to stop polling too often at the same time allowing this thread to do other things
        /// </summary>
        private DateTime mStatusStart;
        /// <summary>
        ///Time out for status poll, if the status is polled for a longer period than this then error thrown
        /// </summary>
        private int mStatusTimeOutSeconds;
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_YKAQ6370 instrumentChassis;

        private StreamWriter sw;
        private string errorLogFilePath = @"LogStore\OSA\error_log";
        private string errorFile;

        #endregion

        #region user defined data type
        /// <summary>
        /// sensitivity enum data type
        /// </summary>

        public enum SensitivityEnum
        {
            NHLD, // normal hold
            NAUT, // normal auto
            NORMAL,
            MID,   //middle
            HIGH1, //
            HIGH2,
            HIGH3

        }
        /// <summary>
        /// 
        /// </summary>
        public enum MarkerUnitEnum
        {
            FREQ, //frequency
            WAV   //wavelength
        }
        public enum SweepModeEnum
        {
            SINGLE,
            REPEAT,
            AUTO,
            SEGMENT

        }
        public enum AmplitudeUnitType
        {
            dBm, w
        }


        #endregion

    }
}


   









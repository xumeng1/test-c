using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.ChassisNS;

namespace TEST
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_YKAQ6370 testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Instr_YKAQ6370 testInstr;        
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_YKAQ6370("yokogawaChassis", "Chassis_YKAQ6370", "GPIB0::2::INSTR");
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Instr_YKAQ6370("yoko6370instu", "Instr_YKAQ6370", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            //testChassis.EnableLogging = true;
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            //testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            Assert.IsNaN(Double.NaN);

            testInstr.SetDefaultState();
            //testInstr.Reset();


            //remed by echo in 26/mar/2010
            //testInstr.RBW_Hz = 3;
            //testInstr.Span_Khz = 0.1;
            //testInstr.CenterFreq_Khz = 771.750;

            //testInstr.PeakSearchMode = PeakSearchType.HI;
            //testInstr.PeakSearch();

            //double amplitudeValue = testInstr.GetPeakAmplitude();

            //double freq = testInstr.GetFrequency();
           
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            //testInstr.AutoRange = true;
            //testInstr.AutoAlign();
            testInstr.WavelengthStart_nm = 1548;
            testInstr.WavelengthStop_nm = 1552;

            testInstr.AutoMeasure();
           // testInstr.AutoSweepPoint = true;
            testInstr.TracePoints = 100;
            //double markeramplitude=testInstr.MarkerAmplitude_dBm;
            //testInstr.MarkerWavelength_nm = 1550;
            
           
            double noise=testInstr.NoiseBandWidth_nm;
            double osnr=testInstr.OSNR_dB;
            double referenceLevel = testInstr.ReferenceLevel_dBm;
            //testInstr.SensorSensitivity = SensitivityEnum.MID;
            //testInstr.WavelengthCentre_nm = 1550;
            //testInstr.WavelengthStart_nm = 1548;
            //testInstr.WavelengthStop_nm = 1552;
            testInstr.MarkerToPeak();
            double markMax=testInstr.MarkerAmplitude_dBm;
            testInstr.MarkerToNextPeak(Bookham.TestLibrary.InstrTypes.InstType_OSA.Direction.Left);

            Bookham.TestLibrary.InstrTypes.InstType_OSA.OptPowerPoint[] a;
            //a = testInstr.GetDisplayTrace;
            
            
            testInstr.SetSingleSweepMode();
            string b=testInstr.SweepMode.ToString();
            testInstr.SweepStart();
            testInstr.SweepStop();
            


            
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

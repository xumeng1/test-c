// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Therm2800.cs
//
// Author: brendan.kavanagh, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument; // btk

// btk: Changed NS from Bookham.TestSolution.ChassisNS to Bookham.TestLibrary.ChassisNS
namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Class for Thermoton 2800 oven controller Chassis.
    /// </summary>
    public class Chassis_Therm2800 : ChassisType_Visa
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Therm2800(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "2800",			// hardware name 
                "0",			// minimum valid firmware version 
                "V2.05.00");	// maximum valid firmware version 
            ValidHardwareData.Add("Therm2800ChassisData", chassisData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string and return the firmware part:
                string combinedVersionId = this.Query_Unchecked_Wrapped("DID", null);
                return this.getHardwareOrFirmwareVersion(combinedVersionId, true); // true for FW ID
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and return the hardware part:
                string combinedVersionId = this.Query_Unchecked_Wrapped("DID", null);
                return this.getHardwareOrFirmwareVersion(combinedVersionId, false); // false selects HW ID
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                // Dev. Test : Replace line with Commented-out line to test off-line. TBD: delete 
                // return true;
                return base.IsOnline;
            }
            set
            {
                // setup base class

                // Dev. Test : Comment-out next line to test off-line. TBD: delete 
                base.IsOnline = value;  // Visa class's IsOnline() resets the instrument set.

                if (value) // if setting online                
                {
                    // The Thermotron 2800 needs a 3 second delay after reset before it
                    // can accept new comms (see pg 6-3 of manual). 

                    System.Threading.Thread.Sleep(4000); // wait 4 seconds (1 extra for margin)

                    // Clear the chassis message buffer:

                    this.Write_Unchecked_Wrapped("CB", null);

                    // Read the chassis error code to see if there's any error:

                    this.CheckErrorStatus(); // this throws if there's been an error. 
                    base.LogEvent(" set to on-line.");
                }
            }
        }

        #endregion

        /// <summary>
        /// Method to test last error code produced. If an error exists a ChassisException is thrown.
        /// </summary>
        public void CheckErrorStatus()
        {
            // get error string
            string errorRegister = this.Query_Unchecked_Wrapped("DEC", null);

            if (errorRegister != "+0000")
            {
                throw new ChassisException("Error Register indicates error - " + errorRegister);
            }
            
            this.Write_Unchecked("LKSO",null); // Switch off lockout
        }

        /// <summary>
        /// Method which wraps the VISA method Query_Unchecked() so that it easy to comment
        /// out the call to it for test development purposes.
        /// </summary>
        /// <param name="command">string command to chassis</param>
        /// <param name="instrument">handle to relevant instrument</param>
        /// <returns>response string</returns>
        public string Query_Unchecked_Wrapped(string command, Instrument instrument)
        {
            string response = "0"; // - the "no-error" response value

            // Dev. Test : Comment-out next line to test off-line. TBD: delete 
            response = this.Query_Unchecked(command, instrument);

            // Dev. Test : Uncomment-out next line to test off-line. TBD: delete
            //base.LogEvent("SIMULATED RESPONSE of 0 for cmd: " + command); 

            this.Write_Unchecked("LKSO", null); // Switch off lockout

            return response;
        }

        /// <summary>
        /// Method which wraps the VISA method Write_Unchecked() so that it easy to comment
        /// out the call to it for test development purposes.
        /// </summary>
        /// <param name="command">string command to chassis</param>
        /// <param name="instrument">handle to relevant instrument</param>
        public void Write_Unchecked_Wrapped(string command, Instrument instrument)
        {
            // Dev. Test : Comment-out next line to test off-line. TBD: delete
            this.Write_Unchecked(command, instrument);

            // Dev. Test : Uncomment-out next line to test off-line. TBD: delete
            //base.LogEvent("COMMENTED-OUT write of command: " + command);
        }

        /// <summary>
        /// Method to get the chassis identity string and extract either the firmware ID or the hardware ID.
        /// </summary>
        /// <param name="versionString">combined ID string</param>
        /// <param name="getFirmwareFlag">true for select firmware ID, else select hardware ID</param>
        /// <returns>sub-string with required ID</returns>
        private string getHardwareOrFirmwareVersion(string versionString, bool getFirmwareFlag)
        {
            // PAW removed CheckErrorStatus(); // throw if an error was encountered. Won't parse crud.

            // Expect "2800-V2.01.00". Treat hw ID as 1st 4 chars, fw ID as 6th char onwards


            // Dev. Test : Uncomment next 4 lines to test off-line. TBD: delete 
            //if (versionString == "0")
            //{
            //    versionString = "2799-V2.01.-01"; // special invalid value just for dev test.
            //}

            string result = null;

            if (getFirmwareFlag == true)
            {
                result = versionString.Substring(5); // start from "5th" char (counting from zero).
            }
            else
            {
                result = versionString.Substring(0, 4); // start from zeroth char for length of 4.
            }

            return result;
        }

    }
}

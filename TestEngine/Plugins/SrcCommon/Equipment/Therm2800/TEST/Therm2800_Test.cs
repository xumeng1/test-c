using System;
using System.Collections.Generic;
// using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
//using Bookham.TestLibrary.InstrTypes;

using Bookham.TestLibrary.ChassisNS; // Chassis_Therm2800 - btk
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes; // Inst_Th2800 ... - btk

namespace Therm2800_Tester
{
    /// <exclude />	
    [TestFixture]
    public class Therm2800_Test
    {
        #region Constants
        //const string visaResource = "//toaeng-305/GPIB1::9::INSTR";
        const string visaResource = "GPIB0::24::INSTR";
 
        const string chassisName = "Oven Chassis";
        const string instrName = "Thermo-Man";

        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Thermotron 2800 Driver Test Initialising ***");

            // create equipment objects
            testChassis = new Chassis_Therm2800(chassisName, "Chassis_Therm2800", visaResource);
            testChassis.EnableLogging = true;
            TestOutput(chassisName, "Created OK");

            testOvenInstr = new Inst_Th2800_EnvironChamber(instrName, "Inst_Th2800_EnvironChamber", "1", "1", testChassis);
            testOvenInstr.EnableLogging = true;
            TestOutput(instrName, "Created OK");


        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testOvenInstr.OutputEnabled = false;
            testOvenInstr.IsOnline = false;
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }        
       

        [Test]
        public void T01_ChassisFunction()
        {
            TestOutput("\n\n*** T01_ChassisFunction ***");

            testChassis.IsOnline = true;

            bool rc = testChassis.IsOnline;

            TestOutput(chassisName, "Chassis has been put on-line: " + rc.ToString());

            string hwVersion = testChassis.HardwareIdentity;

            TestOutput(chassisName, "HW Identity is: " + hwVersion);

            string fwVersion = testChassis.FirmwareVersion;

            TestOutput(chassisName, "FW Version is: " + fwVersion);
        }

        [Test]
        public void T02_InitInstrumentFunction()
        {
            TestOutput("\n\n*** T02_InitInstrumentFunction ***");

            testOvenInstr.IsOnline = true;
            testOvenInstr.SetDefaultState();

            double setpoint = testOvenInstr.SensorTemperatureSetPoint_C;

            TestOutput(instrName, "Set and Get done on temperature set-point: " + setpoint.ToString());
        }

        [Test]
        public void T03_RunInstrumentFunction()
        {
            TestOutput("\n\n*** T02_InitInstrumentFunction ***");

            double setpoint = 70.0;
            
            testOvenInstr.SensorTemperatureSetPoint_C = setpoint;

            double readBackSetPoint = testOvenInstr.SensorTemperatureSetPoint_C;

            TestOutput(instrName, "Set and Get done on temperature set-point: SET: " + setpoint.ToString() +
                        " GET: " + readBackSetPoint.ToString());

            testOvenInstr.OutputEnabled = true;

            TestOutput(instrName, "Switched on oven State: " + testOvenInstr.OutputEnabled.ToString());

            double actualTemp = testOvenInstr.SensorTemperatureActual_C;

            TestOutput(instrName, "Read oven ActualTemp: " + actualTemp.ToString());
        }


        #region Private helper fns and data
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        
        /// <summary>
        /// Chassis & Inst references
        /// </summary>
        private Chassis_Therm2800 testChassis;
        private Inst_Th2800_EnvironChamber testOvenInstr;

        #endregion

    }

}

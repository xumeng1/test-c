//  Author: chongjian.liang 2016.04.21

using System;
using System.Collections.Generic;
using System.IO;

namespace Bookham.TestLibrary.Instruments
{
    public enum Enum_OpticalSwitchLine
    {
        Master,
        Slave
    }

    public enum Enum_LockSchema
    {
        MemoryLock,
        FileLock
    }

    public enum Enum_LineState
    {
        LLLL = 0,
        LLLH = 1,
        LLHL = 2,
        LLHH = 3,
        LHLL = 4,
        LHLH = 5,
        LHHL = 6,
        LHHH = 7,
        HLLL = 8,
        HLLH = 9,
        HLHL = 10,
        HLHH = 11,
        HHLL = 12,
        HHLH = 13,
        HHHL = 14,
        HHHH = 15
    }
}

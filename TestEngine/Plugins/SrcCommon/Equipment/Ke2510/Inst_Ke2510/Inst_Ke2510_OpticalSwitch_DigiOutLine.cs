// Author: chongjian.liang 2016.04.21

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestLibrary.Instruments
{
    public class Inst_Ke2510_OpticalSwitch_DigiOutLine : UnmanagedInstrument
    {
        public Inst_Ke2510_OpticalSwitch_DigiOutLine(Chassis_Ke2510 chassis_Ke2510, Enum_LineState lineState_MasterOn, Enum_LineState lineState_SlaveOn, int switchSpeed_ms)
            : base(chassis_Ke2510.Name)
        {
            this.chassis_Ke2510 = chassis_Ke2510;
            this.lineState_MasterOn = lineState_MasterOn;
            this.lineState_SlaveOn = lineState_SlaveOn;
            this.switchSpeed_ms = switchSpeed_ms;
        }

        public void SwitchOpticalLineToMaster()
        {
            this.LineState = this.lineState_MasterOn;

            System.Threading.Thread.Sleep(this.switchSpeed_ms);
        }

        public void SwitchOpticalLineToSlave()
        {
            this.LineState = this.lineState_SlaveOn;
        }

        public Enum_LineState LineState
        {
            get
            {
                return (Enum_LineState)Enum.Parse(typeof(Enum_LineState), chassis_Ke2510.Query("SOUR2:TTL?", this));
            }
            set
            {
                string cmd = "SOUR2:TTL " + (int)value;

                chassis_Ke2510.Write(cmd, this);
            }
        }

        public override void SetDefaultState()
        {
            this.LineState = Enum_LineState.LLLL;
        }

        private Chassis_Ke2510 chassis_Ke2510;
        private Enum_LineState lineState_MasterOn;
        private Enum_LineState lineState_SlaveOn;
        private int switchSpeed_ms;
    }
}
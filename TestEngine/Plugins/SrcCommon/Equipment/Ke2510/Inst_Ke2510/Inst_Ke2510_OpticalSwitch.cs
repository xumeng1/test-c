//  Author: chongjian.liang 2016.04.21

using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestLibrary.Instruments
{
    public class Inst_Ke2510_OpticalSwitch
    {
        /// <summary>
        /// Initialize the shared instrument of slave line, lock schema is MemoryLock
        /// </summary>
        public Inst_Ke2510_OpticalSwitch()
        {
            this.Init_Slave();

            this.LOCK_SCHEMA = Enum_LockSchema.MemoryLock;
        }

        /// <summary>
        /// Initialize the shared instrument of slave line, lock schema is FileLock
        /// </summary>
        public Inst_Ke2510_OpticalSwitch(string lockFile_ID)
        {
            this.Init_Slave();

            this.LOCK_SCHEMA = Enum_LockSchema.FileLock;
            this.FILELOCK_FILE_PATH = Path.Combine(this.FILELOCK_FILE_REPOSITORY_PATH, lockFile_ID);
        }

        /// <summary>
        /// Initialize the shared instrument of master line, lock schema is MemoryLock
        /// </summary>
        public Inst_Ke2510_OpticalSwitch(Inst_Ke2510 inst_Ke2510, Enum_LineState lineState_MasterOn, Enum_LineState lineState_SlaveOn, int switchSpeed_ms)
        {
            this.Init_Master(inst_Ke2510, lineState_MasterOn, lineState_SlaveOn, switchSpeed_ms);

            this.LOCK_SCHEMA = Enum_LockSchema.MemoryLock;
        }

        /// <summary>
        /// Initialize the shared instrument of master line, lock schema is FileLock
        /// </summary>
        public Inst_Ke2510_OpticalSwitch(string lockFile_ID, Inst_Ke2510 inst_Ke2510, Enum_LineState lineState_MasterOn, Enum_LineState lineState_SlaveOn, int switchSpeed_ms)
        {
            this.Init_Master(inst_Ke2510, lineState_MasterOn, lineState_SlaveOn, switchSpeed_ms);

            this.LOCK_SCHEMA = Enum_LockSchema.FileLock;
            this.FILELOCK_FILE_PATH = Path.Combine(this.FILELOCK_FILE_REPOSITORY_PATH, lockFile_ID);
        }

        /// <summary>
        /// Initialize the shared instrument of slave line
        /// </summary>
        private void Init_Slave()
        {
            this.OPTICAL_SWITCH_LINE = Enum_OpticalSwitchLine.Slave;
        }

        /// <summary>
        /// Initialize the shared instrument of master line
        /// </summary>
        private void Init_Master(Inst_Ke2510 inst_Ke2510, Enum_LineState lineState_MasterOn, Enum_LineState lineState_SlaveOn, int switchSpeed_ms)
        {
            this.opticalSwitchDigiOutLine = new Inst_Ke2510_OpticalSwitch_DigiOutLine(inst_Ke2510.InstrumentChassis as Chassis_Ke2510, lineState_MasterOn, lineState_SlaveOn, switchSpeed_ms);

            this.OPTICAL_SWITCH_LINE = Enum_OpticalSwitchLine.Master;
        }

        public bool IsOnline
        {
            get
            {
                return this.opticalSwitchDigiOutLine.IsOnline;
            }
            set
            {
                this.opticalSwitchDigiOutLine.IsOnline = value;
            }
        }

        public void SwitchOpticalLineOn()
        {
            this.WaitAndOccupyOpticalLine();

            if (this.OPTICAL_SWITCH_LINE == Enum_OpticalSwitchLine.Master)
            {
                if (this.opticalSwitchDigiOutLine == null)
                {
                    throw new Exception("OpticalSwitchDigiOutLine is not available.");
                }
                else
                {
                    this.opticalSwitchDigiOutLine.SwitchOpticalLineToMaster();
                }
            }
        }

        public void SwitchOpticalLineOff()
        {
            if (this.OPTICAL_SWITCH_LINE == Enum_OpticalSwitchLine.Master)
            {
                if (this.opticalSwitchDigiOutLine == null)
                {
                    throw new Exception("OpticalSwitchDigiOutLine is not available.");
                }
                else
                {
                    this.opticalSwitchDigiOutLine.SwitchOpticalLineToSlave();
                }
            }

            this.LiftOpticalLineOccupancy();
        }

        private void WaitAndOccupyOpticalLine()
        {
            if (this.LOCK_SCHEMA == Enum_LockSchema.MemoryLock)
            {
                while (!this.isCurrentOpticalLineOccupied)
                {
                    this.memoryLock_Mutex = new Mutex(true, this.MEMORYLOCK_MUTEX_FLAG, out isCurrentOpticalLineOccupied);

                    if (!this.isCurrentOpticalLineOccupied)
                    {
                        this.memoryLock_Mutex.Close();

                        Thread.Sleep(1);
                    }
                }
            }
            else if (this.LOCK_SCHEMA == Enum_LockSchema.FileLock)
            {
                while (!this.isCurrentOpticalLineOccupied)
                {
                    try
                    {
                        this.fileLock_FileStream = File.Open(this.FILELOCK_FILE_PATH, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None);

                        this.isCurrentOpticalLineOccupied = true;
                    }
                    catch
                    {
                        Thread.Sleep(1);
                    }
                }
            }
        }

        private void LiftOpticalLineOccupancy()
        {
            if (this.LOCK_SCHEMA == Enum_LockSchema.MemoryLock)
            {
                if (this.memoryLock_Mutex != null)
                {
                    this.memoryLock_Mutex.Close();
                }
            }
            else if (this.LOCK_SCHEMA == Enum_LockSchema.FileLock)
            {
                if (this.fileLock_FileStream != null)
                {
                    this.fileLock_FileStream.Close();
                }
            }

            this.isCurrentOpticalLineOccupied = false;
        }

        private readonly string MEMORYLOCK_MUTEX_FLAG = "OPTICAL_MUTEX_FLAG";
        private Mutex memoryLock_Mutex = null;

        private string FILELOCK_FILE_REPOSITORY_PATH = @"\\szn-sfl-01\DEPARTMENTS\SZ_Public\Transmission Tunable\Chongjian.Liang\sys\LockFiles";
        private string FILELOCK_FILE_PATH;
        private FileStream fileLock_FileStream = null;

        public Enum_OpticalSwitchLine OPTICAL_SWITCH_LINE;
        private Enum_LockSchema LOCK_SCHEMA;
        private Inst_Ke2510_OpticalSwitch_DigiOutLine opticalSwitchDigiOutLine;
        private bool isCurrentOpticalLineOccupied = false;
    }
}
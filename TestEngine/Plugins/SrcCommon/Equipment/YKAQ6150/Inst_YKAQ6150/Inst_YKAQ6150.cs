// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_AQ6150.cs
//
// Author: Duke.Du, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using System.IO;

namespace Bookham.TestLibrary.Instruments
{
    public class Inst_YKAQ6150 : InstType_SharedWaveMeter
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Slot ID for the instrument</param>
        /// <param name="subSlotId">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_YKAQ6150(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            // Configure valid hardware information

            // Add all Ag86122x details
            InstrumentDataRecord YKAQ6150Data = new InstrumentDataRecord(
                "YOKOGAWA AQ6150",    				// hardware name 
                "0.0",											// minimum valid firmware version 
                "99.99");									// maximum valid firmware version 
            YKAQ6150Data.Add("MinWavelength_nm", "1270");			// minimum wavelength
            YKAQ6150Data.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("AQ6150", YKAQ6150Data);


            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_YKAQ6150",								// chassis driver name  
                "0.0",									// minimum valid chassis driver version  
                "99.99");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_YKAQ6150", chassisInfo);


            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_YKAQ6150)base.InstrumentChassis;

            safeModeOperation = true;

        }

        #region Public Instrument Methods and Properties

        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the Chassis 
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware, the plugin hardware and the channel number
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }


        /// <summary>
        /// Configures the instrument into a default state.
        /// </summary>
        public override void SetDefaultState()
        {
            this.SwitchOpticalLineOn();

            this.Write("*RST");
            this.Write("*CLS");

            this.ContinueMeasMode = false;

            this.SwitchOpticalLineOff();
        }

        #endregion

        #region Public Wavemeter InstrumentType Methods and Properties

        public override bool ContinueMeasMode
        {
            get
            {
                this.SwitchOpticalLineOn();

                string cmd = ":init:cont?";
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                int status = int.Parse(rtn);
                return status == 1;
            }
            set
            {
                this.SwitchOpticalLineOn();

                string cmd = ":init:cont " + (value ? "on" : "off");
                this.Write(cmd);

                this.SwitchOpticalLineOff();
            }
        }

        /// <summary>
        /// Return frequency in GHz
        /// </summary>
        public override double Frequency_GHz
        {
            get
            {
                //Putting the marker onto the max peak wavelength
                //instrumentChassis.Write("DISP:MARK:MAX", this);

                // Read frequency in GHz
                //string cmd = ":MEAS:SCAL:POW:FREQ? DEF";

                //this.Write(":INIT:CONT ON");

                this.SwitchOpticalLineOn();

                string cmd = ":FETC:POW:FREQ?";//Read max freq directly. Modified by Andy Li.
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                //Hp returns in Hz
                const Double HzToGigaHzScaleFactor = 1e-9;

                double result = getDblFromString(cmd, rtn);

                //this.Write(":INIT:CONT OFF");

                // convert to GHz
                return (result * HzToGigaHzScaleFactor);
            }
        }

        /// <summary>
        /// Return wavelength in nm
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                ////Putting the marker onto the max peak wavelength
                //this.Write("DISP:MARK:MAX");

                //this.Write(":INIT:CONT ON");

                this.SwitchOpticalLineOn();

                //// Read wavelength in nm
                //string cmd = ":MEAS:SCAL:POW:WAV? DEF";
                string cmd = ":FETC:POW:WAV?";//Read max freq directly. Modified by Andy Li.
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                double result = getDblFromString(cmd, rtn);

                //this.Write(":INIT:CONT OFF");
                //Hp returns in Meters
                const Double MetersToNanometers = 1e9;

                return (result * MetersToNanometers);
            }
        }

        /// <summary>
        /// Returns the signal input power level in dBm
        /// </summary>
        public override double Power_dBm
        {
            get
            {
                this.SwitchOpticalLineOn();

                // Set power to dbm
                this.Write(":UNIT:POW DBM");

                string cmd = ":FETC:POW?";
                // Read power in dBm
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                double result = getDblFromString(cmd, rtn);

                return result;
            }
        }

        /// <summary>
        /// Returns the signal input power level in mW
        /// </summary>
        public override double Power_mW
        {
            get
            {
                this.SwitchOpticalLineOn();

                // Set power to W
                this.Write(":UNIT:POW W");

                string cmd = ":FETC:POW?";

                // Read power in W
                string rtn = this.Query(cmd);

                this.SwitchOpticalLineOff();

                double result = getDblFromString(cmd, rtn);

                const double WattsToMilliWattsScaleFactor = 1000;

                // return result converted to mW
                return (result * WattsToMilliWattsScaleFactor);
            }
        }

        /// <summary>
        /// Reads/sets the measurement medium
        /// </summary>
        public override Medium MeasurementMedium
        {
            get
            {
                this.SwitchOpticalLineOn();

                // Query medium
                string rtn = this.Query(":SENS:CORR:MED?");

                this.SwitchOpticalLineOff();

                // Convert to enum 
                Medium medm;
                switch (rtn)
                {
                    case "VAC": medm = Medium.Vacuum; break;
                    case "AIR": medm = Medium.Air; break;
                    default: throw new InstrumentException("Unrecognised measurement medium '" + rtn + "'");
                }

                // Return result
                return medm;
            }

            set
            {
                // Build string
                string medm;
                switch (value)
                {
                    case Medium.Vacuum: medm = "VAC"; break;
                    case Medium.Air: medm = "AIR"; break;
                    default: throw new InstrumentException("Unrecognised measurement medium '" + value.ToString() + "'");
                }

                this.SwitchOpticalLineOn();

                // Write to instrument
                this.Write(":SENS:CORR:MED " + medm);

                this.SwitchOpticalLineOff();

            }
        }

        /// <summary>
        /// Gets / sets whether we are in safe mode of operation, 
        /// we are in this mode by default
        /// </summary>
        public bool SafeModeOperation
        {
            get
            {
                return (safeModeOperation);
            }
            set
            {
                safeModeOperation = value;
            }
        }

        public void EnableDisplay(bool enable)
        {
            this.SwitchOpticalLineOn();

            if (enable)
            {
                this.Write(":DISP:WIND:GRAP:STAT ON");
                this.Write(":DISP:WIND:STAT ON");
                this.Write("DISP:WIND2:STAT ON");
            }
            else
            {
                this.Write(":DISP:WIND:GRAP:STAT OFF");
                this.Write(":DISP:WIND:STAT OFF");
                this.Write("DISP:WIND2:STAT OFF");
            }

            this.SwitchOpticalLineOff();
        }
        #endregion


        #region private functions

        /// <summary>
        /// Write, depending on safemodeoperation will call correct thing in chassis
        /// </summary>
        /// <param name="cmd">the command we wish to write to chassis</param>
        private void Write(string cmd)
        {
            if (this.safeModeOperation)
            {
                instrumentChassis.Write(cmd, this);
                return;
            }
            else
            {
                //async = true, errorcheck = false
                instrumentChassis.Write(cmd, this, true, false);
                return;
            }

        }

        /// <summary>
        /// Query, depending on safemodeoperation will call correct thing in chassis
        /// </summary>
        /// <param name="cmd">the query string</param>
        /// <returns></returns>
        private string Query(string cmd)
        {
            if (this.safeModeOperation)
            {
                return (instrumentChassis.Query(cmd, this));
            }
            else
            {
                // errorcheck = false
                return (instrumentChassis.Query(cmd, this, false));
            }
        }

        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }


        #endregion



        #region Private Data

        // Chassis reference
        private Chassis_YKAQ6150 instrumentChassis;
        private bool safeModeOperation;
        #endregion
    }


}

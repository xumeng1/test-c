using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace TEST
{
    public class Instr_YKAQ6150_WaveMeter_Test
    {
        /// <exclude />
        /// <summary>
        /// Constructor 
        /// </summary>
        public Instr_YKAQ6150_WaveMeter_Test()
        { }

        public void Setup()
        {
            // initialise logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
        }

        /// <exclude />
        //[TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            Debug.WriteLine("Test Finished!");
        }

        //[Test]
        public void T01_CreateInst(string visaResource)
        {
            testChassis = new Chassis_YKAQ6150("TestChassis", "Chassis_YKAQ6150", visaResource);

            Debug.WriteLine("Chassis created OK");
            testInst = new Instr_YKAQ6150("testInst", "Instr_YKAQ6150_WaveMeter", "1", "", testChassis);
            Debug.WriteLine("Instrument created OK");
            string test = testInst.FirmwareVersion;

        }

        /// <exclude />
        //[Test]
        //[ExpectedException(typeof(System.Exception))]
        public void T02_TryCommsNotOnline()
        {
            //Cannot try this as logging shuts down the app after the exception
            try
            {
                Debug.WriteLine(testInst.HardwareIdentity);
            }
            catch (ChassisException e)
            {
                Debug.WriteLine("Expected exception :" + e.Message);
            }
        }

        //[Test]
        public void T03_SetOnline()
        {
            testChassis.IsOnline = true;
            Debug.WriteLine("Chassis IsOnline set true OK");
            testInst.IsOnline = true;
            Debug.WriteLine("Instrument IsOnline set true OK");
        }

        //[Test]
        public void T04_DriverVersion()
        {
            Debug.WriteLine(testInst.DriverVersion);
        }


        public void T05_FirmwareVersion()
        {
            Debug.WriteLine(testInst.FirmwareVersion);
        }


        public void T06_HardwareID()
        {
            Debug.WriteLine(testInst.HardwareIdentity);
        }


        public void T07_SetDefaultState()
        {
            testInst.SetDefaultState();
            Debug.WriteLine("Default state set OK");
        }


        public void T08_Power_mW()
        {
            Debug.WriteLine("Power in Mw = " + testInst.Power_mW);
        }


        public void T09_Power_dBm()
        {
            Debug.WriteLine("Power in dBm = " + testInst.Power_dBm);
        }


        public void T10_MeasurementMedium()
        {
            testInst.MeasurementMedium = InstType_Wavemeter.Medium.Air;
            Debug.WriteLine("Measurement Medium set to Air, reading back = " + testInst.MeasurementMedium);
            testInst.MeasurementMedium = InstType_Wavemeter.Medium.Vacuum;
            Debug.WriteLine("Measurement Medium set to Vacuum, reading back = " + testInst.MeasurementMedium);
        }

        public void T11_Frequency()
        {
            Debug.WriteLine("Measurement frequency =" + testInst.Frequency_GHz + " GHz");
            double f = testInst.Frequency_GHz;
            f = testInst.Frequency_GHz;
            f = testInst.Frequency_GHz;
        }


        public void T12_Wavelength()
        {
            Debug.WriteLine("Wavelength =" + testInst.Wavelength_nm + " nm");
        }




        public void T13_TimingComparision(bool Safemode)
        {

            int i;



            System.DateTime StartTime;

            System.DateTime EndTime;

            System.TimeSpan Duration;



            StartTime = System.DateTime.Now;

            double reading;



            Debug.WriteLine("Start Time: " + StartTime);

            int maxloops = 50;


            testInst.SafeModeOperation = Safemode;

            testInst.MeasurementMedium = InstType_Wavemeter.Medium.Air;

            testInst.SetDefaultState();

            for (i = 0; i < maxloops; i++)
            {
                reading = testInst.Power_dBm;
                reading = testInst.Wavelength_nm;
            }

            EndTime = System.DateTime.Now;

            Debug.WriteLine("End Time: " + EndTime);

            Duration = EndTime - StartTime;

            Debug.WriteLine("SafeMode = " + Safemode + "\n " +

                "Did " + maxloops + " sets of operations in " + Duration.TotalSeconds + " seconds");



        }


        /// <summary>
        /// Chassis & Inst references
        /// </summary>
        private Chassis_YKAQ6150 testChassis;
        private Instr_YKAQ6150 testInst;
    }
}

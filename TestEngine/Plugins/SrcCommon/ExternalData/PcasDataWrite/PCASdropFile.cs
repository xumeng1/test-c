// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasDataWrite\PcasDropFile.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using System.Collections.Specialized;	// StringDictionary
using System.Globalization;				// CultureInfo
using System.IO;						// File.Move
using System.Text;						// StringBuilder



namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
    /// Creates a PCASdropFile object.
	/// </summary>
	public class PCASdropFile
	{

		/// <summary>
		/// Creates a PCASdropFile object.
		/// </summary>
		/// <param name="localDir">The folder in which to create drop files before sending to the drop folder</param>
		/// <param name="dropDir">The root folder from which PCAS picks up drop files</param>
        /// <param name="nodeId">The nodeId of the Test Set. The PCAS Streamer Picks up data from the dropDir/nodeId subdirectory</param>
		public PCASdropFile( string localDir, string dropDir, string nodeId )
		{
			this.localDir = localDir;

            // Set-up the directory from which the PCAS Streamer will pick up data.
            this.dropDir = dropDir + @"\" + nodeId;

            //Create the drop directory if it doesn't exist yet.
            if (!Directory.Exists(this.dropDir))
            {
                Directory.CreateDirectory(this.dropDir);
            }
		}

		/// <summary>
		/// Move the local drop file to the PCAS drop folder.
        /// </summary>
        /// <returns>final drop file path string</returns>
        public string MoveToDropDir()
        {
            if (System.IO.File.Exists(dropFilePath))
                throw new Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException("File ' " + dropFilePath + "' exists. Manually move file '" + localFilePath + "' to resolve this");
            System.IO.File.Move(localFilePath, dropFilePath);

            System.Threading.Thread.Sleep(10);

            return dropFilePath; // for log entry later
        }


		/// <summary>
		/// Creates a drop file on the local PC,
		/// before moving it to the PCAS drop folder.
		/// </summary>
		/// <param name="deviceType">The PCAS device type</param>
		/// <param name="stage">The PCAS test stage</param>
		/// <param name="specName">The PCAS spec_id</param>
		/// <param name="compID">The PCAS serial number</param>
		/// <param name="resultList">The result list to be written</param>
		/// <param name="addTimeDate">Whether to add the TimeDate to the results.</param>
		internal void createLocal( string deviceType, string stage, string specName, string compID, StringDictionary resultList , bool addTimeDate )
		{
			// Use s/no and time specified down to ms to ensure uniqueness of the file.
			filename = compID + "_" + System.DateTime.UtcNow.ToString("yyyyMMddHHmmssfff",CultureInfo.InvariantCulture) + ".pcasDrop";
            if(File.Exists(filename))
            {
                //System.Threading.Thread.Sleep(10);
                //filename = compID + "_" + System.DateTime.UtcNow.ToString("yyyyMMddHHmmssfff", CultureInfo.InvariantCulture) + ".pcasDrop";

                DateTime time = DateTime.UtcNow;
                int millisecond = time.Millisecond;
                millisecond++;
                filename = compID + "_" + time.ToString("yyyyMMddHHmmssfff", CultureInfo.InvariantCulture) + millisecond.ToString() + ".pcasDrop";
            }
			localFilePath = System.IO.Path.Combine(localDir , filename);	
			dropFilePath  = System.IO.Path.Combine(dropDir , filename);	

			TextWriter output = File.CreateText( localFilePath );

			StringBuilder fileContents = dropfileAsText( deviceType, stage, specName, compID, resultList, addTimeDate );
			try
			{
				output.Write( fileContents.ToString() );
			}
			finally
			{
				output.Close();
			}
		}



		//
		// Private methods
		//

		/// <summary>
		/// Adds a param / value pair to the output data
		/// </summary>
		/// <param name="name">Name of param to add</param>
		/// <param name="val">Value to be added</param>
		private void addParam( string name, string val )
		{
			fileContents.Append("$" + name.ToUpper(CultureInfo.InvariantCulture) + "$0\n");
			fileContents.Append(val + "\n");
		}

		/// <summary>
		/// Create the text to go into the drop file
		/// </summary>
		/// <param name="deviceType">PCAS device type e.g. inp_mz</param>
		/// <param name="stage">PCAS test stage e.g. coc_final</param>
		/// <param name="specName">PCAS spec name</param>
		/// <param name="serialNumber">The DUT component ID e.g. CL12345.001</param>
		/// <param name="addTimeDate">If true, add any 'TIME_DATE' parameter appearing in the user supplied data</param>
		/// <param name="resultList">List of name value pairs to create drop file with.</param>		
		/// <returns>The text to go into the drop file</returns>
		private StringBuilder dropfileAsText( string deviceType, string stage, string specName, string serialNumber, StringDictionary resultList , bool addTimeDate )
		{
			// Clear existing contents;
			fileContents = new StringBuilder(200);

			//
			// Header
			//
			fileContents.Append("~DEVICE TYPE\n");
			fileContents.Append( deviceType + "\n");

			fileContents.Append( "~STAGE\n");
			fileContents.Append( stage + "\n");

			fileContents.Append( "~SPECIFICATION\n");
			fileContents.Append( specName + "\n");

			fileContents.Append( "~SERIAL NUMBER\n");
			fileContents.Append( serialNumber + "\n");


			//
			// Data
			//
			bool addedAtLeastOneParam = false;
			foreach ( string name in resultList.Keys )
			{
				string val = resultList[name];
				if( val != null && val.Length != 0 )
				{
					if( name == "SERIAL_NO" )
					{
						// Dont add it
					}
					else if( name == "TIME_DATE" )
					{
						// Optional
						if( addTimeDate )
							addParam( name, val );
					}
					else
					{
						addParam( name, val );
						addedAtLeastOneParam = true;
					}
				}
			}
			if( addedAtLeastOneParam == false )
			{
				throw new Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException( "dropFileAsText - empty dataSet" );
			}

			return fileContents;			
		}


		// Private Data
		/// <summary>
		/// Temporary drop area
		/// </summary>
		private string localDir;
		/// <summary>
		/// Final drop area
		/// </summary>
		private string dropDir;
		/// <summary>
		/// Name of drop file.
		/// </summary>
		private string filename;
		/// <summary>
		/// Local file path
		/// </summary>
		private string localFilePath;
		/// <summary>
		/// Final file path
		/// </summary>
		private string dropFilePath;

		/// <summary>
		/// The contents of the drop file in string format.
		/// </summary>
		private StringBuilder fileContents;
	}
}

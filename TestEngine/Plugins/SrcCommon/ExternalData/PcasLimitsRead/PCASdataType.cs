// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasLimitsRead\PcasDataType.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// Supported data types within the PCAS database.
	/// The values associated with each enum are contained in the 'store' field with the PCAS spec.
	/// </summary>
	public enum PCASdataType
	{
		/// <summary>A number, would you believe</summary>
		PCAS_NUMBER_38 = 1,
		/// <summary>A number, again ...</summary>
		PCAS_FLOAT_126 = 2,
		/// <summary>A long string</summary>
		PCAS_VARCHAR2_256 = 3,
		/// <summary>A longish string</summary>
		PCAS_VARCHAR2_80 = 4,
		/// <summary>A string</summary>
		PCAS_VARCHAR2_20 = 5,
		/// <summary>A short string</summary>
		PCAS_VARCHAR2_14 = 6
	}    
}

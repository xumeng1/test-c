// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasLimitsRead\PcasLimit.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using Bookham.TestEngine.PluginInterfaces.ExternalData;	// ILogging


namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// A single limit within a spec
	/// </summary>
	internal class PCASlimit
	{
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="logger">The component providing logging functionality.</param>
		public PCASlimit(ILogging logger)
		{
			this.logger = logger;
		}

		/// <summary>
		/// Converts a numeric field from the PCAS spec info into a PCASdataType
		/// </summary>
		/// <param name="store">An integer representing the data type, defined as per the enum PCASdataType</param>
		internal  void setDataType( int store )
		{
			switch( store )
			{
				case 1:
					dataType = PCASdataType.PCAS_NUMBER_38;
					break;

				case 2:
					dataType = PCASdataType.PCAS_FLOAT_126;
					break;

				case 3:
					dataType = PCASdataType.PCAS_VARCHAR2_256;
					break;

				case 4:
					dataType = PCASdataType.PCAS_VARCHAR2_80;
					break;

				case 5:
					dataType = PCASdataType.PCAS_VARCHAR2_20;
					break;

				case 6:
					dataType = PCASdataType.PCAS_VARCHAR2_14;
					break;

				default:
					throw new Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException( "PCASlimit.setDataType: must be an integer between 1 and 6" );
			}
		}

        /// <summary>
        /// Returns the maximum length of a specified data type.
        /// </summary>
        /// <returns>Maximum length in number of characters</returns>
        internal int accuracy()
        {
            return PcasLimitConvertUtils.GetAccuracy(this.dataType, this.format);
        }                

		/// <summary>
		/// Name of the parameter associated with the limit.
		/// </summary>
		internal string shortDesc;

        /// <summary>
        /// Units of the parameter associated with the limit.
        /// </summary>
        internal string units;

		/// <summary>
		/// Minumum value of the parameter associated with the limit.
		/// </summary>
		internal double min;

		/// <summary>
		/// Maximum value of the parameter associated with the limit.
		/// </summary>
		internal double max;

		/// <summary>
		/// Format for string representation of the parameter associated with the limit.
		/// </summary>
		internal string format;

		/// <summary>
		/// Priority order of this limit with its parent spec.
		/// </summary>
		internal int priority;

		/// <summary>
		/// Data type of the parameter associated with the limit.
		/// </summary>
		internal PCASdataType dataType;

		/// <summary>
		/// A reference to the component providing logging functionality.
		/// </summary>
		private ILogging logger;
	}
}

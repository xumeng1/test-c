using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;

namespace Bookham.TestLibrary.ExternalData
{

    /// <summary>
    /// 
    /// </summary>
    public class PcasFindFile
    {
        /// <summary>
        /// Get a blob file to the designated local folder 
        /// </summary>
        /// <param name="webServiceUrl">Example -> http://10.22.3.170/PCAS/getblobfile.asmx</param>
        /// <param name="methodName">Example -> GetSZNBlobFile</param>
        /// <param name="nodeId">Example -> node1210</param>
        /// <param name="fileName">Example -> MATRIX_reversePD1Current_DSDBR01_TC419845.001_20150626080639_SM5_P.csv</param>
        /// <param name="timeDate">Example -> 20150625</param>
        /// <param name="localDir">Example- -> c:\\blobfile</param>
        /// <returns></returns>
        public static string GetBlobFile(string webServiceUrl, string methodName, string nodeId,
                                        string fileName, string timeDate, string localDir,
                                        bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain)
        {
            Hashtable ht;
            XmlDocument xmlResult = null;
            string strResult;
            byte[] bytes;
            int intTry = 0;

            if (webServiceUrl.Trim() == "")
            {
                return "webServiceUrl can not be null!";
            }

            if (methodName.Trim() == "")
            {
                return "methodName can not be null!";
            }

            if (nodeId.Trim() == "")
            {
                return "nodeId can not be null!";
            }

            if (fileName.Trim() == "")
            {
                return "fileName can not be null!";
            }

            if (localDir.Trim() == "")
            {
                return "localDir can not be null!";
            }

            if (localDir.EndsWith("\\") == false)
            {
                localDir = localDir + "\\";
            }

            if (Directory.Exists(localDir) == false)
            {
                return "localDir does not exists!";
            }

            ht = new Hashtable();
            ht.Add("fileName", fileName.Trim());
            ht.Add("NodeId", nodeId.Trim());
            ht.Add("timeDate", timeDate.Trim());

            //Get data from webservice, try 3 times
            while (intTry < 3)
            {
                try
                {
                    xmlResult = WebServiceCaller.QuerySoapWebService(webServiceUrl.Trim(), methodName, ht, UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
                    break;
                }
                catch (WebException ex)
                {
                    if (intTry == 2)
                    {
                        return "Fail to call the webservice " + webServiceUrl + methodName + ", Error message: " + ex.Message;
                    }
                    else
                    {
                        intTry = intTry + 1;
                        Thread.Sleep(2000);
                    }
                }
            }

            if (xmlResult != null)
            {
                XmlElement xml = (XmlElement)xmlResult.SelectSingleNode("root");
                strResult = xml.InnerText;
                if (strResult.ToUpper().StartsWith("FAIL"))
                {
                    return strResult;
                }
                else if (strResult.ToUpper().StartsWith("NOT FOUND"))
                {
                    return strResult;
                }
                else if (strResult.ToUpper().StartsWith(@"\\"))
                {
                    intTry = 0;
                    while (intTry < 3)
                    {
                        try
                        {
                            File.Copy(strResult, localDir + fileName, true);
                            return "OK";
                        }
                        catch (Exception ex)
                        {
                            if (intTry == 2)
                            {
                                return "File to copy the file " + fileName + " from file server.Error message: " + ex.Message;
                            }
                            else
                            {
                                intTry = intTry + 1;
                                Thread.Sleep(2000);
                            }
                        }
                    }
                }
                else if (strResult.ToUpper().StartsWith("BASE64:"))
                {
                    try
                    {
                        bytes = Convert.FromBase64String(strResult.Substring(7));

                        FileStream file = new FileStream(localDir + fileName, FileMode.OpenOrCreate);
                        file.Write(bytes, 0, bytes.Length);
                        file.Flush();
                        file.Close();
                        return "OK";
                    }
                    catch (Exception ex)
                    {
                        return "File to rebuild the file " + fileName + " with binary data.Error message: " + ex.Message;
                    }
                }
            }

            return "OK";
        }


                /// <summary>
        /// Get a blob file to the designated local folder 
        /// </summary>
        /// <param name="webServiceUrl">Example -> http://10.22.3.170/PCAS/getblobfile.asmx</param>
        /// <param name="methodName">Example -> GetSZNBlobFile</param>
        /// <param name="nodeId">Example -> node1210</param>
        /// <param name="fileName">Example -> MATRIX_reversePD1Current_DSDBR01_TC419845.001_20150626080639_SM5_P.csv</param>
        /// <param name="timeDate">Example -> 20150625</param>
        /// <returns></returns>
        public static string GetBlobFilePath(string webServiceUrl, string methodName, string nodeId,
                                        string fileName, string timeDate,bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain) 
        {
            Hashtable ht;
            XmlDocument xmlResult = null;
            string strResult;
            byte[] bytes;
            int intTry = 0;
            string localDir = "";

            localDir = System.IO.Path.GetTempPath();
            if (Directory.Exists(localDir) == false)
            {
                return "error:" + localDir + "does not exists!";
            }

            if (webServiceUrl.Trim() == "")
            {
                return "error:webServiceUrl can not be null!";
            }

            if (methodName.Trim() == "")
            {
                return "error:methodName can not be null!";
            }

            if (nodeId.Trim() == "")
            {
                return "error:nodeId can not be null!";
            }

            if (fileName.Trim() == "")
            {
                return "error:fileName can not be null!";
            }

            ht = new Hashtable();
            ht.Add("fileName", fileName.Trim());
            ht.Add("NodeId", nodeId.Trim().ToLower());
            ht.Add("timeDate", timeDate.Trim());

            //Get data from webservice, try 3 times
            while (intTry < 3)
            {
                try
                {
                    xmlResult = WebServiceCaller.QuerySoapWebService(webServiceUrl.Trim(), methodName, ht, UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
                    break;
                }
                catch (WebException ex)
                {
                    if (intTry == 2)
                    {
                        return "error:Fail to call the webservice " + webServiceUrl + methodName + ", Error message: " + ex.Message;
                    }
                    else
                    {
                        intTry = intTry + 1;
                        Thread.Sleep(2000);
                    }
                }
            }
            #region
            if (xmlResult != null)
            {
                XmlElement xml = (XmlElement)xmlResult.SelectSingleNode("root");
                strResult = xml.InnerText;
                if (strResult.ToUpper().StartsWith("FAIL"))
                {
                    return "error:" + strResult;
                }
                else if (strResult.ToUpper().StartsWith("NOT FOUND"))
                {
                    return "error:" + strResult;
                }
                else if (strResult.ToUpper().StartsWith(@"\\"))
                {
                    intTry = 0;
                    while (intTry < 3)
                    {
                        try
                        {
                            File.Copy(strResult, localDir + fileName, true);
                            File.Delete(localDir + fileName);
                            return strResult;
                        }
                        catch (Exception ex)
                        {
                            if (intTry == 2)
                            {
                                return "error:File to copy the file " + fileName + " from file server.Error message: " + ex.Message;
                            }
                            else
                            {
                                intTry = intTry + 1;
                                Thread.Sleep(2000);
                            }
                        }
                    }
                }
                else if (strResult.ToUpper().StartsWith("BASE64:"))
                {
                    try
                    {
                        bytes = Convert.FromBase64String(strResult.Substring(7));

                        FileStream file = new FileStream(localDir + fileName, FileMode.OpenOrCreate);
                        file.Write(bytes, 0, bytes.Length);
                        file.Flush();
                        file.Close();
                        return localDir + fileName;
                    }
                    catch (Exception ex)
                    {
                        return "error:File to rebuild the file " + fileName + " with binary data.Error message: " + ex.Message;
                    }
                }
            }
            #endregion

            return "error:";
        }

    }


    /// <summary>
    ///  Use WebRequest/WebResponse call WebService
    /// </summary>
    public class WebServiceCaller
    {

        #region Tip:使用说明
        //webServices 应该支持Get和Post调用，在web.config应该增加以下代码
        //<webServices>
        //  <protocols>
        //    <add name="HttpGet"/>
        //    <add name="HttpPost"/>
        //  </protocols>
        //</webServices>

        //调用示例：
        //Hashtable ht = new Hashtable();  //Hashtable 为webservice所需要的参数集
        //ht.Add("str", "test");
        //ht.Add("b", "true");
        //XmlDocument xx = WebSvcCaller.QuerySoapWebService("http://localhost:81/service.asmx", "HelloWorld", ht);
        //MessageBox.Show(xx.OuterXml);
        #endregion

        /// <summary>
        /// Need WebService support Post calling
        /// </summary>
        public static XmlDocument QueryPostWebService(string URL, string MethodName, Hashtable Pars, bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(URL + "/" + MethodName);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            SetWebRequest(request, UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
            byte[] data = EncodePars(Pars);
            WriteRequestData(request, data);
            return ReadXmlResponse(request.GetResponse());
        }

        /// <summary>
        /// Need WebService support Get calling
        /// </summary>
        public static XmlDocument QueryGetWebService(string URL, string MethodName, Hashtable Pars, bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(URL + "/" + MethodName + "?" + ParsToString(Pars));
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            SetWebRequest(request, UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
            return ReadXmlResponse(request.GetResponse());
        }

        /// <summary>
        ///  Call WebService
        /// </summary>
        public static XmlDocument QuerySoapWebService(string URL, string MethodName, Hashtable Pars, bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain)
        {
            if (_xmlNamespaces.ContainsKey(URL))
            {
                return QuerySoapWebService(URL, MethodName, Pars, _xmlNamespaces[URL].ToString(), UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
            }
            else
            {
                return QuerySoapWebService(URL, MethodName, Pars, GetNamespace(URL, UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain), UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
            }
        }

        private static XmlDocument QuerySoapWebService(string URL, string MethodName, Hashtable Pars, string XmlNs, bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain)
        {
            _xmlNamespaces[URL] = XmlNs;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "text/xml; charset=utf-8";
            request.Headers.Add("SOAPAction", "\"" + XmlNs + (XmlNs.EndsWith("/") ? "" : "/") + MethodName + "\"");
            SetWebRequest(request, UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
            byte[] data = EncodeParsToSoap(Pars, XmlNs, MethodName);
            WriteRequestData(request, data);
            XmlDocument doc = new XmlDocument(), doc2 = new XmlDocument();
            doc = ReadXmlResponse(request.GetResponse());

            XmlNamespaceManager mgr = new XmlNamespaceManager(doc.NameTable);
            mgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            String RetXml = doc.SelectSingleNode("//soap:Body/*/*", mgr).InnerXml;
            doc2.LoadXml("<root>" + RetXml + "</root>");
            AddDelaration(doc2);
            return doc2;
        }
        private static string GetNamespace(string URL, bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL + "?WSDL");
            SetWebRequest(request, UseSystemProxy, ProxyIP, ProxyPort,
                                        ProxyUserName, ProxyPassword, ProxyDomain);
            WebResponse response = request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sr.ReadToEnd());
            sr.Close();
            return doc.SelectSingleNode("//@targetNamespace").Value;
        }

        private static byte[] EncodeParsToSoap(Hashtable Pars, string XmlNs, string MethodName)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"></soap:Envelope>");
            AddDelaration(doc);
            //XmlElement soapBody = doc.createElement_x_x("soap", "Body", "http://schemas.xmlsoap.org/soap/envelope/");
            XmlElement soapBody = doc.CreateElement("soap", "Body", "http://schemas.xmlsoap.org/soap/envelope/");
            //XmlElement soapMethod = doc.createElement_x_x(MethodName);
            XmlElement soapMethod = doc.CreateElement(MethodName);
            soapMethod.SetAttribute("xmlns", XmlNs);
            foreach (string k in Pars.Keys)
            {
                //XmlElement soapPar = doc.createElement_x_x(k);
                XmlElement soapPar = doc.CreateElement(k);
                soapPar.InnerXml = ObjectToSoapXml(Pars[k]);
                soapMethod.AppendChild(soapPar);
            }
            soapBody.AppendChild(soapMethod);
            doc.DocumentElement.AppendChild(soapBody);
            return Encoding.UTF8.GetBytes(doc.OuterXml);
        }
        private static string ObjectToSoapXml(object o)
        {
            XmlSerializer mySerializer = new XmlSerializer(o.GetType());
            MemoryStream ms = new MemoryStream();
            mySerializer.Serialize(ms, o);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Encoding.UTF8.GetString(ms.ToArray()));
            if (doc.DocumentElement != null)
            {
                return doc.DocumentElement.InnerXml;
            }
            else
            {
                return o.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        private static void SetWebRequest(HttpWebRequest request, bool UseSystemProxy, string ProxyIP, int ProxyPort,
                                        string ProxyUserName, string ProxyPassword, string ProxyDomain)
        {
            if (UseSystemProxy)
            {
                request.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
            }
            else
            {
                WebProxy proxy = new WebProxy(ProxyIP, ProxyPort);
                proxy.Credentials = new NetworkCredential(ProxyUserName, ProxyPassword, ProxyDomain);
                request.Proxy = proxy;
            }
            request.Timeout = 10000;
        }

        private static void WriteRequestData(HttpWebRequest request, byte[] data)
        {
            request.ContentLength = data.Length;
            Stream writer = request.GetRequestStream();
            writer.Write(data, 0, data.Length);
            writer.Close();
        }

        private static byte[] EncodePars(Hashtable Pars)
        {
            return Encoding.UTF8.GetBytes(ParsToString(Pars));
        }

        private static String ParsToString(Hashtable Pars)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string k in Pars.Keys)
            {
                if (sb.Length > 0)
                {
                    sb.Append("&");
                }
                //sb.Append(HttpUtility.UrlEncode(k) + "=" + HttpUtility.UrlEncode(Pars[k].ToString()));
            }
            return sb.ToString();
        }

        private static XmlDocument ReadXmlResponse(WebResponse response)
        {
            StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            String retXml = sr.ReadToEnd();
            sr.Close();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(retXml);
            return doc;
        }

        private static void AddDelaration(XmlDocument doc)
        {
            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.InsertBefore(decl, doc.DocumentElement);
        }

        private static Hashtable _xmlNamespaces = new Hashtable();
    }

}

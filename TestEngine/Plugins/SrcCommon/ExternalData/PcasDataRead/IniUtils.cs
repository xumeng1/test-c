// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasDataRead\IniUtils.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// Provides read access to INI files.
	/// Original code from
	/// http://archive.devx.com/dotnet/discussions/040902/cominterop.asp
	/// </summary>
	internal class NativeMethods
	{
		/// <summary>
		/// Default constructor.
		/// </summary>
		private NativeMethods()
		{
		}

		/// <summary>
		/// Reads from an ini file.
		/// </summary>
		/// <param name="lpAppName">The name of the section containing the key name</param>
		/// <param name="lpKeyName">The name of the key whose associated string is to be retrieved</param>
		/// <param name="lpDefault">Default value if no data is found.</param>
		/// <param name="lpReturnedString">A string buffer to hold the return value. Allocate space first.</param>
		/// <param name="nSize">The size of the buffer 'lpReturnedString'</param>
		/// <param name="lpFileName">The file name of the ini file to be accessed</param>
		/// <returns>An integer representing the number of characters copied to the buffer, not including the terminating null character</returns>
		[ DllImport("KERNEL32.DLL", 
			  EntryPoint="GetPrivateProfileString")]
		protected internal static extern int 
			GetPrivateProfileString(string lpAppName, 
			string lpKeyName, string lpDefault, 
			StringBuilder lpReturnedString, int nSize, 
			string lpFileName);
     
      
		/// <summary>
		/// Read a string from an INI file.
		/// </summary>
		/// <param name="filename">The name of the file.</param>
		/// <param name="section">The section name containing the key to be retrieved.</param>
		/// <param name="key">The key whose value is to be retrieved.</param>
		/// <returns>The value associated with the key. Return NULL if either section or key are not found, or "" if no value found for the key.</returns>
		public static String GetINIValue(String filename, 
			String section, String key) 
		{
			StringBuilder buffer = new StringBuilder(256);
			string sDefault = "";
			if (GetPrivateProfileString(section, key, sDefault, 
				buffer, buffer.Capacity, filename) != 0 ) 
			{
				string iniValue = buffer.ToString();
				int semicolonPos = iniValue.IndexOf(";");
				if ( semicolonPos > -1 ) 
				{
					// Remove any comments and trim
					iniValue = iniValue.Substring(0,semicolonPos).Trim();
				}
				return iniValue;
			}
			else 
			{
				return null;
			}
		}

	}
}

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.ExternalData;
using Bookham.TestEngine.Framework.Limits;

namespace TEST
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test   
        PcasFileLimitsRead limitsRead;
        #endregion


        #region Constants for use during test.
        string fileName = "../../testPcasFile.csv";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.UnhandledExceptionsHandler.Initialise();
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");
#warning Hack to make this compile OK, won't run! - PJA March 2007
            limitsRead = new PcasFileLimitsRead(null, "");
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {            

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_LoadFile()
        {
            TestOutput("\n\n*** T01_LoadFile ***");
            StringDictionary keys = new StringDictionary();
            keys.Add("Filename", this.fileName);
            SpecList specs = limitsRead.GetLimit(keys);
            foreach (Specification spec in specs)
            {
                TestOutput("*** SPECIFICATION: " + spec.Name);
                TestOutput("Nbr Limits = " + spec.Count.ToString());
                foreach (ParamLimit lim in spec)
                {
                    TestOutput("LIMIT: " + lim.ExternalName);
                }
            }
        }
        


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

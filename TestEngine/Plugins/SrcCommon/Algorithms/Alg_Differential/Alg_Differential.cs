// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_Differential.cs
//
// Author: Mark Fullalove
// Design: From Bookham "PcLeslie" software

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Provides methods to calculate the differential of a set of values.
    /// </summary>
    public sealed class Alg_Differential
    {
        /// <summary>
        /// Empty private constructor.
        /// </summary>
        private Alg_Differential()
        {
        }

        /// <summary>
        /// Calculate the differential by using the gradient of the two adjacent points.
        /// </summary>
        /// <param name="xArray">X axis data</param>
        /// <param name="yArray">Y axis data</param>
        /// <returns>The differential (dy/dx)</returns>
        public static double[] Differential(double[] xArray, double[] yArray)
        {
            // PRECONDITIONS
            if (xArray.Length != yArray.Length)
            {
                throw new AlgorithmException("Alg_Differential.Differential : X and Y data arrays are of unequal size");
            }
            if (xArray.Length == 0)
            {
                throw new AlgorithmException("Alg_Differential.Differential : Missing X and Y data");
            }
            if (xArray.Length < 3)
            {
                throw new AlgorithmException("Alg_Differential.Differential : Insufficient data to perform analysis");
            }

            double dx, dy;
            double[] dydx = new double[xArray.Length];
            dydx[0] = 0;

            for (int dataIndex = 1; dataIndex < xArray.Length - 1 ; dataIndex++)
            {
                dx = xArray[dataIndex + 1] - xArray[dataIndex - 1];
                if (dx == 0)
                {
                    throw new AlgorithmException("Alg_Differential.Differential : Duplicate X values");
                }

                dy = yArray[dataIndex + 1] - yArray[dataIndex - 1];
                dydx[dataIndex] = dy / dx;
            }

            // We can't calculate the end points so just duplicate the adjacent values. 
            dydx[dydx.Length - 1] = dydx[dydx.Length - 2];
            dydx[0] = dydx[1];

            return dydx;
        }

        /// <summary>
        /// Calculate the differential by using the gradient of the number of adjacent points specified.
        /// </summary>
        /// <param name="xArray">X axis data</param>
        /// <param name="yArray">Y axis data</param>
        /// <param name="SmoothingFactor">The weighting factor</param>
        /// <returns>The differential (dy/dx)</returns>
        public static double[] SmoothedDifferential(double[] xArray, double[] yArray, int SmoothingFactor)
        {
            // PRECONDITIONS            
            if (xArray.Length != yArray.Length)
            {
                throw new AlgorithmException("Alg_Differential.SmoothedDifferential : X and Y data arrays are of unequal size");
            }
            if (xArray.Length == 0)
            {
                throw new AlgorithmException("Alg_Differential.SmoothedDifferential : Missing X and Y data");
            }
            if (xArray.Length < 2 * SmoothingFactor + 1)
            {
                throw new AlgorithmException("Alg_Differential.SmoothedDifferential : Insufficient data to perform analysis");
            }
            if (SmoothingFactor <= 0)
            {
                throw new AlgorithmException("Alg_Differential.SmoothedDifferential : SmoothingFactor must be > 0");
            }

            double[] dydx = new double[xArray.Length];

            for (int dataIndex = SmoothingFactor; dataIndex < dydx.Length - SmoothingFactor; dataIndex++)
            {
                double dx = 0;
                double dy = 0;

                for (int smoothingIndex = -SmoothingFactor; smoothingIndex <= SmoothingFactor; smoothingIndex++)
                {
                    dx += smoothingIndex * xArray[dataIndex + smoothingIndex];
                    dy += smoothingIndex * yArray[dataIndex + smoothingIndex];
                }

                if (dx == 0)
                {
                    // In unusual cases, the X values could sum to 0. 
                    // In this case we can't analyse the data point.
                    if (dy == 0)
                    {
                        // 0 divided by 0 = NaN
                        dydx[dataIndex] = Double.NaN;
                    }
                    else
                    {
                        if (dy > 0)
                        {
                            // +N divided by 0 = Positive Infinity
                            dydx[dataIndex] = Double.PositiveInfinity;
                        }
                        else
                        {                            
                            // -N divided by 0 = Negative Infinity
                            dydx[dataIndex] = Double.NegativeInfinity;
                        }
                    }
                }
                else
                {
                    dydx[dataIndex] = dy / dx;
                }

                // backfill the ends of the array
                for (int smoothingIndex = 1; smoothingIndex <= SmoothingFactor; smoothingIndex++)
                {
                    dydx[dydx.Length - smoothingIndex] = dydx[dydx.Length - SmoothingFactor - 1];
                    dydx[smoothingIndex-1] = dydx[SmoothingFactor];
                }
            }
            return dydx;
        }

        /// <summary>
        /// Calculate the 3 point smoothed differential by using the gradient of adjacent points.
        /// This equates to the C++ method in Algorithm_Differential_Weighted.
        /// </summary>
        /// <param name="xArray">X axis data</param>
        /// <param name="yArray">Y axis data</param>
        /// <returns>The differential (dy/dx)</returns>
        public static double[] SmoothedDifferential(double[] xArray, double[] yArray)
        {
            return SmoothedDifferential(xArray, yArray, 3);
        }
    }
}

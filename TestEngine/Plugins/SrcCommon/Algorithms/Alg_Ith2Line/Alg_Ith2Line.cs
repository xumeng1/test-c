// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_Ith2Line.cs
//
// Author: Rong Guo
// Design: 

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Algrithm class to calculate ITH as the current at which the two fit lines intersect
    /// </summary>
    public sealed class Alg_Ith2Line
    {
        /// <summary>
        /// Private constructor
        /// </summary>
        private Alg_Ith2Line()
        {
        }

        /// <summary>
        /// Calculate ITH as the current at which the two fit lines interset
        /// </summary>
        /// <param name="currentArray">Current array</param>
        /// <param name="powerArray">Power array</param>
        /// <param name="startPower">Start power</param>
        /// <param name="stopPower">Stop power</param>
        /// <param name="discountFactor">Discount factor - must be between 0 and 1</param>
        /// <returns></returns>
        public static double CalculateIth2Line(double[] currentArray, double[] powerArray, double startPower, double stopPower, double discountFactor)
        {
            //
            // Preconditions
            //
            if (currentArray.Length == 0)
            {
                throw new AlgorithmException("Alg_Ith2Line - currentArray is empty!");
            }
            if (powerArray.Length == 0)
            {
                throw new AlgorithmException("Alg_Ith2Line - powerArray is empty!");
            }
            if (currentArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Alg_Ith2Line - currentArray & powerArray must be the same size!");
            }
            if (startPower > stopPower)
            {
                throw new AlgorithmException("Alg_Ith2Line - startPower > stopPower!");
            }
            if (discountFactor < 0 || discountFactor > 1)
            {
                throw new AlgorithmException("Alg_Ith2Line �� discountFactor must be between 0 and 1!");
            }

            //
            // Find indices corresponding to startPower & stopPower
            //
            // This will be slightly larger fit which we can justify by claiming that its guard band,
            // As unless we have massive step sizes shouldn't produce a significant error
            BoundingIndices boundingIndicesForStartPower = BoundingIndicesAlgorithm.Calculate(powerArray, startPower);
            BoundingIndices boundingIndicesForStopPower = BoundingIndicesAlgorithm.Calculate(powerArray, stopPower);

            //
            // Fit Line #1: from startPower to stopPower and work out the xIntercept
            //
            LinearLeastSquaresFit linearLeastSquaresFitLine1 = LinearLeastSquaresFitAlgorithm.Calculate(
                currentArray, powerArray, boundingIndicesForStartPower.IndexOfArrayValueLessThan, boundingIndicesForStopPower.IndexOfArrayValueGreaterThan);
            double ith1L = -linearLeastSquaresFitLine1.YIntercept / linearLeastSquaresFitLine1.Slope;

            //
            // Find the index at which current = ith1L * discountFactor
            //
            BoundingIndices tempIndex = BoundingIndicesAlgorithm.Calculate(currentArray, ith1L * discountFactor);

            //
            // Fit Line #2: from 0 to the above point
            //
            // This will be slightly larger fit which we can justify by claiming that its guard band,
            // As unless we have massive step sizes shouldn't produce a significant error
            LinearLeastSquaresFit linearLeastSquaresFitLine2 = LinearLeastSquaresFitAlgorithm.Calculate(
                currentArray, powerArray, 0, tempIndex.IndexOfArrayValueGreaterThan);

            // Hopefully, the two slopes will be different, otherwise we might as well do a one line fit
            if (Math.Abs(linearLeastSquaresFitLine2.Slope - linearLeastSquaresFitLine1.Slope) < 0.0001)
            {
                throw new AlgorithmException("ith2L - slope1-slope2 will generate a divide-by-zero error!");
            }

            //
            // Threshold is the current at which the two fit lines intersect
            //
            double ith2L = (linearLeastSquaresFitLine2.YIntercept - linearLeastSquaresFitLine1.YIntercept) / (linearLeastSquaresFitLine1.Slope - linearLeastSquaresFitLine2.Slope);
            return ith2L;
        }
    }
}

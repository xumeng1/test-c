// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_FreqWlConvert.cs
//
// Author: Paul Annetts
// Design: 

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Algorithm class to convert between frequency and wavelength
    /// </summary>
    public sealed class Alg_FreqWlConvert
    {
        /// <summary>
        /// Private constructor
        /// </summary>
        private Alg_FreqWlConvert() { }

        /// <summary>
        /// The speed of light in meters/second
        /// </summary>
        /// <remarks>Reference: NIST website http://physics.nist.gov/cgi-bin/cuu/Value?c</remarks>
        public const double SpeedLight_m_p_s = 299792458.0;

        /// <summary>
        /// Convert wavelength to frequency
        /// </summary>
        /// <param name="wave_m">Wavelength in meters</param>
        /// <returns>Frequency in Hertz</returns>
        public static double Wave_m_TO_Freq_Hz(double wave_m)
        {
            double freq_Hz = SpeedLight_m_p_s / wave_m;
            return freq_Hz;
        }

        /// <summary>
        /// Convert frequency to wavelength 
        /// </summary>
        /// <param name="freq_Hz">Frequency in Hertz</param>
        /// <returns>Wavelength in meters</returns>
        public static double Freq_Hz_TO_Wave_m(double freq_Hz)
        {
            double wave_m = SpeedLight_m_p_s / freq_Hz;
            return wave_m;
        }

        /// <summary>
        /// Convert wavelength to frequency
        /// </summary>
        /// <param name="wave_nm">Wavelength in nanometers</param>
        /// <returns>Frequency in GigaHertz</returns>
        public static double Wave_nm_TO_Freq_GHz(double wave_nm)
        {
            // Would need multiply by 1e-9 to convert from nm to m.
            // And then to multiply by 1e9 to convert from Hz to GHz.
            // These cancel out, so do no multiplication.
            double freq_GHz = SpeedLight_m_p_s / wave_nm;
            return freq_GHz;
        }

        /// <summary>
        /// Convert frequency to wavelength 
        /// </summary>
        /// <param name="freq_GHz">Frequency in GHz</param>
        /// <returns>Wavelength in meters</returns>
        public static double Freq_GHz_TO_Wave_nm(double freq_GHz)
        {
            // Would need multiply by 1e9 to convert from GHz to Hz.
            // And then to multiply by 1e-9 to convert from nm to m.
            // These cancel out, so do no multiplication.
            double wave_nm = SpeedLight_m_p_s / freq_GHz;
            return wave_nm;
        }
    }
}

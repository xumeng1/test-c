using System;



namespace Bookham.TestLibrary.Algorithms
{
	/// <summary>
	/// Struct describing the straight line resulting from a linear least squares fit
	/// </summary>
	public class LinearLeastSquaresFit
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="slope">Slope of fitted line</param>
		/// <param name="yIntercept">Y-axis intercept of fitted line</param>
        /// <param name="correlationCoefficient">Correlation coefficient of fit</param>
		public LinearLeastSquaresFit( double slope, double yIntercept, double correlationCoefficient )
		{
			this.Slope = slope;
			this.YIntercept = yIntercept;
			this.CorrelationCoefficient = correlationCoefficient;
		}

		/// <summary>Slope of fitted line</summary>
		public readonly double Slope;
		/// <summary>Y-axis intercept of fitted line</summary>
		public readonly double YIntercept;
		/// <summary>Correlation coefficient (quality of fit)</summary>
		public readonly double CorrelationCoefficient;
	}
}

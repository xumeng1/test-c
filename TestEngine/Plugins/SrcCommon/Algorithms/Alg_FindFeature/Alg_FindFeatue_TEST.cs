// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_Differential_TEST.cs
//
// Author: Mark Fullalove
// Coverage : nCover reports 100% (excluding private ctor)

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.Algorithms;

// Disable missing XML comment warnings for this
#pragma warning disable 1591

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Test harness for Alg_FindFeature
    /// </summary>
    [TestFixture]
    public class Alg_FindFeature_TEST
    {
        [Test]
        public void Valid_pos_FindAbsMax()
        {
            double[] dataSet = { -19, 20, 7, 44, 301, 299 };
            double absMax = Alg_FindFeature.FindAbsoluteMaxWithSign(dataSet);
            Assert.AreEqual(absMax, 301);
        }
        
        [Test]
        public void Valid_neg_FindAbsMax()
        {
            double[] dataSet = { -19, 20, 7, 44, -300, 299 };
            double absMax = Alg_FindFeature.FindAbsoluteMaxWithSign(dataSet);
            Assert.AreEqual(absMax, -300);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoData_FindAbsMax()
        {
            double[] dataSet = { };
            double absMax = Alg_FindFeature.FindAbsoluteMaxWithSign(dataSet);
        }

        [Test]
        public void Valid_pos_FindAbsMin()
        {
            double[] dataSet = { -19, 20, 7, 44, 301, 299 };
            double absMax = Alg_FindFeature.FindAbsoluteMinWithSign(dataSet);
            Assert.AreEqual(absMax, 7);
        }

        [Test]
        public void Valid_neg_FindAbsMin()
        {
            double[] dataSet = { -19, 20, 7, 44, -6.9, -300, 299 };
            double absMax = Alg_FindFeature.FindAbsoluteMinWithSign(dataSet);
            Assert.AreEqual(absMax, -6.9);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoData_FindAbsMin()
        {
            double[] dataSet = { };
            double absMax = Alg_FindFeature.FindAbsoluteMinWithSign(dataSet);
        }

        [Test]
        public void Valid_FindAbsMaxOfPair()
        {
            double a = -0.9;
            double b = 0.5;
            double absMax = Alg_FindFeature.FindAbsoluteMaxWithSign(a, b);
            Assert.AreEqual(absMax, -0.9);
        }

        [Test]
        public void Valid_FindMaxPeak()
        {
            // For 3 point analysis, index 18 is NOT a peak, neither is the last point
            double[] dataSet = { 1, 2, 3, 4, 5, 6, 5, 4, 3, 4, 5, 6, 7, 8, 7, 6, 7, 8, 9, 8, 9, 10 };
            int peakIndex = Alg_FindFeature.FindIndexForMaxPeak(dataSet);
            Assert.AreEqual(dataSet[peakIndex],8);
            
            // A repeat of the above
            peakIndex = Alg_FindFeature.FindIndexForMaxPeak(dataSet,3);
            Assert.AreEqual(dataSet[peakIndex], 8);

            // 2 point analysis finds the final peak
            peakIndex = Alg_FindFeature.FindIndexForMaxPeak(dataSet, 2);
            Assert.AreEqual(dataSet[peakIndex], 9);

            // 5 point analysis finds the first peak
            peakIndex = Alg_FindFeature.FindIndexForMaxPeak(dataSet, 5);
            Assert.AreEqual(dataSet[peakIndex], 6);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoData_FindMaxPeak()
        {
            double[] dataSet = { };
            int peakIndex = Alg_FindFeature.FindIndexForMaxPeak(dataSet);
        }

        [Test]
        public void Valid_FindMinValley()
        {
            // For 3 point analysis, index 18 is NOT a peak, neither is the last point
            double[] dataSet = { -1, -2, -3, -4, -5, -6, -5, -4, -3, -4, -5, -6, -7, -8, -7, -6, -7, -8, -9, -8, -9, -10 };
            int peakIndex = Alg_FindFeature.FindIndexForMinValley(dataSet);
            Assert.AreEqual(dataSet[peakIndex], -8);

            // A repeat of the above
            peakIndex = Alg_FindFeature.FindIndexForMinValley(dataSet, 3);
            Assert.AreEqual(dataSet[peakIndex], -8);

            // 2 point analysis finds the final valley
            peakIndex = Alg_FindFeature.FindIndexForMinValley(dataSet, 2);
            Assert.AreEqual(dataSet[peakIndex], -9);

            // 5 point analysis finds the first valley
            peakIndex = Alg_FindFeature.FindIndexForMinValley(dataSet, 5);
            Assert.AreEqual(dataSet[peakIndex], -6);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoData_FindMinValley()
        {
            double[] dataSet = { };
            int peakIndex = Alg_FindFeature.FindIndexForMinValley(dataSet);
        }

        [Test]
        public void Valid_FindAllValleys()
        {
            // For 3 point analysis, index 18 is NOT a peak, neither is the last point
            double[] dataSet = { -1, -2, -3, -4, -5, -6, -5, -4, -3, -4, -5, -6, -7, -8, -7, -6, -7, -8, -9, -8, -9, -10 };
            int[] peakIndexes = Alg_FindFeature.FindIndexesForAllValleys(dataSet);
            Assert.AreEqual(peakIndexes.Length, 2);
            Assert.AreEqual(peakIndexes[0], 5);
            Assert.AreEqual(peakIndexes[1], 13);

            peakIndexes = Alg_FindFeature.FindIndexesForAllValleys(dataSet,2);
            Assert.AreEqual(peakIndexes.Length, 3);
            Assert.AreEqual(peakIndexes[0], 5);
            Assert.AreEqual(peakIndexes[1], 13);
            Assert.AreEqual(peakIndexes[2], 18);
        }

        [Test]
        public void Valid_FindAllPeaks()
        {
            // For 3 point analysis, index 18 is NOT a peak, neither is the last point
            double[] dataSet = { 1, 2, 3, 4, 5, 6, 5, 4, 3, 4, 5, 6, 7, 8, 7, 6, 7, 8, 9, 8, 9, 10 };
            int[] peakIndexes = Alg_FindFeature.FindIndexesForAllPeaks(dataSet);
            Assert.AreEqual(peakIndexes.Length, 2);
            Assert.AreEqual(peakIndexes[0], 5);
            Assert.AreEqual(peakIndexes[1], 13);

            peakIndexes = Alg_FindFeature.FindIndexesForAllPeaks(dataSet, 2);
            Assert.AreEqual(peakIndexes.Length, 3);
            Assert.AreEqual(peakIndexes[0], 5);
            Assert.AreEqual(peakIndexes[1], 13);
            Assert.AreEqual(peakIndexes[2], 18);
        }
    }
}
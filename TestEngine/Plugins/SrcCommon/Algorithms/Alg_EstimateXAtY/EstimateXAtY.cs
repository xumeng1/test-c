// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// EstimateXAtY.cs
//
// Author: Bill P. Godfrey
// Design: Algorithm Estimate X at Y by Paul Annetts.

using System;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// This algorithm performs an estimation of x, given a y value and a Euclid line.
    /// </summary>
    public sealed class EstimateXAtY
    {
        /// <summary>
        /// Calculate x for y_target, given a base (x1,y1) point, a (dy/dx) gradient.
        /// </summary>
        /// <param name="x1">Base point, x co-ordinate.</param>
        /// <param name="y1">Base point, y co-ordinate.</param>
        /// <param name="dy_by_dx">(dy/dx) gradient.</param>
        /// <param name="y_target">Target y value on slope.</param>
        /// <returns>Estimated x value for target y.</returns>
        public static double Calculate(double x1, double y1, double dy_by_dx, double y_target)
        {
            return x1 + ((y_target - y1) / dy_by_dx);
        }

        /// <summary>
        /// Private constructor for static-only sealed class.
        /// </summary>
        private EstimateXAtY()
        { }
    }
}

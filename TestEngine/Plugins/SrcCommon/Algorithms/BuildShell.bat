@echo off
echo Adding SDK 2.0 to path
set VS_PATH="C:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
PATH=%PATH%;%VS_PATH%

echo Adding AccuRev to path
set ACC_PATH="C:\Program Files\AccuRev\bin"
PATH=%PATH%;%ACC_PATH%

echo Adding .NET Framework 2.0 bin to path
set DOTNET2_PATH=%windir%\Microsoft.NET\Framework\v2.0.50727
PATH=%PATH%;%DOTNET2_PATH%

echo ...
cmd

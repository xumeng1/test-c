using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            double resistance = SteinhartHart.TemperatureCelsiusToResistanceOhms(25, 0.001129148, 0.000234125, 8.76741E-8);
            double temperature = SteinhartHart.ResistanceOhmsToTemperatureCelsius(10000, 0.001129148, 0.000234125, 8.76741E-8);
            Console.WriteLine("Thermistor Resistance is {0} \n Temperature is {1}", resistance, temperature);

            temperature = CallendarVanDusen.ResistanceOhmsToTemperatureCelsius(115.54, 100, 0.003850, 0.10863, 1.4999);
            resistance = CallendarVanDusen.TemperatureCelsiusToResistanceOhms(40, 100, 0.003850, 0.10863, 1.4999);
            Console.WriteLine("PT100 RTD Resistance is {0} \n Temperature is {1}", resistance, temperature);
            Console.ReadLine();
        }
    }
}

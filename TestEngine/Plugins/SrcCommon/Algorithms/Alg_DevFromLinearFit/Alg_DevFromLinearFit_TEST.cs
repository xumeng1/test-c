using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.Algorithms;

// Disable missing XML comment warnings for this
#pragma warning disable 1591

namespace Bookham.TestLibrary.Algorithms
{
    [TestFixture]
    public class Alg_DevFromLinearFit_TEST
    {
        [Test]
        public void TestDevFromLinearFit1()
        {
            //using very basic data:
            //based on straight line y=mx +c.  In this case y = 1x + 0
            double[] xArray = {0,1,2,3,4,5};
            double[] yArray = {0,1,2,3,4,5};
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit(xArray, yArray, 0, 5);
            
            Assert.AreEqual(results.FromIndexValueUsed,0);
            Assert.AreEqual(results.ToIndexValueUsed, 5);
            Assert.AreEqual(results.MaxAbsDeviationFromLinearValue, 0);
            Assert.AreEqual(results.MaxNegDevFromLinearValue,0);
            Assert.AreEqual(results.MaxPosDevFromLinearValue, 0);
            Assert.AreEqual(results.LinearFittedYAxisData[0], 0);
            Assert.AreEqual(results.LinearFittedYAxisData[1], 1);
            Assert.AreEqual(results.LinearFittedYAxisData[2], 2);
            Assert.AreEqual(results.LinearFittedYAxisData[3], 3);
            Assert.AreEqual(results.LinearFittedYAxisData[4], 4);
            Assert.AreEqual(results.LinearFittedYAxisData[5], 5);

        }
        [Test]
        public void TestDevFromLinearFit2()
        {
            //using very basic data:
            //based on straight line y=mx +c.  In this case y = 2x + 6
            double[] xArray = { 0, 1, 2, 3, 4, 5 };
            double[] yArray = { 6, 8, 10, 12, 14, 16 };
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit(xArray, yArray, 0, 5);

            Assert.AreEqual(results.FromIndexValueUsed, 0);
            Assert.AreEqual(results.ToIndexValueUsed, 5);
            Assert.AreEqual(results.MaxAbsDeviationFromLinearValue, 0);
            Assert.AreEqual(results.MaxNegDevFromLinearValue, 0);
            Assert.AreEqual(results.MaxPosDevFromLinearValue, 0);
            Assert.AreEqual(results.LinearFittedYAxisData[0], 6);
            Assert.AreEqual(results.LinearFittedYAxisData[1], 8);
            Assert.AreEqual(results.LinearFittedYAxisData[2], 10);
            Assert.AreEqual(results.LinearFittedYAxisData[3], 12);
            Assert.AreEqual(results.LinearFittedYAxisData[4], 14);
            Assert.AreEqual(results.LinearFittedYAxisData[5], 16);

        }
        [Test]
        public void TestDevFromLinearFit3()
        {
            //using very basic data:
            //based on straight line y=mx +c.  In this case y = 2x + 6 with 2 points 0.1 adrift
            double[] xArray = { 0, 1, 2, 3, 4, 5 };
            double[] yArray = { 6, 8.1, 9.9, 12, 14, 16 };
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit(xArray, yArray, 0, 5);

            Assert.AreEqual((results.MaxAbsDeviationFromLinearValue < 0.105),true);

        }
        [Test]
        public void TestDevFromLinearFit4()
        {
            //using very basic data:
            //based on straight line y=mx +c.  In this case y = 2x + 6
            //but calling devFromLinearFit_xValues and using a sub array
            double[] xArray = { 0, 1, 2, 3, 4, 5 };
            double[] yArray = { 6, 8, 10, 12, 14, 16 };
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit_xValues(xArray, yArray,1.1, 4.3);

            Assert.AreEqual(results.FromIndexValueUsed, 1);
            Assert.AreEqual(results.ToIndexValueUsed, 4);
            Assert.AreEqual(results.MaxAbsDeviationFromLinearValue, 0);
            Assert.AreEqual(results.MaxNegDevFromLinearValue, 0);
            Assert.AreEqual(results.MaxPosDevFromLinearValue, 0);
            Assert.AreEqual(results.LinearFittedYAxisData[0], 6);
            Assert.AreEqual(results.LinearFittedYAxisData[1], 8);
            Assert.AreEqual(results.LinearFittedYAxisData[2], 10);
            Assert.AreEqual(results.LinearFittedYAxisData[3], 12);
            Assert.AreEqual(results.LinearFittedYAxisData[4], 14);
            Assert.AreEqual(results.LinearFittedYAxisData[5], 16);

        }
        [Test]
        public void TestDevFromLinearFit5()
        {
            //using very basic data:
            //based on straight line y=mx +c.  In this case y = 2x + 6 with 2 points 0.1 adrift
            double[] xArray = { 0, 1, 2, 3, 4, 5 };
            double[] yArray = { 6, 8.1, 9.9, 12, 14, 16 };
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit_xValues(xArray, yArray, 0.2, 5.49);

            Assert.AreEqual((results.MaxAbsDeviationFromLinearValue < 0.105), true);

        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void TestDevFromLinearFit6()
        {
            //using very basic data:
            double[] xArray = { 0, 1,     2,  3, 4 };
            double[] yArray = { 6, 8.1, 9.9, 12, 16 };
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit(xArray, yArray,0, 5);  
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void TestDevFromLinearFit7()
        {
            //using very basic data:
            double[] xArray = { 0, 1, 2, 3, 4 };
            double[] yArray = { 6, 8.1, 9.9, 12, 16 };
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit(xArray, yArray, -1, 5);
        }
        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void TestDevFromLinearFit8()
        {
            //using very basic data:
            double[] xArray = { 0, 1, 2, 3, 4 };
            double[] yArray = null;
            Alg_DevFromLinearFit.DevFromLinearFitResults results = Alg_DevFromLinearFit.DevFromLinearFit(xArray, yArray, 0, 3);
        }
    }
}

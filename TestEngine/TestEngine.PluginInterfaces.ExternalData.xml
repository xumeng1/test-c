<?xml version="1.0"?>
<doc>
    <assembly>
        <name>TestEngine.PluginInterfaces.ExternalData</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException">
            <summary>
            Exceptions thrown by external data library components.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException.#ctor">
            <summary>
            Constructor with no parameters.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException.#ctor(System.String)">
            <summary>
            Constructor with an error message.
            </summary>
            <param name="callerMessage">Error Message</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException.#ctor(System.String,System.Exception)">
            <summary>
            Constructor with error message and caught exception.
            </summary>
            <param name="callerMessage">Error message</param>
            <param name="caughtException">An exception that has been caught.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.BookhamExternalDataException.#ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)">
            <summary>
            Constructor for use with with serialised data.
            </summary>
            <param name="info">Holds the serialized object data about the exception being thrown.</param>
            <param name="context">Contains contextual information about the source or destination.</param>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead">
            <summary>
            Interface to read data from an external data source.
            The implementation of the interface should throw an exception on construction if the data source is unavailable. 
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead.GetLatestResults(System.Collections.Specialized.StringDictionary,System.Boolean)">
            <summary>
            Retrieve previous data from the specified data source.
            </summary>
            <param name="keyObject">name/value pairs of fields required to identify the data. See plugin implementation for key requirements.</param>
            <param name="retrieveRawData">Set true to retrieve complex data objects (e.g. plot data) from the archive. Set false to return a link to the raw data file.</param>
            <returns>DatumList containing the previous results for a single component. If more than one row matches the key data the most recent row is returned.</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead.GetLatestResults(System.Collections.Specialized.StringDictionary,System.Boolean,System.String)">
            <summary>
            Retrieve previous data using a 'where' clause.
            </summary>
            <param name="keyObject">name/value pairs of fields required to identify the data. See plugin implementation for key requirements.</param>
            <param name="retrieveRawData">Set true to retrieve complex data objects (e.g. plot data) from the archive. Set false to return a link to the raw data file.</param>
            <param name="whereClause">A clause to add to the select SQL statement. Do not include the word 'WHERE'</param>
            <returns>DatumList containing the previous results for a single component. If more than one row matches the criteria the most recent row is returned.</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead.GetResults(System.Collections.Specialized.StringDictionary,System.Boolean,System.String)">
            <summary>
            Retrieves ALL previous results matching the keyObject and 'where' clause criteria. As this could potentially be a large amount of data, you MUST supply a 'where' clause.
            </summary>
            <param name="keyObject">name/value pairs of fields required to identify the data. See plugin implementation for key requirements.</param>
            <param name="retrieveRawData">Set true to retrieve complex data objects (e.g. plot data) from the archive. Set false to return a link to the raw data file.</param>
            <param name="whereClause">A clause to add to the select SQL statement. Do not include the word 'WHERE'</param>
            <returns>Array of DatumLists containing previous test results.</returns>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.ExternalData.IDataWrite">
            <summary>
            Interface to write data to an external data source.
            The implementation of the interface should throw an exception on construction if the data source is unavailable.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.IDataWrite.Write(Bookham.TestEngine.Framework.Limits.TestData,System.Collections.Specialized.StringDictionary,System.String,System.Boolean,System.Boolean)">
            <summary>
            Writes data to a single data source
            </summary>
            <param name="testData">Collection of measured data from the test</param>
            <param name="keyData">Name/Value pairs of fields/values to specify how to write the data</param>
            <param name="specName">Internal spec id to identify the values within the testData to be written.</param>
            <param name="ignoreMissing">False means that any missing limit-checked data will throw an error</param>
            <param name="usingUtcDateTime">If true,use UTC Date Time,otherwise,use local date time.</param>
            <returns>Was there missing data? Always false if "ignoreMissing" is true</returns>		
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.ExternalData.ILimitRead">
            <summary>
            Interface to read limits from an external data source.
            The implementation of the interface should throw an exception on construction if the data source is unavailable. 
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.ILimitRead.GetLimit(System.Collections.Specialized.StringDictionary)">
            <summary>
            Interface to read limits from a data source.
            </summary>
            <param name="keyData">Name/Value pairs identifying the limits</param>
            <returns>SpecList containing one or more Specifications</returns>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.ExternalData.ILogging">
            <summary>
            External Data Plug-ins Logging Interface
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.ILogging.ErrorInExternalDataPlugin(System.String)">
            <summary>
            Called when user wants to indicate an External Data Plug-in error has occured - and 
            throw an exception
            </summary>
            <param name="description">Message to be logged</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.ILogging.ErrorWrite(System.String)">
            <summary>
            Called when user wants to indicate an error has occured - write
            into the error log and continue execution
            </summary>
            <param name="description">Message to be logged</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.ILogging.DatabaseTransactionWrite(System.String,System.String)">
            <summary>
            Method to be used by plugin components to call Log.DatabaseTransactionWrite
            </summary>
            <param name="searchLabel">A label to be used to filter log messages</param>
            <param name="message">The messasge to be logged.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.ILogging.DeveloperWrite(System.String,System.String)">
            <summary>
            Method to be used by plugin components to call Log.DeveloperWrite
            </summary>
            <param name="searchLabel">A label to be used to filter log messages</param>
            <param name="userMessage">The messasge to be logged.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.ExternalData.ILogging.DebugOut(System.String,System.String)">
            <summary>
            Method to be used by plugin components to call BugOut
            </summary>
            <param name="label">A label to be used to filter log messages</param>
            <param name="message">The messasge to be logged.</param>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.ExternalData.NamespaceDoc">
            <summary>
            Test Engine External Data Plug-ins Interface Definition.
            </summary>
        </member>
    </members>
</doc>

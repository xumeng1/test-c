<?xml version="1.0"?>
<doc>
    <assembly>
        <name>InstType_MultiMeter</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.InstrTypes.IInstType_MultiMeter">
            <summary>
            Multi-meter channel controller interface.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.IInstType_MultiMeter.GetReading">
            <summary>
            Reads the value measured on the multimeter channel. Units depend on 
            the current mode of the multimeter.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_MultiMeter.MultiMeterMode">
            <summary>
            Reads/sets the multimeter channel's measurement mode.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_MultiMeter.AcType">
            <summary>
            Reads the multimeter channel's AC measurement type (true RMS, or calculated RMS from average).
            This will be fixed for a given instrument.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_MultiMeter.MeterRange">
            <summary>        
            Reads/sets the manual measurement range for the multimeter channel. 
            If "InstType_MultiMeter.AutoRange" is provided or returned, this is the value denoting auto-ranging.
            NOTE: If an exact match is not possible the closest value ABOVE that supplied will be used.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_MultiMeter.IntegrationTime_s">
            <summary>        
            Reads/sets the integration time (measurement averaging) in seconds for the multimeter channel. 
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter">
            <summary>
            Multi-meter channel controller.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.AutoRange">
            <summary>
            Autorange value constant.
            Assigned the value double.NaN (not a number) in this case representing a flag for autorange 
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.Overload">
            <summary>
            Overload value constant.
            Assigned the value double.PositiveInfinity in this case representing a flag for overload
            condition
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.IsAutoRange(System.Double)">
            <summary>
            Helper function to determine if a value represents an autorange condition
            </summary>
            <param name="val">value</param>
            <returns>true if autorange, false otherwise</returns>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.IsOverload(System.Double)">
            <summary>
            Helper function to determine if a value represents an overload condition
            </summary>
            <param name="val">value</param>
            <returns>true if overload, false otherwise</returns>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.#ctor(System.String,System.String,System.String,System.String,Bookham.TestEngine.PluginInterfaces.Chassis.Chassis)">
            <summary>
            Constructor.
            </summary>
            <param name="instrumentName">Instrument name</param>
            <param name="driverName">Instrument driver name</param>
            <param name="slotId">Slot ID for the instrument</param>
            <param name="subSlotId">Sub Slot ID for the instrument</param>
            <param name="chassis">Chassis through which the instrument communicates</param>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.GetReading">
            <summary>
            Reads the value measured on the multimeter channel. Units depend on 
            the current mode of the multimeter.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MultiMeterMode">
            <summary>
            Reads/sets the multimeter channel's measurement mode.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.AcType">
            <summary>
            Reads the multimeter channel's AC measurement type (true RMS, or calculated RMS from average).
            This will be fixed for a given instrument.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterRange">
            <summary>        
            Reads/sets the manual measurement range for the multimeter channel. 
            If "InstType_MultiMeter.AutoRange" is provided or returned, this is the value denoting auto-ranging.
            NOTE: If an exact match is not possible the closest value ABOVE that supplied will be used.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.IntegrationTime_s">
            <summary>        
            Reads/sets the integration time (measurement averaging) in seconds for the multimeter channel. 
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterMode">
            <summary>
            Measurement mode for a multimeter instrument channel.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterMode.Voltage_DC_V">
            <summary>DC voltage measurement mode.</summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterMode.Voltage_AC_V_rms">
            <summary>AC voltage measurement mode.</summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterMode.Current_DC_A">
            <summary>DC current measurement mode.</summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterMode.Current_AC_A_rms">
            <summary>AC current measurement mode.</summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterMode.Resistance2wire_ohms">
            <summary>2-wire Resistance measurement mode.</summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterMode.Resistance4wire_ohms">
            <summary>4-wire Resistance measurement mode </summary>
        </member>
        <member name="T:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterAcType">
            <summary>
            Multimeters in AC mode can measure True RMS , 
            or the instrument can measure average signal magnitude (i.e. average of the rectified signal), 
            and then calculate the RMS assuming that the signal is sinusoidal. 
            The problem with the latter type
            of instrument is that for non-sinusoidal signals, the RMS will be incorrect.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterAcType.True_RMS">
            <summary>
            True RMS instrument
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_MultiMeter.MeterAcType.RMS_from_average">
            <summary>
            Calculated RMS from average signal magnitude
            </summary>
        </member>
    </members>
</doc>

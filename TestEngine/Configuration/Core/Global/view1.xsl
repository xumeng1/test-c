﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:my="http://schemas.microsoft.com/office/infopath/2003/myXSD/2005-09-08T13:33:36" xmlns:xd="http://schemas.microsoft.com/office/infopath/2003" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:xdExtension="http://schemas.microsoft.com/office/infopath/2003/xslt/extension" xmlns:xdXDocument="http://schemas.microsoft.com/office/infopath/2003/xslt/xDocument" xmlns:xdSolution="http://schemas.microsoft.com/office/infopath/2003/xslt/solution" xmlns:xdFormatting="http://schemas.microsoft.com/office/infopath/2003/xslt/formatting" xmlns:xdImage="http://schemas.microsoft.com/office/infopath/2003/xslt/xImage" xmlns:xdUtil="http://schemas.microsoft.com/office/infopath/2003/xslt/Util" xmlns:xdMath="http://schemas.microsoft.com/office/infopath/2003/xslt/Math" xmlns:xdDate="http://schemas.microsoft.com/office/infopath/2003/xslt/Date" xmlns:sig="http://www.w3.org/2000/09/xmldsig#" xmlns:xdSignatureProperties="http://schemas.microsoft.com/office/infopath/2003/SignatureProperties">
	<xsl:output method="html" indent="no"/>
	<xsl:template match="GlobalConfig">
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html"></meta>
				<style controlStyle="controlStyle">@media screen 			{ 			BODY{margin-left:21px;background-position:21px 0px;} 			} 		BODY{color:windowtext;background-color:window;layout-grid:none;} 		.xdListItem {display:inline-block;width:100%;vertical-align:text-top;} 		.xdListBox,.xdComboBox{margin:1px;} 		.xdInlinePicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) } 		.xdLinkedPicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) url(#default#urn::controls/Binder) } 		.xdSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdRepeatingSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdBehavior_Formatting {BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting);} 	 .xdBehavior_FormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting);} 	.xdExpressionBox{margin: 1px;padding:1px;word-wrap: break-word;text-overflow: ellipsis;overflow-x:hidden;}.xdBehavior_GhostedText,.xdBehavior_GhostedTextNoBUI{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#TextField) url(#default#GhostedText);}	.xdBehavior_GTFormatting{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_GTFormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_Boolean{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#BooleanHelper);}	.xdBehavior_Select{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#SelectHelper);}	.xdRepeatingTable{BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word;}.xdScrollableRegion{BEHAVIOR: url(#default#ScrollableRegion);} 		.xdMaster{BEHAVIOR: url(#default#MasterHelper);} 		.xdActiveX{margin:1px; BEHAVIOR: url(#default#ActiveX);} 		.xdFileAttachment{display:inline-block;margin:1px;BEHAVIOR:url(#default#urn::xdFileAttachment);} 		.xdPageBreak{display: none;}BODY{margin-right:21px;} 		.xdTextBoxRTL{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:right;} 		.xdRichTextBoxRTL{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:right;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTTextRTL{height:100%;width:100%;margin-left:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButtonRTL{margin-right:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);}.xdTextBox{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:left;} 		.xdRichTextBox{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:left;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTPicker{;display:inline;margin:1px;margin-bottom: 2px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;} 		.xdDTText{height:100%;width:100%;margin-right:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButton{margin-left:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);} 		.xdRepeatingTable TD {VERTICAL-ALIGN: top;}</style>
				<style tableEditor="TableStyleRulesID">TABLE.xdLayout TD {
	BORDER-RIGHT: medium none; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none
}
TABLE.msoUcTable TD {
	BORDER-RIGHT: 1pt solid; BORDER-TOP: 1pt solid; BORDER-LEFT: 1pt solid; BORDER-BOTTOM: 1pt solid
}
TABLE {
	BEHAVIOR: url (#default#urn::tables/NDTable)
}
</style>
				<style languageStyle="languageStyle">BODY {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
TABLE {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
SELECT {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-LEFT: 20px; FONT-WEIGHT: normal; FONT-SIZE: xx-small; BEHAVIOR: url(#default#xOptional); COLOR: #333333; FONT-STYLE: normal; FONT-FAMILY: Verdana; TEXT-DECORATION: none
}
.langFont {
	FONT-FAMILY: Verdana
}
.defaultInDocUI {
	FONT-SIZE: xx-small; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-RIGHT: 20px
}
</style>
			</head>
			<body>
				<div>
					<img style="WIDTH: 388px; HEIGHT: 93px" height="166" src="83837928.png" width="666"/>
				</div>
				<div><xsl:apply-templates select="." mode="_1"/>
				</div>
				<div> </div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="GlobalConfig" mode="_1">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 387px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:CtrlId="CTRL1" xd:xctname="Section" tabIndex="-1">
			<div>
				<strong>Debug Mode Enabled:</strong>                  <select class="xdComboBox xdBehavior_Select" title="" size="1" tabIndex="0" xd:CtrlId="CTRL7" xd:xctname="DropDown" xd:binding="DebugModeEnabled" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="DebugModeEnabled"/>
					</xsl:attribute>
					<option value="False">
						<xsl:if test="DebugModeEnabled=&quot;False&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>False</option>
					<option value="True">
						<xsl:if test="DebugModeEnabled=&quot;True&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>True</option>
				</select>
			</div>
			<div>
				<strong>Security Enabled: </strong>                       <select class="xdComboBox xdBehavior_Select" title="" size="1" tabIndex="0" xd:CtrlId="CTRL10" xd:xctname="DropDown" xd:binding="SecurityEnabled" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="SecurityEnabled"/>
					</xsl:attribute>
					<option value="False">
						<xsl:if test="SecurityEnabled=&quot;False&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>False</option>
					<option value="True">
						<xsl:if test="SecurityEnabled=&quot;True&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>True</option>
				</select>
			</div>
			<div>
				<strong>Simulation Mode Enabled: </strong>           <select class="xdComboBox xdBehavior_Select" title="" size="1" tabIndex="0" xd:CtrlId="CTRL11" xd:xctname="DropDown" xd:binding="SimulationModeEnabled" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="SimulationModeEnabled"/>
					</xsl:attribute>
					<option value="False">
						<xsl:if test="SimulationModeEnabled=&quot;False&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>False</option>
					<option value="True">
						<xsl:if test="SimulationModeEnabled=&quot;True&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>True</option>
				</select>
			</div>
			<div>
				<strong>Language:  </strong>                                <select class="xdComboBox xdBehavior_Select" title="" size="1" tabIndex="0" xd:CtrlId="CTRL12" xd:xctname="DropDown" xd:binding="Language" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="Language"/>
					</xsl:attribute>
					<option value="ENGLISH">
						<xsl:if test="Language=&quot;ENGLISH&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>ENGLISH</option>
					<option value="CHINESE">
						<xsl:if test="Language=&quot;CHINESE&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>CHINESE</option>
				</select>
			</div>
			<div>
				<strong>Max. Number Of Device Statuses: </strong><span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:CtrlId="CTRL6" xd:xctname="PlainText" xd:binding="DeviceStatusNumLines" xd:boundProp="xd:num" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;grouping:0;negativeOrder:1;&quot;" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="DeviceStatusNumLines"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(DeviceStatusNumLines,&quot;number&quot;,&quot;numDigits:0;grouping:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="DeviceStatusNumLines"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
			<div style="FONT-WEIGHT: normal">
				<strong>Use UTC Date Time:                          </strong><select class="xdComboBox xdBehavior_Select" title="" size="1" tabIndex="0" xd:CtrlId="CTRL14" xd:xctname="DropDown" xd:binding="UseUtcDateTime" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="UseUtcDateTime"/>
					</xsl:attribute>
					<option value="False">
						<xsl:if test="UseUtcDateTime=&quot;False&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>False</option>
					<option value="True">
						<xsl:if test="UseUtcDateTime=&quot;True&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>True</option>
				</select>
			</div>
			<div> </div>
		</div>
	</xsl:template>
</xsl:stylesheet>

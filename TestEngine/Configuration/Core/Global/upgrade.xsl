<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:my="http://schemas.microsoft.com/office/infopath/2003/myXSD/2005-09-08T13:33:36" xmlns:xd="http://schemas.microsoft.com/office/infopath/2003" version="1.0">
	<xsl:output encoding="UTF-8" method="xml"/>
	<xsl:template match="/">
		<xsl:copy-of select="processing-instruction() | comment()"/>
		<xsl:choose>
			<xsl:when test="GlobalConfig">
				<xsl:apply-templates select="GlobalConfig" mode="_0"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="var">
					<xsl:element name="GlobalConfig"/>
				</xsl:variable>
				<xsl:apply-templates select="msxsl:node-set($var)/*" mode="_0"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="GlobalConfig" mode="_0">
		<xsl:copy>
			<xsl:element name="DebugModeEnabled">
				<xsl:choose>
					<xsl:when test="DebugModeEnabled">
						<xsl:copy-of select="DebugModeEnabled/text()[1]"/>
					</xsl:when>
					<xsl:otherwise>False</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="SecurityEnabled">
				<xsl:choose>
					<xsl:when test="SecurityEnabled">
						<xsl:copy-of select="SecurityEnabled/text()[1]"/>
					</xsl:when>
					<xsl:otherwise>False</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="SimulationModeEnabled">
				<xsl:choose>
					<xsl:when test="SimulationModeEnabled">
						<xsl:copy-of select="SimulationModeEnabled/text()[1]"/>
					</xsl:when>
					<xsl:otherwise>False</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="Language">
				<xsl:choose>
					<xsl:when test="Language">
						<xsl:copy-of select="Language/text()[1]"/>
					</xsl:when>
					<xsl:otherwise>ENGLISH</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="DeviceStatusNumLines">
				<xsl:choose>
					<xsl:when test="DeviceStatusNumLines/text()[1]">
						<xsl:copy-of select="DeviceStatusNumLines/text()[1]"/>
					</xsl:when>
					<xsl:otherwise>100</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="UseUtcDateTime">
				<xsl:choose>
					<xsl:when test="UseUtcDateTime">
						<xsl:copy-of select="UseUtcDateTime/text()[1]"/>
					</xsl:when>
					<xsl:otherwise>False</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
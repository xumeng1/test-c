<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:ns1="http://schemas.microsoft.com/office/infopath/2003/myXSD/2005-09-21T09:39:30" xmlns:my="http://schemas.microsoft.com/office/infopath/2003/myXSD/2005-09-30T10:12:21" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xd="http://schemas.microsoft.com/office/infopath/2003" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:xdExtension="http://schemas.microsoft.com/office/infopath/2003/xslt/extension" xmlns:xdXDocument="http://schemas.microsoft.com/office/infopath/2003/xslt/xDocument" xmlns:xdSolution="http://schemas.microsoft.com/office/infopath/2003/xslt/solution" xmlns:xdFormatting="http://schemas.microsoft.com/office/infopath/2003/xslt/formatting" xmlns:xdImage="http://schemas.microsoft.com/office/infopath/2003/xslt/xImage" xmlns:xdUtil="http://schemas.microsoft.com/office/infopath/2003/xslt/Util" xmlns:xdMath="http://schemas.microsoft.com/office/infopath/2003/xslt/Math" xmlns:xdDate="http://schemas.microsoft.com/office/infopath/2003/xslt/Date" xmlns:sig="http://www.w3.org/2000/09/xmldsig#" xmlns:xdSignatureProperties="http://schemas.microsoft.com/office/infopath/2003/SignatureProperties">
	<xsl:output method="html" indent="no"/>
	<xsl:template match="CorePluginsConfig">
		<html>
			<head>
				<style tableEditor="TableStyleRulesID">TABLE.xdLayout TD {
	BORDER-RIGHT: medium none; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none
}
TABLE {
	BEHAVIOR: url (#default#urn::tables/NDTable)
}
TABLE.msoUcTable TD {
	BORDER-RIGHT: 1pt solid; BORDER-TOP: 1pt solid; BORDER-LEFT: 1pt solid; BORDER-BOTTOM: 1pt solid
}
</style>
				<meta http-equiv="Content-Type" content="text/html"></meta>
				<style controlStyle="controlStyle">@media screen 			{ 			BODY{margin-left:21px;background-position:21px 0px;} 			} 		BODY{color:windowtext;background-color:window;layout-grid:none;} 		.xdListItem {display:inline-block;width:100%;vertical-align:text-top;} 		.xdListBox,.xdComboBox{margin:1px;} 		.xdInlinePicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) } 		.xdLinkedPicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) url(#default#urn::controls/Binder) } 		.xdSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdRepeatingSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdBehavior_Formatting {BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting);} 	 .xdBehavior_FormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting);} 	.xdExpressionBox{margin: 1px;padding:1px;word-wrap: break-word;text-overflow: ellipsis;overflow-x:hidden;}.xdBehavior_GhostedText,.xdBehavior_GhostedTextNoBUI{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#TextField) url(#default#GhostedText);}	.xdBehavior_GTFormatting{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_GTFormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_Boolean{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#BooleanHelper);}	.xdBehavior_Select{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#SelectHelper);}	.xdRepeatingTable{BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word;}.xdScrollableRegion{BEHAVIOR: url(#default#ScrollableRegion);} 		.xdMaster{BEHAVIOR: url(#default#MasterHelper);} 		.xdActiveX{margin:1px; BEHAVIOR: url(#default#ActiveX);} 		.xdFileAttachment{display:inline-block;margin:1px;BEHAVIOR:url(#default#urn::xdFileAttachment);} 		.xdPageBreak{display: none;}BODY{margin-right:21px;} 		.xdTextBoxRTL{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:right;} 		.xdRichTextBoxRTL{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:right;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTTextRTL{height:100%;width:100%;margin-left:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButtonRTL{margin-right:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);}.xdTextBox{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:left;} 		.xdRichTextBox{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:left;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTPicker{;display:inline;margin:1px;margin-bottom: 2px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;} 		.xdDTText{height:100%;width:100%;margin-right:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButton{margin-left:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);} 		.xdRepeatingTable TD {VERTICAL-ALIGN: top;}</style>
				<style languageStyle="languageStyle">BODY {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
TABLE {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
SELECT {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-LEFT: 20px; FONT-WEIGHT: normal; FONT-SIZE: xx-small; BEHAVIOR: url(#default#xOptional); COLOR: #333333; FONT-STYLE: normal; FONT-FAMILY: Verdana; TEXT-DECORATION: none
}
.langFont {
	FONT-FAMILY: Verdana
}
.defaultInDocUI {
	FONT-SIZE: xx-small; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-RIGHT: 20px
}
</style>
			</head>
			<body>
				<div>
					<strong>
						<font style="BACKGROUND-COLOR: #99ccff" size="5"></font>
					</strong> </div>
				<div>
					<img style="WIDTH: 600px; HEIGHT: 83px" src="BAD91555.png"/>
				</div>
				<div><xsl:apply-templates select="SecurityPlugin" mode="_1"/>
				</div>
				<div><xsl:apply-templates select="ExternalApplications" mode="_3"/>
				</div>
				<div><xsl:apply-templates select="TestJigPlugin" mode="_6"/>
				</div>
				<div><xsl:apply-templates select="TestControlPlugins" mode="_8"/>
				</div>
				<div><xsl:apply-templates select="MesPlugin" mode="_10"/>
				</div>
				<div><xsl:apply-templates select="ExternalDataPlugins/ReadLimitsPlugins" mode="_13"/>
				</div>
				<div><xsl:apply-templates select="ExternalDataPlugins/ReadDataPlugins" mode="_15"/>
				</div>
				<div><xsl:apply-templates select="ExternalDataPlugins/WriteDataSinkPlugins" mode="_17"/>
				</div>
				<div> </div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="SecurityPlugin" mode="_1">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL1" tabIndex="-1">
			<div>
				<strong>
					<font size="4">Security Plug-in</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="." mode="_2"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="SecurityPlugin" mode="_2">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="Section" xd:CtrlId="CTRL2" tabIndex="-1">
			<div>
				<strong>Security Plugin Name:</strong>   <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL3" tabIndex="0" xd:binding="SecurityPluginName" style="WIDTH: 424px">
					<xsl:value-of select="SecurityPluginName"/>
				</span>
			</div>
			<div>
				<strong>Security Plugin Version:</strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL4" tabIndex="0" xd:binding="SecurityPluginVersion" style="WIDTH: 421px">
					<xsl:value-of select="SecurityPluginVersion"/>
				</span>
			</div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="ExternalApplications" mode="_3">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL5" tabIndex="-1">
			<div>
				<font size="4">
					<strong>External Applications</strong>
				</font>
			</div>
			<div><xsl:apply-templates select="ExternalApplication" mode="_5"/>
				<div class="optionalPlaceholder" xd:xmlToEdit="ExternalApplication_21" tabIndex="0" xd:action="xCollection::insert" align="left" style="WIDTH: 100%">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="ExternalApplication" mode="_5">
		<div class="xdRepeatingSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL12" tabIndex="-1">
			<div>
				<strong>External Application Label:           </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL13" tabIndex="0" xd:binding="ExternalApplicationLabel" style="WIDTH: 365px">
					<xsl:value-of select="ExternalApplicationLabel"/>
				</span>
			</div>
			<div>
				<strong>External Application Name:</strong>        <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL14" tabIndex="0" xd:binding="ExternalApplicationName" style="WIDTH: 367px">
					<xsl:value-of select="ExternalApplicationName"/>
				</span>
			</div>
			<div>
				<strong>External Application Path:             </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL15" tabIndex="0" xd:binding="ExternalApplicationPath" style="WIDTH: 362px">
					<xsl:value-of select="ExternalApplicationPath"/>
				</span>
			</div>
			<div>
				<strong>External Application Arguments: </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL16" tabIndex="0" xd:binding="ExternalApplicationArguments" style="WIDTH: 364px">
					<xsl:value-of select="ExternalApplicationArguments"/>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="TestJigPlugin" mode="_6">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL17" tabIndex="-1">
			<div>
				<strong>
					<font size="4">Test Jig Plug-in</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="." mode="_7"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="TestJigPlugin" mode="_7">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="Section" xd:CtrlId="CTRL18" tabIndex="-1">
			<div>
				<strong>Test Jig Plugin Type Name:</strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL19" tabIndex="0" xd:binding="TestJigPluginTypeName" style="WIDTH: 407px">
					<xsl:value-of select="TestJigPluginTypeName"/>
				</span>
			</div>
			<div>
				<strong>Test Jig Plugin Version:        </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL20" tabIndex="0" xd:binding="TestJigPluginVersion" style="WIDTH: 407px">
					<xsl:value-of select="TestJigPluginVersion"/>
				</span>
			</div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="TestControlPlugins" mode="_8">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL21" tabIndex="-1">
			<div>
				<strong>
					<font size="4">Test Control Plug-ins</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="TestControlPlugin" mode="_9"/>
				<div class="optionalPlaceholder" xd:xmlToEdit="TestControlPlugin_22" tabIndex="0" xd:action="xCollection::insert" align="left" style="WIDTH: 100%">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="TestControlPlugin" mode="_9">
		<div class="xdRepeatingSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL22" tabIndex="-1">
			<div>
				<strong>Test Control Plugin Name:</strong>     <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL23" tabIndex="0" xd:binding="TestControlPluginName" style="WIDTH: 391px">
					<xsl:value-of select="TestControlPluginName"/>
				</span>
			</div>
			<div>
				<strong>Test Control Plugin Version:</strong>   <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL24" tabIndex="0" xd:binding="TestControlPluginVersion" style="WIDTH: 387px">
					<xsl:value-of select="TestControlPluginVersion"/>
				</span>
			</div>
			<div>
				<strong>Test Control Plugin Privilege:</strong><select class="xdComboBox xdBehavior_Select" title="" size="1" xd:xctname="DropDown" xd:CtrlId="CTRL26" tabIndex="0" xd:binding="TestControlPluginPrivilege" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="TestControlPluginPrivilege"/>
					</xsl:attribute>
					<option value="Operator">
						<xsl:if test="TestControlPluginPrivilege=&quot;Operator&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>Operator</option>
					<option value="Technician">
						<xsl:if test="TestControlPluginPrivilege=&quot;Technician&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>Technician</option>
				</select>
			</div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="MesPlugin" mode="_10">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL27" tabIndex="-1">
			<div>
				<strong>
					<font size="4">MES Plug-in</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="." mode="_11"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="MesPlugin" mode="_11">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="Section" xd:CtrlId="CTRL28" tabIndex="-1">
			<div>
				<strong>Mes Plugin Type Name:</strong>          <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL29" tabIndex="0" xd:binding="MesPluginTypeName" style="WIDTH: 388px">
					<xsl:value-of select="MesPluginTypeName"/>
				</span>
			</div>
			<div>
				<strong>Mes Plugin Version:                   </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL30" tabIndex="0" xd:binding="MesPluginVersion" style="WIDTH: 387px">
					<xsl:value-of select="MesPluginVersion"/>
				</span>
			</div>
			<div>
				<strong>Mes Plugin Connection String: </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL31" tabIndex="0" xd:binding="MesPluginConnectionString" style="WIDTH: 385px">
					<xsl:value-of select="MesPluginConnectionString"/>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="ReadLimitsPlugins" mode="_13">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL33" tabIndex="-1">
			<div>
				<strong>
					<font size="4">Read Limits Plug-ins</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="ReadLimitPlugin" mode="_14"/>
				<div class="optionalPlaceholder" xd:xmlToEdit="ReadLimitPlugin_12" tabIndex="0" xd:action="xCollection::insert" align="left" style="WIDTH: 100%">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="ReadLimitPlugin" mode="_14">
		<div class="xdRepeatingSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL34" tabIndex="-1">
			<div>
				<strong>Read Limit Plugin Label:                      </strong> <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL35" tabIndex="0" xd:binding="ReadLimitPluginLabel" style="WIDTH: 339px">
					<xsl:value-of select="ReadLimitPluginLabel"/>
				</span>
			</div>
			<div>
				<strong>Read Limit Plugin Connection String:</strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL36" tabIndex="0" xd:binding="ReadLimitPluginConnectionString" style="WIDTH: 337px">
					<xsl:value-of select="ReadLimitPluginConnectionString"/>
				</span>
			</div>
			<div>
				<strong>Read Limit Plugin Type Name:</strong>           <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL37" tabIndex="0" xd:binding="ReadLimitPluginTypeName" style="WIDTH: 334px">
					<xsl:value-of select="ReadLimitPluginTypeName"/>
				</span>
			</div>
			<div>
				<strong>Read Limit Plugin Version:</strong>                <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL38" tabIndex="0" xd:binding="ReadLimitPluginVersion" style="WIDTH: 334px">
					<xsl:value-of select="ReadLimitPluginVersion"/>
				</span>
			</div>
			<div>
				<strong>Read Limit Plugin Default Flag:</strong>          <select class="xdComboBox xdBehavior_Select" title="" size="1" xd:xctname="DropDown" xd:CtrlId="CTRL40" tabIndex="0" xd:binding="ReadLimitPluginDefaultFlag" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="ReadLimitPluginDefaultFlag"/>
					</xsl:attribute>
					<option value="false">
						<xsl:if test="ReadLimitPluginDefaultFlag=&quot;false&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>FALSE</option>
					<option value="true">
						<xsl:if test="ReadLimitPluginDefaultFlag=&quot;true&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>TRUE</option>
				</select>
			</div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="ReadDataPlugins" mode="_15">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL41" tabIndex="-1">
			<div>
				<strong>
					<font size="4">Read Data Plug-ins</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="ReadDataPlugin" mode="_16"/>
				<div class="optionalPlaceholder" xd:xmlToEdit="ReadDataPlugin_23" tabIndex="0" xd:action="xCollection::insert" align="left" style="WIDTH: 100%">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="ReadDataPlugin" mode="_16">
		<div class="xdRepeatingSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL42" tabIndex="-1">
			<div>
				<strong>Read Data Plugin Label:                       </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL43" tabIndex="0" xd:binding="ReadDataPluginLabel" style="WIDTH: 341px">
					<xsl:value-of select="ReadDataPluginLabel"/>
				</span>
			</div>
			<div>
				<strong>Read Data Plugin Connection String: </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL44" tabIndex="0" xd:binding="ReadDataPluginConnectionString" style="WIDTH: 339px">
					<xsl:value-of select="ReadDataPluginConnectionString"/>
				</span>
			</div>
			<div>
				<strong>Read Data Plugin Type Name:</strong>          <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL45" tabIndex="0" xd:binding="ReadDataPluginTypeName" style="WIDTH: 342px">
					<xsl:value-of select="ReadDataPluginTypeName"/>
				</span>
			</div>
			<div>
				<strong>Read Data Plugin Version:                   </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL46" tabIndex="0" xd:binding="ReadDataPluginVersion" style="WIDTH: 342px">
					<xsl:value-of select="ReadDataPluginVersion"/>
				</span>
			</div>
			<div>
				<strong>Read Data Plugin Default Flag:           </strong><select class="xdComboBox xdBehavior_Select" title="" size="1" xd:xctname="DropDown" xd:CtrlId="CTRL48" tabIndex="0" xd:binding="ReadDataPluginDefaultFlag" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="ReadDataPluginDefaultFlag"/>
					</xsl:attribute>
					<option value="false">
						<xsl:if test="ReadDataPluginDefaultFlag=&quot;false&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>FALSE</option>
					<option value="true">
						<xsl:if test="ReadDataPluginDefaultFlag=&quot;true&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>TRUE</option>
				</select>
			</div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="WriteDataSinkPlugins" mode="_17">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 629px; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #c0c0c0" align="left" xd:xctname="Section" xd:CtrlId="CTRL49" tabIndex="-1">
			<div>
				<strong>
					<font size="4">Write Data Plug-ins</font>
				</strong>
			</div>
			<div>
				<strong>
					<font size="4"></font>
				</strong> </div>
			<div><xsl:apply-templates select="DataSinks" mode="_18"/>
			</div>
			<div><xsl:apply-templates select="." mode="_22"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="DataSinks" mode="_18">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #99ccff" align="left" xd:xctname="Section" xd:CtrlId="CTRL50" tabIndex="-1">
			<div>
				<strong>
					<font size="3">Data Sinks</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="DataSink" mode="_19"/>
				<div class="optionalPlaceholder" xd:xmlToEdit="DataSink_24" tabIndex="0" xd:action="xCollection::insert" align="left" style="WIDTH: 100%">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="DataSink" mode="_19">
		<div class="xdRepeatingSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL51" tabIndex="-1">
			<div>
				<strong>Data Sink Label:                                    </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL52" tabIndex="0" xd:binding="WriteDataSinkLabel" style="WIDTH: 331px">
					<xsl:value-of select="WriteDataSinkLabel"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Plugin Connection String:</strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL53" tabIndex="0" xd:binding="WriteDataSinkPluginConnectionString" style="WIDTH: 330px">
					<xsl:value-of select="WriteDataSinkPluginConnectionString"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Plugin Type Name:</strong>           <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL54" tabIndex="0" xd:binding="WriteDataSinkPluginTypeName" style="WIDTH: 328px">
					<xsl:value-of select="WriteDataSinkPluginTypeName"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Plugin Version:                    </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL55" tabIndex="0" xd:binding="WriteDataSinkPluginVersion" style="WIDTH: 330px">
					<xsl:value-of select="WriteDataSinkPluginVersion"/>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="WriteDataSinkPlugins" mode="_22">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #99ccff" align="left" xd:xctname="Section" xd:CtrlId="CTRL75" tabIndex="-1">
			<div>
				<strong>
					<font size="3">Data Sink Groups</font>
				</strong>
			</div>
			<div><xsl:apply-templates select="WriteDataSinkGroup" mode="_25"/>
				<div class="optionalPlaceholder" xd:xmlToEdit="WriteDataSinkGroup_25" tabIndex="0" xd:action="xCollection::insert" align="left" style="WIDTH: 100%">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="WriteDataSinkGroup" mode="_25">
		<div class="xdRepeatingSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid; BACKGROUND-COLOR: #ccffcc" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL76" tabIndex="-1">
			<div>
				<strong>Data Sink Group Label:            </strong> <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL77" tabIndex="0" xd:binding="WriteDataSinkGroupLabel" style="WIDTH: 378px">
					<xsl:value-of select="WriteDataSinkGroupLabel"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Label 1: </strong>                 <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL78" tabIndex="0" xd:binding="WriteDataSinkLabel_1" style="WIDTH: 376px">
					<xsl:value-of select="WriteDataSinkLabel_1"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Label 2: </strong>                 <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL79" tabIndex="0" xd:binding="WriteDataSinkLabel_2" style="WIDTH: 374px">
					<xsl:value-of select="WriteDataSinkLabel_2"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Label 3:                      </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL80" tabIndex="0" xd:binding="WriteDataSinkLabel_3" style="WIDTH: 375px">
					<xsl:value-of select="WriteDataSinkLabel_3"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Label 4:</strong>                 <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL81" tabIndex="0" xd:binding="WriteDataSinkLabel_4" style="WIDTH: 378px">
					<xsl:value-of select="WriteDataSinkLabel_4"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Label 5:                      </strong><span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL82" tabIndex="0" xd:binding="WriteDataSinkLabel_5" style="WIDTH: 375px">
					<xsl:value-of select="WriteDataSinkLabel_5"/>
				</span>
			</div>
			<div>
				<strong>Data Sink Group Default Flag:</strong><select class="xdComboBox xdBehavior_Select" title="" size="1" xd:xctname="DropDown" xd:CtrlId="CTRL84" tabIndex="0" xd:binding="WriteDataSinkGroupDefaultFlag" xd:boundProp="value" style="WIDTH: 130px">
					<xsl:attribute name="value">
						<xsl:value-of select="WriteDataSinkGroupDefaultFlag"/>
					</xsl:attribute>
					<option value="false">
						<xsl:if test="WriteDataSinkGroupDefaultFlag=&quot;false&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>FALSE</option>
					<option value="true">
						<xsl:if test="WriteDataSinkGroupDefaultFlag=&quot;true&quot;">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>TRUE</option>
				</select>
			</div>
			<div> </div>
		</div>
	</xsl:template>
</xsl:stylesheet>

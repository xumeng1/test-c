<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mstns="http://tempuri.org/FactoryWorksConfigSchema.xsd" xmlns:xd="http://schemas.microsoft.com/office/infopath/2003" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:xdExtension="http://schemas.microsoft.com/office/infopath/2003/xslt/extension" xmlns:xdXDocument="http://schemas.microsoft.com/office/infopath/2003/xslt/xDocument" xmlns:xdSolution="http://schemas.microsoft.com/office/infopath/2003/xslt/solution" xmlns:xdFormatting="http://schemas.microsoft.com/office/infopath/2003/xslt/formatting" xmlns:xdImage="http://schemas.microsoft.com/office/infopath/2003/xslt/xImage" xmlns:xdUtil="http://schemas.microsoft.com/office/infopath/2003/xslt/Util" xmlns:xdMath="http://schemas.microsoft.com/office/infopath/2003/xslt/Math" xmlns:xdDate="http://schemas.microsoft.com/office/infopath/2003/xslt/Date" xmlns:sig="http://www.w3.org/2000/09/xmldsig#" xmlns:xdSignatureProperties="http://schemas.microsoft.com/office/infopath/2003/SignatureProperties">
	<xsl:output method="html" indent="no"/>
	<xsl:template match="mstns:FactoryWorksConfig">
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html"></meta>
				<style controlStyle="controlStyle">@media screen 			{ 			BODY{margin-left:21px;background-position:21px 0px;} 			} 		BODY{color:windowtext;background-color:window;layout-grid:none;} 		.xdListItem {display:inline-block;width:100%;vertical-align:text-top;} 		.xdListBox,.xdComboBox{margin:1px;} 		.xdInlinePicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) } 		.xdLinkedPicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) url(#default#urn::controls/Binder) } 		.xdSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdRepeatingSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdBehavior_Formatting {BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting);} 	 .xdBehavior_FormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting);} 	.xdExpressionBox{margin: 1px;padding:1px;word-wrap: break-word;text-overflow: ellipsis;overflow-x:hidden;}.xdBehavior_GhostedText,.xdBehavior_GhostedTextNoBUI{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#TextField) url(#default#GhostedText);}	.xdBehavior_GTFormatting{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_GTFormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_Boolean{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#BooleanHelper);}	.xdBehavior_Select{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#SelectHelper);}	.xdRepeatingTable{BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word;}.xdScrollableRegion{BEHAVIOR: url(#default#ScrollableRegion);} 		.xdMaster{BEHAVIOR: url(#default#MasterHelper);} 		.xdActiveX{margin:1px; BEHAVIOR: url(#default#ActiveX);} 		.xdFileAttachment{display:inline-block;margin:1px;BEHAVIOR:url(#default#urn::xdFileAttachment);} 		.xdPageBreak{display: none;}BODY{margin-right:21px;} 		.xdTextBoxRTL{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:right;} 		.xdRichTextBoxRTL{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:right;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTTextRTL{height:100%;width:100%;margin-left:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButtonRTL{margin-right:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);}.xdTextBox{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:left;} 		.xdRichTextBox{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:left;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTPicker{;display:inline;margin:1px;margin-bottom: 2px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;} 		.xdDTText{height:100%;width:100%;margin-right:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButton{margin-left:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);} 		.xdRepeatingTable TD {VERTICAL-ALIGN: top;}</style>
				<style tableEditor="TableStyleRulesID">TABLE.xdLayout TD {
	BORDER-RIGHT: medium none; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none
}
TABLE.msoUcTable TD {
	BORDER-RIGHT: 1pt solid; BORDER-TOP: 1pt solid; BORDER-LEFT: 1pt solid; BORDER-BOTTOM: 1pt solid
}
TABLE {
	BEHAVIOR: url (#default#urn::tables/NDTable)
}
</style>
				<style languageStyle="languageStyle">BODY {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
TABLE {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
SELECT {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-LEFT: 20px; FONT-WEIGHT: normal; FONT-SIZE: xx-small; BEHAVIOR: url(#default#xOptional); COLOR: #333333; FONT-STYLE: normal; FONT-FAMILY: Verdana; TEXT-DECORATION: none
}
.langFont {
	FONT-FAMILY: Verdana
}
.defaultInDocUI {
	FONT-SIZE: xx-small; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-RIGHT: 20px
}
</style>
			</head>
			<body>
				<div> </div>
				<div>
					<table class="xdFormLayout xdLayout" style="TABLE-LAYOUT: fixed; WIDTH: 422px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1">
						<colgroup>
							<col style="WIDTH: 422px"></col>
						</colgroup>
						<tbody vAlign="top">
							<tr class="primaryVeryDark">
								<td style="BORDER-TOP-STYLE: none; BORDER-BOTTOM: 5pt solid; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none">
									<div>
										<font size="4">Bookham FactoryWorks Configuration Editor</font>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div><xsl:apply-templates select="." mode="_1"/>
				</div>
				<div> </div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="mstns:FactoryWorksConfig" mode="_1">
		<div class="xdSection xdRepeating" title="" style="MARGIN-BOTTOM: 6px; WIDTH: 418px; HEIGHT: 737px; BACKGROUND-COLOR: transparent" align="left" xd:xctname="Section" xd:CtrlId="CTRL1" tabIndex="-1">
			<div>Node: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL2" xd:binding="mstns:Node" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 69px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Node"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Node,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Node"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
			<div> </div>
			<div>Request Dir:    <span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL3" xd:binding="mstns:RequestDir" style="WIDTH: 305px">
					<xsl:value-of select="mstns:RequestDir"/>
				</span>
			</div>
			<div>Response Dir:  <span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL4" xd:binding="mstns:ResponseDir" style="WIDTH: 307px">
					<xsl:value-of select="mstns:ResponseDir"/>
				</span>
			</div>
			<div style="LAYOUT-GRID:  strict 18pt 12pt"> </div>
			<div> </div>
			<div><xsl:apply-templates select="mstns:LoadBatch" mode="_10"/>
			</div>
			<div><xsl:apply-templates select="mstns:TrackIn" mode="_11"/>
			</div>
			<div><xsl:apply-templates select="mstns:SetAttribute" mode="_13"/>
			</div>
			<div><xsl:apply-templates select="mstns:MarkAsDefective" mode="_15"/>
			</div>
			<div><xsl:apply-templates select="mstns:TrackOutComponentPassReworkOnHold" mode="_16"/>
			</div>
			<div><xsl:apply-templates select="mstns:TrackOutComponentFail" mode="_17"/>
			</div>
			<div><xsl:apply-templates select="mstns:TrackOutBatchPassOnhold" mode="_18"/>
			</div>
			<div> </div>
			<div> </div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:LoadBatch" mode="_10">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL64" tabIndex="-1">
			<div>
				<strong>Load Batch</strong>
			</div>
			<div> </div>
			<div>Timeout S: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL65" xd:binding="mstns:Timeout_s" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Timeout_s"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Timeout_s,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Timeout_s"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TrackIn" mode="_11">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL8" tabIndex="-1">
			<div>
				<strong>Track In</strong>
			</div>
			<div> </div>
			<div><input class="xdBehavior_Boolean" title="" type="checkbox" tabIndex="0" xd:xctname="CheckBox" xd:CtrlId="CTRL9" xd:binding="mstns:WaitForResponse" xd:boundProp="xd:value" xd:offValue="false" xd:onValue="true">
					<xsl:attribute name="xd:value">
						<xsl:value-of select="mstns:WaitForResponse"/>
					</xsl:attribute>
					<xsl:if test="mstns:WaitForResponse=&quot;true&quot;">
						<xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
					</xsl:if>
				</input> Wait For Response    Timeout S: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL10" xd:binding="mstns:Timeout_s" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Timeout_s"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Timeout_s,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Timeout_s"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:SetAttribute" mode="_13">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL39" tabIndex="-1">
			<div>
				<strong>Set Attribute</strong>
			</div>
			<div> </div>
			<div><input class="xdBehavior_Boolean" title="" type="checkbox" tabIndex="0" xd:xctname="CheckBox" xd:CtrlId="CTRL41" xd:binding="mstns:WaitForResponse" xd:boundProp="xd:value" xd:offValue="false" xd:onValue="true">
					<xsl:attribute name="xd:value">
						<xsl:value-of select="mstns:WaitForResponse"/>
					</xsl:attribute>
					<xsl:if test="mstns:WaitForResponse=&quot;true&quot;">
						<xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
					</xsl:if>
				</input> Wait For Response    Timeout S: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL40" xd:binding="mstns:Timeout_s" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Timeout_s"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Timeout_s,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Timeout_s"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:MarkAsDefective" mode="_15">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL45" tabIndex="-1">
			<div>
				<strong>Mark As Defective</strong>
			</div>
			<div> </div>
			<div><input class="xdBehavior_Boolean" title="" type="checkbox" tabIndex="0" xd:xctname="CheckBox" xd:CtrlId="CTRL47" xd:binding="mstns:WaitForResponse" xd:boundProp="xd:value" xd:offValue="false" xd:onValue="true">
					<xsl:attribute name="xd:value">
						<xsl:value-of select="mstns:WaitForResponse"/>
					</xsl:attribute>
					<xsl:if test="mstns:WaitForResponse=&quot;true&quot;">
						<xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
					</xsl:if>
				</input> Wait For Response    Timeout S: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL46" xd:binding="mstns:Timeout_s" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Timeout_s"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Timeout_s,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Timeout_s"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TrackOutComponentPassReworkOnHold" mode="_16">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL66" tabIndex="-1">
			<div>
				<strong>Track-out Component for Pass, Fail or Rework</strong>
			</div>
			<div> </div>
			<div><input class="xdBehavior_Boolean" title="" type="checkbox" tabIndex="0" xd:xctname="CheckBox" xd:CtrlId="CTRL68" xd:binding="mstns:WaitForResponse" xd:boundProp="xd:value" xd:offValue="false" xd:onValue="true">
					<xsl:attribute name="xd:value">
						<xsl:value-of select="mstns:WaitForResponse"/>
					</xsl:attribute>
					<xsl:if test="mstns:WaitForResponse=&quot;true&quot;">
						<xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
					</xsl:if>
				</input> Wait For Response    Timeout S: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL67" xd:binding="mstns:Timeout_s" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Timeout_s"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Timeout_s,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Timeout_s"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TrackOutComponentFail" mode="_17">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL69" tabIndex="-1">
			<div>
				<strong>Track-out Component for Failure</strong>
			</div>
			<div> </div>
			<div><input class="xdBehavior_Boolean" title="" type="checkbox" tabIndex="0" xd:xctname="CheckBox" xd:CtrlId="CTRL70" xd:binding="mstns:WaitForResponse" xd:boundProp="xd:value" xd:offValue="false" xd:onValue="true">
					<xsl:attribute name="xd:value">
						<xsl:value-of select="mstns:WaitForResponse"/>
					</xsl:attribute>
					<xsl:if test="mstns:WaitForResponse=&quot;true&quot;">
						<xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
					</xsl:if>
				</input> Wait For Response    Timeout S: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL71" xd:binding="mstns:Timeout_s" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Timeout_s"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Timeout_s,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Timeout_s"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TrackOutBatchPassOnhold" mode="_18">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL72" tabIndex="-1">
			<div>
				<strong>Track-out Batch</strong>
			</div>
			<div> </div>
			<div><input class="xdBehavior_Boolean" title="" type="checkbox" tabIndex="0" xd:xctname="CheckBox" xd:CtrlId="CTRL73" xd:binding="mstns:WaitForResponse" xd:boundProp="xd:value" xd:offValue="false" xd:onValue="true">
					<xsl:attribute name="xd:value">
						<xsl:value-of select="mstns:WaitForResponse"/>
					</xsl:attribute>
					<xsl:if test="mstns:WaitForResponse=&quot;true&quot;">
						<xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
					</xsl:if>
				</input> Wait For Response    Timeout S: <span class="xdTextBox xdBehavior_Formatting" hideFocus="1" title="" contentEditable="true" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL74" xd:binding="mstns:Timeout_s" xd:datafmt="&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;" xd:boundProp="xd:num" style="WIDTH: 130px">
					<xsl:attribute name="xd:num">
						<xsl:value-of select="mstns:Timeout_s"/>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="function-available('xdFormatting:formatString')">
							<xsl:value-of select="xdFormatting:formatString(mstns:Timeout_s,&quot;number&quot;,&quot;numDigits:0;negativeOrder:1;&quot;)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="mstns:Timeout_s"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>

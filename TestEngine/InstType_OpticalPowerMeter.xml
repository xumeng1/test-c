<?xml version="1.0"?>
<doc>
    <assembly>
        <name>InstType_OpticalPowerMeter</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter">
            <summary>
            Optical power meter channel controller interface.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.ReadPower">
            <summary>
            Initiate and return a power measurement. Returned units depend 
            on the current mode of the instrument (dBm, mW, dB). 
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.ZeroDarkCurrent_Start">
            <summary>
            Starts a calibration of power meter dark-current 
            value on the power meter channel. For this calibration to succeed the meter must 
            have zero light input. This function returns immediately once the command
            has been sent to the instrument (Async command).
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.ZeroDarkCurrent_End">
            <summary>
            Ends a calibration of optical dark-current. Waits for a previously started
            dark current cal to complete.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.ReadPowerMaxMin">
            <summary>
            Return  the max and min power readings observed since the last activation 
            of Max/Min Hold Mode.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.Mode">
            <summary>
            Get / Set the mode of optical power meter. 
            If a set of a mode which is not supported by this instrument is attempted, 
            an exception should be thrown.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.Wavelength_nm">
            <summary>
            Reads/sets the expected input signal wavelength in nanometres
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.CalOffset_dB">
            <summary>
            Get / Set calibration offset in decibels.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.ReferencePower">
            <summary>
            Get/Set reference power for relative measurements. Uses the 
            current MeterMode for units.
            MeterMode.Absolute_dBm -> dBm
            MeterMode.Absolute_mW  -> mW
            MeterMode.Relative_dB  -> dBm
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.Range">
            <summary>
            Sets/Returns the measurement range. Units depend on current mode.
            The range method guarantees to set a power at or *above* that specified in this function.
            I.e. the power level specified in the set is guaranteed to be measureable in this mode.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.AveragingTime_s">
            <summary>
            Sets/returns the measurement averaging value in seconds for the power meter channel.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.IInstType_OpticalPowerMeter.MaxMinOn">
            <summary>
            Get/Set Enable/Disable Min/Max hold function.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter">
            <summary>
            Optical power meter channel controller.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange">
            <summary>
            Autorange value constant
            Assigned the value double.NaN (not a number) in this case representing a flag for autorange 
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.OutOfRange">
            <summary>
            OutOfRange value constant.
            Assigned the value double.PositiveInfinity in this case representing a flag for OutOfRange.
            
            Two possible scenarios for this:
            1. Too much light for the current meter range (or for the meter in general).
            2. Zero light in dBm / dB mode where electrical offsets cause reading of "negative" power.
            This will cause a calculation error in the meter when it trys to take a logarithm of this.
            
            It is often impossible to distinguish the two conditions.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.#ctor(System.String,System.String,System.String,System.String,Bookham.TestEngine.PluginInterfaces.Chassis.Chassis)">
            <summary>
            Constructor.
            </summary>
            <param name="instrumentName">Instrument name</param>
            <param name="driverName">Instrument driver name</param>
            <param name="slotId">Slot ID for the instrument</param>
            <param name="subSlotId">Sub Slot ID for the instrument</param>
            <param name="chassis">Chassis through which the instrument communicates</param>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.IsAutoRange(System.Double)">
            <summary>
            Is the number "AutoRange". Needed as special double values don't 
            behave as expected with "==" operator.
            </summary>
            <param name="val">Value</param>
            <returns>true if it is AutoRange, false otherwise</returns>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.IsOverload(System.Double)">
            <summary>
            Is the number "OutOfRange". Needed as special double values don't 
            behave as expected with "==" operator.
            </summary>
            <param name="val">Value</param>
            <returns>true if it is Overload, false otherwise</returns>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.Convert_dBmtomW(System.Double)">
            <summary>
            Convert dBm to mW
            </summary>
            <param name="power_dBm">Power value in dBm to be converted to mW</param>
            <returns>Value in mW</returns>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.Convert_mWtodBm(System.Double)">
            <summary>
            Convert mW to dBm.
            Note that negative or zero values will throw an exception.
            </summary>
            <param name="power_mW">Power value in mW to be converted to dBm</param>
            <returns>Value in dBm</returns>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.ReadPower">
            <summary>
            Initiate and return a power measurement. Returned units depend 
            on the current mode of the instrument (dBm, mW, dB). 
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.ZeroDarkCurrent_Start">
            <summary>
            Starts a calibration of power meter dark-current 
            value on the power meter channel. For this calibration to succeed the meter must 
            have zero light input. This function returns immediately once the command
            has been sent to the instrument (Async command).
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.ZeroDarkCurrent_End">
            <summary>
            Ends a calibration of optical dark-current. Waits for a previously started
            dark current cal to complete.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.ReadPowerMaxMin">
            <summary>
            Return  the max and min power readings observed since the last activation 
            of Max/Min Hold Mode.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.Mode">
            <summary>
            Get / Set the mode of optical power meter. 
            If a set of a mode which is not supported by this instrument is attempted, 
            an exception should be thrown.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.Wavelength_nm">
            <summary>
            Reads/sets the expected input signal wavelength in nanometres
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.CalOffset_dB">
            <summary>
            Get / Set calibration offset in decibels.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.ReferencePower">
            <summary>
            Get/Set reference power for relative measurements. Uses the 
            current MeterMode for units.
            MeterMode.Absolute_dBm -> dBm
            MeterMode.Absolute_mW  -> mW
            MeterMode.Relative_dB  -> dBm
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.Range">
            <summary>
            Sets/Returns the measurement range. Units depend on current mode.
            The range method guarantees to set a power at or *above* that specified in this function.
            I.e. the power level specified in the set is guaranteed to be measureable in this mode.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AveragingTime_s">
            <summary>
            Sets/returns the measurement averaging value in seconds for the power meter channel.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MaxMinOn">
            <summary>
            Get/Set Enable/Disable Min/Max hold function.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode">
            <summary>
            Optical power meter mode
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_dBm">
            <summary>
            Absolute power measurement in dBm
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW">
            <summary>
            Absolute power measurement in mW
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Relative_dB">
            <summary>
            Relative power measurement in dB
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.PowerMaxMin">
            <summary>
            Max/Min Hold specification 
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.PowerMaxMin.Max">
            <summary>
            Maximum
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.PowerMaxMin.Min">
            <summary>
            Minimum
            </summary>
        </member>
    </members>
</doc>
